#include <fornax.h>

//string.h

char strcmp(char* s1, char* s2)
{
    int i = 0;
    while(s1[i] == s2[i] && s1[i] != '\0'){ ++i; }
    return s1[i] != s2[i];
}

char strncmp(char* s1, char* s2, int n)
{
    int i = 0;
    while(i < n && s1[i] == s2[i] && s1[i] != '\0'){ ++i; }
    return i != n;
}

int strspn(char* s, char c)
{
    int i = 0;
    while(s[i] != '\0' && s[i] == c){ ++i; }
    return i;
}

int strcspn(char* s, char c)
{
    int i = 0;
    while(s[i] != '\0' && s[i] != c){ ++i; }
    return i;
}

int strlen(char* s1)
{
    int i = 0;
    while(s1[i] != '\0'){ ++i; }
    return i;
}

char ppq[26] = {
    0x20 /* a */, 0x40 /* b */, 0x00 /* c */, 0x41 /* d */,
    0x00 /* e */, 0x42 /* f */, 0x43 /* g */, 0x44 /* h */,
    0x21 /* i */, 0x45 /* j */, 0x46 /* k */, 0x47 /* l */,
    0x48 /* m */, 0x49 /* n */, 0x22 /* o */, 0x4a /* p */,
    0x00 /* q */, 0x4b /* r */, 0x4c /* s */, 0x4d /* t */,
    0x23 /* u */, 0x4e /* v */, 0x00 /* w */, 0x00 /* x */,
    0x00 /* y */, 0x4f /* z */,
};

int parse_proqu(char* str, short* out)
{
    int len = strcspn(str, ' ');

    //Check if the length is valid
    if(len != 5){ return 0; }

    *out = 0;
    for(int i = 0; i < len; ++i){
	
		//Check if character is alpha
		if(str[i] < 'a' || str[i] > 'z'){ return 0; }

		//Check that the character is valid in a proquint
		char shiftincr = ppq[str[i] - 'a'];
		char shift = shiftincr >> 4; 
		if(shift == 0){ return 0; }

		//Shift and increment the parsed value
		*out <<= shift;
		*out += shiftincr & 0xf;
    }

    return len;
}

int tokparse_proqu(char** str, short* out)
{
    int spc = strspn(*str, ' ');
    int len = parse_proqu(&str[0][spc], out);
    if(len > 0){ *str += spc + len; }
    return len;
}

int parse_short(char* str, short* out)
{
    int len = strcspn(str, ' ');

    //Check if the length is valid
    if(len <= 0 || len > 4){ return 0; }

    //Check if every character is lowercase hexadecimal
    int i = 0;
    while(
		i < len &&
		(str[i] >= '0' && str[i] <= '9' || str[i] >= 'a' && str[i] <= 'f')
    ){ ++i; }
    if(i != len){ return 0; }

    //Convert hexadecimal characters
    *out = 0;
    for(int i = 0; i < len; ++i){
		*out <<= 4;
		if(str[i] >= '0' && str[i] <= '9'){ *out += str[i] - '0'; } else
		if(str[i] >= 'a' && str[i] <= 'f'){ *out += str[i] - 'a' + 10; }
    }

    return len;
}

int tokparse_short(char** str, short* out)
{
    int spc = strspn(*str, ' ');
    int len = parse_short(&str[0][spc], out);
    if(len > 0){ *str += spc + len; }
    return len;
}

int tokparse_ident(char** str, short* out)
{
    int len = 0;
    len = tokparse_short(str, out); if(len > 0){ return len; }
    len = tokparse_proqu(str, out); if(len > 0){ return len; }
    return 0;
}

/* STRING_H */

//state.h

enum {
    STT_IDLE = 0,
    STT_EXTRACT,
    STT_POWER,
    STT_FOLLOW,
    STT_MIMIC,
    STT_GUARD,
    STT_REPAIR,
    STT_RESTOCK,
    STT_SUSTAIN,
};

typedef struct State
{
    char id;
    short ent1;
} State;

/* STATE_H */

//input.h

enum { INPUT_MAX = 64, };
typedef struct Input { char buf[INPUT_MAX]; char ptr; } Input;

void input_reset(Input* input)
{
    input->ptr = 0;
}


/* INPUT_H */

//devices.h

enum {
    PORT_HOST_READ = 0xe2,
    PORT_HOST_WRITE = 0xe3,
    PORT_HOST_RNG_HI = 0xe4,
    PORT_HOST_RNG_LO = 0xe5,
    PORT_HOST_ARG0 = 0xe8,
    PORT_HOST_ARG1 = 0xe9,
    PORT_HOST_ARG2 = 0xea,
    PORT_HOST_ARG3 = 0xeb,
    PORT_HOST_ARG4 = 0xec,
    PORT_HOST_ARG5 = 0xed,
    PORT_HOST_ARG6 = 0xee,
    PORT_HOST_ARG7 = 0xef,
};

enum {
    HOST_WRITE_NAV_ABS = 0x0,
    HOST_WRITE_NAV_REL = 0x1,
    HOST_WRITE_NAV_ORB = 0x2,
    HOST_WRITE_NAV_IMP = 0x3,
    HOST_WRITE_NAV_ENT_POS = 0x8,
    HOST_WRITE_NAV_ENT_TYP = 0x9,
    HOST_WRITE_NAV_ENT_GET = 0xa,
    HOST_WRITE_NAV_SYN_ENT = 0xb,
    HOST_WRITE_NAV_SIG_SET = 0xc,
    HOST_WRITE_NAV_SIG_GET = 0xd,
    HOST_WRITE_NAV_SYN_SET = 0xe,
    HOST_WRITE_NAV_SYN_GET = 0xf,

    HOST_WRITE_DRV_THR = 0x10,

    HOST_WRITE_VAT_INJ = 0x20,
    HOST_WRITE_VAT_SAM = 0x21,
    HOST_WRITE_VAT_HAR = 0x22,

    HOST_WRITE_EXT_BAR = 0x30,

    HOST_WRITE_SYN_REP = 0x40,
    HOST_WRITE_SYN_RES = 0x41,
    HOST_WRITE_SYN_SAL = 0x42,

    HOST_WRITE_COM_POW = 0x50,

    HOST_WRITE_BAY_LAU = 0x70,

    HOST_WRITE_CRG_GET = 0x80,
    HOST_WRITE_CRG_PUT = 0x81,

    HOST_WRITE_GRO_ADD = 0x90,
    HOST_WRITE_GRO_REM = 0x91,

    HOST_WRITE_RNG  = 0xf0,
    HOST_WRITE_RNG2 = 0xf1,
};

/* DEVICES_H */

//compo.h

typedef enum EntityType
{
    ENTTYPE_NONE = 0,
    ENTTYPE_SHIP,
    ENTTYPE_STAR,
    ENTTYPE_OASIS,
    ENTTYPE_HABITABLE,
    ENTTYPE_MONOLITH,
    ENTTYPE_WORMHOLE,
    ENTTYPE_BARREN,
    ENTTYPE_HIVE,
    ENTTYPE_WASP,
    ENTTYPE_LEVIATHAN,
    ENTTYPE_MISSILE,
    ENTTYPE_WRECKAGE,
    ENTTYPE_CIVILIZATION,
    ENTTYPE_RUIN,
    ENTTYPE_COUNT,
} EntityType;

/* COMPO_H */

void memset_dev(char addr, char c, char len)
{
    char end = addr + len;
    for(int i = addr; i < end; ++i){
		deo(c, i);
    }
}
