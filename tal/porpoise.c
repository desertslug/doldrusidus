#include <fornax.h>

//string.h

char strcmp(char* s1, char* s2)
{
    int i = 0;
    while(s1[i] == s2[i] && s1[i] != '\0'){ ++i; }
    return s1[i] != s2[i];
}

char strncmp(char* s1, char* s2, int n)
{
    int i = 0;
    while(i < n && s1[i] == s2[i] && s1[i] != '\0'){ ++i; }
    return i != n;
}

int strspn(char* s, char c)
{
    int i = 0;
    while(s[i] != '\0' && s[i] == c){ ++i; }
    return i;
}

int strcspn(char* s, char c)
{
    int i = 0;
    while(s[i] != '\0' && s[i] != c){ ++i; }
    return i;
}

int strlen(char* s1)
{
    int i = 0;
    while(s1[i] != '\0'){ ++i; }
    return i;
}

char ppq[26] = {
    0x20 /* a */, 0x40 /* b */, 0x00 /* c */, 0x41 /* d */,
    0x00 /* e */, 0x42 /* f */, 0x43 /* g */, 0x44 /* h */,
    0x21 /* i */, 0x45 /* j */, 0x46 /* k */, 0x47 /* l */,
    0x48 /* m */, 0x49 /* n */, 0x22 /* o */, 0x4a /* p */,
    0x00 /* q */, 0x4b /* r */, 0x4c /* s */, 0x4d /* t */,
    0x23 /* u */, 0x4e /* v */, 0x00 /* w */, 0x00 /* x */,
    0x00 /* y */, 0x4f /* z */,
};

int parse_proqu(char* str, short* out)
{
    int len = strcspn(str, ' ');

    //Check if the length is valid
    if(len != 5){ return 0; }

    *out = 0;
    for(int i = 0; i < len; ++i){
	
		//Check if character is alpha
		if(str[i] < 'a' || str[i] > 'z'){ return 0; }

		//Check that the character is valid in a proquint
		char shiftincr = ppq[str[i] - 'a'];
		char shift = shiftincr >> 4; 
		if(shift == 0){ return 0; }

		//Shift and increment the parsed value
		*out <<= shift;
		*out += shiftincr & 0xf;
    }

    return len;
}

int tokparse_proqu(char** str, short* out)
{
    int spc = strspn(*str, ' ');
    int len = parse_proqu(&str[0][spc], out);
    if(len > 0){ *str += spc + len; }
    return len;
}

int parse_short(char* str, short* out)
{
    int len = strcspn(str, ' ');

    //Check if the length is valid
    if(len <= 0 || len > 4){ return 0; }

    //Check if every character is lowercase hexadecimal
    int i = 0;
    while(
		i < len &&
		(str[i] >= '0' && str[i] <= '9' || str[i] >= 'a' && str[i] <= 'f')
    ){ ++i; }
    if(i != len){ return 0; }

    //Convert hexadecimal characters
    *out = 0;
    for(int i = 0; i < len; ++i){
		*out <<= 4;
		if(str[i] >= '0' && str[i] <= '9'){ *out += str[i] - '0'; } else
		if(str[i] >= 'a' && str[i] <= 'f'){ *out += str[i] - 'a' + 10; }
    }

    return len;
}

int tokparse_short(char** str, short* out)
{
    int spc = strspn(*str, ' ');
    int len = parse_short(&str[0][spc], out);
    if(len > 0){ *str += spc + len; }
    return len;
}

int tokparse_ident(char** str, short* out)
{
    int len = 0;
    len = tokparse_short(str, out); if(len > 0){ return len; }
    len = tokparse_proqu(str, out); if(len > 0){ return len; }
    return 0;
}

/* STRING_H */

//state.h

enum {
    STT_IDLE = 0,
    STT_EXTRACT,
    STT_POWER,
    STT_FOLLOW,
    STT_MIMIC,
    STT_GUARD,
    STT_REPAIR,
    STT_RESTOCK,
    STT_SUSTAIN,
	STT_OBSERVE,
	STT_RELAY,
	STT_EXPLORE,
	STT_CAPTURE,
};

enum {
	STT_SYNC_IDLE = 0,
	STT_SYNC_RECV,
};

typedef struct State
{
    char id;
    short ent1;
    short ent2;
	char sync;
	short syncent;
	char relay;
} State;

/* STATE_H */

//input.h

enum { INPUT_MAX = 64, };
typedef struct Input { char buf[INPUT_MAX]; char ptr; } Input;

void input_reset(Input* input)
{
    input->ptr = 0;
}


/* INPUT_H */

void memset_dev(char addr, char c, char len)
{
    char end = addr + len;
    for(int i = addr; i < end; ++i){
		deo(c, i);
    }
}

enum { SIGNAL_BYTES = 8, };

void signal_set(State* state)
{
    //Zero out host arguments
    memset_dev(PORT_HOST_ARG0, 0, SIGNAL_BYTES);

    //Write state byte
    deo(state->id, PORT_HOST_ARG0);

    switch(state->id){
    case STT_IDLE: {} break;
    case STT_EXTRACT: {

		deo2(state->ent1, PORT_HOST_ARG2);

    } break;
    case STT_POWER: {} break;
    case STT_FOLLOW: {} break;
    case STT_MIMIC: {} break;
    case STT_CAPTURE: {

		deo2(state->ent1, PORT_HOST_ARG2);

    } break;
    }

    deo(HOST_WRITE_NAV_SIG_SET, PORT_HOST_WRITE);
}

void signal_proc(State* state)
{
    //Read state byte
    state->id = dei(PORT_HOST_ARG0);

    switch(state->id){
    case STT_IDLE: {} break;
    case STT_EXTRACT: {

		state->ent1 = dei2(PORT_HOST_ARG2);

    } break;
    case STT_POWER: {} break;
    case STT_FOLLOW: {} break;
    case STT_MIMIC: {} break;
    case STT_CAPTURE: {

		state->ent1 = dei2(PORT_HOST_ARG2);

    } break;
    }
}

void parse_input(Input* input, State* state)
{
    int len = 0;
    short ent;
    short dist;
    short x, y, z;
    short idx;

    //Find the end of the first word
    char* buf = input->buf + strcspn(input->buf, ' ');

    //Null-terminate first word
    if(buf[0] != '\0'){ buf[0] = '\0'; ++buf; }

    if(strcmp("go", input->buf) == 0){

		if(tokparse_short(&buf, &x) == 0){ goto parse_end; }
		if(tokparse_short(&buf, &y) == 0){ goto parse_end; }
		if(tokparse_short(&buf, &z) == 0){ goto parse_end; }

		deo2(x, PORT_HOST_ARG0);
		deo2(y, PORT_HOST_ARG2);
		deo2(z, PORT_HOST_ARG4);
		deo2(0x0000, PORT_HOST_ARG6);
		deo(HOST_WRITE_NAV_ABS, PORT_HOST_WRITE);

    } else
    if(strcmp("app", input->buf) == 0){

		if(tokparse_ident(&buf, &ent) == 0){ goto parse_end; }
		if(tokparse_short(&buf, &dist) == 0){ goto parse_end; }

		deo2(ent, PORT_HOST_ARG0);
		deo(HOST_WRITE_NAV_ENT_POS, PORT_HOST_WRITE);
		deo2(dist, PORT_HOST_ARG6);
		deo(HOST_WRITE_NAV_ABS, PORT_HOST_WRITE);

    } else
    if(strcmp("halt", input->buf) == 0){

		deo(0x7f, PORT_HOST_ARG0);
		deo(0x7f, PORT_HOST_ARG1);
		deo(0x7f, PORT_HOST_ARG2);
		deo(HOST_WRITE_NAV_REL, PORT_HOST_WRITE);

    } else
    if(strcmp("orb", input->buf) == 0){

		if(tokparse_ident(&buf, &ent) == 0){ goto parse_end; }
		if(tokparse_short(&buf, &dist) == 0){ goto parse_end; }

		deo2(ent, PORT_HOST_ARG0);
		deo(HOST_WRITE_NAV_ENT_POS, PORT_HOST_WRITE);
		deo2(dist, PORT_HOST_ARG6);
		deo(HOST_WRITE_NAV_ORB, PORT_HOST_WRITE);

    } else
    if(strcmp("imp", input->buf) == 0){

		if(tokparse_ident(&buf, &ent) == 0){ goto parse_end; }

		deo2(ent, PORT_HOST_ARG0);
		deo(HOST_WRITE_NAV_ENT_POS, PORT_HOST_WRITE);
		deo(HOST_WRITE_NAV_IMP, PORT_HOST_WRITE);

    } else
    if(strcmp("throttle", input->buf) == 0){

		if(tokparse_short(&buf, &dist) == 0){ goto parse_end; }

		deo(dist, PORT_HOST_ARG0);
		deo(HOST_WRITE_DRV_THR, PORT_HOST_WRITE);

    } else
    if(strcmp("idle", input->buf) == 0){

		deo(0x7f, PORT_HOST_ARG0);
		deo(0x7f, PORT_HOST_ARG1);
		deo(0x7f, PORT_HOST_ARG2);
		deo(HOST_WRITE_NAV_REL, PORT_HOST_WRITE);
		state->id = STT_IDLE;
		signal_set(state);

    } else
    if(strcmp("inject", input->buf) == 0){

		if(tokparse_ident(&buf, &ent) == 0){ goto parse_end; }

		deo2(ent, PORT_HOST_ARG0);
		deo(HOST_WRITE_VAT_INJ, PORT_HOST_WRITE);

    } else
    if(strcmp("sample", input->buf) == 0){

		if(tokparse_ident(&buf, &ent) == 0){ goto parse_end; }

		deo2(ent, PORT_HOST_ARG0);
		deo(HOST_WRITE_VAT_SAM, PORT_HOST_WRITE);

    } else
    if(strcmp("harvest", input->buf) == 0){

		if(tokparse_ident(&buf, &ent) == 0){ goto parse_end; }

		deo2(ent, PORT_HOST_ARG0);
		deo(HOST_WRITE_VAT_HAR, PORT_HOST_WRITE);

    } else 
    if(strcmp("extract", input->buf) == 0){

		if(tokparse_ident(&buf, &ent) == 0){ goto parse_end; }

		state->id = STT_EXTRACT;
		state->ent1 = ent;
		signal_set(state);

    } else 
    if(strcmp("grow", input->buf) == 0){

		if(tokparse_short(&buf, &idx) == 0){ goto parse_end; }

		deo(idx, PORT_HOST_ARG0);
		deo(HOST_WRITE_GRO_ADD, PORT_HOST_WRITE);

    } else 
    if(strcmp("absorb", input->buf) == 0){

		if(tokparse_short(&buf, &idx) == 0){ goto parse_end; }

		deo(idx, PORT_HOST_ARG0);
		deo(HOST_WRITE_GRO_REM, PORT_HOST_WRITE);

    } else 
    if(strcmp("power", input->buf) == 0){

		if(tokparse_ident(&buf, &ent) == 0){ goto parse_end; }

		state->id = STT_POWER;
		state->ent1 = ent;
		signal_set(state);

    } else 
    if(strcmp("follow", input->buf) == 0){

		if(tokparse_ident(&buf, &ent) == 0){ goto parse_end; }

		state->id = STT_FOLLOW;
		state->ent1 = ent;
		signal_set(state);

    } else 
    if(strcmp("mimic", input->buf) == 0){

		if(tokparse_ident(&buf, &ent) == 0){ goto parse_end; }

		state->id = STT_MIMIC;
		state->ent1 = ent;
		signal_set(state);

    } else 
    if(strcmp("fire", input->buf) == 0){

		if(tokparse_ident(&buf, &ent) == 0){ goto parse_end; }

		deo2(ent, PORT_HOST_ARG0);
		deo(HOST_WRITE_BAY_LAU, PORT_HOST_WRITE);

    } else 
    if(strcmp("guard", input->buf) == 0){

		if(tokparse_ident(&buf, &ent) == 0){ goto parse_end; }
		
		state->id = STT_GUARD;
		state->ent1 = ent;
		signal_set(state);

    } else 
    if(strcmp("get", input->buf) == 0){

		if(tokparse_ident(&buf, &ent) == 0){ goto parse_end; }
		
		deo2(ent, PORT_HOST_ARG0);
		deo(HOST_WRITE_CRG_GET, PORT_HOST_WRITE);

    } else 
    if(strcmp("put", input->buf) == 0){

		if(tokparse_ident(&buf, &ent) == 0){ goto parse_end; }
		
		deo2(ent, PORT_HOST_ARG0);
		deo(HOST_WRITE_CRG_PUT, PORT_HOST_WRITE);

    } else 
    if(strcmp("synrep", input->buf) == 0){

		if(tokparse_ident(&buf, &ent) == 0){ goto parse_end; }
		
		deo2(ent, PORT_HOST_ARG0);
		deo(HOST_WRITE_SYN_REP, PORT_HOST_WRITE);

    } else 
    if(strcmp("synres", input->buf) == 0){

		if(tokparse_ident(&buf, &ent) == 0){ goto parse_end; }
		
		deo2(ent, PORT_HOST_ARG0);
		deo(HOST_WRITE_SYN_RES, PORT_HOST_WRITE);

    } else 
    if(strcmp("synsal", input->buf) == 0){

		if(tokparse_ident(&buf, &ent) == 0){ goto parse_end; }
		
		deo2(ent, PORT_HOST_ARG0);
		deo(HOST_WRITE_SYN_SAL, PORT_HOST_WRITE);

    } else 
    if(strcmp("repair", input->buf) == 0){

		if(tokparse_ident(&buf, &ent) == 0){ goto parse_end; }
		
		state->id = STT_REPAIR;
		state->ent1 = ent;
		//signal_set(state);

    } else 
    if(strcmp("restock", input->buf) == 0){

		if(tokparse_ident(&buf, &ent) == 0){ goto parse_end; }
		
		state->id = STT_RESTOCK;
		state->ent1 = ent;
		//signal_set(state);

    } else 
    if(strcmp("sustain", input->buf) == 0){

		if(tokparse_ident(&buf, &ent) == 0){ goto parse_end; }
		
		state->id = STT_SUSTAIN;
		state->ent1 = ent;
		//signal_set(state);

    } else 
    if(strcmp("observe", input->buf) == 0){

		state->id = STT_OBSERVE;

    } else 
    if(strcmp("relay", input->buf) == 0){

		if(tokparse_ident(&buf, &ent) == 0){ goto parse_end; }

		state->id = STT_RELAY;
		state->ent1 = ent;

    } else 
    if(strcmp("sync-recv", input->buf) == 0){

		if(tokparse_ident(&buf, &ent) == 0){ goto parse_end; }

		state->sync = STT_SYNC_RECV;
		state->syncent = ent;
    } else 
    if(strcmp("sync-idle", input->buf) == 0){

		state->sync = STT_SYNC_IDLE;

    } else
    if(strcmp("go-explore", input->buf) == 0){

		if(tokparse_short(&buf, &x) == 0){ goto parse_end; }
		if(tokparse_short(&buf, &y) == 0){ goto parse_end; }
		if(tokparse_short(&buf, &z) == 0){ goto parse_end; }

		deo2(x, PORT_HOST_ARG0);
		deo2(y, PORT_HOST_ARG2);
		deo2(z, PORT_HOST_ARG4);
		deo2(0x0000, PORT_HOST_ARG6);
		deo(HOST_WRITE_NAV_ABS, PORT_HOST_WRITE);

		state->id = STT_EXPLORE;

    } else
    if(strcmp("capture", input->buf) == 0){

		if(tokparse_ident(&buf, &ent) == 0){ goto parse_end; }
		if(tokparse_short(&buf, &dist) == 0){ goto parse_end; }
		
		deo2(ent, PORT_HOST_ARG0);
		deo(HOST_WRITE_NAV_ENT_POS, PORT_HOST_WRITE);
		deo2(dist, PORT_HOST_ARG6);
		deo(HOST_WRITE_NAV_ABS, PORT_HOST_WRITE);

		state->id = STT_CAPTURE;
		state->ent1 = ent;
		signal_set(state);

    }

parse_end:
    return;
}

Input input;
State state;

void on_console(void)
{
    //Newline
    if(getchar() == '\n'){

		//Null-terminate input
		input.buf[input.ptr] = '\0';

		//Attempt to parse input
		parse_input(&input, &state);
		
		//Reset input
		input_reset(&input);
    }
    //Any other character
    else {
	
		//If it fits, push char to input buffer (leave space for nullterm)
		if(input.ptr < INPUT_MAX + 1){ input.buf[input.ptr++] = getchar(); }
    }
}

void approach_entity(short ent, short dist)
{
    deo2(ent, PORT_HOST_ARG0);
    deo(HOST_WRITE_NAV_ENT_POS, PORT_HOST_WRITE);

    if(dei(PORT_HOST_READ) != 0){ return; }

    deo2(dist, PORT_HOST_ARG6);
    deo(HOST_WRITE_NAV_ABS, PORT_HOST_WRITE);
}

void state_handle(State* state, char recurse)
{
    //Handle each state
    switch(state->id){
    case STT_IDLE: {} break;
    case STT_EXTRACT: {
	
		//Extract the target entity
		deo2(state->ent1, PORT_HOST_ARG0);
		deo(HOST_WRITE_EXT_BAR, PORT_HOST_WRITE);

    } break;
    case STT_POWER: {

		approach_entity(state->ent1, 0x0020);

		//Attempt to power the target entity
		deo2(state->ent1, PORT_HOST_ARG0);
		deo(HOST_WRITE_COM_POW, PORT_HOST_WRITE);

		//If the return status indicates success
		if(dei(PORT_HOST_READ) == 0){

			//Echo received character
			putchar(dei(PORT_HOST_ARG0));
		}

    } break;
    case STT_FOLLOW: {

		approach_entity(state->ent1, 0x0020);

    } break;
    case STT_MIMIC: {
	
		//If recursion was disabled, break
		if(!recurse){ break; }

		approach_entity(state->ent1, 0x0020);

		//Attempt to read signal from target
		deo2(state->ent1, PORT_HOST_ARG0);
		deo(HOST_WRITE_NAV_SIG_GET, PORT_HOST_WRITE);
		if(dei(PORT_HOST_READ) != 0){ break; }

		//Mimic signal
		deo(HOST_WRITE_NAV_SIG_SET, PORT_HOST_WRITE);

		//Process signal to state
		State state2;
		signal_proc(&state2);
		
		//Handle state
		state_handle(&state2, 0);

    } break;
    case STT_GUARD: {

		approach_entity(state->ent1, 0x0020);

		//Attempt to get a nearby entity
		deo2(0x0200, PORT_HOST_ARG0);
		deo(HOST_WRITE_NAV_ENT_GET, PORT_HOST_WRITE);
		if(dei(PORT_HOST_READ) != 0){ break; }
		short ent2 = dei2(PORT_HOST_ARG0);

		//Attempt to get the type of the entity
		deo(HOST_WRITE_NAV_ENT_TYP, PORT_HOST_WRITE);
		if(dei(PORT_HOST_READ) != 0){ break; }
		char type = dei(PORT_HOST_ARG0);

		//Perform action only on leviathans or wasps
		if(type != ENTTYPE_LEVIATHAN && type != ENTTYPE_WASP){ break; }

		deo2(ent2, PORT_HOST_ARG0);
		deo(HOST_WRITE_BAY_LAU, PORT_HOST_WRITE);

    } break;
    case STT_REPAIR: {

		approach_entity(state->ent1, 0x0020);

		deo2(state->ent1, PORT_HOST_ARG0);
		deo(HOST_WRITE_SYN_REP, PORT_HOST_WRITE);

    } break;
    case STT_RESTOCK: {

		approach_entity(state->ent1, 0x0020);

		deo2(state->ent1, PORT_HOST_ARG0);
		deo(HOST_WRITE_SYN_RES, PORT_HOST_WRITE);

    } break;
    case STT_SUSTAIN: {

		approach_entity(state->ent1, 0x0020);

		//Attempt to get a nearby entity
		deo2(0x0200, PORT_HOST_ARG0);
		deo(HOST_WRITE_NAV_ENT_GET, PORT_HOST_WRITE);
		if(dei(PORT_HOST_READ) != 0){ break; }
		short ent2 = dei2(PORT_HOST_ARG0);

		//Attempt to get the type of the entity
		deo(HOST_WRITE_NAV_ENT_TYP, PORT_HOST_WRITE);
		if(dei(PORT_HOST_READ) != 0){ break; }
		char type = dei(PORT_HOST_ARG0);

		switch(type){
		case ENTTYPE_SHIP: {

			//Get a random byte
			deo(HOST_WRITE_RNG, PORT_HOST_WRITE);
			char rand = dei(PORT_HOST_RNG_LO);

			if(rand & 0x80){

				deo2(ent2, PORT_HOST_ARG0);
				deo(HOST_WRITE_SYN_REP, PORT_HOST_WRITE);
			} else {

				deo2(ent2, PORT_HOST_ARG0);
				deo(HOST_WRITE_SYN_RES, PORT_HOST_WRITE);
			}

		} break;
		case ENTTYPE_BARREN: {

			deo2(ent2, PORT_HOST_ARG0);
			deo(HOST_WRITE_EXT_BAR, PORT_HOST_WRITE);

		} break;
		}

    } break;
    case STT_OBSERVE: {

		//Attempt to get entities to synchronize
		deo2(0x4000, PORT_HOST_ARG0);
		deo(HOST_WRITE_NAV_SYN_ENT, PORT_HOST_WRITE);
		if(dei(PORT_HOST_READ) != 0){ break; }

		//Broadcast entities that could be synchronized
		deo(HOST_WRITE_NAV_SIG_SET, PORT_HOST_WRITE);

    } break;
    case STT_RELAY: {

		//Attempt to read signal from target
		deo2(state->ent1, PORT_HOST_ARG0);
		deo(HOST_WRITE_NAV_SIG_GET, PORT_HOST_WRITE);
		if(dei(PORT_HOST_READ) != 0){ break; }

		//Store entities to be synchronized
		deo(HOST_WRITE_NAV_SYN_SET, PORT_HOST_WRITE);

		if(state->relay){

			//Attempt to get entities to synchronize
			deo2(0x4000, PORT_HOST_ARG0);
			deo(HOST_WRITE_NAV_SYN_ENT, PORT_HOST_WRITE);
			if(dei(PORT_HOST_READ) != 0){ break; }
		}

		//Broadcast entities that could be synchronized
		deo(HOST_WRITE_NAV_SIG_SET, PORT_HOST_WRITE);

		state->relay = 1 - state->relay;

    } break;
    case STT_EXPLORE: {

		//Attempt to get a nearby entity
		deo2(0x0c00, PORT_HOST_ARG0);
		deo(HOST_WRITE_NAV_ENT_GET, PORT_HOST_WRITE);
		if(dei(PORT_HOST_READ) != 0){ break; }
		short ent2 = dei2(PORT_HOST_ARG0);

		//Attempt to get the type of the entity
		deo(HOST_WRITE_NAV_ENT_TYP, PORT_HOST_WRITE);
		if(dei(PORT_HOST_READ) != 0){ break; }
		char type = dei(PORT_HOST_ARG0);

		//Perform action only on leviathans or wasps
		if(type != ENTTYPE_LEVIATHAN && type != ENTTYPE_WASP){ break; }

		deo(0x7f, PORT_HOST_ARG0);
		deo(0x7f, PORT_HOST_ARG1);
		deo(0x7f, PORT_HOST_ARG2);
		deo(HOST_WRITE_NAV_REL, PORT_HOST_WRITE);
		state->id = STT_IDLE;
		signal_set(state);

	} break;
    case STT_CAPTURE: {

		//Extract the target entity
		deo2(state->ent1, PORT_HOST_ARG0);
		deo(HOST_WRITE_EXT_BAR, PORT_HOST_WRITE);

		short ent2;
		char type;

		//Attempt to get a nearby entity
		deo2(0x0040, PORT_HOST_ARG0);
		deo(HOST_WRITE_NAV_ENT_GET, PORT_HOST_WRITE);
		if(dei(PORT_HOST_READ) != 0){ break; }
		ent2 = dei2(PORT_HOST_ARG0);

		//Attempt to get the type of the entity
		deo(HOST_WRITE_NAV_ENT_TYP, PORT_HOST_WRITE);
		if(dei(PORT_HOST_READ) != 0){ break; }
		type = dei(PORT_HOST_ARG0);

		switch(type){
		case ENTTYPE_SHIP: {

			//Get a random byte
			deo(HOST_WRITE_RNG, PORT_HOST_WRITE);
			char rand = dei(PORT_HOST_RNG_LO);

			if(rand & 0x80){

				deo2(ent2, PORT_HOST_ARG0);
				deo(HOST_WRITE_SYN_REP, PORT_HOST_WRITE);
			} else {

				deo2(ent2, PORT_HOST_ARG0);
				deo(HOST_WRITE_SYN_RES, PORT_HOST_WRITE);
			}

		} break;
		}

		//Attempt to get a nearby entity
		deo2(0x0200, PORT_HOST_ARG0);
		deo(HOST_WRITE_NAV_ENT_GET, PORT_HOST_WRITE);
		if(dei(PORT_HOST_READ) != 0){ break; }
		ent2 = dei2(PORT_HOST_ARG0);

		//Attempt to get the type of the entity
		deo(HOST_WRITE_NAV_ENT_TYP, PORT_HOST_WRITE);
		if(dei(PORT_HOST_READ) != 0){ break; }
		type = dei(PORT_HOST_ARG0);

		//Perform action only on leviathans or wasps
		if(type != ENTTYPE_LEVIATHAN && type != ENTTYPE_WASP){ break; }

		deo2(ent2, PORT_HOST_ARG0);
		deo(HOST_WRITE_BAY_LAU, PORT_HOST_WRITE);

    } break;
    }

	//Handle synchronization state
	switch(state->sync){
	case STT_SYNC_IDLE: {} break;
	case STT_SYNC_RECV: {

		//Attempt to read signal from target
		deo2(state->syncent, PORT_HOST_ARG0);
		deo(HOST_WRITE_NAV_SIG_GET, PORT_HOST_WRITE);
		if(dei(PORT_HOST_READ) != 0){ break; }

		//Store entities to be synchronized
		deo(HOST_WRITE_NAV_SYN_SET, PORT_HOST_WRITE);

	} break;
	}
}

void on_screen(void)
{
    //Handle state
    state_handle(&state, 1);
}

void main()
{
    //Initialize
    input_reset(&input);
    state.id = STT_IDLE;
	state.sync = STT_SYNC_IDLE;
	state.relay = 0;
}
