#!/bin/sh

# Usage
# $ ./chibicc <input>.c <output>.rom

# Default path to chibicc
# Override with an environment variable
# $ CHIBICC=<your-path> ./chibi.sh
if [ -z "$CHIBICC" ] ; then
    CHIBICC="chibicc"
fi
if ! [ -x "$CHIBICC" ] ; then
    echo "$CHIBICC is not an executable file, exiting..."
    exit 1
fi
echo $CHIBICC

# Default path to uxn assembler
# Override with an environment variable
# $ UXNASM=<your-path> ./chibi.sh
if [ -z "$UXNASM" ] ; then
    UXNASM="uxnasm"
fi
if ! [ -x "$UXNASM" ] ; then
    echo "$UXNASM is not an executable file, exiting..."
    exit 1
fi
echo $UXNASM

# Invoke C compiler for preprocessing
# -I. to add the current directory as an include search path
# -P to eliminate line markers (lines starting with '#')
# -E to stop after the preprocessing stage
# -x to specify the language explicitly
if ! cc -Iinclude -P -E -x c "$1" -o tmp.c ; then
    echo "preprocessing failed, exiting..."
    exit 1
fi

# Invoke chibicc for transpilation
if ! $CHIBICC -O1 tmp.c > tmp.tal ; then
    echo "compilation failed, exiting..."
    exit 1
fi

# Invoke uxnasm for assembling
if ! $UXNASM tmp.tal "$2" ; then
    echo "assembling failed, exiting..."
    exit 1
fi

# Remove build artifacts after a successful build
rm tmp.c tmp.tal
