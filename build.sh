#!/bin/sh
BUILD_DIR=""

# Parse arguments
for arg in "$@" ; do
    case $arg in
        (--dir=* | -d=*) BUILD_DIR=${arg##*=} ;;
	    (--help | --he | -h)
	    cat <<-EOF
Usage: ./build.sh --dir=<>
options:

    --dir=<>, -d=<>
        Builds the project in the argument directory.

Run this script to build the project, after having
configured a build directory using ./configure.sh.
EOF
	    exit 0
	;;
    esac
done

# Assert that the build directoryexists
if ! [ -d "${BUILD_DIR}" ] ; then
    echo "error: build directory does not exist: \"${BUILD_DIR}\""
    exit 1
fi

# Change into the build directory
echo "[changing into build directory]"
if ! cd "${BUILD_DIR}" ; then
    echo "error: failed to change into the build directory"
    exit 1
fi

# Run the build usng the ninja backend"
echo "[running build]"
if ninja ; then

    echo "[running packaging]"

    # Strip trailing slashes from the build directory
    BUILD_PATH="$(echo ${BUILD_DIR} | sed 's/\/*$//g')"

    # Get the name of the built target executables, package manually
    SRV_TARGET="$(ls | grep "srv-..*-.\..\..\(.exe\)\?$")"
    CLI_TARGET="$(ls | grep "cli-..*-.\..\..\(.exe\)\?$")"
    echo "[server target executable: ${SRV_TARGET}]"
    echo "[client target executable: ${CLI_TARGET}]"
    (cd .. && make PROJ_NAME="doldrusidus" BUILD_DIR=${BUILD_PATH} EXEC=${SRV_TARGET})
    (cd .. && make PROJ_NAME="doldrusidus" BUILD_DIR=${BUILD_PATH} EXEC=${CLI_TARGET})
else
    echo "error: failed to run build using the ninja backend"
    exit 1
fi

