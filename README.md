# DOLDRUSIDUS

doldrum - noun:
1. doldrums (plural) - a spell of listlessness or despondency
2. archaic - a sluggish or slow-witted person
3. doldrums (plural) - a region over the ocean near the equator abounding in calms, squalls, and light baffling winds

sidus (latin) - neutral noun:
1. a group of stars, constellation, heavenly body 
2. tempest

Doldrusidus is an open-ended, obscure simulation realized as a multiplayer game taking place in a small universe, where you can participate by assembling and emulating machine code for the uxn virtual machine architecture.

To quickly learn the basics of play, see `doc/quickstart.md`. To learn about the default game world, see `mod/FORNAX.md`. If you would like to know more about how the engine works and the rationale behind it, see `doc/manual.md`.

## Modifiability

The engine core is configured through `res/config.json`, and the parameters of server and client modifications are intended to be stored and carried with you in `mod/srv/srvconf.h` and `mod/cli/cliconf.h`, respectively.

For an example on how a modded game world can be implemented, see `mod/FORNAX.md`.

## Requirements

Currently, the game is BYOCC (bring-your-own-c-compiler). This means that your system must have a C compiler installed that the engine will use to compile mods (hint: to check whether you have one, try running `whereis cc` or `cc --version`). Once you've installed one, set it in the `cc_path` option in `res/config.json`. By default, this option is the following.

```json
{ "cc_path" : [ "cc", "path to c compiler" ] }
```

The reason behind this is that currently, packaging a C compiler with the game is an unsolved problem. TinyCC was used initially, but the problem is that it doesn't seem to cross-compile to ARM architectures from x86, and it would be nice not to have to make builds inside a virtual machine.

The game is also BYOU (bring-your-own-uxnasm). This means that you need to have an uxn assembler on hand, because the server only accepts assembled ROMs for the time being.

The currently required minimum version of OpenGL is 3.3 (core profile). However, even when your hardware supports it, the proper drivers are installed, and even when you are sure that direct rendering should work, shader compilation may fail, or calling a specific graphics function may cause a crash. This could happen because the OpenGL version may be purposefully reported as 3.0 for maximum compatibility. The reported version is logged during initialization (`glGetString(GL_VERSION)` in the log file).

In this case, overriding the version of your OpenGL implementation through a command-line option may work. For example, if you use Mesa, try setting the `MESA_GL_VERSION_OVERRIDE` environment variable before the command: `$ MESA_GL_VERSION_OVERRIDE=3.3 ./cli-doldrusidus-*.*.*`, and opening a graphical window for the client either through the TUI or by passing the `-w` option on startup.

## Building

Library dependencies:
- `cglm` for mathematics,
- `enet` for networking,
- `glfw` for window & input handling,
- `glew` for loading OpenGL extensions,
- `crypto` & `libssl` for cryptographical fundamentals.

Build system:
- `cmake` for building some dependencies,
- `meson` as the main build system,
- `ninja` as the build backend.

During the build process, as its first step, you need to ensure that all library dependencies can be found and linked against. Currently, builds require those libraries to be embedded into the source tree, directly under the `subprojects` directory, where meson will look for them. The following projects must be present in the `subprojects` directory: `cglm, enet, glew, glfw`.

Before building a certain build type (debug/release), the project must be configured first using `configure.sh`, which can configure a project to build both natively and cross.

### Native configuration

For native builds, the host machine's OS is derived in `meson.build`, so only the build type must be specified, which has to be a value of meson's `buildtype` core option.

```
$ ./configure.sh --buildtype=<build-shorthand>
```

### Cross configuration

For cross builds, the host target triplet (the designation for the system that the project is being compiled for) must be specified, the host OS will be derived from that.

```
$ ./configure.sh --host=<target-triplet> --buildtype=<build-shorthand>
```

The shorthand for the build type, again, must be a value of meson's `buildtype` core option. For a list of the currently supported target triplets, see the `.ini` files at the top level of the source tree. Adding support for as-of-yet unsupported systems, while should not be complicated (thanks to the flexible build system), is done only on a best-effort basis, due to having access to only a few platforms and processor architectures.

### Compilation

After the desired buildtype is successfully configured, build the project using `build.sh`.

```
$ ./build.sh --dir=<build-directory>
```

The name of the directory containing the game package is just the name of the project followed by the build type, with the host triplet in between them for cross builds.

For more information on how to use these scripts, see `$ ./<script.sh> --help`.

### Execution

Once it is built, the game will be automatically packaged with the top-level `Makefile` to the appropriately named directory, described in the `Compilation` section.

## Building inside a docker container

The project has dockerfiles in `/docker` to create reproducible (and hopefully somewhat portable) binaries on older releases of Debian.

Currently, the following containers are supported:
- `Dockerfile.amd64.deb10`: builds `x86_64-linux-gnu`, `aarch64-linux-gnu`, on Debian 10 Buster.
- `Dockerfile.i386.deb10`: builds `i686-linux-gnu`, `arm-linux-gnueabihf`, on Debian 10 Buster.

First, the container executing the build itself must be built.

```
$ sudo docker build . -t <your-image-name> -f <chosen-docker-file>
```

Once the container `your-image-name` is ready to execute the build, a specific cross target may be built using a convenience script. This creates (or, if it already exists, cleans) a directory at the top level on your machine, into which the game will be packaged by the container, as it's directory of the same name is bound to that directory on the host.

```
$ sudo ./dockbuild.sh --image=<your-image-name> <meson-cross-file>
```

Alternatively, all meson cross files (`*.ini`) can be matched against an expression (or nothing, which means all of them will be built) using another convenience script. For example, specify `i686` if you wish to build 32-bit builds only, or `x86_64` for 64-bit builds.

```
sudo ./multidockbuild.sh --image=<your-image-name> [<expression>]
```

Note that whether you need root privileges to run docker depends on you system's configuration. If you do, the packaged game files will be owned by `root`, which is undesirable if you wish to handle them without those privileges. This can be resolved by specifying `USER=<your-username>` after `sudo` and before the script's name, so that information may be used to change the owner of those files.

## Platform support

As of now, the targeted operating systems for binary releases only include Linux running on 64-bit and 32-bit, on x86 and arm cpu architectures. Native or cross builds on/for other systems should not be difficult to set up either, but the above systems exhaust current testing capabilities.

## License

MIT, see `LICENSE`.
