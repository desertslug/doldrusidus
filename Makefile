# Specify PROJ_NAME, BUILD_DIR, EXEC, CC on the command line

# Do not use the built-in inference rules
.SUFFIXES:

.PHONY: all
all: pack

PROJ_NAME = doldrusidus

CC = gcc

PACKAGE_DIR = $(PROJ_NAME)-$(BUILD_DIR)
$(PACKAGE_DIR):
	echo "creating package directory: \"$(PACKAGE_DIR)\""
	mkdir -p $(PACKAGE_DIR)

RES_NAME = res
RES_DIR = $(PACKAGE_DIR)/$(RES_NAME)
$(RES_DIR):
	mkdir -p $(RES_DIR)

AUTH = auth.csv
AUTH_SRC  = $(AUTH:%.csv=$(RES_NAME)/%.csv)
AUTH_DEST = $(AUTH:%.csv=$(RES_DIR)/%.csv)
$(AUTH_DEST): $(AUTH_SRC)
	cp $(AUTH_SRC) $(RES_DIR)

CONFIG = config.json
CONFIG_SRC  = $(CONFIG:%.json=$(RES_NAME)/%.json)
CONFIG_DEST = $(CONFIG:%.json=$(RES_DIR)/%.json)
$(CONFIG_DEST): $(CONFIG_SRC)
	cp $(CONFIG_SRC) $(RES_DIR)

.PHONY: RES_COPY
RES_COPY:
	cp -r $(RES_NAME)/* $(RES_DIR)

DOC_NAME = doc
DOC_DIR = $(PACKAGE_DIR)/$(DOC_NAME)
$(DOC_DIR):
	mkdir -p $(DOC_DIR)

DOC_SRC = manual.md quickstart.md
DOC_DEST = $(DOC_SRC:%=$(DOC_DIR)/%)
$(DOC_DEST): $(DOC_SRC:%=$(DOC_NAME)/%)
	cp $(DOC_SRC:%=$(DOC_NAME)/%) $(DOC_DIR)

README_FILE = README.md
README: $(README_FILE)
	cp $(README_FILE) $(PACKAGE_DIR)

LUA_NAME = lua
LUA_DIR = $(PACKAGE_DIR)/$(LUA_NAME)
$(LUA_DIR):
	mkdir -p $(LUA_DIR)

.PHONY: LUA_COPY
LUA_COPY:
	cp -r $(LUA_NAME)/* $(LUA_DIR)

LUA_CDEF: c/common/cdef.h
	gcc -E c/common/cdef.h -DLUA_EXPORTS | grep -v "^#" > $(LUA_DIR)/common/cdef.lua

TAL_NAME = tal
TAL_DIR = $(PACKAGE_DIR)/$(TAL_NAME)
$(TAL_DIR):
	mkdir -p $(TAL_DIR)

.PHONY: TAL_COPY
TAL_COPY:
	cp -r $(TAL_NAME)/* $(TAL_DIR)

CMD_NAME = cmd
CMD_DIR = $(PACKAGE_DIR)/$(CMD_NAME)
$(CMD_DIR):
	mkdir -p $(CMD_DIR)

.PHONY: CMD_COPY
CMD_COPY:
	cp -r $(CMD_NAME)/* $(CMD_DIR)

TCC_NAME = tinycc
TCC_DIR = $(PACKAGE_DIR)/$(TCC_NAME)
$(TCC_DIR):
	mkdir -p $(TCC_DIR)

#cp -r ${BUILD_DIR}/subprojects/$(TCC_NAME)/* $(TCC_DIR)
.PHONY: TCC_COPY
TCC_COPY:
	cp -r ${BUILD_DIR}/subprojects/$(TCC_NAME)/* $(TCC_DIR)

MOD_NAME = mod
MOD_DIR = $(PACKAGE_DIR)/$(MOD_NAME)
$(MOD_DIR):
	mkdir -p $(MOD_DIR)

.PHONY: MOD_COPY
MOD_COPY:
	cp -r $(MOD_NAME)/* $(MOD_DIR)

INC_NAME = include
INC_DIR = $(PACKAGE_DIR)/$(INC_NAME)
$(INC_DIR):
	mkdir -p $(INC_DIR)

.PHONY: INC_COPY
INC_COPY:
	cp c/common/auth.h $(INC_DIR)
	cp c/common/config.h $(INC_DIR)
	cp c/common/constants.h $(INC_DIR)
	cp c/common/csv.h $(INC_DIR)
	cp c/common/defs.h $(INC_DIR)
	cp c/common/fasthash.h $(INC_DIR)
	cp c/cli/glfw.h $(INC_DIR)
	cp c/common/helper.h $(INC_DIR)
	cp c/common/hmap.h $(INC_DIR)
	cp c/common/jsmn.h $(INC_DIR)
	cp c/common/json.h $(INC_DIR)
	cp c/common/lstr.h $(INC_DIR)
	cp c/common/macros.h $(INC_DIR)
	cp c/common/msg.h $(INC_DIR)
	cp c/common/nanosleep.h $(INC_DIR)
	cp c/common/packet.h $(INC_DIR)
	cp c/common/plancklog.h $(INC_DIR)
	cp c/common/proquint.h $(INC_DIR)
	cp c/srv/session.h $(INC_DIR)
	cp c/common/srp.h $(INC_DIR)
	cp c/common/uvec.h $(INC_DIR)
	cp -r subprojects/enet/include/enet $(INC_DIR)
	cp -r subprojects/cglm/include/cglm $(INC_DIR)
	cp -r subprojects/glew/include/GL $(INC_DIR)
	cp -r subprojects/glfw/include/GLFW $(INC_DIR)

#$(LUA_DIR) LUA_COPY LUA_CDEF
#$(TCC_DIR) TCC_COPY
pack : $(PACKAGE_DIR) $(BUILD_DIR)/$(EXEC) README $(RES_DIR) RES_COPY $(AUTH_DEST) $(CONFIG_DEST) $(MOD_DIR) MOD_COPY $(INC_DIR) INC_COPY $(TAL_DIR) TAL_COPY $(CMD_DIR) CMD_COPY $(DOC_DIR) $(DOC_DEST)
	
	cp $(BUILD_DIR)/$(EXEC) $(PACKAGE_DIR)

