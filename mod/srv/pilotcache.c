#include "pilotcache.h"

void pilotcache_alloc(rtcp_hmap_t* assoc)
{
    rtcp_hmap_malloc(assoc, 1,
        sizeof(char) * UNAME_MAX, sizeof(rtcp_uvec_t));
    rtcp_hmap_init(assoc);
}

void pilotcache_dealloc(rtcp_hmap_t* assoc)
{
    rtcp_hmap_iter(assoc, (void (*)(void *))rtcp_uvec_free);
    rtcp_hmap_free(assoc);
}

int pilotcache_uname_ents(rtcp_hmap_t* assoc, const char* username)
{
    assert_or_exit(strlen(username) < UNAME_MAX,
        "pilotcache_uname_ents: username too long\n");
    char uname[UNAME_MAX] = { '\0', }; strcpy(uname, username);

    rtcp_uvec_t* uvec = rtcp_hmap_get(assoc, uname);

    //If there is no entry for the uname, return zero entities
    if(!uvec){ return 0; }

    return uvec->size;
}

rtcp_uvec_t* pilotcache_uname_get_ents(rtcp_hmap_t* assoc, const char* username)
{
    assert_or_exit(strlen(username) < UNAME_MAX,
        "pilotcache_uname_get_ents: username too long\n");
    char uname[UNAME_MAX] = { '\0', }; strcpy(uname, username);

    rtcp_uvec_t* uvec = rtcp_hmap_get(assoc, uname);

    //Callee must ensure there is an entry for the username
    assert_or_exit(uvec, "pilotcache_uname_get_ents: no user entry\n");

    return uvec;
}

int pilotcache_uname_find_entity(rtcp_uvec_t* uvec, Entity ent)
{
    int i = 0;
    while(i < uvec->size && *(Entity*)rtcp_uvec_get(uvec, i) != ent){ ++i; }
    return i < uvec->size ? i : -1;
}

void pilotcache_addentity(rtcp_hmap_t* assoc, const char* username, Entity ent)
{
    assert_or_exit(strlen(username) < UNAME_MAX,
        "pilotcache_addentity: username too long\n");
    char uname[UNAME_MAX] = { '\0', }; strcpy(uname, username);

    //If there is no entry for the uname yet
    if(!rtcp_hmap_get(assoc, uname)){

        //Add an entry to hold entities piloted by the user
        rtcp_uvec_t uvec;
        rtcp_uvec_malloc(&uvec, 1, sizeof(Entity));
        rtcp_uvec_init(&uvec);
        rtcp_hmap_add(assoc, uname, &uvec);
    }
    rtcp_uvec_t* uvec = rtcp_hmap_get(assoc, uname);

    //Callee must ensure the entity is not cached already
    assert_or_exit(pilotcache_uname_find_entity(uvec, ent) < 0,
        "pilotcache_addentity: entity already cached\n");

    //Add entity to the entry's cache
    rtcp_uvec_add(uvec, &ent);
}

void pilotcache_delentity(rtcp_hmap_t* assoc, const char* username, Entity ent)
{
    assert_or_exit(strlen(username) < UNAME_MAX,
        "pilotcache_delentity: username too long\n");
    char uname[UNAME_MAX] = { '\0', }; strcpy(uname, username);

    rtcp_uvec_t* uvec = rtcp_hmap_get(assoc, uname);

    //Callee must ensure there is an entry for the username
    assert_or_exit(uvec, "pilotcache_delentity: no user entry\n");

    int idx = pilotcache_uname_find_entity(uvec, ent);

    //Callee must ensure the entity is cached
    assert_or_exit(idx >= 0, "pilotcache_delentity: entity not cached\n");

    //Pop the entity at the found index
    rtcp_uvec_pop(uvec, idx);

    //Remove cache entry if it was emptied
    if(uvec->size == 0){
        rtcp_uvec_free(uvec);
        rtcp_hmap_del(assoc, uname);
    }
}