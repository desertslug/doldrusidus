#ifndef GENERATE_H
#define GENERATE_H

#include <assert.h>

#include "cglm/util.h"

#include "defs.h"
#include "memory.h"
#include "modsrv.h"
//#include "ephemeral.h"

Entity ship_generate(ModServer* modsrv, uint32_t spawn[3]);

void world_generate(ModServer* modsrv, uint32_t seed);

void system_generate(ModServer* modsrv, Hash1* hash, uint32_t spawn[3]);

Entity star_generate(ModServer* modsrv, Hash1* hash, uint32_t spawn[3]);

Entity oasis_generate(ModServer* modsrv, Hash1* hash, uint32_t spawn[3]);

Entity habitable_generate(ModServer* modsrv, Hash1* hash, uint32_t spawn[3]);

Entity monolith_generate(ModServer* modsrv, Hash1* hash, uint32_t spawn[3], MonolithSecret secret);

Entity wormhole_generate(ModServer* modsrv, Hash1* hash, uint32_t spawn[3], uint32_t target[3]);

Entity barren_generate(ModServer* modsrv, Hash1* hash, uint32_t spawn[3]);

Entity hive_generate(ModServer* modsrv, Hash1* hash, uint32_t spawn[3]);

Entity leviathan_generate(ModServer* modsrv, Hash1* hash, uint32_t spawn[3]);

Entity civilization_generate(ModServer* modsrv, Hash1* hash, uint32_t spawn[3]);

Entity ruin_generate(ModServer* modsrv, Hash1* hash, uint32_t spawn[3]);

#endif /* GENERATE_H */