#include "srvmsg.h"

bool user_entity_access(ECS* ecs, const char* uname, Access access, Entity ent)
{
    //Check that the entity is alive
    if(!ecs_entity_alive(ecs, ent)){ return false; }

    //Root users bypass ownership check
    if(access <= AccessRoot){ return true; }

    //Check that the entity has a pilot component
    const Pilot* pilot = ecs_get_id(ecs, ent, PilotID);
    if(!pilot){ return false; }

    //Check that the user pilots the entity
    return strcmp(uname, pilot->uname) == 0;
}

void pkt_process(Session* session, void* ipkt, void* opkt, ModServer* modsrv)
{
    World* world = &modsrv->world;
    rtcp_hmap_t* pilots = &modsrv->pilots;

    uint8_t* chunk = pkt_data(ipkt);
    if(!pkt_chunk_check(ipkt, chunk)){ return; }

    //Attempt to get message index
    uint16_t cmdidx;
    if(!msg_pkt_chunk_u16_single(ipkt, chunk, &cmdidx)){ return; }
    chunk += pkt_chunk_bytes(ipkt, chunk);

    switch(cmdidx){
    case MSG_CHATGLOBAL: {

        //Process only for authenticated session
        if(!session_authenticated(session)){ break; }

        //Attempt to get text
        char* text;
        if(!msg_pkt_chunk_ztprintascii(ipkt, chunk,
            1, CHAT_TEXT_MAX, &text)){ break; }
        chunk += pkt_chunk_bytes(ipkt, chunk);

        //Create broadcasted packet
        const char* uname = session_get_uname(session);
        void* pkt = pkt_create(NULL, 0, ENET_PACKET_FLAG_RELIABLE);
        pkt_chunk_add(pkt, (uint8_t*)&cmdidx, sizeof(uint16_t), PKT_CHUNK_U16);
        pkt_chunk_add(pkt, (uint8_t*)uname, strlen(uname) + 1, PKT_CHUNK_U8);
        pkt_chunk_add(pkt, (uint8_t*)text, strlen(text) + 1, PKT_CHUNK_U8);

        //Broadcast text to sessions
        rtcp_uvec_t* sessions = server_get_sessions(modsrv->server);
        for(int i = 0; i < sessions->size; ++i){
            Session* session2 = rtcp_uvec_get(sessions, i);

            //Skip unauthenticated sessions
            if(!session_authenticated(session2)){ break; }

            //Send a copy of the packet
            void* pkt2 = pkt_create(
                pkt_data(pkt), pkt_len(pkt), ENET_PACKET_FLAG_RELIABLE);
            enet_peer_send(session2->peer, 0, pkt2);
        }

        //Free broadcasted packet
        pkt_destroy(pkt);

    } break;
    case MSG_CHATWHISPER: {

        //Process only for authenticated session
        if(!session_authenticated(session)){ break; }

        //Attempt to get username
        char* uname;
        if(!msg_pkt_chunk_ztprintascii(ipkt, chunk,
            UNAME_MIN + 1, UNAME_MAX, &uname)){ break; }
        chunk += pkt_chunk_bytes(ipkt, chunk);

        //Attempt to get text
        char* text;
        if(!msg_pkt_chunk_ztprintascii(ipkt, chunk,
            1, CHAT_TEXT_MAX, &text)){ break; }
        chunk += pkt_chunk_bytes(ipkt, chunk);

        //Create broadcasted packet
        const char* uname0 = session_get_uname(session);
        void* pkt = pkt_create(NULL, 0, ENET_PACKET_FLAG_RELIABLE);
        pkt_chunk_add(pkt, (uint8_t*)&cmdidx, sizeof(uint16_t), PKT_CHUNK_U16);
        pkt_chunk_add(pkt, (uint8_t*)uname0, strlen(uname0) + 1, PKT_CHUNK_U8);
        pkt_chunk_add(pkt, (uint8_t*)uname, strlen(uname) + 1, PKT_CHUNK_U8);
        pkt_chunk_add(pkt, (uint8_t*)text, strlen(text) + 1, PKT_CHUNK_U8);

        //Broadcast text to sessions
        rtcp_uvec_t* sessions = server_get_sessions(modsrv->server);
        for(int i = 0; i < sessions->size; ++i){
            Session* session2 = rtcp_uvec_get(sessions, i);

            //Skip unauthenticated sessions
            if(!session_authenticated(session2)){ continue; }

            //Skip sessions that do not belong to the target user
            if(strcmp(session_get_uname(session2), uname) != 0){ continue; }

            //Send a copy of the packet
            void* pkt2 = pkt_create(
                pkt_data(pkt), pkt_len(pkt), ENET_PACKET_FLAG_RELIABLE);
            enet_peer_send(session2->peer, 0, pkt2);
        }

        //Send a copy to the originator
        void* pkt2 = pkt_create(
            pkt_data(pkt), pkt_len(pkt), ENET_PACKET_FLAG_RELIABLE);
        enet_peer_send(session->peer, 0, pkt2);

        //Free broadcasted packet
        pkt_destroy(pkt);

    } break;
    case MOD_MSG_SAVEWORLD: {

        //Process only for authenticated session
        if(!session_authenticated(session)){ break; }

        //Attempt to find user
        const char* uname0 = srp_verifier_get_username(session->verifier);
        Auth* auth = server_get_auth(modsrv->server);
        User* user0; if(auth_find_user(auth, uname0, &user0) < 0){ break; }

        //Check access privileges
        if(!(user0->access <= AccessRoot)){ break; }

        //Attempt to get name
        char* name;
        if(!msg_pkt_chunk_ztprintascii(ipkt, chunk,
            1, WORLD_FILEPATH_MAX, &name)){ break; }
        chunk += pkt_chunk_bytes(ipkt, chunk);

        //Compose path
        lstr_t path; lstr_malloc(&path, 1); lstr_init(&path);
        lstr_cat_str(&path, name);
        lstr_cat_str(&path, ".bin");

        //Attempt to open the file for writing, truncating previous contents
        FILE* file = fopen(lstr_get(&path), "w");
        if(!file){ break; }

        //Serialize world
        struct timespec start; clock_gettime(CLOCK_REALTIME, &start);
        int ret = world_serialize(world, file);
        struct timespec end; clock_gettime(CLOCK_REALTIME, &end);

        if(ret == 0){
            LOG_("serialized world to \"%s\" in %d ms",
                lstr_get(&path), diff_msec(&start, &end));
        } else {
            LOG_("failed to serialize world to \"%s\" in %d ms",
                lstr_get(&path), diff_msec(&start, &end));
        }

        //Attempt to close the file, flushing the user-space buffer
        if(fclose(file)){
            LOG_WARN_("MSG_SAVEWORLD: could not close file");
            LOG_WARN_("strerror(errno): %s", strerror(errno));
        }

        //Free path
        lstr_free(&path);

    } break;
    case MOD_MSG_LOADWORLD: {

        //Process only for authenticated session
        if(!session_authenticated(session)){ break; }

        //Attempt to find user
        const char* uname0 = srp_verifier_get_username(session->verifier);
        Auth* auth = server_get_auth(modsrv->server);
        User* user0; if(auth_find_user(auth, uname0, &user0) < 0){ break; }

        //Check access privileges
        if(!(user0->access <= AccessRoot)){ break; }

        //Attempt to get name
        char* name;
        if(!msg_pkt_chunk_ztprintascii(ipkt, chunk,
            1, WORLD_FILEPATH_MAX, &name)){ break; }
        chunk += pkt_chunk_bytes(ipkt, chunk);

        //Compose path
        lstr_t path; lstr_malloc(&path, 1); lstr_init(&path);
        lstr_cat_str(&path, name);
        lstr_cat_str(&path, ".bin");

        //Attempt to open the file for reading
        FILE* file = fopen(lstr_get(&path), "r");
        if(!file){ break; }

        //Restart world
        StartConfig* config = server_get_config(modsrv->server);
        world_stop(world);
        world_init(world, config->srv_entity_max);

        //Deserialize world
        struct timespec start; clock_gettime(CLOCK_REALTIME, &start);
        int ret = world_deserialize(world, file);
        struct timespec end; clock_gettime(CLOCK_REALTIME, &end);

        if(ret == 0){
            LOG_("deserialized world from \"%s\" in %d ms",
                lstr_get(&path), diff_msec(&start, &end));
        } else {
            LOG_("failed to deserialize world from \"%s\" in %d ms",
                lstr_get(&path), diff_msec(&start, &end));
        }

        //Actualize world
        if(ret == 0){ on_world_actualize(world, pilots); }

        //Attempt to close the file, flushing the user-space buffer
        if(fclose(file)){
            LOG_WARN_("MSG_LOADWORLD: could not close file");
            LOG_WARN_("strerror(errno): %s", strerror(errno));
        }

        //Free path
        lstr_free(&path);

    } break;
    case MOD_MSG_GENWORLD: {

        //Process only for authenticated session
        if(!session_authenticated(session)){ break; }

        //Attempt to find user
        const char* uname0 = srp_verifier_get_username(session->verifier);
        Auth* auth = server_get_auth(modsrv->server);
        User* user0; if(auth_find_user(auth, uname0, &user0) < 0){ break; }

        //Check access privileges
        if(!(user0->access <= AccessRoot)){ break; }

        //Attempt to get seed
        uint32_t seed;
        if(!msg_pkt_chunk_u32_single(ipkt, chunk, &seed)){ break; }
        chunk += pkt_chunk_bytes(ipkt, chunk);

        //Generate world
        world_generate(modsrv, seed);

    } break;
    case MOD_MSG_TESTWORLD: {
		
        //Process only for authenticated session
        if(!session_authenticated(session)){ break; }

        //Attempt to find user
        const char* uname0 = srp_verifier_get_username(session->verifier);
        Auth* auth = server_get_auth(modsrv->server);
        User* user0; if(auth_find_user(auth, uname0, &user0) < 0){ break; }

        //Check access privileges
        if(!(user0->access <= AccessRoot)){ break; }

        //Attempt to get name
        char* name;
        if(!msg_pkt_chunk_ztprintascii(ipkt, chunk,
            1, TESTGEN_NAME_MAX, &name)){ break; }
        chunk += pkt_chunk_bytes(ipkt, chunk);

		//Generate test world
		test_generate(modsrv, name);

    } break;
    case MOD_MSG_ADDSHIP: {

        //Process only for authenticated session
        if(!session_authenticated(session)){ break; }

        //Check that the global entity limit is not yet reached
        if(world->ecs.entnext >= world->ecs.entsmax){ break; }

        const char* uname = srp_verifier_get_username(session->verifier);

        //Check that the per-user entity limit is not yet reached
        if(pilotcache_uname_ents(pilots, uname) >= SRV_ENT_OWNED_MAX){ break; }

        //for(int i = 0; i < SRV_ENT_OWNED_MAX; ++i){

        //Add ship
        uint32_t spawn[3];
        pos_around_pos_in_radius(&modsrv->rand,
            (uint32_t[3]){ 0x80000000, 0x80000000, 0x80000000 },
            0xff0000, spawn);
        Entity ent = ship_generate(modsrv, spawn);

        //Add pilot component
        Pilot* pilot = ecs_get_mut_id(&world->ecs, ent, PilotID);
        strcpy(pilot->uname, uname);

        //Cache piloted ship
        pilotcache_addentity(pilots, pilot->uname, ent);

        //}

    } break;
    case MOD_MSG_DELSHIP: {

        //Process only for authenticated session
        if(!session_authenticated(session)){ break; }

        //Attempt to find user
        const char* uname = srp_verifier_get_username(session->verifier);
        Auth* auth = server_get_auth(modsrv->server);
        User* user; if(auth_find_user(auth, uname, &user) < 0){ break; }

        //Attempt to get entity
        uint16_t ent;
        if(!msg_pkt_chunk_u16_single(ipkt, chunk, &ent)){ break; }
        chunk += pkt_chunk_bytes(ipkt, chunk);

        //Check that the user has access to the entity
        if(!user_entity_access(&world->ecs, uname, user->access, ent)){ break; }

        //Remove entity
        wrap_entity_rem(modsrv, ent);

    } break;
    case MOD_MSG_LISTSHIP: {

        //Process only for authenticated session
        if(!session_authenticated(session)){ break; }

        const char* uname = srp_verifier_get_username(session->verifier);

        //Check that the user pilots any entities at all
        if(pilotcache_uname_ents(pilots, uname) <= 0){ break; }
        rtcp_uvec_t* pilotents = pilotcache_uname_get_ents(pilots, uname);

        //Attach msg type
        pkt_chunk_add(opkt, (uint8_t*)&cmdidx, sizeof(uint16_t), PKT_CHUNK_U16);

        //Attach all piloted entities
        for(int i = 0; i < pilotents->size; ++i){
            Entity* ent = rtcp_uvec_get(pilotents, i);
            pkt_chunk_add(opkt, (uint8_t*)ent, sizeof(uint16_t), PKT_CHUNK_U16);
        }

    } break;
    case MOD_MSG_SYNC_SESSION: {

        //Process only for authenticated session
        if(!session_authenticated(session)){ break; }

        //Attempt to get bytes
        uint8_t* bytes;
        if(!msg_pkt_chunk_u8_minmax(ipkt, chunk, 1, SESSION_BYTES, &bytes))
        { break; }
        chunk += pkt_chunk_bytes(ipkt, chunk);

        ModSession* modsession = (ModSession*)session->data;
        modsession_deserialize(modsession, 0, bytes);

    } break;
    case MOD_MSG_UXN_RAM: {

        //Process only for authenticated session
        if(!session_authenticated(session)){ break; }

        //Attempt to find user
        const char* uname = srp_verifier_get_username(session->verifier);
        Auth* auth = server_get_auth(modsrv->server);
        User* user; if(auth_find_user(auth, uname, &user) < 0){ break; }

        //Attempt to get entity
        uint16_t ent;
        if(!msg_pkt_chunk_u16_single(ipkt, chunk, &ent)){ break; }
        chunk += pkt_chunk_bytes(ipkt, chunk);
	
        //Attempt to get ROM
        uint8_t* rom;
        if(!msg_pkt_chunk_u8_minmax(ipkt, chunk, 1, UXN_ROM_BYTES, &rom))
        { break; }
        uint32_t rom_bytes = pkt_chunk_size(ipkt, chunk);
        chunk += pkt_chunk_bytes(ipkt, chunk);
	
        //for(int i = 0; i < SRV_ENT_OWNED_MAX; ++i){

        //Check that the user has access to the entity
        if(!user_entity_access(&world->ecs, uname, user->access, ent)){ break; }

        //Check that the entity is a ship
        const Type* type = ecs_get_id(&world->ecs, ent, TypeID);
        if(!type || type->type != ENTTYPE_SHIP){ break; }

        Uxn* uxn = ecs_get_mut_id(&world->ecs, ent, UxnID);

        //Load the ROM into RAM
        memcpy(&uxn->ram[UXN_PAGE_PROG], rom, rom_bytes);

        //Reset machine
        uxn_reset(uxn);

        //Boot machine by unsetting halt & break flags
        uxn->halt = uxn->brk = 0;

        //++ent; }

    } break;
    case MOD_MSG_UXN_BOOT: {

        //Process only for authenticated session
        if(!session_authenticated(session)){ break; }

        //Attempt to find user
        const char* uname = srp_verifier_get_username(session->verifier);
        Auth* auth = server_get_auth(modsrv->server);
        User* user; if(auth_find_user(auth, uname, &user) < 0){ break; }

        //Attempt to get entity
        uint16_t ent;
        if(!msg_pkt_chunk_u16_single(ipkt, chunk, &ent)){ break; }
        chunk += pkt_chunk_bytes(ipkt, chunk);

        //Check that the user has access to the entity
        if(!user_entity_access(&world->ecs, uname, user->access, ent)){ break; }

        //Check that the entity has the required component
        if(!ecs_get_id(&world->ecs, ent, UxnID)){ break; }

        Uxn* uxn = ecs_get_mut_id(&world->ecs, ent, UxnID);

        //Reset machine
        uxn_reset(uxn);

        //Boot machine by unsetting halt & break flags
        uxn->halt = uxn->brk = 0;

    } break;
    case MOD_MSG_UXN_HALT: {

        //Process only for authenticated session
        if(!session_authenticated(session)){ break; }

        //Attempt to find user
        const char* uname = srp_verifier_get_username(session->verifier);
        Auth* auth = server_get_auth(modsrv->server);
        User* user; if(auth_find_user(auth, uname, &user) < 0){ break; }

        //Attempt to get entity
        uint16_t ent;
        if(!msg_pkt_chunk_u16_single(ipkt, chunk, &ent)){ break; }
        chunk += pkt_chunk_bytes(ipkt, chunk);

        //Check that the user has access to the entity
        if(!user_entity_access(&world->ecs, uname, user->access, ent)){ break; }

        //Check that the entity has the required component
        if(!ecs_get_id(&world->ecs, ent, UxnID)){ break; }

        Uxn* uxn = ecs_get_mut_id(&world->ecs, ent, UxnID);

        //Set halt flag
        uxn->halt = 1;

    } break;
    case MOD_MSG_UXN_RESUME: {

        //Process only for authenticated session
        if(!session_authenticated(session)){ break; }

        //Attempt to find user
        const char* uname = srp_verifier_get_username(session->verifier);
        Auth* auth = server_get_auth(modsrv->server);
        User* user; if(auth_find_user(auth, uname, &user) < 0){ break; }

        //Attempt to get entity
        uint16_t ent;
        if(!msg_pkt_chunk_u16_single(ipkt, chunk, &ent)){ break; }
        chunk += pkt_chunk_bytes(ipkt, chunk);

        //Check that the user has access to the entity
        if(!user_entity_access(&world->ecs, uname, user->access, ent)){ break; }

        //Check that the entity has the required component
        if(!ecs_get_id(&world->ecs, ent, UxnID)){ break; }

        Uxn* uxn = ecs_get_mut_id(&world->ecs, ent, UxnID);

        //Unset halt flag
        uxn->halt = 0;

    } break;
    case MOD_MSG_UXN_FEED: {

        //Process only for authenticated session
        if(!session_authenticated(session)){ break; }

        //Attempt to find user
        const char* uname = srp_verifier_get_username(session->verifier);
        Auth* auth = server_get_auth(modsrv->server);
        User* user; if(auth_find_user(auth, uname, &user) < 0){ break; }

        //Attempt to get entity
        uint16_t ent;
        if(!msg_pkt_chunk_u16_single(ipkt, chunk, &ent)){ break; }
        chunk += pkt_chunk_bytes(ipkt, chunk);

        //Check that the user has access to the entity
        if(!user_entity_access(&world->ecs, uname, user->access, ent)){ break; }

        //Check that the entity has the required component
        if(!ecs_get_id(&world->ecs, ent, UxnID)){ break; }

        //Attempt to get bytes
        uint8_t* evts;
        if(!msg_pkt_chunk_u8_minmax(ipkt, chunk,
            sizeof(Event), UXN_DEV_EVENTS * sizeof(Event), &evts)){ break; }
        uint32_t evts_len = pkt_chunk_size(ipkt, chunk);
        chunk += pkt_chunk_bytes(ipkt, chunk);

        //Check that the struct size evenly divides the data size
        if(evts_len % sizeof(Event)){ break; }

        Uxn* uxn = ecs_get_mut_id(&world->ecs, ent, UxnID);

        //Push input events
        buffer_push_multi(&uxn->ievt, (Event*)evts, evts_len / sizeof(Event));
    
    } break;
    default: {} break;
    }
}
