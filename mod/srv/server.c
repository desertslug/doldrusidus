#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define PLANCKLOG_FILE
#define PLANCKLOG_HEADER
#include "plancklog.h"

#include "srvconf.h"
#include "macros.h"
#include "packet.h"

#include "world.c"
#include "pilotcache.c"
#include "modsrv.c"

ModServer modsrv;

void on_world_actualize(World* world, rtcp_hmap_t* pilots);
uint8_t on_dei(UXN_DEV_ARGS);
void on_deo(UXN_DEV_ARGS);

#include "srvmsg.c"
#include "ecs.c"
#include "compo.c"
#include "uxn.c"
#include "dev.c"
#include "random.c"
#include "romu.c"
#include "memory.c"
#include "ivec.c"
#include "generate.c"
#include "testgen.c"
#include "sweep.c"
#include "system.c"

void on_server_start(void* srv)
{
    if(sizeof(ModSession) > SESSION_BYTES){
        LOG_ERROR_("modsession_init: size exceeds SESSION_BYTES");
        raise(SIGINT);
    }

    modserver_start(&modsrv, srv);
}

void on_server_stop()
{
    modserver_stop(&modsrv);
}

void on_world_actualize(World* world, rtcp_hmap_t* pilotc)
{
    modserver_actualize(&modsrv);
}

void server_sync_sessions(void* server, ModServer* modsrv)
{
    rtcp_uvec_t* sessions = server_get_sessions(server);
    World* world = &modsrv->world;
    ECS* ecs = &world->ecs;
    rtcp_hmap_t* pilots = &modsrv->pilots;

    //Synchronize reliably
    for(int i = 0; i < sessions->size; ++i){
        Session* session = rtcp_uvec_get(sessions, i);

        //Process only for authenticated session
        if(!session_authenticated(session)){ continue; }
        const char* uname = session_get_uname(session);

        //Create packet
        ENetPacket* opkt = pkt_create(NULL, 0, ENET_PACKET_FLAG_RELIABLE);

        uint16_t idx = MOD_MSG_SYNC_ECS;
        pkt_chunk_add(opkt, (uint8_t*)&idx, sizeof(uint16_t), PKT_CHUNK_U16);

        uint16_t style = SERIAL_NETW;
        pkt_chunk_add(opkt, (uint8_t*)&style, sizeof(uint16_t), PKT_CHUNK_U16);

        //Append chunk: age
        world_serialize_age_pkt(world, opkt);

        //Piloted entities
        if(pilotcache_uname_ents(pilots, uname)){

            rtcp_uvec_t* ents = pilotcache_uname_get_ents(pilots, uname);

            //Append chunks: entities
            world_serialize_ents_pkt(world, ents->values, ents->size, opkt, style);
        }

        //pkt_log(opkt);
        enet_peer_send(session->peer, 0, opkt);
    }

    //Synchronize unreliably
    for(int i = 0; i < sessions->size; ++i){
        Session* session = rtcp_uvec_get(sessions, i);
        ModSession* modsession = (ModSession*)session->data;

        //Process only for authenticated session
        if(!session_authenticated(session)){ continue; }

		Entity ent = modsession->syncent;

		//Attempt to find user
		const char* uname = session_get_uname(session);
        Auth* auth = server_get_auth(modsrv->server);
        User* user; if(auth_find_user(auth, uname, &user) < 0){ continue; }

		//Check sufficient access
		bool access = ent == 0xffff ?
			user->access <= AccessRoot :
			user_entity_access(ecs, uname, user->access, ent);
		if(!access){ continue; }

		//Sweep position and radius
		uint32_t pos[3];
		uint32_t radius;

		//Synchronized entities
		int found = 0;
        uint16_t ents[SRV_ENT_SYNC_PER_TICK];

		//Extremal (invalid/empty) entity ID
		if(ent == 0xffff){

			//Synchronization position is in the mod session
			memcpy(pos, modsession->pos, sizeof(uint32_t[3]));

			//Synchronization radius is in the mod session
			radius = modsession->radius;
		}
		//Non-extremal (valid) entity ID
		else {

			//Synchronization position is the entity position
			const Position* pos2 = ecs_get_id(ecs, ent, PositionID);
			if(!pos2){ continue; }
			memcpy(pos, pos2->pos, sizeof(uint32_t[3]));

			//Synchronization target must have a pilot component
			const Pilot* pilot = ecs_get_id(ecs, ent, PilotID);
			if(!pilot){ continue; }

			//If the target has a pilot, it must have navigation
			const Nav* nav = ecs_get_id(ecs, ent, NavID);
			assert_or_exit(nav,
				"server_sync_sessions: pilot but no nav\n");

			//Synchronization radius is the navigation range
			radius = nav_range(nav) << 16;

			//Check ordering between time points
			struct timespec synctime = { .tv_sec = nav->syncents.sec };
			assert_or_exit(!timespec_less(&world->age, &synctime),
				"server_sync_sessions: world age less than synctime\n");

			//If the time difference is within the limit
			struct timespec diff = timespec_diff(&synctime, &world->age);
			struct timespec maxtime = { .tv_sec = SYNC_ENT_SEC_MAX };
			if(timespec_less(&diff, &maxtime)){

				//Add entities inside the navigation buffer
				int entsmax = SRV_ENT_SYNC_PER_TICK - found;
				found += syncents_add_upto(&nav->syncents,
					ecs, &ents[found], entsmax);
			}
		}

        //Find bounding box
        uint32_t bbox[3][2];
        v3uint32_bbox(pos, radius, bbox);

        //Entities inside that bounding box
		int entsmax = SRV_ENT_SYNC_PER_TICK - found;
		found += sweep_bbox_upto(&modsrv->sweep,
			bbox, hash_next(&modsrv->rand), &ents[found], entsmax);

        //Create packet
        ENetPacket* opkt = pkt_create(NULL, 0, 0);

        uint16_t idx = MOD_MSG_SYNC_ECS;
        pkt_chunk_add(opkt, (uint8_t*)&idx, sizeof(uint16_t), PKT_CHUNK_U16);

        uint16_t style = SERIAL_NETW_UNRELIABLE;
        pkt_chunk_add(opkt, (uint8_t*)&style, sizeof(uint16_t), PKT_CHUNK_U16);

        //Append chunk: age
        world_serialize_age_pkt(world, opkt);

        //Append chunks: entities
        world_serialize_ents_pkt(world, ents, found, opkt, style);

        enet_peer_send(session->peer, 0, opkt);
    }

    //Synchronize entity deletion reliably
    if(modsrv->entdelq.size > 0){

        assert_or_exit(sizeof(Entity) == sizeof(uint16_t),
            "server_sync_sessions: entity data size mismatch");
        
        for(int i = 0; i < sessions->size; ++i){
            Session* session = rtcp_uvec_get(sessions, i);

            //Process only for authenticated session
            if(!session_authenticated(session)){ continue; }

            //Create packet
            ENetPacket* opkt = pkt_create(NULL, 0, ENET_PACKET_FLAG_RELIABLE);

            uint16_t idx = MOD_MSG_SYNC_ECS_DELETE;
            pkt_chunk_add(opkt, (uint8_t*)&idx, sizeof(uint16_t), PKT_CHUNK_U16);

            //Add entities
            for(int j = 0; j < modsrv->entdelq.size; ++j){
                pkt_chunk_add(opkt,
                    (uint8_t*)rtcp_uvec_get(&modsrv->entdelq, j),
                    sizeof(Entity), PKT_CHUNK_U16);
            }

            //pkt_log(opkt);
            enet_peer_send(session->peer, 0, opkt);
        }
    }

    //Handle input events from sessions
    for(int i = 0; i < sessions->size; ++i){
        Session* session = rtcp_uvec_get(sessions, i);

        //Process only for authenticated session
        if(!session_authenticated(session)){ continue; }
        const char* uname = session_get_uname(session);
        Auth* auth = server_get_auth(modsrv->server);
        User* user; if(auth_find_user(auth, uname, &user) < 0){ continue; }

        //Get input event buffer
        ModSession* modsession = (ModSession*)session->data;
        Buffer* ievt = &modsession->ievt;

        //If there are no input events waiting, skip
        if(ievt->size <= 0){ continue; }

        //Check that the focused entity is alive
        Entity ent = modsession->focusent;
        if(!ecs_entity_alive(ecs, ent)){ continue; }

        //Check that the user has access rights to the entity
        if(!user_entity_access(ecs, uname, user->access, ent)){ continue; }

        //Check that the entity has an uxn component
        Uxn* uxn = (Uxn*)ecs_get_id(ecs, ent, UxnID);
        if(!uxn){ continue; }

        //Push buffer head & tail portions
        buffer_push_multi(&uxn->ievt,
            &ievt->buf[ievt->first], buffer_head_size(ievt));
        buffer_push_multi(&uxn->ievt,
            &ievt->buf[0], buffer_tail_size(ievt));

        //Reset buffer
        buffer_reset(ievt);
    }
}

void on_server_step(struct timespec delta)
{
    World* world = &modsrv.world;

    //Allocate query
    Query q; query_alloc(&q);

    //Run systems
    system_nav_drive_pos_pphys(world, &q);
    system_pos_pphys_integrate(world, &q);
    system_uxn_screen_device_push(world, &q);
    system_uxn_run(world, &q, &modsrv.sweep);
    system_uxn_compute_sync(world, &q);
    system_trophs(world, &q);
    system_trophs_habitable_accumulate(world, &q);
    system_monolith_maze(world, &q);
    system_pos_wormhole(world, &q, &modsrv.sweep);
    system_pos_hive_synth(world, &q, &modsrv.spawner);
    system_pos_extract_synth(world, &q);
    system_pos_synth(world, &q);
    system_pos_pphys_bay(world, &q, &modsrv.spawner);
    system_type_pos_pphys_nav_synth_hull_wasp(world, &q, &modsrv.sweep, &modsrv.spawner, &modsrv.entdelq);
    system_pos_pphys_nav_drive_hull_leviathan(world, &q, &modsrv.sweep, &modsrv.entdelq);
    system_pos_pphys_nav_drive_hull_missile(world, &q, &modsrv.entdelq);
    system_ship(world, &q, &modsrv.spawner, &modsrv.entdelq);
    system_wreckage(world, &q, &modsrv.entdelq);

    //Synchronize sessions
    server_sync_sessions(modsrv.server, &modsrv);

    //Reset systems
    system_pphys_reset(world, &q);

    query_dealloc(&q);

    modserver_step(&modsrv, delta);
}

void on_server_recv(Session* session, void* pkt)
{
    assert_or_exit(session->peer, "on_server_recv: session->peer was NULL\n");

    //Attempt to process packet
    ENetPacket* opkt = pkt_create(NULL, 0, ENET_PACKET_FLAG_RELIABLE);
    pkt_process(session, pkt, opkt, &modsrv);

    //Send nonempty output packet
    if(pkt_len(opkt)){ enet_peer_send(session->peer, 0, opkt); }
    else { pkt_destroy(opkt); }
}
