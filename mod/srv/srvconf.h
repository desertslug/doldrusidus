#ifndef SRVCONF_H
#define SRVCONF_H

#include <stdint.h>

/* Maximum number of entities owned by a user. */
enum { SRV_ENT_OWNED_MAX = 8, };

/* Maximum number of entities serialized for a session. */
enum { SRV_ENT_SYNC_PER_TICK = 64, };

/* Number of star systems to generate. */
enum { SYSTEMS_MIN = 192, };
enum { SYSTEMS_MAX = 256, };

enum { SYSTEM_RADIUS_MIN = 50, };
enum { SYSTEM_RADIUS_MAX = 200, };
#define SYSTEM_STARS_INNER_TIMEVAL 0.25f

/* Solar mass. */
enum { SOLAR_MASS_MIN = 1 };
enum { SOLAR_MASS_MAX = 9 };

/* Number of oases to generate. */
enum { OASES_MIN = 48, };
enum { OASES_MAX = 64, };

/* Size of oasis cellular automaton grid */
enum { OASIS_GRID_WIDTH = 16, };

/* Oasis cellular automaton step frequency (Hz) */
//#define OASIS_STEP_FREQ 2.0f

/* Chance for a generated oasis to host trophs. */
#define OASIS_INIT_INJECT_CHANCE 0.7f

/* Size of habitable cellular automaton grid */
enum { HABITABLE_GRID_WIDTH = 32, };

/* Habitable cellular automaton step frequency (Hz) */
//#define HABITABLE_STEP_FREQ 2.0f

/* Chance for a generated oasis to host trophs. */
#define HABITABLE_INIT_INJECT_CHANCE 0.1f

/* Chance for a habitable world to spawn in a star system. */
#define HABITABLE_IN_SYSTEM_SPAWN_CHANCE 0.7f

/* Number of distinct secrets for monoliths. */
enum { MONOLITH_SECRET_COUNT = 1, };

typedef struct MonolithSecret
{
    int difficulty;
    int instructions;
    const char* secret;
} MonolithSecret;

MonolithSecret MONOLITH_SECRETS[MONOLITH_SECRET_COUNT] = {
    { 32, 4, "The hydra Universe, twisting its body scaled in stars.\n" },
};

/* Number of wormhole pairs to generate. */
enum { WORMHOLES_MIN = 10, };
enum { WORMHOLES_MAX = 14, };

/* Distance between paired wormholes. */
enum { WORMHOLES_DIST_MIN = 0x0800, };
enum { WORMHOLES_DIST_MAX = 0x4000, };

/* Sphere of influence radius of wormholes. */
enum { WORMHOLE_SOI = 0x0100, };

/* Chance for a barren world to spawn in a star system. */
#define BARREN_IN_SYSTEM_SPAWN_CHANCE 0.7f

/* Barren world resource richness. */
#define BARREN_RICHNESS_MIN 1.01f
#define BARREN_RICHNESS_MAX 5.99f
#define BARREN_RICHNESS_POW 2.0f

/* Number of hives along each axis of the world. */
enum { HIVES_PER_AXIS = 2, };

/* Distance between hives. */
enum { BETWEEN_HIVES = 0x10000 * (uint32_t)(0x10000 / HIVES_PER_AXIS), };

/* Number of leviathans in the world. */
enum { LEVIATHANS_MIN = 16, };
enum { LEVIATHANS_MAX = 16, };

/* Chance for a civilization to spawn in a star system. */
#define CIVIL_IN_SYSTEM_SPAWN_CHANCE 0.4f

#endif /* SRVCONF_H */
