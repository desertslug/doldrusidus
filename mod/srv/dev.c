#include "dev.h"

void cons_write(UXN_DEV_ARGS)
{
    buffer_push(&u->oevt, (Event){ .devport = addr, .data = u->dev[addr] });
}

const int HOST_WRITE_CYCLES[UXN_DEV_BYTES_TOTAL] = {
    [HOST_WRITE_NAV_ABS] = 1,
    [HOST_WRITE_NAV_REL] = 1,
    [HOST_WRITE_NAV_ORB] = 1,
    [HOST_WRITE_NAV_IMP] = 1,
    [HOST_WRITE_NAV_ENT_POS] = 10,
    [HOST_WRITE_NAV_ENT_GET] = 25,
    [HOST_WRITE_NAV_SYN_ENT] = 25,
    [HOST_WRITE_NAV_SIG_SET] = 1,
    [HOST_WRITE_NAV_SIG_GET] = 1,
    [HOST_WRITE_NAV_SYN_SET] = 10,
    [HOST_WRITE_NAV_SYN_GET] = 10,

    [HOST_WRITE_DRV_THR] = 1,

    [HOST_WRITE_VAT_INJ] = 10,
    [HOST_WRITE_VAT_SAM] = 10,
    [HOST_WRITE_VAT_HAR] = 10,

    [HOST_WRITE_EXT_BAR] = 10,

    [HOST_WRITE_SYN_REP] = 5,
    [HOST_WRITE_SYN_RES] = 5,
    [HOST_WRITE_SYN_SAL] = 10,

    [HOST_WRITE_COM_POW] =  1,

    [HOST_WRITE_BAY_LAU] =  5,

    [HOST_WRITE_CRG_GET] = 10,
    [HOST_WRITE_CRG_PUT] = 10,

    [HOST_WRITE_GRO_ADD] = 10,
    [HOST_WRITE_GRO_REM] = 10,

    [HOST_WRITE_RNG] = 10,
    [HOST_WRITE_RNG2] = 10,
};

void host_write(UXN_DEV_ARGS)
{
    static uint32_t romu_trio32[3] = { 0x8badf00d, 0xabadc0de, 0xf00dcafe };

    //Convenience pointers
    Uxn* uxn = (Uxn*)u;
    uint8_t* host = &uxn->dev[UXN_DEV_HOST];
    uint8_t* args = &uxn->dev[UXN_DEV_HOST + UXN_PORT_HOST_DATA];
    World* w = (World*)world;
    ECS* ecs = &w->ecs;

    //Subtract command cost cycles from remaining instruction count
    uxn->instr -= HOST_WRITE_CYCLES[uxn->dev[addr]];

	//Dirty byte will be set by default, and unset by some exceptions
	uint8_t dirty = 1;

    //Branch based on command
    switch(uxn->dev[addr]){
    case HOST_WRITE_NAV_ABS: {

        Nav* nav = (Nav*)ecs_get_id(ecs, ent, NavID);
        if(!nav){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Motion type, target coordinates, target distance
        nav->motion = NAV_MOTION_APPROACH;
        for(int i = 0; i < 3; ++i){
            nav->target[i] = TWOPOW16 * ((args[i * 2] << 8) + args[i * 2 + 1]);
        }
        nav->distance = MIN((args[6] << 8) + args[7], NAV_MAX_DIST) << 16;

        //Set return code
        host[UXN_PORT_HOST_READ] = HOST_READ_OK;

    } break;
    case HOST_WRITE_NAV_REL: {

        Nav* nav = (Nav*)ecs_get_id(ecs, ent, NavID);
        if(!nav){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Motion type, target direction
        nav->motion = NAV_MOTION_RELATIVE;
        for(int i = 0; i < 3; ++i){ nav->target[i] = args[i] * TWOPOW16; }

        //Set return code
        host[UXN_PORT_HOST_READ] = HOST_READ_OK;

    } break;
    case HOST_WRITE_NAV_ORB: {

        Nav* nav = (Nav*)ecs_get_id(ecs, ent, NavID);
        if(!nav){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Motion type, target coordinates, target distance
        nav->motion = NAV_MOTION_ORBIT;
        for(int i = 0; i < 3; ++i){
            nav->target[i] = TWOPOW16 * ((args[i * 2] << 8) + args[i * 2 + 1]);
        }
        nav->distance = MIN((args[6] << 8) + args[7], NAV_MAX_DIST) << 16;

        //Set return code
        host[UXN_PORT_HOST_READ] = HOST_READ_OK;

    } break;
    case HOST_WRITE_NAV_IMP: {

        Nav* nav = (Nav*)ecs_get_id(ecs, ent, NavID);
        if(!nav){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Motion type, target coordinates
        nav->motion = NAV_MOTION_IMPACT;
        for(int i = 0; i < 3; ++i){
            nav->target[i] = TWOPOW16 * ((args[i * 2] << 8) + args[i * 2 + 1]);
        }

        //Set return code
        host[UXN_PORT_HOST_READ] = HOST_READ_OK;

    } break;
    case HOST_WRITE_NAV_ENT_POS: {

        //Extract target entity
        Entity ent2 = (args[0] << 8) + args[1];
        if(!ecs_entity_alive(ecs, ent2))
        { host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Get entity position
        const Position* pos2 = ecs_get_id(ecs, ent2, PositionID);
        if(!pos2){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Write query result
        for(int i = 0; i < 3; ++i){
            args[i * 2] = pos2->pos[i] >> 24;
            args[i * 2 + 1] = (pos2->pos[i] >> 16) & 0xff;
        }
        
        //Set return code
        host[UXN_PORT_HOST_READ] = HOST_READ_OK;

    } break;
    case HOST_WRITE_NAV_ENT_TYP: {

        //Extract target entity
        Entity ent2 = (args[0] << 8) + args[1];
        if(!ecs_entity_alive(ecs, ent2))
        { host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Get entity type
        const Type* type2 = ecs_get_id(ecs, ent2, TypeID);
        if(!type2){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        assert_or_exit(type2->type <= 255,
            "HOST_WRITE_NAV_ENT_TYP: enttype does not fit into a byte");

        //Write the type of the entity
        args[0] = type2->type & 0xff;

        //Set return code
        host[UXN_PORT_HOST_READ] = HOST_READ_OK;

    } break;
    case HOST_WRITE_NAV_ENT_GET: {
        
        Nav* nav = (Nav*)ecs_get_id(ecs, ent, NavID);
        if(!nav){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

		//Synchronization radius is either the navigation range
		//or the range specified as an argument, whichever is smaller
		uint16_t rad_nav = nav_range(nav);
		uint16_t rad_arg = (args[0] << 8) + args[1];
		uint16_t radius = MIN(rad_nav, rad_arg);

        //Get entity position
        const Position* pos = ecs_get_id(ecs, ent, PositionID);
        if(!pos){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Get up to a single entity within that range
        uint16_t ents[1];
        int found = sweep_pos_radius_upto(sweep,
            pos->pos, radius << 16, romu_trio32_random(romu_trio32), ents, 1);

        //Check that there were any entities found
        if(found <= 0){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        const Position* pos2 = ecs_get_id(ecs, ents[0], PositionID);
        if(!pos2){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Check that the entity is within the spherical radius
        if(v3uint32_dist(pos->pos, pos2->pos) > radius << 16)
        { host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }
		
        //Write info about the first entity to the arguments
        args[0] = ents[0] >> 8; args[1] = ents[0] & 0xff;

        //Set return code
        host[UXN_PORT_HOST_READ] = HOST_READ_OK;

    } break;
    case HOST_WRITE_NAV_SYN_ENT: {

        Nav* nav = (Nav*)ecs_get_id(ecs, ent, NavID);
        if(!nav){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

		//Synchronization radius is either the navigation range
		//or the range specified as an argument, whichever is smaller
		uint16_t rad_nav = nav_range(nav);
		uint16_t rad_arg = (args[0] << 8) + args[1];
		uint16_t radius = MIN(rad_nav, rad_arg);

        //Get entity position
        const Position* pos = ecs_get_id(ecs, ent, PositionID);
        if(!pos){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Get a number of entities within that range
        uint16_t ents[SYNC_ENT_MAX];
		for(int i = 0; i < SYNC_ENT_MAX; ++i){ ents[i] = 0xffff; }
        int found = sweep_pos_radius_upto(sweep, pos->pos, radius << 16,
			romu_trio32_random(romu_trio32), ents, SYNC_ENT_MAX);

        //Check that there were any entities found
        if(found <= 0){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

		//Create synchronized entities structure from the result
		SyncEnts syncents = { .sec = w->age.tv_sec, };
		memcpy(syncents.ents, ents, SYNC_ENT_MAX * sizeof(uint16_t));

		//Serialize synchronized entities
		syncents_serialize_to_signal(&syncents, args);

		//Reset dirty byte
		dirty = 0;

        //Set return code
        host[UXN_PORT_HOST_READ] = HOST_READ_OK;

    } break;
    case HOST_WRITE_NAV_SIG_SET: {

        Nav* nav = (Nav*)ecs_get_id(ecs, ent, NavID);
        if(!nav){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Set signal
        memcpy(nav->signal, args, SIGNAL_BYTES);

		//Keep dirty byte
		dirty = host[UXN_PORT_HOST_DIRTY];

        //Set return code
        host[UXN_PORT_HOST_READ] = HOST_READ_OK;

    } break;
    case HOST_WRITE_NAV_SIG_GET: {

        Nav* nav = (Nav*)ecs_get_id(ecs, ent, NavID);
        if(!nav){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        const Position* pos = ecs_get_id(ecs, ent, PositionID);
        if(!pos){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Extract target entity
        Entity ent2 = (args[0] << 8) + args[1];
        if(!ecs_entity_alive(ecs, ent2))
        { host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        Nav* nav2 = (Nav*)ecs_get_id(ecs, ent2, NavID);
        if(!nav2){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        const Position* pos2 = ecs_get_id(ecs, ent2, PositionID);
        if(!pos2){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

		//The radius is the sum of the two nav ranges
		uint16_t radius = nav_range(nav) + nav_range(nav2);

        //Check that the entity is within the spherical radius
        if(v3uint32_dist(pos->pos, pos2->pos) > radius << 16)
        { host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }
		
        //Get signal
        memcpy(args, nav2->signal, SIGNAL_BYTES);

		Uxn* uxn2 = (Uxn*)ecs_get_id(ecs, ent2, UxnID);
        if(!uxn2){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

		//Inherit dirty byte of the target entity
		dirty = uxn2->dev[UXN_DEV_HOST + UXN_PORT_HOST_DIRTY];

        //Set return code
        host[UXN_PORT_HOST_READ] = HOST_READ_OK;

    } break;
    case HOST_WRITE_NAV_SYN_SET: {

        Nav* nav = (Nav*)ecs_get_id(ecs, ent, NavID);
        if(!nav){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

		//The dirty byte must be reset
		if(host[UXN_PORT_HOST_DIRTY])
		{ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

		//Check protocol index
		if(args[0] != NAV_SIG_PROTO_SYNC_ENT)
		{ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

		//Deserialize synchronized entities
		syncents_deserialize_from_signal(&nav->syncents, args);

		//Keep dirty byte
		dirty = host[UXN_PORT_HOST_DIRTY];

        //Set return code
        host[UXN_PORT_HOST_READ] = HOST_READ_OK;

    } break;
    case HOST_WRITE_NAV_SYN_GET: {

        Nav* nav = (Nav*)ecs_get_id(ecs, ent, NavID);
        if(!nav){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

		//Serialize synchronized entities
		syncents_serialize_to_signal(&nav->syncents, args);

		//Reset dirty byte
		dirty = 0;

        //Set return code
        host[UXN_PORT_HOST_READ] = HOST_READ_OK;

    } break;
    case HOST_WRITE_DRV_THR: {

        Drive* drive = (Drive*)ecs_get_id(ecs, ent, DriveID);
        if(!drive){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Set throttle value of the ship's drive
        drive->throttle = args[0];

        //Set return code
        host[UXN_PORT_HOST_READ] = HOST_READ_OK;
    } break;
    case HOST_WRITE_VAT_INJ: {

        Position* pos = (Position*)ecs_get_id(ecs, ent, PositionID);
        if(!pos){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }
        Vat* vat = (Vat*)ecs_get_id(ecs, ent, VatID);
        if(!vat){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Extract target entity
        Entity ent2 = (args[0] << 8) + args[1];
        if(!ecs_entity_alive(ecs, ent2))
        { host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Attempt to retrieve required modules of the target entity
        Trophs* trophs2 = (Trophs*)ecs_get_id(ecs, ent2, TrophsID);
        if(!trophs2){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }
        Position* pos2 = (Position*)ecs_get_id(ecs, ent2, PositionID);
        if(!pos2){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Check interaction range
        if(v3uint32_dist(pos->pos, pos2->pos) >= TROPHS_INTERACTION_UNITS)
        { host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Inject possibly all available cells
        cells_t injected[TROPH_COUNT];
        trophs_inject(trophs2,
            (cells_t[TROPH_COUNT]){
                [TROPH_DEAD] = 0,
                [TROPH_AUTO] = vat->cells[TROPH_AUTO],
                [TROPH_HETE] = vat->cells[TROPH_HETE],
            },
            injected
        );

        //Update cell count by removing injected cells
        vat_cells_remove(vat, injected);

        //Set return code
        host[UXN_PORT_HOST_READ] = HOST_READ_OK;

    } break;
    case HOST_WRITE_VAT_SAM: {

        Position* pos = (Position*)ecs_get_id(ecs, ent, PositionID);
        if(!pos){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }
        Vat* vat = (Vat*)ecs_get_id(ecs, ent, VatID);
        if(!vat){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Extract target entity
        Entity ent2 = (args[0] << 8) + args[1];
        if(!ecs_entity_alive(ecs, ent2))
        { host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Attempt to retrieve required modules of the target entity
        Trophs* trophs2 = (Trophs*)ecs_get_id(ecs, ent2, TrophsID);
        if(!trophs2){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }
        Position* pos2 = (Position*)ecs_get_id(ecs, ent2, PositionID);
        if(!pos2){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Check interaction range
        if(v3uint32_dist(pos->pos, pos2->pos) >= TROPHS_INTERACTION_UNITS)
        { host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Sample possibly as many cells as the vat can fit
        cells_t sampled[TROPH_COUNT];
        trophs_sample(trophs2,
            (cells_t[TROPH_COUNT]){
                [TROPH_DEAD] = 0,
                [TROPH_AUTO] = vat->cells_max[TROPH_AUTO] - vat->cells[TROPH_AUTO],
                [TROPH_HETE] = vat->cells_max[TROPH_HETE] - vat->cells[TROPH_HETE],
            },
            sampled
        );

        //Update cell count by adding sampled cells
        vat_cells_add(vat, sampled);

        //Set return code
        host[UXN_PORT_HOST_READ] = HOST_READ_OK;

    } break;
    case HOST_WRITE_VAT_HAR: {

        Position* pos = (Position*)ecs_get_id(ecs, ent, PositionID);
        if(!pos){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }
        Synth* synth = (Synth*)ecs_get_id(ecs, ent, SynthID);
        if(!synth){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Extract target entity
        Entity ent2 = (args[0] << 8) + args[1];
        if(!ecs_entity_alive(ecs, ent2))
        { host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Attempt to retrieve habitable
        Habitable* habit = (Habitable*)ecs_get_id(ecs, ent2, HabitableID);
        if(!habit){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Attempt to retrieve required modules of the target entity
        Trophs* trophs2 = (Trophs*)ecs_get_id(ecs, ent2, TrophsID);
        if(!trophs2){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }
        Position* pos2 = (Position*)ecs_get_id(ecs, ent2, PositionID);
        if(!pos2){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Check interaction range
        if(v3uint32_dist(pos->pos, pos2->pos) >= TROPHS_INTERACTION_UNITS)
        { host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Harvesting resets the population
        memset(trophs2->cells[trophs2->grid], TROPH_DEAD, TROPHS_CELLS_MAX);
        trophs_count(trophs2);

        //Extract the mats that fit into the capacity
        mats_t harvested = MIN(
            habit->output,
            synth->mats_max[SYNTH_MAT_ORGANIC] - synth->mats[SYNTH_MAT_ORGANIC]
        );

        habit->output -= harvested;
        synth_mat_add(synth, SYNTH_MAT_ORGANIC, harvested);

        //Set return code
        host[UXN_PORT_HOST_READ] = HOST_READ_OK;
        
    } break;
    case HOST_WRITE_EXT_BAR: {

        Extract* ext = (Extract*)ecs_get_id(ecs, ent, ExtractID);
        if(!ext){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Extract target entity
        Entity ent2 = (args[0] << 8) + args[1];
        if(!ecs_entity_alive(ecs, ent2))
        { host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Check that the extract is ready
        if(ext->state != EXTRACT_STT_IDLE)
        { host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Set extract state
        ext->target = ent2;
        ext->state = EXTRACT_STT_PULL;

        //Set return code
        host[UXN_PORT_HOST_READ] = HOST_READ_OK;

    } break;
    case HOST_WRITE_SYN_REP: {

        Synth* synth = (Synth*)ecs_get_id(ecs, ent, SynthID);
        if(!synth){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Check that the synth is ready
        if(synth->state != SYNTH_STT_IDLE)
        { host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Extract target entity
        Entity ent2 = (args[0] << 8) + args[1];
        if(!ecs_entity_alive(ecs, ent2))
        { host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Set synth state
        synth->target = ent2;
        synth->state = SYNTH_STT_REPAIR;

        //Set return code
        host[UXN_PORT_HOST_READ] = HOST_READ_OK;

    } break;
    case HOST_WRITE_SYN_RES: {

        Synth* synth = (Synth*)ecs_get_id(ecs, ent, SynthID);
        if(!synth){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Check that the synth is ready
        if(synth->state != SYNTH_STT_IDLE)
        { host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Extract target entity
        Entity ent2 = (args[0] << 8) + args[1];
        if(!ecs_entity_alive(ecs, ent2))
        { host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Set synth state
        synth->target = ent2;
        synth->state = SYNTH_STT_RESTOCK;

        //Set return code
        host[UXN_PORT_HOST_READ] = HOST_READ_OK;

    } break;
    case HOST_WRITE_SYN_SAL: {

        Position* pos = (Position*)ecs_get_id(ecs, ent, PositionID);
        if(!pos){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }
        Synth* synth = (Synth*)ecs_get_id(ecs, ent, SynthID);
        if(!synth){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Extract target entity
        Entity ent2 = (args[0] << 8) + args[1];
        if(!ecs_entity_alive(ecs, ent2))
        { host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        Position* pos2 = (Position*)ecs_get_id(ecs, ent2, PositionID);
        if(!pos2){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }
        Synth* synth2 = (Synth*)ecs_get_id(ecs, ent2, SynthID);
        if(!synth2){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Salvaging currently works only for wreckages
        const Wreckage* wreck2 = ecs_get_id(ecs, ent2, WreckageID);
        if(!wreck2){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Check interaction range
        if(v3uint32_dist(pos->pos, pos2->pos) >= SYNTH_INTERACTION_UNITS)
        { host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Materials that fit into the salvaging synth
        mats_t fit[SYNTH_MAT_COUNT];
        for(int i = 0; i < SYNTH_MAT_COUNT; ++i){
            fit[i] = synth->mats_max[i] - synth->mats[i];
        }

        //Materials that will be absorbed from the salvaged synth
        mats_t absorbed[SYNTH_MAT_COUNT];
        for(int i = 0; i < SYNTH_MAT_COUNT; ++i){
            absorbed[i] = MIN(fit[i], synth2->mats[i]);
        }
        synth_mats_add(synth, absorbed);
        synth_mats_del(synth2, absorbed);

        //Set return code
        host[UXN_PORT_HOST_READ] = HOST_READ_OK;

    } break;
    case HOST_WRITE_COM_POW: {

        Position* pos = (Position*)ecs_get_id(ecs, ent, PositionID);
        if(!pos){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }
        Compute* com = (Compute*)ecs_get_id(ecs, ent, ComputeID);
        if(!com){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }
        Uxn* uxn = (Uxn*)ecs_get_id(ecs, ent, UxnID);
        if(!uxn){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Set default return code
        host[UXN_PORT_HOST_READ] = HOST_READ_NOK;

        //Extract target entity
        Entity ent2 = (args[0] << 8) + args[1];
        if(!ecs_entity_alive(ecs, ent2))
        { host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        Type* type2 = (Type*)ecs_get_id(ecs, ent2, TypeID);
        if(!type2){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }
        Position* pos2 = (Position*)ecs_get_id(ecs, ent2, PositionID);
        if(!pos2){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Handle different entity types
        switch(type2->type){
        case ENTTYPE_MONOLITH: {

            //Check interaction range
            if(v3uint32_dist(pos->pos, pos2->pos) >= MONOLITH_INTERACTION_UNITS)
            { host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

            Monolith* monol = (Monolith*)ecs_get_id(ecs, ent2, MonolithID);
            if(!monol){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }
            Maze* maze = (Maze*)ecs_get_id(ecs, ent2, MazeID);
            if(!maze){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

            //If the maze inside has finished generating
            if(maze->stacksize == 0){

                //Start generating again
                maze_reset(maze);

                //Set result character & return code
                args[0] = monol->secret[monol->actual];
                host[UXN_PORT_HOST_READ] = HOST_READ_OK;

                //Step actual character
                monol->actual = (monol->actual + 1) % monol->length;

            } else {

                //Donate remaining instructions
                monol->accumulator += uxn->instr;
                uxn->instr = 0;
            }

        } break;
        case ENTTYPE_SHIP: {
            
            //Check interaction range
            if(v3uint32_dist(pos->pos, pos2->pos) >= MONOLITH_INTERACTION_UNITS)
            { host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

            Compute* com2 = (Compute*)ecs_get_id(ecs, ent2, ComputeID);
            if(!com2){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

            if(uxn->instr > com2->received){

                //Donate remaining instructions
                com2->received += uxn->instr;
                uxn->instr = 0;
            }
            
        } break;
        default: {} break;
        };

    } break;
    case HOST_WRITE_BAY_LAU: {
	
        Bay* bay = (Bay*)ecs_get_id(ecs, ent, BayID);
        if(!bay){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Check that the bay is ready to launch
        if(bay->state != BAY_STT_IDLE)
        { host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Extract target entity
        Entity ent2 = (args[0] << 8) + args[1];
        if(!ecs_entity_alive(ecs, ent2))
        { host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Launch
        bay->target = ent2;
        bay->state = BAY_STT_FIRE;

        //Set return code
        host[UXN_PORT_HOST_READ] = HOST_READ_OK;

    } break;
    case HOST_WRITE_CRG_GET: {

        Position* pos = (Position*)ecs_get_id(ecs, ent, PositionID);
        if(!pos){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }
        Cargo* cargo = (Cargo*)ecs_get_id(ecs, ent, CargoID);
        if(!cargo){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Extract target entity
        Entity ent2 = (args[0] << 8) + args[1];
        if(!ecs_entity_alive(ecs, ent2))
        { host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        Type* type2 = (Type*)ecs_get_id(ecs, ent2, TypeID);
        if(!type2){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }
        Position* pos2 = (Position*)ecs_get_id(ecs, ent2, PositionID);
        if(!pos2){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Check that the target is a civilization
        if(type2->type != ENTTYPE_CIVILIZATION)
        { host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Check interaction range
        if(v3uint32_dist(pos->pos, pos2->pos) >= CIVIL_INTERACTION_UNITS)
        { host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Check cargo space
        if(cargo->artifact != ART_NONE)
        { host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Get random artifact
        cargo->artifact = romu_trio32_random(romu_trio32) % ART_COUNT + 1;
        cargo->originator = ent2;

        //Set return code
        host[UXN_PORT_HOST_READ] = HOST_READ_OK;

    } break;
    case HOST_WRITE_CRG_PUT: {

        Position* pos = (Position*)ecs_get_id(ecs, ent, PositionID);
        if(!pos){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }
        Cargo* cargo = (Cargo*)ecs_get_id(ecs, ent, CargoID);
        if(!cargo){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Extract target entity
        Entity ent2 = (args[0] << 8) + args[1];
        if(!ecs_entity_alive(ecs, ent2))
        { host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        Type* type2 = (Type*)ecs_get_id(ecs, ent2, TypeID);
        if(!type2){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }
        Position* pos2 = (Position*)ecs_get_id(ecs, ent2, PositionID);
        if(!pos2){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Check that the target is a civilization
        if(type2->type != ENTTYPE_CIVILIZATION)
        { host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Check interaction range
        if(v3uint32_dist(pos->pos, pos2->pos) >= CIVIL_INTERACTION_UNITS)
        { host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        Type* type3 = (Type*)ecs_get_id(ecs, cargo->originator, TypeID);
        if(!type3){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }
        Position* pos3 = (Position*)ecs_get_id(ecs, cargo->originator, PositionID);
        if(!pos3){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Check that the originator is a civilization
        if(type3->type != ENTTYPE_CIVILIZATION)
        { host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Check cargo
        if(cargo->artifact == ART_NONE)
        { host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Put artifact
        cargo->artifact = ART_NONE;
        cargo->originator = 0;

        Growth* growth = (Growth*)ecs_get_id(ecs, ent, GrowthID);

        //Add points toward growth bonus
        if(growth){

            //Add only points that can fit
            uint16_t points = MIN(
                artifact_points(pos2, pos3),
                growth->bonus_max - growth->bonus_acc
            );
            growth_bonus_add(growth, points);

            //Update modules
            entity_update_compos(ecs, ent);
        }

        //Set return code
        host[UXN_PORT_HOST_READ] = HOST_READ_OK;

    } break;
    case HOST_WRITE_GRO_ADD: {

        Type* type = (Type*)ecs_get_id(ecs, ent, TypeID);
        if(!type){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }
        Synth* synth = (Synth*)ecs_get_id(ecs, ent, SynthID);
        if(!synth){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }
        Growth* growth = (Growth*)ecs_get_id(ecs, ent, GrowthID);
        if(!growth){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Extract module index
        uint8_t id = args[0];

        //WARNING: UPKEEP
        //Add the first ship component's component index for convenience
        id += NavID;

        //Check that the component index is valid
        if(id >= COMPO_COUNT)
        { host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }
        
        //Check that the module is growable by that entity type
        if(!MODULE_GROWABLE[type->type][id])
        { host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }
        
        //Check that the required materials are available
        if(!synth_mats_available(synth, COMPO_MAT_COST[id]))
        { host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }
        
        //Attempt to increase module count
        if(!growth_add(growth, id, ecs, ent))
        { host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }
        
        //Consume required materials
        synth_mats_del(synth, COMPO_MAT_COST[id]);
        
        //Update modules
        entity_update_compos(ecs, ent);

        //Set return code
        host[UXN_PORT_HOST_READ] = HOST_READ_OK;

    } break;
    case HOST_WRITE_GRO_REM: {

        Type* type = (Type*)ecs_get_id(ecs, ent, TypeID);
        if(!type){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }
        Synth* synth = (Synth*)ecs_get_id(ecs, ent, SynthID);
        if(!synth){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }
        Growth* growth = (Growth*)ecs_get_id(ecs, ent, GrowthID);
        if(!growth){ host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Extract module index
        uint8_t id = args[0];

        //WARNING: UPKEEP
        //Add the first ship component's component index for convenience
        id += NavID;

        //Check that the component index is valid
        if(id >= COMPO_COUNT)
        { host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Check that the module is growable by that entity type
        if(!MODULE_GROWABLE[type->type][id])
        { host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Attempt to decrease module count
        if(!growth_del(growth, id, ecs, ent))
        { host[UXN_PORT_HOST_READ] = HOST_READ_NOK; break; }

        //Absorb a portion of the required materials
        mats_t absorbed[SYNTH_MAT_COUNT];
        for(int i = 0; i < SYNTH_MAT_COUNT; ++i){
            absorbed[i] = MIN(
                MODULE_MAT_ABSORB * COMPO_MAT_COST[id][i],
                synth->mats_max[i] - synth->mats[i]
            );
        }
        synth_mats_add(synth, absorbed);

        //Update modules
        entity_update_compos(ecs, ent);

        //Set return code
        host[UXN_PORT_HOST_READ] = HOST_READ_OK;

    } break;
    case HOST_WRITE_RNG: {

        uint8_t rand = romu_trio32_random(romu_trio32);
        host[UXN_PORT_HOST_RNG_LO] = rand;

    } break;
    case HOST_WRITE_RNG2: {

        uint16_t rand = romu_trio32_random(romu_trio32);
        host[UXN_PORT_HOST_RNG_HI] = rand >> 8;
        host[UXN_PORT_HOST_RNG_LO] = rand & 0xff;

    } break;
    default: {} break;
    }

	//Write dirty byte with what emerged during handling
	uxn->dev[UXN_DEV_HOST + UXN_PORT_HOST_DIRTY] = dirty;

    //TODO should the written byte be pushed?
}

void host_dirty(UXN_DEV_ARGS)
{
    //Convenience pointers
    Uxn* uxn = (Uxn*)u;
    uint8_t* args = &uxn->dev[UXN_DEV_HOST + UXN_PORT_HOST_DATA];

	//If a zero byte was written into the dirty byte
	if(uxn->dev[UXN_DEV_HOST + UXN_PORT_HOST_DIRTY] == 0){

		//Zero out argument bytes
		memset(args, 0, SIGNAL_BYTES);
	}
}

void host_data(UXN_DEV_ARGS)
{
    //Convenience pointer
    Uxn* uxn = (Uxn*)u;

	//Set dirty byte
	uxn->dev[UXN_DEV_HOST + UXN_PORT_HOST_DIRTY] = 1;
}

uint8_t on_dei(UXN_DEV_ARGS)
{
    return u->dev[addr];
}

void on_deo(UXN_DEV_ARGS)
{
    typedef void (*deo_handler)(UXN_DEV_ARGS);
    static const deo_handler handler_funs[UXN_DEV_BYTES_TOTAL] = {
        [UXN_DEV_CONS + UXN_PORT_CONS_WRITE] = cons_write,
        [UXN_DEV_HOST + UXN_PORT_HOST_WRITE] = host_write,
        [UXN_DEV_HOST + UXN_PORT_HOST_DIRTY] = host_dirty,
        [UXN_DEV_HOST + UXN_PORT_HOST_DATA + 0x0] = host_data,
        [UXN_DEV_HOST + UXN_PORT_HOST_DATA + 0x1] = host_data,
        [UXN_DEV_HOST + UXN_PORT_HOST_DATA + 0x2] = host_data,
        [UXN_DEV_HOST + UXN_PORT_HOST_DATA + 0x3] = host_data,
        [UXN_DEV_HOST + UXN_PORT_HOST_DATA + 0x4] = host_data,
        [UXN_DEV_HOST + UXN_PORT_HOST_DATA + 0x5] = host_data,
        [UXN_DEV_HOST + UXN_PORT_HOST_DATA + 0x6] = host_data,
        [UXN_DEV_HOST + UXN_PORT_HOST_DATA + 0x7] = host_data,
        [UXN_DEV_HOST + UXN_PORT_HOST_DATA + 0x8] = host_data,
        [UXN_DEV_HOST + UXN_PORT_HOST_DATA + 0x9] = host_data,
        [UXN_DEV_HOST + UXN_PORT_HOST_DATA + 0xa] = host_data,
        [UXN_DEV_HOST + UXN_PORT_HOST_DATA + 0xb] = host_data,
        [UXN_DEV_HOST + UXN_PORT_HOST_DATA + 0xc] = host_data,
        [UXN_DEV_HOST + UXN_PORT_HOST_DATA + 0xd] = host_data,
        [UXN_DEV_HOST + UXN_PORT_HOST_DATA + 0xe] = host_data,
        [UXN_DEV_HOST + UXN_PORT_HOST_DATA + 0xf] = host_data,
    };

    //Call non-null handler function
    deo_handler handler = handler_funs[addr];
    if(handler){ handler(u, addr, world, ent, sweep); }
}
