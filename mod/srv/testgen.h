#ifndef TESTGEN_H
#define TESTGEN_H

#include "random.h"

enum { TESTGEN_NAME_MAX = 64, };

void test_generate(ModServer* modsrv, char* str);

void pilot_entity_sync_range_generate(ModServer* modsrv, uint32_t seed);

void signal_entity_sync_generate(ModServer* modsrv, uint32_t seed);

void porpoise_explore_generate(ModServer* modsrv, uint32_t seed);

#endif /* TESTGEN_H */

