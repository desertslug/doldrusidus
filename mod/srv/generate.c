#include "generate.h"

Entity ship_generate(ModServer* modsrv, uint32_t spawn[3])
{
    ECS* ecs = &modsrv->world.ecs;
    Entity ent = wrap_entity_add(modsrv, (Type){ .type = ENTTYPE_SHIP }, spawn);

    ecs_get_mut_id(ecs, ent, PointphysID);

    ecs_get_mut_id(ecs, ent, NavID);
    ecs_get_mut_id(ecs, ent, DriveID);
    ecs_get_mut_id(ecs, ent, VatID);
    ecs_get_mut_id(ecs, ent, ExtractID);
    ecs_get_mut_id(ecs, ent, SynthID);
    ecs_get_mut_id(ecs, ent, ComputeID);
    ecs_get_mut_id(ecs, ent, HullID);
    ecs_get_mut_id(ecs, ent, BayID);
    ecs_get_mut_id(ecs, ent, CargoID);
    ecs_get_mut_id(ecs, ent, GrowthID);

    entity_reset_compos(ecs, ent);
    /*Growth* growth = ecs_get_mut_id(ecs, ent, GrowthID);
    growth->bonus_acc = 16;
    Synth* synth = ecs_get_mut_id(ecs, ent, SynthID);
    synth->mats[0] = synth->mats[1] = 32;*/
    entity_update_compos(ecs, ent);

    return ent;
}

void world_generate(ModServer* modsrv, uint32_t seed)
{
    //Initialize pseudorandom number generator
    Hash1 hash; hash.buf[0] = seed; hash.last = seed;

    //Origin
    uint32_t origin[3] = { 0x80000000, 0x80000000, 0x80000000 };

    //Origin star, to protect spawn location from leviathans
    star_generate(modsrv, &hash, origin);

    //Generate hives to cover the world evenly
    for(int i = 0; i < HIVES_PER_AXIS; ++i){
    for(int j = 0; j < HIVES_PER_AXIS; ++j){
    for(int k = 0; k < HIVES_PER_AXIS; ++k){

        hive_generate(modsrv, &hash,
            (uint32_t[3]){
                BETWEEN_HIVES / 2 + i * BETWEEN_HIVES,
                BETWEEN_HIVES / 2 + j * BETWEEN_HIVES,
                BETWEEN_HIVES / 2 + k * BETWEEN_HIVES,
            }
        );
    }}}
    
    //Star systems
    uint32_t systems = glm_lerp(SYSTEMS_MIN, SYSTEMS_MAX, hash_next_norm(&hash));
    uint32_t radius = ((TWOPOW16 - 1) >> 1) - SYSTEM_RADIUS_MAX;
    for(int i = 0; i < systems; ++i){
        uint32_t pos[3];
        pos_around_pos_in_radius(&hash, origin, radius << 16, pos);
        system_generate(modsrv, &hash, pos);
    }
    
    //Oases
    uint32_t oases = glm_lerp(OASES_MIN, OASES_MAX, hash_next_norm(&hash));
    for(int i = 0; i < oases; ++i){
        uint32_t pos[3];
        pos_around_pos_in_radius(&hash, origin, 0x7fffffff, pos);
        oasis_generate(modsrv, &hash, pos);
    }

    //Monoliths
    for(int i = 0; i < MONOLITH_SECRET_COUNT; ++i){
        uint32_t pos[3];
        pos_around_pos_in_radius(&hash, origin, radius << 16, pos);
        monolith_generate(modsrv, &hash, pos, MONOLITH_SECRETS[i]);
    }

    //Wormholes
    uint32_t wormholes = glm_lerp(WORMHOLES_MIN, WORMHOLES_MAX, hash_next_norm(&hash));
    for(int i = 0; i < wormholes; ++i){
        uint32_t pos[3];
        pos_around_pos_in_radius(&hash, origin,
            ((uint32_t)((TWOPOW16 - 1) >> 1)) << 16, pos);
        uint32_t pos2[3];
        pos_around_pos_in_kernel(&hash, pos,
            WORMHOLES_DIST_MIN << 16, WORMHOLES_DIST_MAX << 16, pos2);

        wormhole_generate(modsrv, &hash, pos, pos2);
        wormhole_generate(modsrv, &hash, pos2, pos);
    }

    //Leviathans
    uint32_t levias = glm_lerp(LEVIATHANS_MIN, LEVIATHANS_MAX, hash_next_norm(&hash));
    for(int i = 0; i < levias; ++i){
        uint32_t pos[3];
        pos_around_pos_in_radius(&hash, origin, radius << 16, pos);
        /*pos_around_pos_in_radius(&hash,
            //(uint32_t[3]){ 0xc0000000, 0xc0000000, 0x40000000 },
            origin,
            0x80 << 16, pos);*/
        leviathan_generate(modsrv, &hash, pos);
    }
}

void system_generate(ModServer* modsrv, Hash1* hash, uint32_t spawn[3])
{
    //Calculate inner radius where stars are located
    uint32_t inner_radius = glm_lerp(SYSTEM_RADIUS_MIN, SYSTEM_RADIUS_MAX,
        SYSTEM_STARS_INNER_TIMEVAL);

    //Generate first star of the system
    uint32_t unary_pos[3];
    pos_around_pos_in_radius(hash, spawn, inner_radius << 16, unary_pos);
    star_generate(modsrv, hash, unary_pos);
    
    /*
    //Generate a binary companion
    if(0.5f > hash_next_norm(hash)){
        uint32_t binary_pos[3];
        pos_around_pos_in_radius(hash, pos, inner_radius << 16, binary_pos);
        star_generate(world, hash, binary_pos);

        //Generate a ternary companion
        if(0.25f > hash_next_norm(hash)){
            uint32_t ternary_pos[3];
            pos_around_pos_in_radius(hash, pos, inner_radius << 16, ternary_pos);
            star_generate(world, hash, ternary_pos);
        }
    }
    */

    //Generate habitable companion
    if(HABITABLE_IN_SYSTEM_SPAWN_CHANCE > hash_next_norm(hash)){
        uint32_t habit_pos[3];
        pos_around_pos_in_radius(hash, spawn, inner_radius << 16, habit_pos);
        habitable_generate(modsrv, hash, habit_pos);
    }
    
    //Generate barren companion
    if(BARREN_IN_SYSTEM_SPAWN_CHANCE > hash_next_norm(hash)){

        uint32_t barr_pos[3];
        pos_around_pos_in_radius(hash, spawn, inner_radius << 16, barr_pos);
        barren_generate(modsrv, hash, barr_pos);

        //Generate ruin companion
        if(CIVIL_IN_SYSTEM_SPAWN_CHANCE > hash_next_norm(hash)){
            uint32_t ruin_pos[3];
            pos_around_pos_in_radius(hash, spawn, inner_radius << 16, ruin_pos);
            ruin_generate(modsrv, hash, ruin_pos);
        }

    } else {

        //Generate civilization companion
        if(CIVIL_IN_SYSTEM_SPAWN_CHANCE > hash_next_norm(hash)){
            uint32_t civ_pos[3];
            pos_around_pos_in_radius(hash, spawn, inner_radius << 16, civ_pos);
            civilization_generate(modsrv, hash, civ_pos);
        }
    }
}

Entity star_generate(ModServer* modsrv, Hash1* hash, uint32_t spawn[3])
{
    ECS* ecs = &modsrv->world.ecs;
    Entity ent = wrap_entity_add(modsrv, (Type){ .type = ENTTYPE_STAR }, spawn);

    //Mass
    Star* star = ecs_get_mut_id(ecs, ent, StarID);
    star->mass = SOLAR_MASS_MIN +
	hash_next(hash) % (SOLAR_MASS_MAX - SOLAR_MASS_MIN);

    return ent;
}

Entity oasis_generate(ModServer* modsrv, Hash1* hash, uint32_t spawn[3])
{
    ECS* ecs = &modsrv->world.ecs;
    Entity ent = wrap_entity_add(modsrv, (Type){ .type = ENTTYPE_OASIS }, spawn);

    Trophs* trophs = ecs_get_mut_id(ecs, ent, TrophsID);
    trophs_initialize(trophs, OASIS_GRID_WIDTH,
        (float[TROPH_PARAM_COUNT]){
            [TROPH_AUTO_REPRO] = 0.27f, [TROPH_AUTO_DEATH] = 0.01f,
            [TROPH_HETE_REPRO] = 0.12f, [TROPH_HETE_DEATH] = 0.06f,
        },
        (float[TROPH_PARAM_COUNT]){
            [TROPH_AUTO_REPRO] = 0.0f, [TROPH_AUTO_DEATH] = 0.0f,
            [TROPH_HETE_REPRO] = 0.0f, [TROPH_HETE_DEATH] = 0.0f,
        },
        (uint32_t[3]){ hash_next(hash), hash_next(hash), hash_next(hash) },
        0.0f);

    //Inject initial cells with some chance
    if(hash_next_norm(hash) < OASIS_INIT_INJECT_CHANCE){
        cells_t injected[TROPH_COUNT];
        trophs_inject(trophs,
            (cells_t[TROPH_COUNT]){
                [TROPH_DEAD] = 0,
                [TROPH_AUTO] = trophs->width * trophs->width / 2,
                [TROPH_HETE] = trophs->width * trophs->width / 2,
            },
            injected
        );
    }

    return ent;
}

Entity habitable_generate(ModServer* modsrv, Hash1* hash, uint32_t spawn[3])
{
    ECS* ecs = &modsrv->world.ecs;
    Entity ent = wrap_entity_add(modsrv, (Type){ .type = ENTTYPE_HABITABLE }, spawn);

    ecs_get_mut_id(ecs, ent, HabitableID);

    Trophs* trophs = ecs_get_mut_id(ecs, ent, TrophsID);
    trophs_initialize(trophs, HABITABLE_GRID_WIDTH,
        (float[TROPH_PARAM_COUNT]){
            [TROPH_AUTO_REPRO] = 0.27f, [TROPH_AUTO_DEATH] = 0.01f,
            [TROPH_HETE_REPRO] = 0.13f, [TROPH_HETE_DEATH] = 0.06f,
        },
        (float[TROPH_PARAM_COUNT]){
            [TROPH_AUTO_REPRO] = 0.0f, [TROPH_AUTO_DEATH] = 0.0f,
            [TROPH_HETE_REPRO] = 0.0f, [TROPH_HETE_DEATH] = 0.0f,
        },
        (uint32_t[3]){ hash_next(hash), hash_next(hash), hash_next(hash) },
        1.0f);

    //Inject initial cells with some chance
    if(hash_next_norm(hash) < HABITABLE_INIT_INJECT_CHANCE){
        cells_t injected[TROPH_COUNT];
        trophs_inject(trophs,
            (cells_t[TROPH_COUNT]){
                [TROPH_DEAD] = 0,
                [TROPH_AUTO] = trophs->width * trophs->width / 2,
                [TROPH_HETE] = trophs->width * trophs->width / 2,
            },
            injected
        );
    }

    return ent;
}

Entity monolith_generate(ModServer* modsrv, Hash1* hash, uint32_t spawn[3], MonolithSecret secret)
{
    assert_or_exit(strlen(secret.secret) < SECRET_MAX,
        "monolith_generate: secret too long");

    ECS* ecs = &modsrv->world.ecs;
    Entity ent = wrap_entity_add(modsrv, (Type){ .type = ENTTYPE_MONOLITH }, spawn);

    Monolith* monol = ecs_get_mut_id(ecs, ent, MonolithID);

    //Initialize
    monol->actual = 0;
    monol->length = strlen(secret.secret);
    monol->accumulator = 0;
    monol->threshold = secret.instructions;
    memcpy(monol->secret, secret.secret, monol->length);

    Maze* maze = ecs_get_mut_id(ecs, ent, MazeID);
    maze_initialize(maze, hash, secret.difficulty,
        (uint32_t[3]){ hash_next(hash), hash_next(hash), hash_next(hash) });

    return ent;
}

Entity wormhole_generate(ModServer* modsrv, Hash1* hash, uint32_t spawn[3], uint32_t target[3])
{
    ECS* ecs = &modsrv->world.ecs;
    Entity ent = wrap_entity_add(modsrv, (Type){ .type = ENTTYPE_WORMHOLE }, spawn);

    Wormhole* worm = ecs_get_mut_id(ecs, ent, WormholeID);

    //Initialize
    worm->radius = WORMHOLE_SOI << 16;
    memcpy(worm->target, target, sizeof(uint32_t[3]));
    for(int i = 0; i < 3; ++i){ worm->romu_trio32[i] = hash_next(hash); }

    return ent;
}

Entity barren_generate(ModServer* modsrv, Hash1* hash, uint32_t spawn[3])
{
    ECS* ecs = &modsrv->world.ecs;

    //Get the hive that guards the position's sector
    Entity hive_id = 1 +
        ((spawn[0] / BETWEEN_HIVES) << 2) +
        ((spawn[1] / BETWEEN_HIVES) << 1) +
        ((spawn[2] / BETWEEN_HIVES) << 0);
    Hive* hive = (Hive*)ecs_get_id(ecs, hive_id, HiveID);
    assert_or_exit(hive, "barren_generate: could not find hive\n");

    //Check whether the hive in the position's sector has capacity
    if(hive->guarded >= BARREN_PER_HIVE){ return -1; }

    Entity ent = wrap_entity_add(modsrv, (Type){ .type = ENTTYPE_BARREN }, spawn);

    Barren* barr = ecs_get_mut_id(ecs, ent, BarrenID);
    barr->richness = glm_lerp(
        BARREN_RICHNESS_MIN, BARREN_RICHNESS_MAX,
        powf(hash_next_norm(hash), BARREN_RICHNESS_POW)
    );

    //Add the barren world to the hive
    hive_add_barren(hive, ent, ecs);

    return ent;
}

Entity hive_generate(ModServer* modsrv, Hash1* hash, uint32_t spawn[3])
{
    ECS* ecs = &modsrv->world.ecs;
    Entity ent = wrap_entity_add(modsrv, (Type){ .type = ENTTYPE_HIVE }, spawn);

    Hive* hive = ecs_get_mut_id(ecs, ent, HiveID);

    //Initialize
    for(int i = 0; i < 3; ++i){ hive->romu_trio32[i] = hash_next(hash); }
    hive->guarded = 0;

    Synth* synth = ecs_get_mut_id(ecs, ent, SynthID);
    synth_reset(synth, ecs, ent);
    synth->mats_max[SYNTH_MAT_INORGANIC] = HIVE_MAT_INORGANIC_CAPACITY;
    synth_mats_add(synth,
        (uint16_t[SYNTH_MAT_COUNT]){
            [SYNTH_MAT_ORGANIC] = 0,
            [SYNTH_MAT_INORGANIC] = HIVE_MAT_INORGANIC_CAPACITY / 2,
        }
    );

    return ent;
}

Entity leviathan_generate(ModServer* modsrv, Hash1* hash, uint32_t spawn[3])
{
    ECS* ecs = &modsrv->world.ecs;
    Entity ent = wrap_entity_add(modsrv, (Type){ .type = ENTTYPE_LEVIATHAN }, spawn);

    Leviathan* levia = ecs_get_mut_id(ecs, ent, LeviathanID);

    //Initialize
    levia->state = LEVIA_STT_WANDER;
    for(int i = 0; i < 3; ++i){ levia->romu_trio32[i] = hash_next(hash); }

    ecs_get_mut_id(ecs, ent, PointphysID);
    ecs_get_mut_id(ecs, ent, NavID);
    ecs_get_mut_id(ecs, ent, DriveID);
    ecs_get_mut_id(ecs, ent, HullID);

    entity_reset_compos(ecs, ent);
    entity_update_compos(ecs, ent);

    //Set navigation to the spawn position, see WANDER state
    Nav* nav = ecs_get_mut_id(ecs, ent, NavID);
    nav_set(nav, NAV_MOTION_APPROACH, spawn, 0);

    return ent;
}

Entity civilization_generate(ModServer* modsrv, Hash1* hash, uint32_t spawn[3])
{
    //ECS* ecs = &modsrv->world.ecs;
    Entity ent = wrap_entity_add(modsrv, (Type){ .type = ENTTYPE_CIVILIZATION }, spawn);

    return ent;
}

Entity ruin_generate(ModServer* modsrv, Hash1* hash, uint32_t spawn[3])
{
    ECS* ecs = &modsrv->world.ecs;
    Entity ent = wrap_entity_add(modsrv, (Type){ .type = ENTTYPE_RUIN }, spawn);

    Ruin* ruin = ecs_get_mut_id(ecs, ent, RuinID);
    ruin->description = hash_next(hash) % RUIN_COUNT + 1;

    return ent;
}
