#ifndef PILOTCACHE_H
#define PILOTCACHE_H

#include "auth.h"
#include "ecs.h"
#include "uvec.h"
#include "hmap.h"

/*typedef struct PilotCache
{
    rtcp_hmap_t assoc; //maps uname to rtcp_uvec_t
} PilotCache;*/

void pilotcache_alloc(rtcp_hmap_t* assoc);
void pilotcache_dealloc(rtcp_hmap_t* assoc);

int pilotcache_uname_ents(rtcp_hmap_t* assoc, const char* username);
rtcp_uvec_t* pilotcache_uname_get_ents(rtcp_hmap_t* assoc, const char* username);
int pilotcache_uname_find_entity(rtcp_uvec_t* uvec, Entity ent);
void pilotcache_addentity(rtcp_hmap_t* assoc, const char* username, Entity ent);
void pilotcache_delentity(rtcp_hmap_t* assoc, const char* username, Entity ent);

#endif /* PILOTCACHE_H */