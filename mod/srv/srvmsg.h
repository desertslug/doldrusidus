#ifndef SRVMSG_H
#define SRVMSG_H

#include "srp.h"
#include "defs.h"
#include "modmsg.h"
#include "world.h"
#include "lstr.h"
#include "auth.h"
#include "session.h"
#include "config.h"
#include "uxn.h"
#include "generate.h"
#include "testgen.h"

bool user_entity_access(ECS* ecs, const char* uname, Access access, Entity ent);

void pkt_process(Session* session, void* ipkt, void* opkt, ModServer* modsrv);

#endif /* SRVMSG_H */
