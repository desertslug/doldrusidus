#include "modsrv.h"

void modserver_start(ModServer* modsrv, void* server)
{
    //Store
    modsrv->server = server;

    //World
    StartConfig* config = server_get_config(modsrv->server);
    world_alloc(&modsrv->world);
    world_init(&modsrv->world, config->srv_entity_max);

    //Pilot cache
    pilotcache_alloc(&modsrv->pilots);

    //Initialize random number generator
    modsrv->rand.buf[0] = 1;
    modsrv->rand.last = 0x8badf00d;

    //Initialize sweep-and-prune
    sweep_init(&modsrv->sweep);

    //Allocate spawner
    ecs_alloc(&modsrv->spawner);
    ecs_init(&modsrv->spawner, modsrv->world.ecs.entsmax, COMPO_COUNT, COMPO_SIZE);

    //Allocate entity deletion queue
    rtcp_uvec_malloc(&modsrv->entdelq, 1, sizeof(Entity));
    rtcp_uvec_init(&modsrv->entdelq);
}

void modserver_stop(ModServer* modsrv)
{
    //Deallocate world
    world_dealloc(&modsrv->world);

    //Deallocate pilot cache
    pilotcache_dealloc(&modsrv->pilots);

    //Deallocate spawner
    ecs_dealloc(&modsrv->spawner);

    //Deallocate entity deletion queue
    rtcp_uvec_free(&modsrv->entdelq);
}

void modserver_step(ModServer* modsrv, struct timespec delta)
{
    world_step(&modsrv->world, delta);

    sweep_step(&modsrv->sweep, &modsrv->world.ecs);

    //Despawn entities
    modsrv_despawn(modsrv);

    //Spawn entities
    modsrv_spawn(modsrv);

    //Tick the lazily shrinking data structures of the spawner
    ecs_tick(&modsrv->spawner);
}

void modserver_actualize(ModServer* modsrv)
{
    ECS* ecs = &modsrv->world.ecs;

    ArchID arch;
    Query q; query_alloc(&q);

    //Reset pilotcache
    pilotcache_dealloc(&modsrv->pilots);
    pilotcache_alloc(&modsrv->pilots);

    //Run query on pilot components
    arch = archid_setbit((ArchID){0}, PilotID);
    query_init(&q, ecs, arch);
    while(query_next(&q)){
        Entity* ents = query_ents(ecs, &q);
        Pilot* pils = (Pilot*)query_term(ecs, &q, PilotID);
        for(int i = 0; i < q.size; ++i){

            //Associate entity with its pilot in the cache
            pilotcache_addentity(&modsrv->pilots, pils[i].uname, ents[i]);
        }        
    }

    //Reset sweep-and-prune
    sweep_init(&modsrv->sweep);

    //Sweep-and-prune
    arch = (ArchID){0};
    query_init(&q, ecs, arch);
    while(query_next(&q)){
        Entity* ents = query_ents(ecs, &q);
        for(int i = 0; i < q.size; ++i){

            //Add entity to sweep-and-prune
            sweep_add(&modsrv->sweep, ecs, ents[i]);
        }        
    }


    query_dealloc(&q);
}

Entity wrap_entity_add(ModServer* modsrv, Type type, uint32_t spawn[3])
{
    ECS* ecs = &modsrv->world.ecs;

    //Add entity to the ECS
    Entity ent = ecs_entity_add(ecs);

    //Add & init type component
    Type* typec = ecs_get_mut_id(ecs, ent, TypeID);
    memcpy(typec, &type, sizeof(Type));
    assert_or_exit(typec->type > ENTTYPE_NONE && typec->type < ENTTYPE_COUNT,
        "wrap_entity_add: unknown type");

    //Add & init position component
    Position* pos = ecs_get_mut_id(ecs, ent, PositionID);
    memcpy(pos->pos, spawn, sizeof(uint32_t[3]));

    //Add entity to sweep-and-prune
    sweep_add(&modsrv->sweep, ecs, ent);

    return ent;
}

void wrap_entity_rem(ModServer* modsrv, Entity ent)
{
    ECS* ecs = &modsrv->world.ecs;

    //If the entity has a pilot component, dissociate entity from it
    const Pilot* pilot = ecs_get_id(ecs, ent, PilotID);
    if(pilot){ pilotcache_delentity(&modsrv->pilots, pilot->uname, ent); }

    //Remove entity from sweep-and-prune
    sweep_rem(&modsrv->sweep, ent);

    //Remove entity from the ECS
    ecs_entity_rem(ecs, ent);
}

void modsrv_spawn(ModServer* modsrv)
{
    ECS* ecs = &modsrv->world.ecs;
    ECS* spawner = &modsrv->spawner;

    //Serialize spawned entities from one ECS into the other
    Query q; query_alloc(&q);
    ArchID arch = {0};
    arch = archid_setbit(arch, TypeID);
    arch = archid_setbit(arch, PositionID);
    query_init(&q, spawner, arch);
    while(query_next(&q)){
        Entity* ents = query_ents(spawner, &q);
        for(int i = 0; i < q.size; ++i){
            
            //Attempt to add entity
            if(ecs->entnext >= ecs->entsmax){ continue; }
            Entity ent = ecs_entity_add(ecs);

            //Serialize & deserialize, change entity ID midway
            uint8_t chunk[ENTITY_MEMORY_MAX];
            ecs_serialize_entity(spawner, ents[i], SERIAL_FULL, chunk);
            serialized_entity_id_edit(chunk, ent);
            ecs_deserialize_entity(ecs, SERIAL_FULL, chunk);

            //Add entity to sweep-and-prune
            sweep_add(&modsrv->sweep, ecs, ent);
        }
    }
    query_dealloc(&q);

    //Re-initialize the spawner, if it contains any entities
    if(spawner->ents.size > 0){
		ecs_init(spawner, spawner->entsmax, COMPO_COUNT, COMPO_SIZE);
    }
}

void modsrv_despawn(ModServer* modsrv)
{
    //Delete entities waiting for deletion
    rtcp_uvec_t* entdelq = &modsrv->entdelq;
    for(int i = 0; i < entdelq->size; ++i){
        wrap_entity_rem(modsrv, *(Entity*)rtcp_uvec_get(entdelq, i));
    }
    rtcp_uvec_init(entdelq);
    rtcp_uvec_tick(entdelq);
}
