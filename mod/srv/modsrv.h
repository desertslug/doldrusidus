#ifndef MODSRV_H
#define MODSRV_H

#include "config.h"
#include "world.h"
#include "random.h"
#include "sweep.h"
#include "uvec.h"

typedef struct ModServer
{
    void* server;
    World world;
    rtcp_hmap_t pilots;
    Hash1 rand;
    Sweep sweep;
    ECS spawner;
    rtcp_uvec_t entdelq;
} ModServer;

void modserver_start(ModServer* modsrv, void* server);
void modserver_stop(ModServer* modsrv);
void modserver_step(ModServer* modsrv, struct timespec delta);

Entity wrap_entity_add(ModServer* modsrv, Type type, uint32_t spawn[3]);
void wrap_entity_rem(ModServer* modsrv, Entity ent);

void modsrv_spawn(ModServer* modsrv);
void modsrv_despawn(ModServer* modsrv);

#endif /* MODSRV_H */