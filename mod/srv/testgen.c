#include "testgen.h"

void test_generate(ModServer* modsrv, char* str)
{
	int curr = 0; int next = 0;

	//Attempt to parse a name
	curr = next; next += strcspn(&str[curr], WHITESPACE);
	if(curr == next){ return; }
	const char* name = &str[curr]; str[next++] = '\0';
	curr = next; next += strspn(&str[curr], WHITESPACE);

	//Generate test world
	if(strcmp(name, "pilot-entity-sync-range") == 0){

		pilot_entity_sync_range_generate(modsrv, 0x8badf00d);

	} //else
	if(strcmp(name, "signal-entity-sync") == 0){

		signal_entity_sync_generate(modsrv, 0x8badf00d);

	} //else
	if(strcmp(name, "porpoise-explore") == 0){

		porpoise_explore_generate(modsrv, 0x8badf00d);

	} //else
}

Entity test_generate_piloted_ship(ModServer* modsrv, uint32_t spawn[3], char* uname)
{
    World* world = &modsrv->world;
    rtcp_hmap_t* pilots = &modsrv->pilots;

	//Ship
	Entity ship = ship_generate(modsrv, spawn);

	//Add pilot component
	Pilot* pilot = ecs_get_mut_id(&world->ecs, ship, PilotID);
	strcpy(pilot->uname, uname);

	//Cache piloted ship
	pilotcache_addentity(pilots, pilot->uname, ship);

	return ship;
}

bool growth_bonus_set_max(ECS* ecs, Entity ent)
{
	//Entity must have growth module
	Growth* growth = (Growth*)ecs_get_id(ecs, ent, GrowthID);
	if(!growth){ return false; }

	//Add growth bonus to the exact maximum
	uint16_t remain = growth->bonus_max - growth->bonus_acc;
	growth_bonus_add(growth, remain);

	return true;
}

bool entity_growth_add(ECS* ecs, Entity ent, CompoID id, int count)
{
	//Entity must have growth module
	Growth* growth = (Growth*)ecs_get_id(ecs, ent, GrowthID);
	if(!growth){ return false; }

	//Add growth for the specified component
	//without checking whether it could be added
	for(int i = 0; i < count; ++i){
		growth_add(growth, id, ecs, ent);
	}

	//Update modules
	entity_update_compos(ecs, ent);

	return true;
}

void pilot_entity_sync_range_generate(ModServer* modsrv, uint32_t seed)
{
	ECS* ecs = &modsrv->world.ecs;

    //Initialize pseudorandom number generator
    Hash1 hash; hash.buf[0] = seed; hash.last = seed;

    //Origin
    uint32_t origin[3] = { 0x80000000, 0x80000000, 0x80000000 };

	//A series of piloted ships
	uint32_t shipsep = 0x80000000 / SRV_ENT_OWNED_MAX;
	uint32_t ships = SRV_ENT_OWNED_MAX;
	for(int i = 0; i < ships; ++i){

		uint32_t pos[3] = {
			origin[0] + 0 * shipsep,
			shipsep / 2 + i * shipsep * 2,
			origin[2],
		};
		Entity ship = test_generate_piloted_ship(modsrv, pos, "root");

		//Grow nav module further
		growth_bonus_set_max(ecs, ship);
		entity_growth_add(ecs, ship, NavID, i);

		//A series of stars
		uint32_t starsep = 0x01000000;
		uint32_t stars = 0x80000000 / starsep;
		for(int j = 0; j < stars; ++j){
			uint32_t pos2[3] = {
				pos[0] - starsep * (j + 1), pos[1], pos[2],
			};
			star_generate(modsrv, &hash, pos2);
		}
	}
}

void signal_entity_sync_generate(ModServer* modsrv, uint32_t seed)
{
	ECS* ecs = &modsrv->world.ecs;

    //Initialize pseudorandom number generator
    Hash1 hash; hash.buf[0] = seed; hash.last = seed;

    //Origin
    uint32_t origin[3] = { 0x80000000, 0x80000000, 0x80000000 };

	uint32_t err = 0x1 << 4;

	//A piloted ship with long nav range
    uint32_t pos0[3] = { origin[0], origin[1], origin[2] };
	Entity ship0 = test_generate_piloted_ship(modsrv, pos0, "guest");
			
	//Grow nav module further
	growth_bonus_set_max(ecs, ship0);
	entity_growth_add(ecs, ship0, NavID, 10);

	//A piloted ship with long nav range
    uint32_t pos1[3] = {
		pos0[0] + (NAV_MAX_RANGE << 16) * 2 - err, pos0[1], pos0[2]
	};
	Entity ship1 = test_generate_piloted_ship(modsrv, pos1, "guest");
			
	//Grow nav module further
	growth_bonus_set_max(ecs, ship1);
	entity_growth_add(ecs, ship1, NavID, 10);

	//A piloted ship with short nav range
	uint32_t pos2[3] = {
		pos1[0] + (NAV_CAPA_CONF.base << 16) * 2 - err, pos1[1], pos1[2],
	};
	Entity ship2 = test_generate_piloted_ship(modsrv, pos2, "guest");

	//Stars
    uint32_t stars = 128;
    uint32_t radius = NAV_MAX_RANGE - err;
    for(int i = 0; i < stars; ++i){
        uint32_t pos[3];
        pos_around_pos_in_radius(&hash, origin, radius << 16, pos);
        star_generate(modsrv, &hash, pos);
    }

	(void)ship2;
}

void porpoise_explore_generate(ModServer* modsrv, uint32_t seed)
{
	ECS* ecs = &modsrv->world.ecs;
	ECS* spawner = &modsrv->spawner;

    //Initialize pseudorandom number generator
    Hash1 hash; hash.buf[0] = seed; hash.last = seed;

	//Initialize romu trio
	uint32_t romu_trio32[3] = { seed, seed, seed };

    //Origin
    uint32_t origin[3] = { 0x80000000, 0x80000000, 0x80000000 };

	uint32_t err = 0x10 << 4;

	//A piloted ship with long nav range
    uint32_t pos0[3] = { origin[0], origin[1], origin[2] };
	Entity ship0 = test_generate_piloted_ship(modsrv, pos0, "guest");

	//Navigation range of the ship
	Nav* nav = (Nav*)ecs_get_id(ecs, ship0, NavID);
	if(!nav){ return; }
	uint32_t offset = (nav_range(nav) + 0x200) << 16;

	//A wasp
	uint32_t pos1[3] = { pos0[0] - offset - err, pos0[1], pos0[2] };
	Entity wasp0 = wasp_generate(spawner, pos1, 0, 0, romu_trio32);

	(void)hash;
	(void)ship0;
	(void)wasp0;
}
