#ifndef RANDOM_H
#define RANDOM_H

#include "cglm/util.h"

#include "romu.h"
#include "ivec.h"

typedef struct Hash1
{
    uint32_t buf[1];
    uint32_t last;
} Hash1;

void hash_step(Hash1* hash1);

uint32_t hash_next(Hash1* hash1);

float hash_next_norm(Hash1* hash1);

void pos_around_pos_in_radius(Hash1* hash, uint32_t in[3], uint32_t radius, uint32_t out[3]);

void pos_around_pos_in_kernel(Hash1* hash, uint32_t in[3], uint32_t rad_min, uint32_t rad_max, uint32_t out[3]);

float romu_trio32_norm(uint32_t romustt[3]);

uint32_t romu_trio32_range(uint32_t romustt[3], uint32_t min, uint32_t max);

/*void romu_pos_around_pos_in_radius(uint32_t romustt[3], uint32_t in[3], uint32_t radius, uint32_t out[3]);*/

void romu_trio32_pos_around_pos_in_kernel(uint32_t romustt[3], uint32_t in[3], uint32_t rad_min, uint32_t rad_max, uint32_t out[3]);

#endif /* RANDOM_H */
