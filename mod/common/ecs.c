#include "ecs.h"

bool archid_eq(struct ArchID lhs, struct ArchID rhs)
{
    return lhs.mask == rhs.mask;
}

struct ArchID archid_and(struct ArchID lhs, struct ArchID rhs)
{
    return (struct ArchID){ .mask = lhs.mask & rhs.mask };
}

struct ArchID archid_or(struct ArchID lhs, struct ArchID rhs)
{
    return (struct ArchID){ .mask = lhs.mask | rhs.mask };
}

struct ArchID archid_setbit(struct ArchID lhs, uint8_t bit)
{
    return (struct ArchID){ .mask = lhs.mask | 0x1 << bit };
}

struct ArchID archid_unsetbit(struct ArchID lhs, uint8_t bit)
{
    return (struct ArchID){ .mask = lhs.mask & ~(0x1 << bit) };
}

uint8_t archid_getbit(struct ArchID lhs, uint8_t bit)
{
    return lhs.mask >> bit & 0x1;
}

uint8_t archid_bits(struct ArchID lhs)
{
    uint32_t x = lhs.mask;
    int n = 0; while(x > 0){ ++n; x >>= 1; }
    return n;
}

uint8_t archid_ones(struct ArchID lhs)
{
    uint32_t x = lhs.mask;
    int n = 0; while(x > 0){ if(x & 0x1){ ++n; } x >>= 1; }
    return n;
}

void archetype_alloc(Archetype* arch, ArchID aid, rtcp_uvec_t* sizes)
{
    //Allocate
    rtcp_uvec_malloc(&arch->ents, 1, sizeof(Entity));
    rtcp_uvec_malloc(&arch->cols, 1, sizeof(rtcp_uvec_t));

    //Initialize values
    arch->aid = aid;
    rtcp_uvec_init(&arch->ents);
    rtcp_uvec_init(&arch->cols);

    //Create a column for every component the archetype has
    uint8_t bits = archid_bits(arch->aid);
    for(int i = 0; i < bits; ++i){
        if(archid_getbit(arch->aid, i)){

            //Push column
            rtcp_uvec_t col;
	    uint32_t* size = rtcp_uvec_get(sizes, i);
            rtcp_uvec_malloc(&col, 1, *size);
            rtcp_uvec_init(&col);
            rtcp_uvec_add(&arch->cols, &col);
        }
    }
}

void archetype_dealloc(void* archetype)
{
    Archetype* arch = (Archetype*)archetype;

    //Deallocate
    for(int i = 0; i < arch->cols.size; ++i){
        rtcp_uvec_free(rtcp_uvec_get(&arch->cols, i));
    }
    rtcp_uvec_free(&arch->cols);
    rtcp_uvec_free(&arch->ents);
}

void archetype_tick(Archetype* arch)
{
    //Tick the lazily shrinking data structures of the archetype
    rtcp_uvec_tick(&arch->ents);
    for(int i = 0; i < arch->cols.size; ++i){
        rtcp_uvec_tick(rtcp_uvec_get(&arch->cols, i));
    }
}

void archetype_pushrow(Archetype* arch, Entity ent)
{
    rtcp_uvec_add(&arch->ents, &ent);
    for(int i = 0; i < arch->cols.size; ++i){
        rtcp_uvec_add(rtcp_uvec_get(&arch->cols, i), NULL);
    }
}

void archetype_delrow(Archetype* arch, uint32_t row, ECS* ecs)
{
    assert_or_exit(row >= 0 && row < arch->ents.size,
        "archetype_delrow: invalid row");

    //Update the row index of the moved entity
    if(arch->ents.size > 1){

        //ID of the entity that will be moved into the deleted row
        Entity* ent = rtcp_uvec_get(&arch->ents, arch->ents.size - 1);

        //Row record of the moved entity
        Record* rec = rtcp_hmap_get(&ecs->ents, ent);

        //Update row record of the moved entity to point to the deleted row
        rec->row = row;
    }
    
    rtcp_uvec_del(&arch->ents, row);
    for(int i = 0; i < arch->cols.size; ++i){
        rtcp_uvec_del(rtcp_uvec_get(&arch->cols, i), row);
    }
}

void query_alloc(Query* query)
{
    rtcp_uvec_malloc(&query->archetypes, 1, sizeof(ArchID));
}

void query_dealloc(Query* query)
{
    rtcp_uvec_free(&query->archetypes);
}

void query_init_elem(void* elem, void* user)
{
    Archetype* arch = (Archetype*)elem;
    Query* query = (Query*)user;

    //If the archetype contains all queried components, push result
    if(archid_eq(query->arch, archid_and(query->arch, arch->aid))){
        rtcp_uvec_add(&query->archetypes, &arch->aid);
    }
}

void query_init(Query* query, ECS* ecs, ArchID aid)
{
    //Initialize values
    query->size = 0;

    //Store values
    query->arch = aid;

    //Index points to just before the zeroeth result
    query->idx = -1;

    //Collect list of ArchIDs that fit the queried one
    rtcp_uvec_init(&query->archetypes);
    rtcp_hmap_iter_user(&ecs->arch, query_init_elem, query);
}

bool query_next(Query* query)
{
    //Increment current archetype if it's within bounds
    if(query->idx < query->archetypes.size){ ++query->idx; }

    //If the last archetype was reached, return early
    if(query->idx >= query->archetypes.size){ return false; }
    
    return true;
}

void* query_term(ECS* ecs, Query* query, int compo)
{
    //Get the current archetype in the query
    ArchID* aid = rtcp_uvec_get(&query->archetypes, query->idx);
    Archetype* arch = rtcp_hmap_get(&ecs->arch, aid);

    //Get the component entry & the component's place in the archetype
    rtcp_hmap_t* cidx = rtcp_uvec_get(&ecs->cmps, compo);
    uint32_t* place = rtcp_hmap_get(cidx, aid);

    //Get the column of the archetype where the component is located
    rtcp_uvec_t* col = rtcp_uvec_get(&arch->cols, *place);

    //Store the result count in the query
    query->size = col->size;

    return col->values;
}

Entity* query_ents(ECS* ecs, Query* query)
{
    //Get the current archetype in the query
    ArchID* aid = rtcp_uvec_get(&query->archetypes, query->idx);
    Archetype* arch = rtcp_hmap_get(&ecs->arch, aid);

    //Store the result count in the query
    query->size = arch->ents.size;

    return arch->ents.values;
}

void ecs_alloc(ECS* ecs)
{
    rtcp_uvec_malloc(&ecs->size, 1, sizeof(uint32_t));
    rtcp_uvec_init(&ecs->size);
    rtcp_hmap_malloc(&ecs->arch, 1, sizeof(ArchID), sizeof(Archetype));
    rtcp_hmap_init(&ecs->arch);
    rtcp_hmap_malloc(&ecs->ents, 1, sizeof(Entity), sizeof(Record));
    rtcp_hmap_init(&ecs->ents);
    rtcp_uvec_malloc(&ecs->cmps, 1, sizeof(rtcp_hmap_t));
    rtcp_uvec_init(&ecs->cmps);
}

void ecs_dealloc(ECS* ecs)
{
    rtcp_uvec_free(&ecs->size);
    rtcp_hmap_iter(&ecs->arch, archetype_dealloc);
    rtcp_hmap_free(&ecs->arch);
    rtcp_hmap_free(&ecs->ents);
    for(int i = 0; i < ecs->cmps.size; ++i){
        rtcp_hmap_free(rtcp_uvec_get(&ecs->cmps, i));
    }
    rtcp_uvec_free(&ecs->cmps);
}

void ecs_tick(ECS* ecs)
{
    //Tick the lazily shrinkking data structures of the ecs
    rtcp_hmap_iter(&ecs->arch, (void(*)(void*))archetype_tick);
    rtcp_hmap_tick(&ecs->arch);
    rtcp_hmap_tick(&ecs->ents);
    for(int i = 0; i < ecs->cmps.size; ++i){
        rtcp_hmap_tick(rtcp_uvec_get(&ecs->cmps, i));
    }
}

void ecs_init(ECS* ecs, uint32_t entsmax, uint32_t compos, uint32_t* sizes)
{
    //Store parameters
    ecs->entsmax = entsmax;

    //Initialize values
    ecs->entnext = 0;

    //Reallocate ECS
    ecs_dealloc(ecs);
    ecs_alloc(ecs);

    //Initialize component sizes
    for(int i = 0; i < compos; ++i){
	rtcp_uvec_add(&ecs->size, &sizes[i]);
    }

    //Initialize component index maps
    for(int i = 0; i < COMPO_COUNT; ++i){
        rtcp_hmap_t place;
        rtcp_hmap_malloc(&place, 1, sizeof(ArchID), sizeof(uint32_t));
        rtcp_hmap_init(&place);
        rtcp_uvec_add(&ecs->cmps, &place);
    }

    //Assert that the component sizes are registered
    for(int i = 0; i < ecs->size.size; ++i){
	uint32_t* size = rtcp_uvec_get(&ecs->size, i);
        assert_or_exit(*size > 0, "ecs_init: empty component\n");
    }

    //Add empty, default archetype
    ecs_archetype_add(ecs, (ArchID){0});
}

void ecs_archetype_add(ECS* ecs, ArchID aid)
{
    //Callee should ensure the archetype does not exist
    assert_or_exit(!rtcp_hmap_get(&ecs->arch, &aid),
        "ecs_archetype_add: archetype already exists\n");

    //Add archetype
    Archetype arch; archetype_alloc(&arch, aid, &ecs->size);
    rtcp_hmap_add(&ecs->arch, &aid, &arch);

    //Add new component locations that are present in the archetype
    uint32_t col = 0;
    uint8_t bits = archid_bits(aid);
    for(uint8_t i = 0; i < bits; ++i){ if(archid_getbit(aid, i)){

        //Find component index entry
        rtcp_hmap_t* cidx = rtcp_uvec_get(&ecs->cmps, i);

        //Add the column index of the component in the archetype
        rtcp_hmap_add(cidx, &aid, &col);

        ++col;
    }}
}

void ecs_archetype_rem(ECS* ecs, ArchID aid)
{
    //Callee should ensure the archetype exists
    assert_or_exit(rtcp_hmap_get(&ecs->arch, &aid),
        "ecs_archetype_add: archetype does not exist\n");

    //Remove all component places associated with the archetype
    uint8_t bits = archid_bits(aid);
    for(uint8_t i = 0; i < bits; ++i){ if(archid_getbit(aid, i)){

        //Find & remove existing component entry
        rtcp_hmap_t* cidx = rtcp_uvec_get(&ecs->cmps, i);
        rtcp_hmap_del(cidx, &aid);
    }}

    //Remove archetype
    archetype_dealloc(rtcp_hmap_get(&ecs->arch, &aid));
    rtcp_hmap_del(&ecs->arch, &aid);
}

bool ecs_entity_alive(ECS* ecs, Entity ent)
{
    return rtcp_hmap_get(&ecs->ents, &ent) != NULL;
}

Entity ecs_entity_add(ECS* ecs)
{
    //Callee should ensure maximum entity count isn't reached
    assert_or_exit(ecs->ents.size < ecs->entsmax,
        "ecs_entity_add: entity count reached max\n");

    Entity ent = ecs->entnext;

    //Set the entity to be of the default, empty archetype
    Record rec = { .aid = (ArchID){0}, };
    rtcp_hmap_add(&ecs->ents, &ent, &rec);

    //Find the next lowest unused entity ID
    do { ecs->entnext++; }
    while(rtcp_hmap_get(&ecs->ents, &ecs->entnext));

    //Add the entity to the empty archetype
    Archetype* empty = rtcp_hmap_get(&ecs->arch, &rec.aid);
    //assert(empty);
    archetype_pushrow(empty, ent);

    return ent;
}

void ecs_entity_rem(ECS* ecs, Entity ent)
{
    //Callee should ensure the entity ID to be deleted is valid
    assert_or_exit(ecs_entity_alive(ecs, ent),
        "ecs_entity_rem: entity does not exist\n");

    //Change the entity's archetype back to the default
    Record* rec = rtcp_hmap_get(&ecs->ents, &ent);
    ecs_delta_id(ecs, ent, rec->aid, (ArchID){0});

    //Get the new archetype of the entity (which should be empty)
    Archetype* arch = rtcp_hmap_get(&ecs->arch, &rec->aid);
    assert_or_exit(archid_ones(arch->aid) == 0,
        "ecs_entity_rem: entity archetype did not become empty");

    //Remove the entity from its archetype (which should be empty)
    archetype_delrow(arch, rec->row, ecs);

    //Remove entity entry
    rtcp_hmap_del(&ecs->ents, &ent);

    //Swap next lowest unused entity ID if the deleted is lower
    if(ent < ecs->entnext){ ecs->entnext = ent; }
}

void ecs_add_id(ECS* ecs, Entity ent, int id)
{
    //Callee must ensure the entity exists
    assert_or_exit(ecs_entity_alive(ecs, ent),
        "ecs_add_id: entity does not exist\n");

    //Find entity record
    Record* rec = rtcp_hmap_get(&ecs->ents, &ent);

    //Compute new archetype
    ArchID newaid = archid_setbit(rec->aid, id);

    //If the archetypes match, return early
    if(archid_eq(rec->aid, newaid)){ return; }

    //Handle the entity record moving between the old and new archetypes
    ecs_delta_id(ecs, ent, rec->aid, newaid);
}

void ecs_add_ids(ECS* ecs, Entity ent, ArchID ids)
{
    //Callee must ensure the entity exists
    assert_or_exit(ecs_entity_alive(ecs, ent),
        "ecs_add_id: entity does not exist\n");

    //Find entity record
    Record* rec = rtcp_hmap_get(&ecs->ents, &ent);

    //Compute new archetype
    ArchID newaid = archid_or(rec->aid, ids);

    //If the archetypes match, return early
    if(archid_eq(rec->aid, newaid)){ return; }

    //Handle the entity record moving between the old and new archetypes
    ecs_delta_id(ecs, ent, rec->aid, newaid);
}

void ecs_rem_id(ECS* ecs, Entity ent, int id)
{
    //Callee must ensure the entity exists
    assert_or_exit(ecs_entity_alive(ecs, ent),
        "ecs_rem_id: entity does not exist\n");

    //Find entity record
    Record* rec = rtcp_hmap_get(&ecs->ents, &ent);

    //Compute new archetype
    ArchID newaid = archid_unsetbit(rec->aid, id);

    //If the archetypes match, return early
    if(archid_eq(rec->aid, newaid)){ return; }

    //Handle the entity record moving between the old and new archetypes
    ecs_delta_id(ecs, ent, rec->aid, newaid);
}

void ecs_delta_id(ECS* ecs, Entity ent, ArchID oldaid, ArchID newaid)
{
    //Add the new archetype if it does not exist yet
    if(!rtcp_hmap_get(&ecs->arch, &newaid)){
        ecs_archetype_add(ecs, newaid);
    }

    //Old & new archetype storages
    Archetype* oldarch = rtcp_hmap_get(&ecs->arch, &oldaid);
    Archetype* newarch = rtcp_hmap_get(&ecs->arch, &newaid);

    //Entity record
    Record* rec = rtcp_hmap_get(&ecs->ents, &ent);

    //Old & new row positions
    size_t oldrow = rec->row; size_t newrow = newarch->ents.size;

    //Push row to new archetype
    archetype_pushrow(newarch, ent);

    //Handle new components
    uint8_t bits = archid_bits(newaid);
    for(uint8_t i = 0; i < bits; ++i){
        
        //If component is present in new archetype
        if(archid_getbit(newaid, i)){

            //Get the place of the component in the new & old archetype
            rtcp_hmap_t* cidx = rtcp_uvec_get(&ecs->cmps, i);
            uint32_t* newplace = rtcp_hmap_get(cidx, &newaid);

            //New component storage
            rtcp_uvec_t* newcol = rtcp_uvec_get(&newarch->cols, *newplace);
            uint8_t* newdat = rtcp_uvec_get(newcol, newrow);

	    uint32_t* size = rtcp_uvec_get(&ecs->size, i);

            //Copy existing component data in the case of an overlap
            if(archid_getbit(oldaid, i)){

                //Old component storage
                uint32_t* oldplace = rtcp_hmap_get(cidx, &oldaid);
                rtcp_uvec_t* oldcol = rtcp_uvec_get(&oldarch->cols, *oldplace);
                uint8_t* olddat = rtcp_uvec_get(oldcol, oldrow);
                memcpy(newdat, olddat, *size);
            }
            //Initialize component otherwise
            else {

                //Zero out entire component
                memset(newdat, 0, *size);
            }
        }
    }

    //Delete row from old archetype
    archetype_delrow(oldarch, oldrow, ecs);

    //Remove unused non-default archetype entry
    if(archid_ones(oldaid) > 0 && oldarch->ents.size == 0){
        ecs_archetype_rem(ecs, oldaid);
    }

    //Update record
    *rec = (Record){ .aid = newaid, .row = newrow, };
}

void* ecs_get_mut_id(ECS* ecs, Entity ent, int id)
{
    //Callee must ensure the entity exists
    assert_or_exit(ecs_entity_alive(ecs, ent),
        "ecs_rem_id: entity does not exist\n");

    //Add component to entity if it does not already have it
    Record* rec = rtcp_hmap_get(&ecs->ents, &ent);
    if(!archid_getbit(rec->aid, id)){ ecs_add_id(ecs, ent, id); }

    return ecs_get_id_existing(ecs, rec, id);
}

const void* ecs_get_id(ECS* ecs, Entity ent, int id)
{
    //Callee must ensure the entity exists
    assert_or_exit(ecs_entity_alive(ecs, ent),
        "ecs_get_id: entity does not exist\n");

    //Return NULL if the entity does not have the component
    Record* rec = rtcp_hmap_get(&ecs->ents, &ent);
    if(!archid_getbit(rec->aid, id)){ return NULL; }

    return ecs_get_id_existing(ecs, rec, id);
}

void* ecs_get_id_existing(ECS* ecs, Record* rec, int id)
{
    //Get archetype
    Archetype* arch = rtcp_hmap_get(&ecs->arch, &rec->aid);

    //Get the component entry & the component's place in the archetype
    rtcp_hmap_t* cidx = rtcp_uvec_get(&ecs->cmps, id);
    assert_or_exit(cidx, "ecs_get_id_existing: cidx was NULL\n");
    uint32_t* place = rtcp_hmap_get(cidx, &rec->aid);

    //Get the column of the archetype where the component is located
    rtcp_uvec_t* col = rtcp_uvec_get(&arch->cols, *place);

    return rtcp_uvec_get(col, rec->row);
}

void hmap_print(rtcp_hmap_t* hmap)
{
    fprintf(LOGFILE, "[ ");
    for(int i = 0; i < hmap->capacity; ++i){
        if(hmap->records[i] == RTCP_HMAP_OCC){
            fprintf(LOGFILE, "%d : key %x ", i, hmap->keys[i * hmap->keysize]);
        }
    }
    fprintf(LOGFILE, "]\n");
}

