#include "shader.h"

void shaderinfo_init(ShaderInfo* shinfo, int type, const char* path)
{
    assert_or_exit(strlen(path) < SHADER_STR_MAX, "path to shader too long\n");

    shinfo->type = type;
    strcpy(shinfo->path, path);
}

int shaders_compile_program(ShaderInfo* shaders, int count, GLuint* program)
{
    int ret = 1;

    //GL handles for the shaders and the program
    GLuint* shandles = malloc(sizeof(GLuint) * count);
    GLuint phandle = 0;

    //Compile shaders
    int s = 0;
    while(s < count){

        //Open the shader file
        FILE* sfile = fopen(shaders[s].path, "r");
        if(!sfile){
            LOG_ERROR_("could not open shader under path: \"%s\"", shaders[s].path);
            goto shaders_compile_program_cleanup;
        }

        //File contents
        char* contents = NULL;

        //Find the length of the file
        fseek(sfile, 0L, SEEK_END);
        int length = ftell(sfile);
        fseek(sfile, 0L, SEEK_SET);

        //Allocate contents, and one character for the null terminator
        contents = malloc(sizeof(char) * length + 1);

        //Read the contents of the shader
        if(fread(contents, sizeof(char), length, sfile) != length){
            LOG_ERROR_("could not read shader contents");
            LOG_("strerror(errno): %s", strerror(errno));
            free(contents);
            goto shaders_compile_program_cleanup;
        }
        contents[length] = '\0';

        //Create shader
        shandles[s] = glCreateShader(shaders[s].type);
        if(!shandles[s]){
            LOG_ERROR_("glCreateShader() returned zero");
            free(contents);
            goto shaders_compile_program_cleanup;
        }

        //Insert the source code into the shader object & attempt compilation
        glShaderSource(shandles[s], 1, (const GLchar* const*)&contents, NULL);
        glCompileShader(shandles[s]);

        //Check compilation status
        GLint compstate;
        glGetShaderiv(shandles[s], GL_COMPILE_STATUS, &compstate);
        if(!compstate){
            LOG_ERROR_("failed to compile shader under path: \"%s\"", shaders[s].path);

            char log[4096];
            glGetShaderInfoLog(shandles[s], 4096, NULL, log);
            fprintf(LOGFILE, "%s\n", log);

            free(contents);
            goto shaders_compile_program_cleanup;
        }

        ++s;
    }

    //If all shaders compiled successfully
    if(s == count){

        //Create program
        phandle = glCreateProgram();
        if(!phandle){
            LOG_ERROR_("glCreateProgram() returned 0");
            goto shaders_compile_program_cleanup;
        }

        //Attach shaders (supposes all parameters are correct)
        for(int i = 0; i < s; ++i){ glAttachShader(phandle, shandles[i]); }

        //Attemp to link program, check result
        glLinkProgram(phandle);
        GLint linkstate;
        glGetProgramiv(phandle, GL_LINK_STATUS, &linkstate);
        if(!linkstate){
            LOG_ERROR_("failed to compile program");

            char log[4096];
            glGetProgramInfoLog(phandle, 4096, NULL, log);
            fprintf(LOGFILE, "%s\n", log);
            goto shaders_compile_program_cleanup;
        }

        //Detach shaders, as the program has them now
        for(int i = 0; i < s; ++i){ glDetachShader(phandle, shandles[i]); }
    }


    ret = 0;
    *program = phandle;
shaders_compile_program_cleanup:
    for(int i = 0; i < s; ++i){ glDeleteShader(shandles[i]); }
    if(shandles){ free(shandles); }
    fflush(LOGFILE); // in case a graphics-related crash occurs later
    return ret;
}
