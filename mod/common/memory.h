#ifndef MEMORY_H
#define MEMORY_H

#include <math.h>
#include <assert.h>

#include "cglm/vec3.h"

#include "fasthash.h"
#include "romu.h"

#include "defs.h"
#include "macros.h"
#include "ivec.h"
#include "uvec.h"
#include "world.h"
#include "compo.h"
#include "session.h"
#include "random.h"
#include "sweep.h"
#include "artifact.h"

enum {
    RUIN_NONE = 0,
    RUIN_SPIRES,
    RUIN_GLASS,
    RUIN_SCORCHED,
    RUIN_ASHES,
    RUIN_DIRIGIBLES,
    RUIN_DERELICTS,
    RUIN_BRIDGES,
    RUIN_RADIOS,
    RUIN_LAST,
};
enum { RUIN_COUNT = RUIN_LAST - 1, };
const char* RUIN_DESC[RUIN_LAST];

void timer_decrease(float* timer, float delta);

typedef enum Modality
{
    NormalMode = 0,
    ControllerMode,
    ModalityCount,
} Modality;

typedef struct ModSession
{
    uint32_t pos[3];
    uint32_t radius;
    Modality modality;
    Entity focusent;
    Entity resolvent;
    Entity syncent;
    Buffer ievt;
    //Buffer ievt; //TODO move elsewhere if possible
    uint8_t delta;
} ModSession;

void modsession_init(ModSession* session);
void modsession_sent(ModSession* session);

uint32_t modsession_serialize(ModSession* modsess, SerialStyle style, uint8_t* buf);
uint32_t modsession_deserialize(ModSession* modsess, SerialStyle style, uint8_t* buf);

int capacity_config_total(CapacityConfig conf, int count);

int syncents_add_upto(const SyncEnts* syncents, ECS* ecs, uint16_t* ents, int entsmax);
uint32_t syncents_serialize_to_signal(SyncEnts* syncents, uint8_t* buf);
uint32_t syncents_deserialize_from_signal(SyncEnts* syncents, uint8_t* buf);

uint16_t nav_range(const Nav* nav);

void nav_set(Nav* nav, uint16_t motion, const uint32_t target[3], uint32_t distance);

void nav_step(Nav* nav, Position* pos, Pointphys* pphys, float maxvel, float maxacc, float throttle);

void nav_deltav_between_pos_vel(uint32_t pos1[3], int32_t vel1[3], uint32_t pos2[3], int32_t vel2[3], float maxvel, float maxacc, int32_t deltavel[3]);

void pos_pphys_predict_impact(const Position* pos1, const Pointphys* pphys1, float maxvel, const Position* pos2, const Pointphys* pphys2, float delta, uint32_t pos[3]);

void vat_cells_add(Vat* vat, cells_t cells[TROPH_COUNT]);

void vat_cells_remove(Vat* vat, cells_t cells[TROPH_COUNT]);

void synth_mat_add(Synth* synth, SynthMats mat, mats_t count);

void synth_mat_del(Synth* synth, SynthMats mat, mats_t count);

bool synth_mat_fit(Synth* synth, SynthMats mat, mats_t count);

bool synth_mat_available(Synth* synth, SynthMats mat, mats_t count);

void synth_mats_add(Synth* synth, const mats_t mats[SYNTH_MAT_COUNT]);

void synth_mats_del(Synth* synth, const mats_t mats[SYNTH_MAT_COUNT]);

bool synth_mats_fit(Synth* synth, const mats_t mats[SYNTH_MAT_COUNT]);

bool synth_mats_available(Synth* synth, const mats_t mats[SYNTH_MAT_COUNT]);

void hull_damage(Hull* hull, int hit);

void hull_repair(Hull* hull, int hit);

bool hull_destroyed(const Hull* hull);

bool hull_undamaged(const Hull* hull);

void bay_restock(Bay* bay, int ammo);

bool bay_full(Bay* bay);

bool growth_add(Growth* growth, CompoID id, ECS* ecs, Entity ent);
bool growth_del(Growth* growth, CompoID id, ECS* ecs, Entity ent);
void growth_bonus_add(Growth* growth, uint16_t points);
int artifact_points(Position* pos1, Position* pos2);

void stepper_init(Stepper* stepper, float frequency);

void stepper_stagger(Stepper* stepper, float frequency, float rand_norm);

int stepper_step(Stepper* stepper, float delta);

void trophs_initialize(Trophs* trophs, uint8_t width, float params[TROPH_PARAM_COUNT], float params_rand_range[TROPH_PARAM_COUNT], uint32_t seed[3], float weather_mul);

void trophs_count(Trophs* trophs);

void trophs_inject(Trophs* trophs, cells_t counts[TROPH_COUNT], cells_t injected_out[TROPH_COUNT]);

void trophs_sample(Trophs* trophs, cells_t counts[TROPH_COUNT], cells_t sampled_out[TROPH_COUNT]);

int trophs_change_cell(Trophs* trophs, TrophCell source, TrophCell target, int count);

void trophs_step(Trophs* trophs, float delta);

void trophs_convert_rgba(const Trophs* trophs, uint8_t* dest, vec4 colors[4]);

void maze_initialize(Maze* maze, Hash1* hash, int width, uint32_t seed[3]);
void maze_reset(Maze* maze);

void maze_push(Maze* maze, uint8_t pos[2]);
void maze_pop(Maze* maze, uint8_t pos[2]);
void maze_set(Maze* maze, uint8_t* dest, uint8_t pos[2], uint8_t byte);

void maze_generate(Maze* maze, int steps);

void maze_cell_neigh_type_get(Maze* maze, uint8_t* src, uint8_t x, uint8_t y, MazeCell type, uint8_t out[4][2], int* count);

void hive_add_barren(Hive* hive, uint16_t barren, ECS* ecs);

void wasp_send_journey(Wasp* wasp, Entity hive, Entity barren, uint32_t seed[3]);

void nav_reset(void* compo, ECS* ecs, Entity ent);

void nav_update(void* compo, ECS* ecs, Entity ent);

void drive_reset(void* compo, ECS* ecs, Entity ent);

void drive_update(void* compo, ECS* ecs, Entity ent);

void vat_reset(void* compo, ECS* ecs, Entity ent);

void vat_update(void* compo, ECS* ecs, Entity ent);

void extract_reset(void* compo, ECS* ecs, Entity ent);

void synth_reset(void* compo, ECS* ecs, Entity ent);

void synth_update(void* compo, ECS* ecs, Entity ent);

void compute_reset(void* compo, ECS* ecs, Entity ent);

void compute_update(void* compo, ECS* ecs, Entity ent);

void hull_reset(void* compo, ECS* ecs, Entity ent);

void hull_update(void* compo, ECS* ecs, Entity ent);

void bay_reset(void* compo, ECS* ecs, Entity ent);

void bay_update(void* compo, ECS* ecs, Entity ent);

void cargo_reset(void* compo, ECS* ecs, Entity ent);

void cargo_update(void* compo, ECS* ecs, Entity ent);

void growth_reset(void* compo, ECS* ecs, Entity ent);

void growth_update(void* compo, ECS* ecs, Entity ent);

Entity wasp_generate(ECS* ecs, uint32_t spawn[3], Entity hive, Entity barren, uint32_t seed[3]);

Entity missile_generate(ECS* ecs, uint32_t spawn[3], int32_t vel[3], uint16_t target, Type sender);

Entity wreckage_generate(ECS* ecs, ECS* ecs_orig, Entity orig, uint32_t spawn[3], Type* type, Nav* nav);

//WARNING: UPKEEP
void entity_count_compos(ECS* ecs, Entity ent, uint8_t counts[COMPO_COUNT]);

//WARNING: UPKEEP
void entity_reset_compos(ECS* ecs, Entity ent);

//WARNING: UPKEEP
void entity_update_compos(ECS* ecs, Entity ent);

bool check_aggro(ECS* ecs, Entity ent, Entity ent2);

#endif /* MEMORY_H */
