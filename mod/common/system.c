#include "system.h"

void system_uxn_run(World* world, Query* q, Sweep* sweep)
{
    ArchID arch = {0};
    arch = archid_setbit(arch, UxnID);
    query_init(q, &world->ecs, arch);
    while(query_next(q)){
        Entity* ents = query_ents(&world->ecs, q);
        Uxn* uxn = query_term(&world->ecs, q, UxnID);
        for(int i = 0; i < q->size; ++i){
            
            uxn_run(&uxn[i], world, ents[i], sweep);
        }
    }
}

void system_uxn_screen_device_push(World* world, Query* q)
{
    ArchID arch = {0};
    arch = archid_setbit(arch, UxnID);
    query_init(q, &world->ecs, arch);
    while(query_next(q)){
        Uxn* uxn = query_term(&world->ecs, q, UxnID);
        for(int i = 0; i < q->size; ++i){
            
            //If the machine is busy, push no event
            if(!uxn[i].brk){ continue; }

            //Push frame/tick event through the screen device
            buffer_push(&uxn[i].ievt,
                (Event){ UXN_DEV_SCRN + UXN_PORT_SCRN_EMPTY, 0 });
        }
    }
}

void system_uxn_compute_sync(World* world, Query* q)
{
    float delta = timespec_float(&world->delta);

    ArchID arch = {0};
    arch = archid_setbit(arch, UxnID);
    arch = archid_setbit(arch, ComputeID);
    query_init(q, &world->ecs, arch);
    while(query_next(q)){
        Uxn* uxn = query_term(&world->ecs, q, UxnID);
        Compute* com = query_term(&world->ecs, q, ComputeID);
        for(int i = 0; i < q->size; ++i){

            //Compute modules can generate as many instructions:
            //as they generate by default, plus what they've received
            uxn[i].inspt = com[i].received + roundf(com[i].frequency * delta);

            //Store what was received for debug purposes, reset received
            com[i].received_pre = com[i].received;
            com[i].received = 0;
        }
    }
}

void system_pos_pphys_integrate(World* world, Query* q)
{
    float delta = timespec_float(&world->delta);

    ArchID arch = {0};
    arch = archid_setbit(arch, PositionID);
    arch = archid_setbit(arch, PointphysID);
    query_init(q, &world->ecs, arch);
    while(query_next(q)){
        Position* pos = query_term(&world->ecs, q, PositionID);
        Pointphys* pphys = query_term(&world->ecs, q, PointphysID);
        for(int i = 0; i < q->size; ++i){
            
            for(int j = 0; j < 3; ++j){

                //Integrate position by velocity
                pos[i].pos[j] += abs(pphys[i].vel[j]) > VEL_MIN ?
                    delta * pphys[i].vel[j] : 0;

                //Integrate velocity by acceleration
                pphys[i].vel[j] += delta * pphys[i].acc[j];
            }
        }
    }
}

void system_pphys_reset(World* world, Query* q)
{
    ArchID arch = {0};
    arch = archid_setbit(arch, PointphysID);
    query_init(q, &world->ecs, arch);
    while(query_next(q)){
        Pointphys* pphys = query_term(&world->ecs, q, PointphysID);
        for(int i = 0; i < q->size; ++i){
            
            for(int j = 0; j < 3; ++j){

		//Reset acceleration
                pphys[i].acc[j] = 0;
            }
        }
    }
}

void system_nav_drive_pos_pphys(World* world, Query* q)
{
    ArchID arch = {0};
    arch = archid_setbit(arch, NavID);
    arch = archid_setbit(arch, DriveID);
    arch = archid_setbit(arch, PositionID);
    arch = archid_setbit(arch, PointphysID);
    query_init(q, &world->ecs, arch);
    while(query_next(q)){
        Nav* nav = query_term(&world->ecs, q, NavID);
        Drive* drive = query_term(&world->ecs, q, DriveID);
        Position* pos = query_term(&world->ecs, q, PositionID);
        Pointphys* pphys = query_term(&world->ecs, q, PointphysID);
        for(int i = 0; i < q->size; ++i){
            
            if(drive[i].throttle){
                nav_step(&nav[i], &pos[i], &pphys[i],
                    drive[i].maxvel, drive[i].maxacc,
                    drive[i].throttle / (float)0xff);
            }
        }
    }
}

void system_trophs(World* world, Query* q)
{
    float delta = timespec_float(&world->delta);

    ArchID arch = {0};
    arch = archid_setbit(arch, TrophsID);
    query_init(q, &world->ecs, arch);
    while(query_next(q)){
        Trophs* trophs = query_term(&world->ecs, q, TrophsID);
        for(int i = 0; i < q->size; ++i){

            //Step stepper and get step count
            int steps = stepper_step(&trophs[i].stepper, delta);
            for(int j = 0; j < steps; ++j){
                trophs_step(&trophs[i], trophs[i].stepper.wavelength);
            }
        }
    }
}

void system_trophs_habitable_accumulate(World* world, Query* q)
{
    float delta = timespec_float(&world->delta);

    ArchID arch = {0};
    arch = archid_setbit(arch, TrophsID);
    arch = archid_setbit(arch, HabitableID);
    query_init(q, &world->ecs, arch);
    while(query_next(q)){
        Trophs* trophs = query_term(&world->ecs, q, TrophsID);
        Habitable* habits = query_term(&world->ecs, q, HabitableID);
        for(int i = 0; i < q->size; ++i){

            //Calculate how close to the ideal count each cell type is
            float ideal[TROPH_COUNT];
            for(int j = 0; j < TROPH_COUNT; ++j){
                ideal[j] = TROPH_RATIO[j] == 0.0f ? 1.0f :
                    trophs[i].cellcount[j] /
                    (float)(trophs[i].width * trophs[i].width);
            }

            //Calculate overall productivity
            float productivity = 1.0f;
            for(int i = 0; i < TROPH_COUNT; ++i){ productivity *= ideal[i]; }

            //Accumulate up to the limit
            uint16_t new_acc = habits[i].accumulator +
                delta * productivity *
                (trophs[i].width * trophs[i].width);
            habits[i].accumulator = MIN(new_acc, HABITABLE_ACC_LIMIT);

            //If the accumulator reached its threshold, and the output has not
            if(
                habits[i].accumulator >= HABITABLE_ACC_LIMIT &&
                habits[i].output < HABITABLE_OUT_LIMIT
            ){
                //Convert one step of accumulated to output
                habits[i].accumulator -= HABITABLE_ACC_LIMIT;
                ++habits[i].output;
            }
        }
    }
}

void system_monolith_maze(World* world, Query* q)
{
    ArchID arch = {0};
    arch = archid_setbit(arch, MonolithID);
    arch = archid_setbit(arch, MazeID);
    query_init(q, &world->ecs, arch);
    while(query_next(q)){
        Monolith* monol = query_term(&world->ecs, q, MonolithID); 
        Maze* maze = query_term(&world->ecs, q, MazeID); 
        for(int i = 0; i < q->size; ++i){

            //Step count is how many times the accumulator exceed the threshold
            int stepcount = monol[i].accumulator / monol[i].threshold;

            //Run maze generation for the designated number of steps
            if(stepcount > 0 && maze[i].stacksize > 0){
                maze_generate(&maze[i], stepcount);

                //Subtract spent steps
                monol[i].accumulator -= stepcount * monol[i].threshold;
            }
        }
    }
}

void system_pos_wormhole(World* world, Query* q, Sweep* sweep)
{
    ArchID arch = {0};
    arch = archid_setbit(arch, PositionID);
    arch = archid_setbit(arch, WormholeID);
    query_init(q, &world->ecs, arch);
    while(query_next(q)){
        Position* pos = query_term(&world->ecs, q, PositionID);
        Wormhole* worm = query_term(&world->ecs, q, WormholeID);
        for(int i = 0; i < q->size; ++i){

            //Find bounding box
            uint32_t bbox[3][2];
            v3uint32_bbox(pos[i].pos, worm[i].radius, bbox);

            //Get entities inside that bounding box
            Entity ents[WORMHOLE_SENSOR_ENTS];
            int found = sweep_bbox_upto(sweep, bbox,
                romu_trio32_random(worm[i].romu_trio32), ents, WORMHOLE_SENSOR_ENTS);

    for(int j = 0; j < found; ++j){

        Position* pos2 = (Position*)ecs_get_id(&world->ecs, ents[j], PositionID);
        if(!pos2){ continue; }
        Nav* nav2 = (Nav*)ecs_get_id(&world->ecs, ents[j], NavID);
        if(!nav2){ continue; }

        //Get vector towards entity
        int32_t toward[3];
        v3uint32_diff(pos2->pos, pos[i].pos, toward);

        //Check that the entity is within the radius
        if(v3int32_norm(toward) < worm[i].radius){
            
            //Override navigation to approach the wormhole
            nav_set(nav2, NAV_MOTION_APPROACH, pos[i].pos, 0);
        }

        //Check that the entity is within the horizon
        if(v3int32_norm(toward) < worm[i].radius * WORMHOLE_HORIZON){

            //Place entity at the exit distance
            v3int32_scale_as(toward, worm[i].radius*(-WORMHOLE_EXIT), toward);
            v3uint32_add(worm[i].target, toward, pos2->pos);

            //Reset navigation to stop
            nav2->motion = NAV_MOTION_RELATIVE;
            for(int i = 0; i < 3; ++i){
                nav2->target[i] = UXN_CONT_AXIS_CTR * TWOPOW16;
            }
        }
    }
        }
    }
}

void system_pos_hive_synth(World* world, Query* q, ECS* spawner)
{
    float delta = timespec_float(&world->delta);

    ArchID arch = {0};
    arch = archid_setbit(arch, PositionID);
    arch = archid_setbit(arch, HiveID);
    arch = archid_setbit(arch, SynthID);
    query_init(q, &world->ecs, arch);
    while(query_next(q)){
        Entity* ents = query_ents(&world->ecs, q);
        Position* pos = query_term(&world->ecs, q, PositionID);
        Hive* hive = query_term(&world->ecs, q, HiveID);
        Synth* synth = query_term(&world->ecs, q, SynthID);
        for(int i = 0; i < q->size; ++i){
            
    //Handle all guarded entities
    for(int j = 0; j < hive[i].guarded; ++j){

        //Increment timer
        hive[i].cooldowns[j] += delta;
        
        //If the timer has exceeded the launch delay,
        //and the materials to spawn a wasp are available
        if(
            hive[i].cooldowns[j] > hive[i].delays[j] &&
            synth_mats_available(&synth[i], WASP_MAT_COST)
        ){
            //Spawn position
            Position* pos2 =
                ecs_get_mut_id(&world->ecs, hive[i].barrens[j], PositionID);
            int32_t toward[3]; v3uint32_diff(pos2->pos, pos[i].pos, toward);
            v3int32_scale_as(toward, HIVE_WASP_UNITS * 2, toward);
            uint32_t spawn[3]; v3uint32_add(pos[i].pos, toward, spawn);

            //Spawn wasp
            romu_trio32_random(hive[i].romu_trio32);
            Entity ent2 = wasp_generate(spawner,
                spawn, ents[i], hive[i].barrens[j], hive[i].romu_trio32);

            //Set navigation
            Nav* nav = ecs_get_mut_id(spawner, ent2, NavID);
            nav_set(nav, NAV_MOTION_ORBIT, pos2->pos, WASP_GUARD_UNITS);

            //Reset cooldown
            hive[i].cooldowns[j] = 0.0f;

            //Subtract wasp cost from hive materials
            synth_mats_del(&synth[i], WASP_MAT_COST);
        }
    }
        }
    }
}

void system_pos_extract_synth(World* world, Query* q)
{
    float delta = timespec_float(&world->delta);

    ArchID arch = {0};
    arch = archid_setbit(arch, PositionID);
    arch = archid_setbit(arch, ExtractID);
    arch = archid_setbit(arch, SynthID);
    query_init(q, &world->ecs, arch);
    while(query_next(q)){
        Position* pos = query_term(&world->ecs, q, PositionID);
        Extract* ext = query_term(&world->ecs, q, ExtractID);
        Synth* synth = query_term(&world->ecs, q, SynthID);
        for(int i = 0; i < q->size; ++i){

    switch(ext[i].state){
    case EXTRACT_STT_IDLE: {} break;
    case EXTRACT_STT_COOL: {

        //COOL -> IDLE
        if(ext[i].timer == 0.0f){ ext[i].state = EXTRACT_STT_IDLE; break; }

        //COOL -> COOL

        //Decrement cooldown timer
        timer_decrease(&ext[i].timer, delta);

    } break;
    case EXTRACT_STT_PULL: {

        //REPAIR -> IDLE
        if(!ecs_entity_alive(&world->ecs, ext[i].target))
        { ext[i].state = EXTRACT_STT_IDLE; break; }

        //REPAIR -> IDLE
        const Barren* barr = ecs_get_id(&world->ecs, ext[i].target, BarrenID);
        if(!barr){ ext[i].state = EXTRACT_STT_IDLE; break; }

        //REPAIR -> IDLE
        const Position* pos2 = ecs_get_id(&world->ecs, ext[i].target, PositionID);
        if(v3uint32_dist(pos[i].pos, pos2->pos) > BARREN_INTERACTION_UNITS)
        { ext[i].state = EXTRACT_STT_IDLE; break; }

        //Extracted materials
        mats_t extracted[SYNTH_MAT_COUNT] = {
            [SYNTH_MAT_INORGANIC] = MIN(
                1,
                synth[i].mats_max[SYNTH_MAT_INORGANIC] -
                synth[i].mats[SYNTH_MAT_INORGANIC]
            )
        };

        //Check that the materials fit into the capacity
        if(!synth_mats_fit(&synth[i], extracted))
        { ext[i].state = EXTRACT_STT_IDLE; break; }

        //Add materials
        synth_mats_add(&synth[i], extracted);

        //Calculate cooldown time from richness
        float cooldown = EXTRACT_PULL_TIME / barr->richness;

        ext[i].timer = cooldown;
        ext[i].state = EXTRACT_STT_COOL;

    } break;
    default: { assert_or_exit(false, "system_pos_extract_synth\n"); }
    }
        }
    }
}

void system_pos_synth(World* world, Query* q)
{
    float delta = timespec_float(&world->delta);
    ECS* ecs = &world->ecs;

    ArchID arch = {0};
    arch = archid_setbit(arch, PositionID);
    arch = archid_setbit(arch, SynthID);
    query_init(q, &world->ecs, arch);
    while(query_next(q)){
        Position* pos = query_term(&world->ecs, q, PositionID);
        Synth* synth = query_term(&world->ecs, q, SynthID);
        for(int i = 0; i < q->size; ++i){

    switch(synth[i].state){
    case SYNTH_STT_IDLE: {} break;
    case SYNTH_STT_COOL: {

        //COOL -> IDLE
        if(synth[i].timer == 0.0f){ synth[i].state = SYNTH_STT_IDLE; break; }

        //COOL -> COOL

        timer_decrease(&synth[i].timer, delta);

    } break;
    case SYNTH_STT_REPAIR: {

        //REPAIR -> IDLE
        if(!ecs_entity_alive(ecs, synth[i].target))
        { synth[i].state = SYNTH_STT_IDLE; break; }

        //REPAIR -> IDLE
        Position* pos2 = (Position*)ecs_get_id(ecs, synth[i].target, PositionID);
        if(!pos2){ synth[i].state = SYNTH_STT_IDLE; break; }

        //REPAIR -> IDLE
        Hull* hull2 = (Hull*)ecs_get_id(ecs, synth[i].target, HullID);
        if(!hull2){ synth[i].state = SYNTH_STT_IDLE; break; }

        //REPAIR -> IDLE
        if(hull_undamaged(hull2)){ synth[i].state = SYNTH_STT_IDLE; break; }

        //REPAIR -> IDLE
        if(!synth_mats_available(&synth[i], SYNTH_REPAIR_MAT_COST))
        { synth[i].state = SYNTH_STT_IDLE; break; }

        //REPAIR -> IDLE
        if(v3uint32_dist(pos[i].pos, pos2->pos) > SYNTH_INTERACTION_UNITS)
        { synth[i].state = SYNTH_STT_IDLE; break; }

        //REPAIR -> COOL

        //Repair hull
        hull_repair(hull2, SYNTH_REPAIR_HIT);

        //Subtract materials
        synth_mats_del(&synth[i], SYNTH_REPAIR_MAT_COST);

        //Speed penalty upon repairing self
        /*float timemult = (ents[i] == synth[i].target) ?
            SYNTH_SELF_REPAIR_MULT : 1.0f;*/

        synth[i].state = SYNTH_STT_COOL;
        synth[i].timer = SYNTH_REPAIR_TIME;

    } break;
    case SYNTH_STT_RESTOCK: {

        //REPAIR -> IDLE
        if(!ecs_entity_alive(ecs, synth[i].target))
        { synth[i].state = SYNTH_STT_IDLE; break; }

        //REPAIR -> IDLE
        Position* pos2 = (Position*)ecs_get_id(ecs, synth[i].target, PositionID);
        if(!pos2){ synth[i].state = SYNTH_STT_IDLE; break; }

        //REPAIR -> IDLE
        Bay* bay2 = (Bay*)ecs_get_id(ecs, synth[i].target, BayID);
        if(!bay2){ synth[i].state = SYNTH_STT_IDLE; break; }

        //REPAIR -> IDLE
        if(bay_full(bay2)){ synth[i].state = SYNTH_STT_IDLE; break; }

        //REPAIR -> IDLE
        if(!synth_mats_available(&synth[i], SYNTH_RESTOCK_MAT_COST))
        { synth[i].state = SYNTH_STT_IDLE; break; }

        //REPAIR -> IDLE
        if(v3uint32_dist(pos[i].pos, pos2->pos) > SYNTH_INTERACTION_UNITS)
        { synth[i].state = SYNTH_STT_IDLE; break; }

        //RESTOCK -> COOL

        //Restock bay
        bay_restock(bay2, SYNTH_RESTOCK_AMMO);

        //Subtract materials
        synth_mats_del(&synth[i], SYNTH_RESTOCK_MAT_COST);

        synth[i].state = SYNTH_STT_COOL;
        synth[i].timer = SYNTH_RESTOCK_TIME;

    } break;
    default: { assert_or_exit(false, "system_pos_synth_hull_bay\n"); }
    }
        }
    }
}

void system_pos_pphys_bay(World* world, Query* q, ECS* spawner)
{
    float delta = timespec_float(&world->delta);

    ArchID arch = {0};
    arch = archid_setbit(arch, TypeID);
    arch = archid_setbit(arch, PositionID);
    arch = archid_setbit(arch, PointphysID);
    arch = archid_setbit(arch, BayID);
    query_init(q, &world->ecs, arch);
    while(query_next(q)){
        Type* type = query_term(&world->ecs, q, TypeID);
        Position* pos = query_term(&world->ecs, q, PositionID);
        Pointphys* pphys = query_term(&world->ecs, q, PointphysID);
        Bay* bay = query_term(&world->ecs, q, BayID);
        for(int i = 0; i < q->size; ++i){

    switch(bay[i].state){
    case BAY_STT_IDLE: {} break;
    case BAY_STT_FIRE: {

        //FIRE -> IDLE
        if(bay[i].ammo == 0){ bay[i].state = BAY_STT_IDLE; break; }

        //FIRE -> COOL
        bay[i].ammo -= 1;
        bay[i].timer = BAY_LAUNCH_TIME;
        bay[i].state = BAY_STT_COOL;

        missile_generate(spawner,
            pos[i].pos, pphys[i].vel, bay[i].target, type[i]);
    
    } break;
    case BAY_STT_COOL: {

        //COOL -> IDLE
        if(bay[i].timer == 0.0f){ bay[i].state = BAY_STT_IDLE; break; }

        //COOL -> COOL

        timer_decrease(&bay[i].timer, delta);
    
    } break;
    default: { assert_or_exit(false, "system_pos_pphys_bay\n"); }
    }
        }
    }
}

void system_type_pos_pphys_nav_synth_hull_wasp(World* world, Query* q, Sweep* sweep, ECS* spawner, rtcp_uvec_t* entdelq)
{
    float delta = timespec_float(&world->delta);

    ArchID arch = {0};
    arch = archid_setbit(arch, TypeID);
    arch = archid_setbit(arch, PositionID);
    arch = archid_setbit(arch, PointphysID);
    arch = archid_setbit(arch, WaspID);
    arch = archid_setbit(arch, NavID);
    arch = archid_setbit(arch, SynthID);
    arch = archid_setbit(arch, HullID);
    query_init(q, &world->ecs, arch);
    while(query_next(q)){
        Entity* ents = query_ents(&world->ecs, q);
        Type* type = query_term(&world->ecs, q, TypeID);
        Position* pos = query_term(&world->ecs, q, PositionID);
        Pointphys* pphys = query_term(&world->ecs, q, PointphysID);
        Wasp* wasp = query_term(&world->ecs, q, WaspID);
        Nav* nav = query_term(&world->ecs, q, NavID);
        Synth* synth = query_term(&world->ecs, q, SynthID);
        Hull* hull = query_term(&world->ecs, q, HullID);
        for(int i = 0; i < q->size; ++i){

    switch(wasp[i].state){
    case WASP_STT_JOURNEY:
    case WASP_STT_GUARD_BARREN:
    case WASP_STT_RETURN: {

        //If the hull is destroyed
        if(hull_destroyed(&hull[i])){

            //Delete and set state to death
            rtcp_uvec_add(entdelq, &ents[i]);
            wasp[i].state = WASP_STT_DEATH;
            break;
        }

        //Decrease ranged timer
        timer_decrease(&wasp[i].ranged, delta);

        //Find entities in range
        uint16_t ents2[WASP_SENSOR_ENTS];
        int found = sweep_pos_radius_upto(sweep,
            pos[i].pos, WASP_SENSOR_UNITS,
            romu_trio32_random(wasp[i].romu_trio32),
            ents2, WASP_SENSOR_ENTS);

        for(int j = 0; j < found; ++j){
            if(
                check_aggro(&world->ecs, ents[i], ents2[j]) &&
                wasp[i].ranged == 0.0f
            ){
                //Spawn missile
                missile_generate(spawner,
                    pos[i].pos, pphys[i].vel, ents2[j], type[i]);
                //(void)pphys; (void)type;

                //Reset ranged cooldown
                wasp[i].ranged = WASP_RANGED_TIME;
                break;
            }
        }

    } break;
    default: {}
    }

    //State machine
    switch(wasp[i].state){
    case WASP_STT_DEATH: {} break;
    case WASP_STT_JOURNEY: {

        //JOURNEY -> GUARD_BARREN
        if(v3uint32_dist(pos[i].pos, nav[i].target) < WASP_GUARD_UNITS * 2){
            wasp[i].timer = WASP_VISIT_SEC;
            wasp[i].state = WASP_STT_GUARD_BARREN;
        }

        //JOURNEY -> JOURNEY

    } break;
    case WASP_STT_GUARD_BARREN: {

        timer_decrease(&wasp[i].timer, delta);

        //GUARD_BARREN -> RETURN
        if(wasp[i].timer == 0.0f){

            //Add mat capacity to mats
            mats_t add = MIN(
                synth[i].mats_max[SYNTH_MAT_INORGANIC],
                synth[i].mats_max[SYNTH_MAT_INORGANIC] -
                synth[i].mats[SYNTH_MAT_INORGANIC]
            );
            synth_mat_add(&synth[i], SYNTH_MAT_INORGANIC, add);

            //Set navigation to approach the hive
            const Position* pos2 = ecs_get_id(&world->ecs, wasp[i].hive, PositionID);
            nav_set(&nav[i], NAV_MOTION_APPROACH, pos2->pos, HIVE_WASP_UNITS * 0.8f);

            wasp[i].state = WASP_STT_RETURN;
        }

        //GUARD_BARREN -> GUARD_BARREN

    } break;
    case WASP_STT_RETURN: {

        //RETURN -> DEATH
        if(v3uint32_dist(pos[i].pos, nav[i].target) < HIVE_WASP_UNITS){

            Synth* synth2 = ecs_get_mut_id(&world->ecs, wasp[i].hive, SynthID);

            //If the materials fit into the hive, add them
            if(synth_mats_fit(synth2, synth[i].mats)){
                synth_mats_add(synth2, synth[i].mats);
            }

            //Delete wasp
            rtcp_uvec_add(entdelq, &ents[i]);
            wasp[i].state = WASP_STT_DEATH;
            break;
        }

        //RETURN -> RETURN

    } break;
    default: { assert_or_exit(false, "system_pos_pphys_nav_synth_hull_wasp\n"); }
    }
        }
    }
}

void system_pos_pphys_nav_drive_hull_leviathan(World* world, Query* q, Sweep* sweep, rtcp_uvec_t* entdelq)
{
    float delta = timespec_float(&world->delta);

    ArchID arch = {0};
    arch = archid_setbit(arch, PositionID);
    arch = archid_setbit(arch, PointphysID);
    arch = archid_setbit(arch, NavID);
    arch = archid_setbit(arch, DriveID);
    arch = archid_setbit(arch, HullID);
    arch = archid_setbit(arch, LeviathanID);
    query_init(q, &world->ecs, arch);
    while(query_next(q)){
        Entity* ents = query_ents(&world->ecs, q);
        Position* pos = query_term(&world->ecs, q, PositionID);
        Pointphys* pphys = query_term(&world->ecs, q, PointphysID);
        Nav* nav = query_term(&world->ecs, q, NavID);
        Drive* drive = query_term(&world->ecs, q, DriveID);
        Hull* hull = query_term(&world->ecs, q, HullID);
        Leviathan* levia = query_term(&world->ecs, q, LeviathanID);
        for(int i = 0; i < q->size; ++i){

    //Death
    switch(levia[i].state){
    case LEVIA_STT_WANDER:
    case LEVIA_STT_ESCAPE:
    case LEVIA_STT_AGGRO: {

        //If the hull is destroyed
        if(hull_destroyed(&hull[i])){

            drive[i].throttle = 0x40;
            levia[i].timer = LEVIA_DORMANT_TIME;
            levia[i].state = LEVIA_STT_DORMANT;
        }

    } break;
    default: {}
    }

    //Escape
    switch(levia[i].state){
    case LEVIA_STT_DORMANT:
    case LEVIA_STT_WANDER:
    case LEVIA_STT_AGGRO: {

        //Find entities in range
        uint16_t ents2[LEVIA_SENSOR_ENTS];
        int found = sweep_pos_radius_upto(sweep,
            pos[i].pos, LEVIA_SENSOR_UNITS,
            romu_trio32_random(levia[i].romu_trio32),
            ents2, LEVIA_SENSOR_ENTS);

        for(int j = 0; j < found; ++j){

            const Type* type = ecs_get_id(&world->ecs, ents2[j], TypeID);
            if(type->type != ENTTYPE_STAR){ continue; }

            const Position* pos2 = ecs_get_id(&world->ecs, ents2[j], PositionID);
            if(v3uint32_dist(pos[i].pos, pos2->pos) < LEVIA_SENSOR_UNITS){

                //Choose position to escape to
                int32_t diff[3]; v3uint32_diff(pos[i].pos, pos2->pos, diff);
                v3int32_scale_as(diff, LEVIA_SENSOR_UNITS * 2, diff);
                uint32_t to[3]; v3uint32_add(pos2->pos, diff, to);

                nav_set(&nav[i], NAV_MOTION_APPROACH, to, 0);

                levia[i].state = LEVIA_STT_ESCAPE;
            }
        }

    } break;
    default: {}
    }

    //State machine
    switch(levia[i].state){
    case LEVIA_STT_DORMANT: {

        //DORMANT -> WANDER
        if(levia[i].timer == 0.0f){

            hull_update(&hull[i], &world->ecs, ents[i]);
            drive[i].throttle = 0xff;

            levia[i].state = LEVIA_STT_WANDER;
        }

        //DORMANT -> DORMANT

        timer_decrease(&levia[i].timer, delta);

        //If the target is reached, choose a new position to wander to
        if(v3uint32_dist(pos[i].pos, nav[i].target) < LEVIA_WANDER_CLOSE_UNITS){
            uint32_t pos2[3];
            romu_trio32_pos_around_pos_in_kernel(
                levia[i].romu_trio32, pos[i].pos,
                0, LEVIA_WANDER_MAX_UNITS, pos2);
            nav_set(&nav[i], NAV_MOTION_APPROACH, pos2, 0);
        }

    } break;
    case LEVIA_STT_WANDER: {

        //WANDER -> AGGRO
        {
            //Find entities in range
            uint16_t ents2[LEVIA_SENSOR_ENTS];
            int found = sweep_pos_radius_upto(sweep,
                pos[i].pos, LEVIA_SENSOR_UNITS,
                romu_trio32_random(levia[i].romu_trio32),
                ents2, LEVIA_SENSOR_ENTS);
            
            for(int j = 0; j < found; ++j){
                if(check_aggro(&world->ecs, ents[i], ents2[j])){
                    const Position* pos2 = ecs_get_id(
                        &world->ecs, ents2[j], PositionID);
                    if(v3uint32_dist(pos[i].pos, pos2->pos) < LEVIA_SENSOR_UNITS){
                        levia[i].target = ents2[j];
                        levia[i].timer = LEVIA_MELEE_TIME;
                        levia[i].state = LEVIA_STT_AGGRO;
                        break;
                    }
                }
            }
        }

        //WANDER -> WANDER

        //If the target is reached, choose a new position to wander to
        if(v3uint32_dist(pos[i].pos, nav[i].target) < LEVIA_WANDER_CLOSE_UNITS){
            uint32_t pos2[3];
            romu_trio32_pos_around_pos_in_kernel(
                levia[i].romu_trio32, pos[i].pos,
                0, LEVIA_WANDER_MAX_UNITS, pos2);
            nav_set(&nav[i], NAV_MOTION_APPROACH, pos2, 0);
        }

    } break;
    case LEVIA_STT_ESCAPE: {
        
        //ESCAPE -> WANDER
        if(v3uint32_dist(pos[i].pos, nav[i].target) < LEVIA_WANDER_CLOSE_UNITS){
            levia[i].state = LEVIA_STT_WANDER;
        }
        
    } break;
    case LEVIA_STT_AGGRO: {

        //AGGRO -> WANDER
        if(!check_aggro(&world->ecs, ents[i], levia[i].target))
        { levia[i].state = LEVIA_STT_WANDER; break; }

        const Position* pos2 =
            ecs_get_id(&world->ecs, levia[i].target, PositionID);
        const Pointphys* pphys2 =
            ecs_get_id(&world->ecs, levia[i].target, PointphysID);

        assert_or_exit(!pos2 || pphys2,
            "LEVIA_STT_AGGRO: target has pos but no pphys");

        //AGGRO -> WANDER
        if(pos2 && v3uint32_dist(pos[i].pos, pos2->pos) > LEVIA_SENSOR_UNITS)
        { levia[i].state = LEVIA_STT_WANDER; break; }

        //AGGRO -> AGGRO

        //Impact target entity
        if(pos2 && pphys2){
            uint32_t pred[3];
            pos_pphys_predict_impact(&pos[i], &pphys[i], drive[i].maxvel,
                pos2, pphys2, delta, pred);

            nav_set(&nav[i], NAV_MOTION_IMPACT, pred, 0);
        }

        timer_decrease(&levia[i].timer, delta);

        //If melee cooldown is complete & target is within range
        if(
            levia[i].timer == 0.0f &&
            v3uint32_dist(pos[i].pos, pos2->pos) < LEVIA_MELEE_UNITS
        ){
            //Damage the hull of the target
            Hull* hull = (Hull*)ecs_get_id(&world->ecs, levia[i].target, HullID);
            assert_or_exit(hull, "LEVIA_STT_AGGRO: hull was NULL\n");
            hull_damage(hull, LEVIA_MELEE_DMG);

            levia[i].timer = LEVIA_MELEE_TIME;
        }

    } break;
    default: { assert_or_exit(false, "system_pos_pphys_nav_drive_leviathan"); }
    }
        }
    }
}

void system_pos_pphys_nav_drive_hull_missile(World* world, Query* q, rtcp_uvec_t* entdelq)
{
    float delta = timespec_float(&world->delta);

    ArchID arch = {0};
    arch = archid_setbit(arch, PositionID);
    arch = archid_setbit(arch, PointphysID);
    arch = archid_setbit(arch, NavID);
    arch = archid_setbit(arch, DriveID);
    arch = archid_setbit(arch, HullID);
    arch = archid_setbit(arch, MissileID);
    query_init(q, &world->ecs, arch);
    while(query_next(q)){
        Entity* ents = query_ents(&world->ecs, q);
        Position* pos = query_term(&world->ecs, q, PositionID);
        Pointphys* pphys = query_term(&world->ecs, q, PointphysID);
        Nav* nav = query_term(&world->ecs, q, NavID);
        Drive* drive = query_term(&world->ecs, q, DriveID);
        Hull* hull = query_term(&world->ecs, q, HullID);
        Missile* miss = query_term(&world->ecs, q, MissileID);
        for(int i = 0; i < q->size; ++i){

#define MISSILE_DEATH() \
rtcp_uvec_add(entdelq, &ents[i]); \
miss[i].state = MISS_STT_DEATH

    switch(miss[i].state){
    case MISS_STT_DEATH: {} break;
    case MISS_STT_FLY: {

        //If the hull is destroyed
        if(hull_destroyed(&hull[i])){ MISSILE_DEATH(); break; }

        //FLY -> DEATH
        if(miss[i].timer == 0.0f){ MISSILE_DEATH(); break; }

        //FLY -> DEATH
        if(!check_aggro(&world->ecs, ents[i], miss[i].target))
        { MISSILE_DEATH(); break; }

        const Position* pos2 =
            ecs_get_id(&world->ecs, miss[i].target, PositionID);
        const Pointphys* pphys2 =
            ecs_get_id(&world->ecs, miss[i].target, PointphysID);

        assert_or_exit(!pos2 || pphys2,
            "MISS_STT_FLY: target has pos but no pphys");

        //FLY -> DEATH
        if(pos2 && v3uint32_dist(pos[i].pos, pos2->pos) > MISS_SENSOR_UNITS)
        { MISSILE_DEATH(); break; }

        //FLY -> DEATH
        if(pos2 && v3uint32_dist(pos[i].pos, pos2->pos) < MISS_MELEE_UNITS){

            //Damage the hull of the target
            Hull* hull = ecs_get_mut_id(&world->ecs, miss[i].target, HullID);
            hull_damage(hull, MISS_MELEE_DMG);

            //Delete missile
            { MISSILE_DEATH(); break; }
        }

        //Impact target entity
        if(pos2 && pphys2){
            uint32_t pred[3];
            pos_pphys_predict_impact(&pos[i], &pphys[i], drive[i].maxvel,
                pos2, pphys2, delta, pred);

            nav_set(&nav[i], NAV_MOTION_IMPACT, pred, 0);
        }

        timer_decrease(&miss[i].timer, delta);

    } break;
    default: { assert_or_exit(false, "system_pos_pphys_nav_hull_missile\n"); }
    }
        }
    }
}

void system_ship(World* world, Query* q, ECS* spawner, rtcp_uvec_t* entdelq)
{
    //Query a set of components that is unique to ship types
    ArchID arch = {0};
    arch = archid_setbit(arch, TypeID);
    arch = archid_setbit(arch, PositionID);
    arch = archid_setbit(arch, NavID);
    arch = archid_setbit(arch, VatID);
    arch = archid_setbit(arch, SynthID);
    arch = archid_setbit(arch, HullID);
    query_init(q, &world->ecs, arch);
    while(query_next(q)){
        Entity* ents = query_ents(&world->ecs, q);
        Type* type = query_term(&world->ecs, q, TypeID);
        Position* pos = query_term(&world->ecs, q, PositionID);
        Nav* nav = query_term(&world->ecs, q, NavID);
        //Vat* vat = query_term(&world->ecs, q, VatID);
        //Synth* synth = query_term(&world->ecs, q, SynthID);
        Hull* hull = query_term(&world->ecs, q, HullID);
        for(int i = 0; i < q->size; ++i){
            
            //If the hull is destroyed
            if(hull_destroyed(&hull[i])){

                //Spawn wrecakge
                wreckage_generate(spawner, &world->ecs, ents[i], pos[i].pos, &type[i], &nav[i]);

                //Delete entity
                rtcp_uvec_add(entdelq, &ents[i]);
            }
        }
    }
}

void system_wreckage(World* world, Query* q, rtcp_uvec_t* entdelq)
{
    float delta = timespec_float(&world->delta);

    ArchID arch = {0};
    arch = archid_setbit(arch, WreckageID);
    query_init(q, &world->ecs, arch);
    while(query_next(q)){
        Entity* ents = query_ents(&world->ecs, q);
        Wreckage* wreck = query_term(&world->ecs, q, WreckageID);
        for(int i = 0; i < q->size; ++i){

            timer_decrease(&wreck[i].timer, delta);

            if(wreck[i].timer == 0.0f){

                //Delete entity
                rtcp_uvec_add(entdelq, &ents[i]);
            }
        }
    }
}
