#ifndef IVEC_H
#define IVEC_H

float uint32_to_float(const uint32_t pos);
void uint32_to_vec3(const uint32_t* pos, vec3 out);

float int32_to_float(int32_t pos);
void int32_to_vec3(int32_t* pos, vec3 out);
void vec3_to_int32(const vec3 pos, int32_t* out);

void v3uint32_diff(const uint32_t* p1, const uint32_t* p2, int32_t* diff);
void v3int32_diff(const int32_t* p1, const int32_t* p2, int32_t* diff);
float v3uint32_dist(const uint32_t* p1, const uint32_t* p2);

float v3uint32_norm(const uint32_t* p1);
float v3int32_norm(const int32_t* p1);

void v3uint32_add(const uint32_t* a, const int32_t* b, uint32_t* out);
void v3int32_scale_as(int32_t* a, int32_t s, int32_t* out);

float v3int32_normdot(int32_t* a, int32_t* b);

void v3uint32_bbox(const uint32_t* a, const uint32_t radius, uint32_t out[3][2]);
int v3uint32_in_bbox(const uint32_t* a, const uint32_t bbox[3][2]);

#endif /* IVEC_H */
