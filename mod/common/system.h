#ifndef SYSTEM_H
#define SYSTEM_H

#include <math.h>
#include <assert.h>

#include "cglm/vec3.h"

#include "fasthash.h"
#include "romu.h"

#include "defs.h"
#include "macros.h"
#include "ivec.h"
#include "uvec.h"
#include "world.h"
#include "compo.h"
#include "session.h"
#include "random.h"
#include "sweep.h"

void system_uxn_run(World* world, Query* q, Sweep* sweep);

void system_uxn_screen_device_push(World* world, Query* q);

void system_uxn_compute_sync(World* world, Query* q);

void system_pos_pphys_integrate(World* world, Query* q);

void system_pphys_reset(World* world, Query* q);

void system_nav_drive_pos_pphys(World* world, Query* q);

void system_trophs_habitable_accumulate(World* world, Query* q);

void system_trophs(World* world, Query* q);

void system_monolith_maze(World* world, Query* q);

void system_pos_wormhole(World* world, Query* q, Sweep* sweep);

void system_pos_hive_synth(World* world, Query* q, ECS* spawner);

void system_pos_extract_synth(World* world, Query* q);

void system_pos_synth(World* world, Query* q);

void system_pos_pphys_bay(World* world, Query* q, ECS* spawner);

void system_type_pos_pphys_nav_synth_hull_wasp(World* world, Query* q, Sweep* sweep, ECS* spawner, rtcp_uvec_t* entdelq);

void system_pos_pphys_nav_drive_hull_leviathan(World* world, Query* q, Sweep* sweep, rtcp_uvec_t* entdelq);

void system_pos_pphys_nav_drive_hull_missile(World* world, Query* q, rtcp_uvec_t* entdelq);

void system_ship(World* world, Query* q, ECS* spawner, rtcp_uvec_t* entdelq);

void system_wreckage(World* world, Query* q, rtcp_uvec_t* entdelq);

#endif /* SYSTEM_H */
