#ifndef ECS_H
#define ECS_H

#include "uvec.h"
#include "hmap.h"

typedef uint16_t Entity;

typedef struct ArchID
{
    uint32_t mask;
} ArchID;

bool archid_eq(struct ArchID lhs, struct ArchID rhs);
struct ArchID archid_and(struct ArchID lhs, struct ArchID rhs);
struct ArchID archid_or(struct ArchID lhs, struct ArchID rhs);
struct ArchID archid_setbit(struct ArchID lhs, uint8_t bit);
struct ArchID archid_unsetbit(struct ArchID lhs, uint8_t bit);
uint8_t archid_getbit(struct ArchID lhs, uint8_t bit);
uint8_t archid_bits(struct ArchID lhs);
uint8_t archid_ones(struct ArchID lhs);

typedef struct ECS ECS;

typedef struct Archetype
{
    ArchID aid;
    rtcp_uvec_t ents; //stores Entity
    rtcp_uvec_t cols; //stores rtcp_uvec_t,
                      //which stores raw component data
} Archetype;

void archetype_alloc(Archetype* arch, ArchID aid, rtcp_uvec_t* sizes);
void archetype_dealloc(void* archetype);
void archetype_tick(Archetype* arch);
void archetype_pushrow(Archetype* arch, Entity ent);
void archetype_delrow(Archetype* arch, uint32_t row, ECS* ecs);

typedef struct Record
{
    ArchID aid; //ArchetypeID
    uint32_t row;
} Record;

typedef struct Query
{
    int idx;
    int size;
    ArchID arch;
    rtcp_uvec_t archetypes; //stores ArchID
} Query;

void query_alloc(Query* query);
void query_dealloc(Query* query);
void query_init(Query* query, ECS* ecs, ArchID aid);
bool query_next(Query* query);
void* query_term(ECS* ecs, Query* query, int compo);
Entity* query_ents(ECS* ecs, Query* query);

struct ECS
{
    uint32_t entsmax;
    Entity entnext;
    uint32_t* composize;

    rtcp_uvec_t size; //maps int to uint32_t
    rtcp_hmap_t arch; //maps ArchID to Archetype
    rtcp_hmap_t ents; //maps Entity to Record
    rtcp_uvec_t cmps; //maps Component ID to rtcp_hmap_t,
                      //which maps ArchID to uint32_t
};

void ecs_alloc(ECS* ecs);
void ecs_dealloc(ECS* ecs);
void ecs_tick(ECS* ecs);
void ecs_init(ECS* ecs, uint32_t entsmax, uint32_t compos, uint32_t* sizes);

void ecs_archetype_add(ECS* ecs, ArchID aid);
void ecs_archetype_rem(ECS* ecs, ArchID aid);

bool ecs_entity_alive(ECS* ecs, Entity ent);

Entity ecs_entity_add(ECS* ecs);
void ecs_entity_rem(ECS* ecs, Entity ent);
void ecs_add_id(ECS* ecs, Entity ent, int id);
void ecs_add_ids(ECS* ecs, Entity ent, ArchID ids);
void ecs_rem_id(ECS* ecs, Entity ent, int id);
void ecs_delta_id(ECS* ecs, Entity ent, ArchID oldaid, ArchID newaid);

void* ecs_get_mut_id(ECS* ecs, Entity ent, int id);
const void* ecs_get_id(ECS* ecs, Entity ent, int id);
void* ecs_get_id_existing(ECS* ecs, Record* rec, int id);

void ecs_spawn(ECS* ecs, ECS* spawner);

void hmap_print(rtcp_hmap_t* hmap);

#endif /* ECS_H */
