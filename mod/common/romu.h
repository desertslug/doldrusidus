#ifndef ROMU_H
#define ROMU_H

#include <stdint.h>

#define ROTL(d,lrot) ((d<<(lrot)) | (d>>(8*sizeof(d)-(lrot))))

/*
uint64_t romu_quad_random();

uint64_t romu_trio_random();

uint64_t romu_duo_random();

uint64_t romu_duojr_random();

uint32_t romu_quad32();
*/

uint32_t romu_trio32_random(uint32_t state[3]);
/*
void romu_mono32_init(uint32_t seed);

uint16_t romu_mono32_random();
*/
#endif /* ROMU_H */