#ifndef COMPO_H
#define COMPO_H

#include "auth.h"
#include "uxn.h"
#include "ecs.h"

void compress_2bitsperbyte(uint8_t* dest, uint8_t* src, int bytes);

void decompress_2bitsperbyte(uint8_t* dest, uint8_t* src, int bytes);

void convert_rgba_2bitsperbyte(uint8_t* dest, const uint8_t* src, int bytes, float colors[4][4]);

typedef enum SerialStyle
{
    SERIAL_FULL = 0,
    SERIAL_NETW,
    SERIAL_NETW_UNRELIABLE,
} SerialStyle;

int archetype_serialize_rows(Archetype* arch, FILE* file);
uint32_t archetype_serialize_row(Archetype* arch, uint32_t row, SerialStyle style, uint8_t* buf);

int ecs_serialize(ECS* ecs, FILE* file);
int ecs_deserialize(ECS* ecs, FILE* file);

uint32_t ecs_serialize_entity(ECS* ecs, Entity ent, SerialStyle style, uint8_t* buf);
Entity ecs_deserialize_entity(ECS* ecs, SerialStyle style, uint8_t* buf);
void serialized_entity_id_edit(uint8_t* buf, Entity ent);

typedef enum EntityType
{
    ENTTYPE_NONE = 0,
    ENTTYPE_SHIP,
    ENTTYPE_STAR,
    ENTTYPE_OASIS,
    ENTTYPE_HABITABLE,
    ENTTYPE_MONOLITH,
    ENTTYPE_WORMHOLE,
    ENTTYPE_BARREN,
    ENTTYPE_HIVE,
    ENTTYPE_WASP,
    ENTTYPE_LEVIATHAN,
    ENTTYPE_MISSILE,
    ENTTYPE_WRECKAGE,
    ENTTYPE_CIVILIZATION,
    ENTTYPE_RUIN,
    ENTTYPE_COUNT,
} EntityType;

typedef struct Type
{
    uint16_t type;
} Type;

uint32_t type_serialize(void* compo, SerialStyle style, uint8_t* buf);
uint32_t type_deserialize(void* compo, SerialStyle style, uint8_t* buf);

typedef struct Pilot
{
    char uname[UNAME_MAX];
} Pilot;

uint32_t pilot_serialize(void* compo, SerialStyle style, uint8_t* buf);
uint32_t pilot_deserialize(void* compo, SerialStyle style, uint8_t* buf);

uint32_t uxn_stack_serialize(Stack* s, uint8_t* buf);
uint32_t uxn_stack_deserialize(Stack* s, uint8_t* buf);

uint32_t uxn_buffer_serialize(Buffer* b, SerialStyle style, uint8_t* buf);
uint32_t uxn_buffer_deserialize(Buffer* b, SerialStyle style, uint8_t* buf);

uint32_t uxn_serialize(void* compo, SerialStyle style, uint8_t* buf);
uint32_t uxn_deserialize(void* compo, SerialStyle style, uint8_t* buf);

typedef struct Chrono
{
    uint32_t sec;
    uint32_t nsec;
    bool fading;
} Chrono;

typedef struct Position
{
    uint32_t pos[3];
} Position;

uint32_t position_serialize(void* compo, SerialStyle style, uint8_t* buf);
uint32_t position_deserialize(void* compo, SerialStyle style, uint8_t* buf);

//Velocity threshold that must be exceeded for integration to take place.
enum { VEL_MIN = 100, };

//Smallest delta velocity at which navigation will update.
enum { VEL_MINDELTA = 100, };

typedef struct Pointphys
{
    int32_t vel[3];
    int32_t acc[3];
} Pointphys;

uint32_t pointphys_serialize(void* compo, SerialStyle style, uint8_t* buf);
uint32_t pointphys_deserialize(void* compo, SerialStyle style, uint8_t* buf);

typedef struct CapacityConfig
{
    uint16_t base;
    uint16_t more;
    uint16_t bonus;
} CapacityConfig;

enum {
    NAV_MOTION_NONE = 0,
    NAV_MOTION_APPROACH,
    NAV_MOTION_RELATIVE,
    NAV_MOTION_ORBIT,
    NAV_MOTION_IMPACT,
};
enum { NAV_MAX_DIST = 0x4000, };
enum { NAV_MAX_RANGE = 0x2400, };
enum { SIGNAL_BYTES = 16, };

enum {
	NAV_SIG_PROTO_SYNC_ENT = 0xff,
};

const CapacityConfig NAV_CAPA_CONF = { 0x0c00, 0x0400, 0 };

enum { SYNC_ENT_MAX = 5, };
enum { SYNC_ENT_SEC_MAX = 4, };

typedef struct SyncEnts
{
	uint32_t sec;
	Entity ents[SYNC_ENT_MAX];
} SyncEnts;

typedef struct Nav
{
    uint8_t count;
    uint8_t motion;
	uint16_t range;
    uint32_t target[3];
    uint32_t distance;
    uint8_t signal[SIGNAL_BYTES];
	SyncEnts syncents;
} Nav;

uint32_t nav_serialize(void* compo, SerialStyle style, uint8_t* buf);
uint32_t nav_deserialize(void* compo, SerialStyle style, uint8_t* buf);

//What power to raise TWR to upon calculating maximum velocity.
const float POW_TWR = 1.0f;

//Maximum velocity at a TWR of exactly one.
enum { VEL_UNITTWR = 200 * TWOPOW16, };

//Maximum acceleration at a TWR of exactly one.
enum { ACC_UNITTWR = 100 * TWOPOW16, };

//Thrust per drive in units of mass.
const float DRIVE_THRUST_UNITS = 3.0f;

typedef struct Drive
{
    uint8_t count;
    uint8_t throttle;
    float twr;
    uint32_t maxvel;
    uint32_t maxacc;
} Drive;

uint32_t drive_serialize(void* compo, SerialStyle style, uint8_t* buf);
uint32_t drive_deserialize(void* compo, SerialStyle style, uint8_t* buf);

typedef enum TrophCell
{
    TROPH_DEAD = 0,
    TROPH_AUTO,
    TROPH_HETE,
    TROPH_COUNT,
} TrophCell;

typedef uint16_t cells_t;

const CapacityConfig VAT_CAPA_CONF = { 64, 32, 8 };

typedef struct Vat
{
    uint8_t count;
    cells_t cells[TROPH_COUNT];
    cells_t cells_max[TROPH_COUNT];
} Vat;

uint32_t vat_serialize(void* compo, SerialStyle style, uint8_t* buf);
uint32_t vat_deserialize(void* compo, SerialStyle style, uint8_t* buf);

const CapacityConfig EXTRACT_CAPA_CONF = { 64, 32, 8 };

#define EXTRACT_PULL_TIME 10.0f

enum {
    EXTRACT_STT_IDLE = 0,
    EXTRACT_STT_COOL,
    EXTRACT_STT_PULL,
};

typedef struct Extract
{
    uint8_t count;
    uint8_t state;
    float timer;
    uint16_t target;
} Extract;

uint32_t extract_serialize(void* compo, SerialStyle style, uint8_t* buf);
uint32_t extract_deserialize(void* compo, SerialStyle style, uint8_t* buf);

typedef uint16_t mats_t;

typedef enum SynthMats
{
    SYNTH_MAT_ORGANIC = 0,
    SYNTH_MAT_INORGANIC,
    SYNTH_MAT_COUNT,
} SynthMats;

//Distance below which synth can access entities.
enum { SYNTH_INTERACTION_UNITS = 0x0100 * TWOPOW16, };

enum {
    SYNTH_STT_IDLE = 0,
    SYNTH_STT_COOL,
    SYNTH_STT_REPAIR,
    SYNTH_STT_RESTOCK,
};

const CapacityConfig SYNTH_CAPA_CONF = { 32, 16, 4 };

//Repair speed penality when repairing self.
#define SYNTH_SELF_REPAIR_MULT 2.0f

//Hitpoints repaired per event.
enum { SYNTH_REPAIR_HIT = 20, };

//Materials required for a single repair event.
const mats_t SYNTH_REPAIR_MAT_COST[SYNTH_MAT_COUNT] = {
    [SYNTH_MAT_ORGANIC] = 1,
    [SYNTH_MAT_INORGANIC] = 1,
};

//Hull repair cooldown.
#define SYNTH_REPAIR_TIME 3.0f

//Ammo restocked per event.
enum { SYNTH_RESTOCK_AMMO = 1, };

//Materials required for a single restock event.
const mats_t SYNTH_RESTOCK_MAT_COST[SYNTH_MAT_COUNT] = {
    [SYNTH_MAT_ORGANIC] = 0,
    [SYNTH_MAT_INORGANIC] = 1,
};

//Bay restock cooldown.
#define SYNTH_RESTOCK_TIME 4.0f

typedef struct Synth
{
    uint8_t count;
    uint8_t state;
    float timer;
    uint16_t target;
    mats_t mats[SYNTH_MAT_COUNT];
    mats_t mats_max[SYNTH_MAT_COUNT];
} Synth;

uint32_t synth_serialize(void* compo, SerialStyle style, uint8_t* buf);
uint32_t synth_deserialize(void* compo, SerialStyle style, uint8_t* buf);

const CapacityConfig COMPUTE_CAPA_CONF = { 1000, 500, 100 };

typedef struct Compute
{
    uint8_t count;
    uint16_t frequency;
    uint16_t received;
    uint16_t received_pre;
} Compute;

uint32_t compute_serialize(void* compo, SerialStyle style, uint8_t* buf);
uint32_t compute_deserialize(void* compo, SerialStyle style, uint8_t* buf);

const CapacityConfig HULL_CAPA_CONF = { 100, 50, 10 };

typedef struct Hull
{
    uint8_t count;
    uint16_t hp;
    uint16_t hp_max;
} Hull;

uint32_t hull_serialize(void* compo, SerialStyle style, uint8_t* buf);
uint32_t hull_deserialize(void* compo, SerialStyle style, uint8_t* buf);

const CapacityConfig BAY_CAPA_CONF = { 4, 1, 1 };

enum {
    BAY_STT_IDLE = 0,
    BAY_STT_FIRE,
    BAY_STT_COOL,
};

#define BAY_LAUNCH_TIME 2.0f

typedef struct Bay
{
    uint8_t count;
    uint8_t state;
    float timer;
    uint16_t target;
    uint16_t ammo;
    uint16_t ammo_max;
} Bay;

uint32_t bay_serialize(void* compo, SerialStyle style, uint8_t* buf);
uint32_t bay_deserialize(void* compo, SerialStyle style, uint8_t* buf);

typedef struct Cargo
{
    uint16_t artifact;
    uint16_t originator;
} Cargo;

uint32_t cargo_serialize(void* compo, SerialStyle style, uint8_t* buf);
uint32_t cargo_deserialize(void* compo, SerialStyle style, uint8_t* buf);

enum { GROWTH_BONUS_COUNT = 8, };
const uint16_t GROWTH_BONUS_PTS[GROWTH_BONUS_COUNT] = {
    16,  32,  48,  64,  80,
    96, 112, 128, //144, 160,
};

typedef struct Growth
{
    uint8_t total;
    uint8_t max;
    uint16_t bonus_acc;
    uint16_t bonus_max;
} Growth;

uint32_t growth_serialize(void* compo, SerialStyle style, uint8_t* buf);
uint32_t growth_deserialize(void* compo, SerialStyle style, uint8_t* buf);

typedef struct Star
{
    uint16_t mass;
} Star;

uint32_t star_serialize(void* compo, SerialStyle style, uint8_t* buf);
uint32_t star_deserialize(void* compo, SerialStyle style, uint8_t* buf);

enum { TROPHS_WIDTH_MAX = 32, };
enum { TROPHS_CELLS_MAX = TROPHS_WIDTH_MAX * TROPHS_WIDTH_MAX, };

//Ratio according to which trophs are injected and sampled.
const float TROPH_RATIO[TROPH_COUNT];

//Population percentage above which sampling is allowed.
#define TROPHS_SAMPLE_THRESHOLD 0.6f

//Range in which trophs can be sampled (units).
enum { TROPHS_INTERACTION_UNITS = 0x0100 * TWOPOW16, };

typedef enum TrophParams
{
    TROPH_AUTO_REPRO = 0,
    TROPH_AUTO_DEATH,
    TROPH_HETE_REPRO,
    TROPH_HETE_DEATH,
    TROPH_PARAM_COUNT,
} TrophsParams;

typedef enum TrophWeather
{
    TROPH_WEATH_CALM = 0,
    TROPH_WEATH_AUTO_DISEASE,
    TROPH_WEATH_HETE_DISEASE,
    TROPH_WEATH_COUNT
} TrophWeather;

//Parameter modifications for troph weather conditions
const float TROPH_WEATH_PARAM[TROPH_WEATH_COUNT][TROPH_PARAM_COUNT];

//Length bounds of a weather event, in seconds.
enum { TROPHS_WEATHER_MIN_LEN = 60, };
enum { TROPHS_WEATHER_MAX_LEN = 120, };

//Trophs cellular automaton step frequency (Hz)
#define TROPHS_STEP_FREQ 2.0f

typedef struct Stepper
{
    float wavelength;
    float remainder;
} Stepper;

uint32_t stepper_serialize(void* compo, SerialStyle style, uint8_t* buf);
uint32_t stepper_deserialize(void* compo, SerialStyle style, uint8_t* buf);

typedef struct Trophs
{
    uint8_t width;
    float params[TROPH_PARAM_COUNT];
    uint8_t hete_hunt;
    uint8_t grid;
    uint8_t cells[2][TROPHS_CELLS_MAX];
    uint16_t cellcount[TROPH_COUNT];
    uint16_t threshold[TROPH_COUNT];

    uint8_t weather;
    float weather_mul;
    float weather_pass;
    float weather_full;

    Stepper stepper;
    int8_t neigh[8][2];
    uint32_t romu_trio32[3];
} Trophs;

uint32_t trophs_serialize(void* compo, SerialStyle style, uint8_t* buf);
uint32_t trophs_deserialize(void* compo, SerialStyle style, uint8_t* buf);

enum { HABITABLE_ACC_LIMIT = 1024 };
enum { HABITABLE_OUT_LIMIT = 128 };

typedef struct Habitable
{
    uint16_t accumulator;
    uint16_t output;
} Habitable;

uint32_t habitable_serialize(void* compo, SerialStyle style, uint8_t* buf);
uint32_t habitable_deserialize(void* compo, SerialStyle style, uint8_t* buf);

//Range at which monoliths can be interacted with.
enum { MONOLITH_INTERACTION_UNITS = 0x0100 * TWOPOW16, };

enum { SECRET_MAX = 1024, };

typedef struct Monolith
{
    uint16_t actual;
    uint16_t length;
    uint32_t accumulator;
    uint32_t threshold;
    uint8_t secret[SECRET_MAX];
} Monolith;

uint32_t monolith_serialize(void* compo, SerialStyle style, uint8_t* buf);
uint32_t monolith_deserialize(void* compo, SerialStyle style, uint8_t* buf);

typedef enum MazeCell
{
    MAZE_BLOCK = 0,
    MAZE_EMPTY,
    MAZE_COUNT,
} MazeCell;

enum { MAZE_WIDTH_MAX = 32, };
enum { MAZE_CELLS_MAX = MAZE_WIDTH_MAX * MAZE_WIDTH_MAX, };

typedef struct Maze
{
    uint8_t width;
    uint16_t stacksize;
    uint8_t stack[MAZE_CELLS_MAX];
    uint8_t cells[MAZE_CELLS_MAX];
    int8_t neigh[4][2];
    uint32_t romu_trio32[3];
} Maze;

uint32_t maze_serialize(void* compo, SerialStyle style, uint8_t* buf);
uint32_t maze_deserialize(void* compo, SerialStyle style, uint8_t* buf);

enum { WORMHOLE_SENSOR_ENTS = 8, };

//Sphere of influence multiplier that produces the warp radius.
#define WORMHOLE_HORIZON 0.25f

//Sphere of influence multiplier that produces the warp exit distance.
#define WORMHOLE_EXIT 2.0f

typedef struct Wormhole
{
    uint32_t radius;
    uint32_t target[3];
    uint32_t romu_trio32[3];
} Wormhole;

uint32_t wormhole_serialize(void* compo, SerialStyle style, uint8_t* buf);
uint32_t wormhole_deserialize(void* compo, SerialStyle style, uint8_t* buf);

enum { BARREN_INTERACTION_UNITS = 0x0100 * TWOPOW16, };

typedef struct Barren
{
    uint16_t richness;
} Barren;

uint32_t barren_serialize(void* compo, SerialStyle style, uint8_t* buf);
uint32_t barren_deserialize(void* compo, SerialStyle style, uint8_t* buf);

//Maximum number of barren worlds guarded by a hive.
enum { BARREN_PER_HIVE = 8, };

//Ratio of time in which a hive will guard a lowest-richness barren world.
#define HIVE_COVER_RATIO 0.75f

//Wasp visit length, in seconds.
enum { WASP_VISIT_SEC = 90, };

//Range at which hives interact with wasps.
enum { HIVE_WASP_UNITS = 0x0040 * TWOPOW16, };

//Maximum number of inorganic materials a hive can hold.
enum { HIVE_MAT_INORGANIC_CAPACITY = 2048, };

typedef struct Hive
{
    uint32_t romu_trio32[3];
    uint16_t guarded;
    uint16_t barrens[BARREN_PER_HIVE];
    uint16_t delays[BARREN_PER_HIVE];
    float cooldowns[BARREN_PER_HIVE];
} Hive;

uint32_t hive_serialize(void* compo, SerialStyle style, uint8_t* buf);
uint32_t hive_deserialize(void* compo, SerialStyle style, uint8_t* buf);

const mats_t WASP_MAT_COST[SYNTH_MAT_COUNT] = {
    [SYNTH_MAT_ORGANIC] = 0, [SYNTH_MAT_INORGANIC] = 16,
};

#define WASP_RANGED_TIME 4.0f

enum { WASP_GUARD_UNITS = 0x0080 * TWOPOW16, };

enum { WASP_SENSOR_ENTS = 2, };
enum { WASP_SENSOR_UNITS = 0x01ff * TWOPOW16, };

enum {
    WASP_STT_DEATH = 0,
    WASP_STT_JOURNEY,
    WASP_STT_GUARD_BARREN,
    WASP_STT_RETURN,
    WASP_STT_COUNT,
};

typedef struct Wasp
{
    uint8_t state;
    uint32_t romu_trio32[3];
    uint16_t hive;
    uint16_t barren;
    float timer;
    float ranged;
} Wasp;

uint32_t wasp_serialize(void* compo, SerialStyle style, uint8_t* buf);
uint32_t wasp_deserialize(void* compo, SerialStyle style, uint8_t* buf);

//Distance to target below which the leviathan will wander elsewhere.
enum { LEVIA_WANDER_CLOSE_UNITS = 0x0010 * TWOPOW16, };
enum { LEVIA_WANDER_MAX_UNITS = 0x0800 * TWOPOW16, };

//TODO
enum { LEVIA_SENSOR_ENTS = 2, };
enum { LEVIA_SENSOR_UNITS = 0x0200 * TWOPOW16, };

#define LEVIA_MELEE_TIME 3.0f
enum { LEVIA_MELEE_UNITS = 0x0010 * TWOPOW16, };
enum { LEVIA_MELEE_DMG = 100, };

#define LEVIA_DORMANT_TIME 300.0f

enum {
    LEVIA_STT_DORMANT = 0,
    LEVIA_STT_WANDER,
    LEVIA_STT_ESCAPE,
    LEVIA_STT_AGGRO,
    LEVIA_STT_COUNT,
};

typedef struct Leviathan
{
    uint8_t state;
    uint32_t romu_trio32[3];
    float timer;
    uint16_t target;
} Leviathan;

uint32_t leviathan_serialize(void* compo, SerialStyle style, uint8_t* buf);
uint32_t leviathan_deserialize(void* compo, SerialStyle style, uint8_t* buf);

enum {
    MISS_STT_DEATH = 0,
    MISS_STT_FLY,
};

#define MISS_EXPIRY_TIME 8.0f

enum { MISS_SENSOR_UNITS = 0x0200 * TWOPOW16, };

enum { MISS_MELEE_UNITS = 0x0020 * TWOPOW16, };
enum { MISS_MELEE_DMG = 50, };

typedef struct Missile
{
    uint8_t state;
    float timer;
    uint16_t target;
    Type sender;
} Missile;

uint32_t missile_serialize(void* compo, SerialStyle style, uint8_t* buf);
uint32_t missile_deserialize(void* compo, SerialStyle style, uint8_t* buf);

#define WRECK_DECAY_TIME 900.0f

typedef struct Wreckage
{
    float timer;
} Wreckage;

uint32_t wreckage_serialize(void* compo, SerialStyle style, uint8_t* buf);
uint32_t wreckage_deserialize(void* compo, SerialStyle style, uint8_t* buf);

typedef struct Ruin
{
    uint8_t description;
} Ruin;

uint32_t ruin_serialize(void* compo, SerialStyle style, uint8_t* buf);
uint32_t ruin_deserialize(void* compo, SerialStyle style, uint8_t* buf);

//Distance below which civilizations may be interacted with.
enum { CIVIL_INTERACTION_UNITS = 0x0100 * TWOPOW16, };

typedef enum CompoID
{
    TypeID = 0,
    PilotID,
    UxnID,
    ChronoID,
    PositionID,
    PointphysID,
    NavID,
    DriveID,
    VatID,
    ExtractID,
    SynthID,
    ComputeID,
    HullID,
    BayID,
    CargoID,
    GrowthID,
    StarID,
    TrophsID,
    HabitableID,
    MonolithID,
    MazeID,
    WormholeID,
    BarrenID,
    HiveID,
    WaspID,
    LeviathanID,
    MissileID,
    WreckageID,
    RuinID,

    COMPO_COUNT
} CompoID;

uint32_t COMPO_SIZE[COMPO_COUNT];

const uint32_t COMPO_MIN_COUNT[ENTTYPE_COUNT][COMPO_COUNT] = {
    [ENTTYPE_SHIP] = {
        [NavID] = 1,
        [DriveID] = 1,
        [VatID] = 1,
        [ExtractID] = 1,
        [SynthID] = 1,
        [ComputeID] = 1,
        [HullID] = 1,
        [BayID] = 1,
    },
    [ENTTYPE_WASP] = {
        [NavID] = 1,
        [DriveID] = 1,
        [SynthID] = 1,
        [HullID] = 3,
    },
    [ENTTYPE_LEVIATHAN] = {
        [NavID] = 1,
        [DriveID] = 6,
        [HullID] = 9,
    },
    [ENTTYPE_MISSILE] = {
        [NavID] = 1,
        [DriveID] = 4,
        [HullID] = 1,
    },
};

const uint32_t COMPO_MAX_COUNT[ENTTYPE_COUNT] = {
    [ENTTYPE_SHIP] = 8,
    [ENTTYPE_WASP] = 6,
    [ENTTYPE_LEVIATHAN] = 16,
    [ENTTYPE_MISSILE] = 6,
};

const int MODULE_GROWABLE[ENTTYPE_COUNT][COMPO_COUNT] = {
    [ENTTYPE_SHIP] = {
        [NavID]     = 1,
        [DriveID]   = 1,
        [VatID]     = 1,
        [ExtractID] = 1,
        [SynthID]   = 1,
        [ComputeID] = 1,
        [HullID]    = 1,
        [BayID]     = 1,
    },
};

const float MODULE_MAT_ABSORB = 0.5f;

const mats_t COMPO_MAT_COST[COMPO_COUNT][SYNTH_MAT_COUNT] = {
    [NavID]     = { [SYNTH_MAT_ORGANIC] = 16, [SYNTH_MAT_INORGANIC] = 16, },
    [DriveID]   = { [SYNTH_MAT_ORGANIC] = 16, [SYNTH_MAT_INORGANIC] = 16, },
    [VatID]     = { [SYNTH_MAT_ORGANIC] = 16, [SYNTH_MAT_INORGANIC] = 16, },
    [ExtractID] = { [SYNTH_MAT_ORGANIC] = 16, [SYNTH_MAT_INORGANIC] = 16, },
    [SynthID]   = { [SYNTH_MAT_ORGANIC] = 16, [SYNTH_MAT_INORGANIC] = 16, },
    [ComputeID] = { [SYNTH_MAT_ORGANIC] = 16, [SYNTH_MAT_INORGANIC] = 16, },
    [HullID]    = { [SYNTH_MAT_ORGANIC] = 16, [SYNTH_MAT_INORGANIC] = 16, },
    [BayID]     = { [SYNTH_MAT_ORGANIC] = 16, [SYNTH_MAT_INORGANIC] = 16, },
};

typedef uint32_t (*fn_compo_serialize_t)(void*, SerialStyle, uint8_t*);
fn_compo_serialize_t COMPO_SERIALIZE[COMPO_COUNT];
fn_compo_serialize_t COMPO_DESERIALIZE[COMPO_COUNT];

enum { ENTITY_MEMORY_MAX =
    sizeof(Entity) +
    sizeof(uint8_t) +
    sizeof(uint8_t) * COMPO_COUNT +
    sizeof(Type) +
    sizeof(Pilot) +
    sizeof(Uxn) +
    sizeof(Chrono) +
    sizeof(Position) +
    sizeof(Pointphys) +
    sizeof(Nav) +
    sizeof(Drive) +
    sizeof(Vat) +
    sizeof(Extract) +
    sizeof(Synth) +
    sizeof(Compute) +
    sizeof(Hull) +
    sizeof(Bay) +
    sizeof(Growth) +
    sizeof(Star) +
    sizeof(Trophs) +
    sizeof(Habitable) +
    sizeof(Monolith) +
    sizeof(Maze) +
    sizeof(Wormhole) +
    sizeof(Barren) +
    sizeof(Hive) +
    sizeof(Wasp) +
    sizeof(Leviathan) +
    sizeof(Missile)
}; /* WARNING: UPKEEP */

#endif /* COMPO_H */
