#ifndef SHADER_H
#define SHADER_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
//#include <stdbool.h>
#include <string.h>

#define PLANCKLOG_HEADER
#include "plancklog.h"

#include "helper.h"

enum { SHADER_STR_MAX = 256, };

typedef struct ShaderInfo
{
    GLenum type;
    char path[SHADER_STR_MAX];
} ShaderInfo;

void shaderinfo_init(ShaderInfo* shinfo, int type, const char* path);

int shaders_compile_program(ShaderInfo* shaders, int count, GLuint* program);

#endif /* SHADER_H */