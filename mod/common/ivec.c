#include "ivec.h"

float uint32_to_float(const uint32_t pos)
{
    return ((pos & 0xffff0000) >> 16) + (pos & 0x0000ffff) / TWOPOW16F;
}

void uint32_to_vec3(const uint32_t* pos, vec3 out)
{
    for(int i = 0; i < 3; ++i){
        out[i] = uint32_to_float(pos[i]);
    }
}

float int32_to_float(int32_t pos)
{
    return ((pos & 0xffff0000) >> 16) + (pos & 0x0000ffff) / TWOPOW16F;
}

void int32_to_vec3(int32_t* pos, vec3 out)
{
    for(int i = 0; i < 3; ++i){
        out[i] = int32_to_float(pos[i]);
    }
}

void vec3_to_int32(const vec3 pos, int32_t* out)
{
    for(int i = 0; i < 3; ++i){
	out[i] = pos[i];
    }
}

void v3uint32_diff(const uint32_t* p1, const uint32_t* p2, int32_t* diff)
{
    for(int i = 0; i < 3; ++i){
        diff[i] = (int32_t)(p1[i] >> 2) - (int32_t)(p2[i] >> 2);
        diff[i] <<= 2;
    }
}

void v3int32_diff(const int32_t* p1, const int32_t* p2, int32_t* diff)
{
    for(int i = 0; i < 3; ++i){
        diff[i] = p1[i] - p2[i];
    }
}

float v3uint32_dist(const uint32_t* p1, const uint32_t* p2)
{
    int32_t d[3]; v3uint32_diff(p1, p2, d);
    vec3 diff = { d[0], d[1], d[2], };
    
    return glm_vec3_norm(diff);
}

float v3uint32_norm(const uint32_t* p1)
{
    return glm_vec3_norm((vec3){ p1[0], p1[1], p1[2] });
}

float v3int32_norm(const int32_t* p1)
{
    return glm_vec3_norm((vec3){ p1[0], p1[1], p1[2] });
}

void v3uint32_add(const uint32_t* a, const int32_t* b, uint32_t* out)
{
    for(int i = 0; i < 3; ++i){ out[i] = a[i] + b[i]; }
}

void v3int32_scale_as(int32_t* a, int32_t s, int32_t* out)
{
    vec3 av = { a[0], a[1], a[2], };
    float len = glm_vec3_norm(av);
    if(len > 0){
        for(int i = 0; i < 3; ++i){ out[i] = a[i] * (s / len); }
    } else {
        out[0] = out[1] = out[2] = 0;
    }
    
}

float v3int32_normdot(int32_t* a, int32_t* b)
{
    vec3 av; glm_vec3_normalize_to((vec3){ a[0], a[1], a[2] }, av);
    vec3 bv; glm_vec3_normalize_to((vec3){ b[0], b[1], b[2] }, bv);
    return glm_vec3_dot(av, bv);
}

void v3uint32_bbox(const uint32_t* a, const uint32_t radius, uint32_t out[3][2])
{
    for(int i = 0; i < 3; ++i){
        out[i][0] = a[i] - radius;
        out[i][0] = out[i][0] > a[i] ? 0 : out[i][0];
        out[i][1] = a[i] + radius;
        out[i][1] = out[i][1] < a[i] ? 0xffffffff : out[i][1];
    }
}

int v3uint32_in_bbox(const uint32_t* a, const uint32_t bbox[3][2])
{
	return
		a[0] >= bbox[0][0] && a[0] < bbox[0][1] &&
		a[1] >= bbox[1][0] && a[1] < bbox[1][1] &&
		a[2] >= bbox[2][0] && a[2] < bbox[2][1];
}
