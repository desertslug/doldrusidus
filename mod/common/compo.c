#include "compo.h"

void compress_2bitsperbyte(uint8_t* dest, uint8_t* src, int bytes)
{
    //Assert that the compressed byte count is divisible by 4
    assert(bytes % 4 == 0);

    //Assert that the memory areas do not overlap
    assert(src < dest ? src + bytes <= dest : dest + bytes / 4 <= src);

    //Compress 2 bits from 4 cells into 1 byte
    for(int i = 0; i < bytes; i += 4){
        uint8_t sum = 0;
        for(int j = 0; j < 4; ++j){
            sum += (src[i + j] & 0x3) << j * 2;
        }
        dest[i / 4] = sum;
    }
}

void decompress_2bitsperbyte(uint8_t* dest, uint8_t* src, int bytes)
{
    //Assert that the memory areas do not overlap
    assert(src < dest ? src + bytes <= dest : dest + bytes * 4 <= src);

    //Decompress 1 byte into the low 2 bits of 4 bytes
    for(int i = 0; i < bytes; ++i){
        for(int j = 0; j < 4; ++j){
            int shift = j * 2;
            dest[i * 4 + j] = (src[i] & (0x3 << shift)) >> shift;
        }
    }
}

void convert_rgba_2bitsperbyte(uint8_t* dest, const uint8_t* src, int bytes, float colors[4][4])
{
    for(int i = 0; i < bytes; ++i){
        for(int j = 0; j < 4; ++j){
            dest[i * 4 + j] = colors[src[i]][j] * 255.0f;
        }
    }
}

int archetype_serialize_rows(Archetype* arch, FILE* file)
{
    int ret = 0;

    //Serializing rows of an empty archetype is a no-op
    if(arch->ents.size == 0){ return ret; }

    //Serialize rows of archetype
    uint8_t buf[ENTITY_MEMORY_MAX];
    for(int i = 0; i < arch->ents.size; ++i){
        
        uint32_t size = archetype_serialize_row(arch, i, SERIAL_FULL, buf);

        //Write chunk size and contents
        if(fwrite(&size, sizeof(uint32_t), 1, file) != 1){ ret = 1; break; };
        if(fwrite(buf, size, 1, file) != 1){ ret = 1; break; };
    }

    return ret;
}

uint32_t archetype_serialize_row(Archetype* arch, uint32_t row, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;

    //Callee must ensure the entity is of a nonempty archetype
    assert_or_exit(arch->cols.size > 0,
        "archetype_serialize_row: empty archetype\n");

    //Serialize entity ID
    memcpy(&buf[size], rtcp_uvec_get(&arch->ents, row), sizeof(Entity));
    size += sizeof(Entity);

    //Serialize serialization style
    //TODO: necessary?

    //Serialize number of columns
    uint8_t columns = archid_ones(arch->aid);
    memcpy(&buf[size], &columns, sizeof(uint8_t));
    size += sizeof(uint8_t);

    //Serialize comopnents
    uint8_t colidx = 0;
    uint8_t bits = archid_bits(arch->aid);
    for(uint8_t i = 0; i < bits; ++i){ if(archid_getbit(arch->aid, i)){

        //Serialize component ID
        memcpy(&buf[size], &i, sizeof(uint8_t));
        size += sizeof(uint8_t);

        //Serialize component
        rtcp_uvec_t* col = rtcp_uvec_get(&arch->cols, colidx);
        size += COMPO_SERIALIZE[i](
            rtcp_uvec_get(col, row), style, &buf[size]);

        //Increment column index
        ++colidx;
    }}

    return size;
}

int ecs_serialize(ECS* ecs, FILE* file)
{
    int ret = 0;

    //Serialize archetypes
    rtcp_hmap_t* hmap = &ecs->arch;
    for(int i = 0; i < hmap->capacity; ++i){
        if(hmap->records[i] != RTCP_HMAP_OCC){ continue; }
        Archetype* arch = (Archetype*)(hmap->buckets + i * hmap->typesize);

        //Serialize all rows of the archetype
        if(archetype_serialize_rows(arch, file) != 0){ ret = 1; break; }
    }

    return ret;
}

int ecs_deserialize(ECS* ecs, FILE* file)
{
    //Deserialize entities
    for(;;){

        //Attempt to read data size
        uint32_t size;
        if(!fread(&size, sizeof(uint32_t), 1, file)){ break; }

        //Read in data chunk
        uint8_t chunk[ENTITY_MEMORY_MAX];
        if(!fread(chunk, size, 1, file)){ break; }

        //Deserialize entity
        ecs_deserialize_entity(ecs, SERIAL_FULL, chunk);
    }

    //Return 0 success status if the EOF was reached without errors
    return feof(file) && !ferror(file) ? 0 : 1;
}

uint32_t ecs_serialize_entity(ECS* ecs, Entity ent, SerialStyle style, uint8_t* buf)
{
    //Get entity record
    Record* rec = rtcp_hmap_get(&ecs->ents, &ent);
    assert_or_exit(rec, "ecs_serialize_entity: missing record\n");

    //Get archetype
    Archetype* arch = rtcp_hmap_get(&ecs->arch, &rec->aid);
    assert_or_exit(arch, "ecs_serialize_entity: missing archetype\n");

    //Serialize row
    return archetype_serialize_row(arch, rec->row, style, buf);
}

Entity ecs_deserialize_entity(ECS* ecs, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;

    //Deserialize entity ID
    Entity ent;
    memcpy(&ent, &buf[size], sizeof(Entity));
    size += sizeof(Entity);

    //Add empty entity to ECS if it is not already a part of it
    if(!ecs_entity_alive(ecs, ent)){
        
        //Set archetype of the entity
        Record rec = { .aid = (ArchID){0}, };
        rtcp_hmap_add(&ecs->ents, &ent, &rec);

        //Find the next lowest unused entity ID
        do { ecs->entnext++; }
        while(rtcp_hmap_get(&ecs->ents, &ecs->entnext));

        //Add entity to empty archetype
        Archetype* empty = rtcp_hmap_get(&ecs->arch, &rec.aid);
        assert(empty);
        archetype_pushrow(empty, ent);
    }

    //Deserialize number of columns
    uint8_t columns;
    memcpy(&columns, &buf[size], sizeof(uint8_t));
    size += sizeof(uint8_t);

    //Deserialize components
    for(uint8_t i = 0; i < columns; ++i){

        //Deserialize component ID
        uint8_t id;
        memcpy(&id, &buf[size], sizeof(uint8_t));
        size += sizeof(uint8_t);

        //Get pointer to component data in the ECS
        uint8_t* compo = ecs_get_mut_id(ecs, ent, id);

        //Deserialize component
        size += COMPO_DESERIALIZE[id](compo, style, &buf[size]);
    }

    return ent;
}

void serialized_entity_id_edit(uint8_t* buf, Entity ent)
{
    //Format-specific wizardry
    //WARNING: UPKEEP
    Entity* dest = (Entity*)buf;
    *dest = ent;
}

uint32_t COMPO_SIZE[COMPO_COUNT] = {
    [TypeID] = sizeof(Type),
    [PilotID] = sizeof(Pilot),
    [UxnID] = sizeof(Uxn),
    [ChronoID] = sizeof(Chrono),
    [PositionID] = sizeof(Position),
    [PointphysID] = sizeof(Pointphys),
    [NavID] = sizeof(Nav),
    [DriveID] = sizeof(Drive),
    [VatID] = sizeof(Vat),
    [ExtractID] = sizeof(Extract),
    [SynthID] = sizeof(Synth),
    [ComputeID] = sizeof(Compute),
    [HullID] = sizeof(Hull),
    [BayID] = sizeof(Bay),
    [CargoID] = sizeof(Cargo),
    [GrowthID] = sizeof(Growth),
    [StarID] = sizeof(Star),
    [TrophsID] = sizeof(Trophs),
    [HabitableID] = sizeof(Habitable),
    [MonolithID] = sizeof(Monolith),
    [MazeID] = sizeof(Maze),
    [WormholeID] = sizeof(Wormhole),
    [BarrenID] = sizeof(Barren),
    [HiveID] = sizeof(Hive),
    [WaspID] = sizeof(Wasp),
    [LeviathanID] = sizeof(Leviathan),
    [MissileID] = sizeof(Missile),
    [WreckageID] = sizeof(Wreckage),
    [RuinID] = sizeof(Ruin),
};

fn_compo_serialize_t COMPO_SERIALIZE[COMPO_COUNT] = {
    [TypeID] = type_serialize,
    [PilotID] = pilot_serialize,
    [UxnID] = uxn_serialize,
    [ChronoID] = NULL,
    [PositionID] = position_serialize,
    [PointphysID] = pointphys_serialize,
    [NavID] = nav_serialize,
    [DriveID] = drive_serialize,
    [VatID] = vat_serialize,
    [ExtractID] = extract_serialize,
    [SynthID] = synth_serialize,
    [ComputeID] = compute_serialize,
    [HullID] = hull_serialize,
    [BayID] = bay_serialize,
    [CargoID] = cargo_serialize,
    [GrowthID] = growth_serialize,
    [StarID] = star_serialize,
    [TrophsID] = trophs_serialize,
    [HabitableID] = habitable_serialize,
    [MonolithID] = monolith_serialize,
    [MazeID] = maze_serialize,
    [WormholeID] = wormhole_serialize,
    [BarrenID] = barren_serialize,
    [HiveID] = hive_serialize,
    [WaspID] = wasp_serialize,
    [LeviathanID] = leviathan_serialize,
    [MissileID] = missile_serialize,
    [WreckageID] = wreckage_serialize,
    [RuinID] = ruin_serialize,
};

fn_compo_serialize_t COMPO_DESERIALIZE[COMPO_COUNT] = {
    [TypeID] = type_deserialize,
    [PilotID] = pilot_deserialize,
    [UxnID] = uxn_deserialize,
    [ChronoID] = NULL,
    [PositionID] = position_deserialize,
    [PointphysID] = pointphys_deserialize,
    [NavID] = nav_deserialize,
    [DriveID] = drive_deserialize,
    [VatID] = vat_deserialize,
    [ExtractID] = extract_deserialize,
    [SynthID] = synth_deserialize,
    [ComputeID] = compute_deserialize,
    [HullID] = hull_deserialize,
    [BayID] = bay_deserialize,
    [CargoID] = cargo_deserialize,
    [GrowthID] = growth_deserialize,
    [StarID] = star_deserialize,
    [TrophsID] = trophs_deserialize,
    [HabitableID] = habitable_deserialize,
    [MonolithID] = monolith_deserialize,
    [MazeID] = maze_deserialize,
    [WormholeID] = wormhole_deserialize,
    [BarrenID] = barren_deserialize,
    [HiveID] = hive_deserialize,
    [WaspID] = wasp_deserialize,
    [LeviathanID] = leviathan_deserialize,
    [MissileID] = missile_deserialize,
    [WreckageID] = wreckage_deserialize,
    [RuinID] = ruin_deserialize,
};

uint32_t type_serialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Type* type = (Type*)compo;

    memcpy(&buf[size], &type->type, sizeof(uint16_t));
    size += sizeof(uint16_t);

    return size;
}

uint32_t type_deserialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Type* type = (Type*)compo;

    memcpy(&type->type, &buf[size], sizeof(uint16_t));
    size += sizeof(uint16_t);

    return size;
}

uint32_t pilot_serialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Pilot* pilot = (Pilot*)compo;

    uint16_t len = strlen(pilot->uname);

    memcpy(&buf[size], pilot->uname, (len + 1) * sizeof(char));
    size += (len + 1) * sizeof(char);

    return size;
}

uint32_t pilot_deserialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Pilot* pilot = (Pilot*)compo;

    uint16_t len = strlen((char*)&buf[size]);

    memcpy(pilot->uname, &buf[size], (len + 1) * sizeof(char));
    size += (len + 1) * sizeof(char);

    return size;
}

uint32_t uxn_stack_serialize(Stack* s, uint8_t* buf)
{
    uint32_t size = 0;

    memcpy(&buf[size], &s->ptr, sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&buf[size], &s->dat, s->ptr * sizeof(uint8_t));
    size += s->ptr * sizeof(uint8_t);

    return size;
}

uint32_t uxn_stack_deserialize(Stack* s, uint8_t* buf)
{
    uint32_t size = 0;

    memcpy(&s->ptr, &buf[size], sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&s->dat, &buf[size], s->ptr * sizeof(uint8_t));
    size += s->ptr * sizeof(uint8_t);

    return size;
}

uint32_t uxn_buffer_serialize(Buffer* b, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;

    if(style > SERIAL_NETW){ return size; }

    uint32_t head = buffer_head_size(b);
    uint32_t tail = buffer_tail_size(b);
    uint32_t total = head + tail;

    memcpy(&buf[size], &total, sizeof(uint32_t));
    size += sizeof(uint32_t);

    memcpy(&buf[size], &b->buf[b->first], head * sizeof(Event));
    size += head * sizeof(Event);
    memcpy(&buf[size], &b->buf[0], tail * sizeof(Event));
    size += tail * sizeof(Event);

    return size;
}

uint32_t uxn_buffer_deserialize(Buffer* b, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;

    if(style > SERIAL_NETW){ return size; }

    uint32_t total;
    memcpy(&total, &buf[size], sizeof(uint32_t));
    size += sizeof(uint32_t);

    buffer_push_multi(b, (Event*)&buf[size], total);
    size += total * sizeof(Event);

    return size;
}

uint32_t uxn_serialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Uxn* uxn = (Uxn*)compo;

    //Halt & brk
    memcpy(&buf[size], &uxn->halt, sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&buf[size], &uxn->brk, sizeof(uint8_t));
    size += sizeof(uint8_t);

    //Program counter
    memcpy(&buf[size], &uxn->pc, sizeof(uint16_t));
    size += sizeof(uint16_t);

    //Remaining instructions & instructions per tick
    memcpy(&buf[size], &uxn->instr, sizeof(uint32_t));
    size += sizeof(uint32_t);
    memcpy(&buf[size], &uxn->inspt, sizeof(uint32_t));
    size += sizeof(uint32_t);

    //Error
    memcpy(&buf[size], &uxn->err, sizeof(uint8_t));
    size += sizeof(uint8_t);

    //Display stack pointers
    memcpy(&buf[size], &uxn->wstptr, sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&buf[size], &uxn->rstptr, sizeof(uint8_t));
    size += sizeof(uint8_t);

    //Input & output event buffers
    size += uxn_buffer_serialize(&uxn->ievt, style, &buf[size]);
    size += uxn_buffer_serialize(&uxn->oevt, style, &buf[size]);

    if(style > SERIAL_FULL){ return size; }

    //Working & return stacks
    size += uxn_stack_serialize(&uxn->wst, &buf[size]);
    size += uxn_stack_serialize(&uxn->rst, &buf[size]);

    //Device memory
    memcpy(&buf[size], uxn->dev, UXN_DEV_BYTES_TOTAL * sizeof(uint8_t));
    size += UXN_DEV_BYTES_TOTAL * sizeof(uint8_t);

    //RAM
    memcpy(&buf[size], uxn->ram, UXN_RAM_BYTES * sizeof(uint8_t));
    size += UXN_RAM_BYTES * sizeof(uint8_t);

    return size;
}

uint32_t uxn_deserialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Uxn* uxn = (Uxn*)compo;

    //Halt & brk
    memcpy(&uxn->halt, &buf[size], sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&uxn->brk, &buf[size], sizeof(uint8_t));
    size += sizeof(uint8_t);

    //Program counter
    memcpy(&uxn->pc, &buf[size], sizeof(uint16_t));
    size += sizeof(uint16_t);

    //Remaining instructions & instructions per tick
    memcpy(&uxn->instr, &buf[size], sizeof(uint32_t));
    size += sizeof(uint32_t);
    memcpy(&uxn->inspt, &buf[size], sizeof(uint32_t));
    size += sizeof(uint32_t);

    //Error
    memcpy(&uxn->err, &buf[size], sizeof(uint8_t));
    size += sizeof(uint8_t);

    //Display stack pointers
    memcpy(&uxn->wstptr, &buf[size], sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&uxn->rstptr, &buf[size], sizeof(uint8_t));
    size += sizeof(uint8_t);

    //Input & output event buffers
    size += uxn_buffer_deserialize(&uxn->ievt, style, &buf[size]);
    size += uxn_buffer_deserialize(&uxn->oevt, style, &buf[size]);

    if(style > SERIAL_FULL){ return size; }

    //Working & return stacks
    size += uxn_stack_deserialize(&uxn->wst, &buf[size]);
    size += uxn_stack_deserialize(&uxn->rst, &buf[size]);

    //Device memory
    memcpy(uxn->dev, &buf[size], UXN_DEV_BYTES_TOTAL * sizeof(uint8_t));
    size += UXN_DEV_BYTES_TOTAL * sizeof(uint8_t);
    
    //RAM
    memcpy(uxn->ram, &buf[size], UXN_RAM_BYTES * sizeof(uint8_t));
    size += UXN_RAM_BYTES * sizeof(uint8_t);

    return size;
}

uint32_t position_serialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Position* pos = (Position*)compo;

    memcpy(&buf[size], pos->pos, sizeof(uint32_t[3]));
    size += sizeof(uint32_t[3]);

    return size;
}

uint32_t position_deserialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Position* pos = (Position*)compo;

    memcpy(pos->pos, &buf[size], sizeof(uint32_t[3]));
    size += sizeof(uint32_t[3]);

    return size;
}

uint32_t pointphys_serialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Pointphys* pphys = (Pointphys*)compo;

    memcpy(&buf[size], pphys->vel, sizeof(int32_t[3]));
    size += sizeof(int32_t[3]);
    memcpy(&buf[size], pphys->acc, sizeof(int32_t[3]));
    size += sizeof(int32_t[3]);

    return size;
}

uint32_t pointphys_deserialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Pointphys* pphys = (Pointphys*)compo;

    memcpy(pphys->vel, &buf[size], sizeof(int32_t[3]));
    size += sizeof(int32_t[3]);
    memcpy(pphys->acc, &buf[size], sizeof(int32_t[3]));
    size += sizeof(int32_t[3]);

    return size;
}

uint32_t nav_serialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Nav* nav = (Nav*)compo;

    memcpy(&buf[size], &nav->count, sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&buf[size], &nav->motion, sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&buf[size], &nav->range, sizeof(uint16_t));
    size += sizeof(uint16_t);
    memcpy(&buf[size], &nav->target, sizeof(uint32_t[3]));
    size += sizeof(uint32_t[3]);
    memcpy(&buf[size], &nav->distance, sizeof(uint32_t));
    size += sizeof(uint32_t);
    memcpy(&buf[size], nav->signal, SIGNAL_BYTES);
    size += SIGNAL_BYTES;

	SyncEnts* syncents = &nav->syncents;

    memcpy(&buf[size], &syncents->sec, sizeof(uint32_t));
    size += sizeof(uint32_t);
    memcpy(&buf[size], syncents->ents, sizeof(Entity) * SYNC_ENT_MAX);
    size += sizeof(Entity) * SYNC_ENT_MAX;

    return size;
}

uint32_t nav_deserialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Nav* nav = (Nav*)compo;

    memcpy(&nav->count, &buf[size], sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&nav->motion, &buf[size], sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&nav->range, &buf[size], sizeof(uint16_t));
    size += sizeof(uint16_t);
    memcpy(&nav->target, &buf[size], sizeof(uint32_t[3]));
    size += sizeof(uint32_t[3]);
    memcpy(&nav->distance, &buf[size], sizeof(uint32_t));
    size += sizeof(uint32_t);
    memcpy(nav->signal, &buf[size], SIGNAL_BYTES);
    size += SIGNAL_BYTES;

	SyncEnts* syncents = &nav->syncents;

    memcpy(&syncents->sec, &buf[size], sizeof(uint32_t));
    size += sizeof(uint32_t);
    memcpy(syncents->ents, &buf[size], sizeof(Entity) * SYNC_ENT_MAX);
    size += sizeof(Entity) * SYNC_ENT_MAX;

    return size;
}

uint32_t drive_serialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Drive* drive = (Drive*)compo;

    memcpy(&buf[size], &drive->count, sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&buf[size], &drive->throttle, sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&buf[size], &drive->twr, sizeof(float));
    size += sizeof(float);
    memcpy(&buf[size], &drive->maxvel, sizeof(uint32_t));
    size += sizeof(uint32_t);
    memcpy(&buf[size], &drive->maxacc, sizeof(uint32_t));
    size += sizeof(uint32_t);

    return size;
}

uint32_t drive_deserialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Drive* drive = (Drive*)compo;

    memcpy(&drive->count, &buf[size], sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&drive->throttle, &buf[size], sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&drive->twr, &buf[size], sizeof(float));
    size += sizeof(float);
    memcpy(&drive->maxvel, &buf[size], sizeof(uint32_t));
    size += sizeof(uint32_t);
    memcpy(&drive->maxacc, &buf[size], sizeof(uint32_t));
    size += sizeof(uint32_t);

    return size;
}

uint32_t vat_serialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Vat* vat = (Vat*)compo;

    memcpy(&buf[size], &vat->count, sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&buf[size], vat->cells, sizeof(cells_t[TROPH_COUNT]));
    size += sizeof(cells_t[TROPH_COUNT]);
    memcpy(&buf[size], vat->cells_max, sizeof(cells_t[TROPH_COUNT]));
    size += sizeof(cells_t[TROPH_COUNT]);

    return size;
}

uint32_t vat_deserialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Vat* vat = (Vat*)compo;

    memcpy(&vat->count, &buf[size], sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(vat->cells, &buf[size], sizeof(cells_t[TROPH_COUNT]));
    size += sizeof(cells_t[TROPH_COUNT]);
    memcpy(vat->cells_max, &buf[size], sizeof(cells_t[TROPH_COUNT]));
    size += sizeof(cells_t[TROPH_COUNT]);

    return size;
}

uint32_t extract_serialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Extract* ext = (Extract*)compo;

    memcpy(&buf[size], &ext->count, sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&buf[size], &ext->state, sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&buf[size], &ext->timer, sizeof(float));
    size += sizeof(float);
    memcpy(&buf[size], &ext->target, sizeof(uint16_t));
    size += sizeof(uint16_t);

    return size;
}

uint32_t extract_deserialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Extract* ext = (Extract*)compo;

    memcpy(&ext->count, &buf[size], sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&ext->state, &buf[size], sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&ext->timer, &buf[size], sizeof(float));
    size += sizeof(float);
    memcpy(&ext->target, &buf[size], sizeof(uint16_t));
    size += sizeof(uint16_t);

    return size;
}

uint32_t synth_serialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Synth* synth = (Synth*)compo;

    memcpy(&buf[size], &synth->count, sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&buf[size], &synth->state, sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&buf[size], &synth->timer, sizeof(float));
    size += sizeof(float);
    memcpy(&buf[size], &synth->target, sizeof(uint16_t));
    size += sizeof(uint16_t);
    memcpy(&buf[size], synth->mats, sizeof(mats_t[SYNTH_MAT_COUNT]));
    size += sizeof(mats_t[SYNTH_MAT_COUNT]);
    memcpy(&buf[size], synth->mats_max, sizeof(mats_t[SYNTH_MAT_COUNT]));
    size += sizeof(mats_t[SYNTH_MAT_COUNT]);

    return size;
}

uint32_t synth_deserialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Synth* synth = (Synth*)compo;

    memcpy(&synth->count, &buf[size], sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&synth->state, &buf[size], sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&synth->timer, &buf[size], sizeof(float));
    size += sizeof(float);
    memcpy(&synth->target, &buf[size], sizeof(uint16_t));
    size += sizeof(uint16_t);
    memcpy(synth->mats, &buf[size], sizeof(mats_t[SYNTH_MAT_COUNT]));
    size += sizeof(mats_t[SYNTH_MAT_COUNT]);
    memcpy(synth->mats_max, &buf[size], sizeof(mats_t[SYNTH_MAT_COUNT]));
    size += sizeof(mats_t[SYNTH_MAT_COUNT]);

    return size;
}

uint32_t compute_serialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Compute* com = (Compute*)compo;

    memcpy(&buf[size], &com->count, sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&buf[size], &com->frequency, sizeof(uint16_t));
    size += sizeof(uint16_t);
    memcpy(&buf[size], &com->received, sizeof(uint16_t));
    size += sizeof(uint16_t);
    memcpy(&buf[size], &com->received_pre, sizeof(uint16_t));
    size += sizeof(uint16_t);

    return size;
}

uint32_t compute_deserialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Compute* com = (Compute*)compo;

    memcpy(&com->count, &buf[size], sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&com->frequency, &buf[size], sizeof(uint16_t));
    size += sizeof(uint16_t);
    memcpy(&com->received, &buf[size], sizeof(uint16_t));
    size += sizeof(uint16_t);
    memcpy(&com->received_pre, &buf[size], sizeof(uint16_t));
    size += sizeof(uint16_t);

    return size;
}

uint32_t hull_serialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Hull* hull = (Hull*)compo;

    memcpy(&buf[size], &hull->count, sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&buf[size], &hull->hp, sizeof(uint16_t));
    size += sizeof(uint16_t);
    memcpy(&buf[size], &hull->hp_max, sizeof(uint16_t));
    size += sizeof(uint16_t);

    return size;
}

uint32_t hull_deserialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Hull* hull = (Hull*)compo;

    memcpy(&hull->count, &buf[size], sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&hull->hp, &buf[size], sizeof(uint16_t));
    size += sizeof(uint16_t);
    memcpy(&hull->hp_max, &buf[size], sizeof(uint16_t));
    size += sizeof(uint16_t);

    return size;
}

uint32_t bay_serialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Bay* bay = (Bay*)compo;

    memcpy(&buf[size], &bay->count, sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&buf[size], &bay->state, sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&buf[size], &bay->timer, sizeof(float));
    size += sizeof(float);
    memcpy(&buf[size], &bay->target, sizeof(uint16_t));
    size += sizeof(uint16_t);
    memcpy(&buf[size], &bay->ammo, sizeof(uint16_t));
    size += sizeof(uint16_t);
    memcpy(&buf[size], &bay->ammo_max, sizeof(uint16_t));
    size += sizeof(uint16_t);

    return size;
}

uint32_t bay_deserialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Bay* bay = (Bay*)compo;

    memcpy(&bay->count, &buf[size], sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&bay->state, &buf[size], sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&bay->timer, &buf[size], sizeof(float));
    size += sizeof(float);
    memcpy(&bay->target, &buf[size], sizeof(uint16_t));
    size += sizeof(uint16_t);
    memcpy(&bay->ammo, &buf[size], sizeof(uint16_t));
    size += sizeof(uint16_t);
    memcpy(&bay->ammo_max, &buf[size], sizeof(uint16_t));
    size += sizeof(uint16_t);

    return size;
}

uint32_t cargo_serialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Cargo* cargo = (Cargo*)compo;

    memcpy(&buf[size], &cargo->artifact, sizeof(uint16_t));
    size += sizeof(uint16_t);
    memcpy(&buf[size], &cargo->originator, sizeof(uint16_t));
    size += sizeof(uint16_t);

    return size;
}

uint32_t cargo_deserialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Cargo* cargo = (Cargo*)compo;

    memcpy(&cargo->artifact, &buf[size], sizeof(uint16_t));
    size += sizeof(uint16_t);
    memcpy(&cargo->originator, &buf[size], sizeof(uint16_t));
    size += sizeof(uint16_t);

    return size;
}

uint32_t growth_serialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Growth* growth = (Growth*)compo;

    memcpy(&buf[size], &growth->total, sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&buf[size], &growth->max, sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&buf[size], &growth->bonus_acc, sizeof(uint16_t));
    size += sizeof(uint16_t);
    memcpy(&buf[size], &growth->bonus_max, sizeof(uint16_t));
    size += sizeof(uint16_t);

    return size;
}

uint32_t growth_deserialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Growth* growth = (Growth*)compo;

    memcpy(&growth->total, &buf[size], sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&growth->max, &buf[size], sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&growth->bonus_acc, &buf[size], sizeof(uint16_t));
    size += sizeof(uint16_t);
    memcpy(&growth->bonus_max, &buf[size], sizeof(uint16_t));
    size += sizeof(uint16_t);

    return size;
}

uint32_t star_serialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Star* star = (Star*)compo;

    memcpy(&buf[size], &star->mass, sizeof(uint16_t));
    size += sizeof(uint16_t);

    return size;
}

uint32_t star_deserialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Star* star = (Star*)compo;

    memcpy(&star->mass, &buf[size], sizeof(uint16_t));
    size += sizeof(uint16_t);

    return size;
}

uint32_t stepper_serialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Stepper* stepper = (Stepper*)compo;

    memcpy(&buf[size], &stepper->wavelength, sizeof(float));
    size += sizeof(float);
    memcpy(&buf[size], &stepper->remainder, sizeof(float));
    size += sizeof(float);

    return size;
}

uint32_t stepper_deserialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Stepper* stepper = (Stepper*)compo;

    memcpy(&stepper->wavelength, &buf[size], sizeof(float));
    size += sizeof(float);
    memcpy(&stepper->remainder, &buf[size], sizeof(float));
    size += sizeof(float);

    return size;
}

uint32_t trophs_serialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Trophs* trophs = (Trophs*)compo;

    memcpy(&buf[size], &trophs->width, sizeof(uint8_t));
    size += sizeof(uint8_t);

    memcpy(&buf[size], trophs->params, sizeof(float[TROPH_PARAM_COUNT]));
    size += sizeof(float[TROPH_PARAM_COUNT]);
    memcpy(&buf[size], &trophs->hete_hunt, sizeof(uint8_t));
    size += sizeof(uint8_t);

    memcpy(&buf[size], &trophs->grid, sizeof(uint8_t));
    size += sizeof(uint8_t);
    
    compress_2bitsperbyte(&buf[size], trophs->cells[trophs->grid],
        trophs->width * trophs->width);
    size += trophs->width * trophs->width / 4;

    memcpy(&buf[size], trophs->cellcount, sizeof(uint16_t[TROPH_COUNT]));
    size += sizeof(uint16_t[TROPH_COUNT]);
    memcpy(&buf[size], trophs->threshold, sizeof(uint16_t[TROPH_COUNT]));
    size += sizeof(uint16_t[TROPH_COUNT]);

    memcpy(&buf[size], &trophs->weather, sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&buf[size], &trophs->weather_mul, sizeof(float));
    size += sizeof(float);
    memcpy(&buf[size], &trophs->weather_pass, sizeof(float));
    size += sizeof(float);
    memcpy(&buf[size], &trophs->weather_full, sizeof(float));
    size += sizeof(float);

    size += stepper_serialize(&trophs->stepper, style, &buf[size]);

    if(style > SERIAL_FULL){ return size; }

    memcpy(&buf[size], trophs->neigh, sizeof(int8_t[8][2]));
    size += sizeof(int8_t[8][2]);
    memcpy(&buf[size], trophs->romu_trio32, sizeof(uint32_t[3]));
    size += sizeof(uint32_t[3]);

    return size;
}

uint32_t trophs_deserialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Trophs* trophs = (Trophs*)compo;

    memcpy(&trophs->width, &buf[size], sizeof(uint8_t));
    size += sizeof(uint8_t);

    memcpy(trophs->params, &buf[size], sizeof(float[TROPH_PARAM_COUNT]));
    size += sizeof(float[TROPH_PARAM_COUNT]);
    memcpy(&trophs->hete_hunt, &buf[size], sizeof(uint8_t));
    size += sizeof(uint8_t);

    memcpy(&trophs->grid, &buf[size], sizeof(uint8_t));
    size += sizeof(uint8_t);

    decompress_2bitsperbyte(trophs->cells[trophs->grid], &buf[size],
        trophs->width * trophs->width / 4);
    size += trophs->width * trophs->width / 4;

    memcpy(trophs->cellcount, &buf[size], sizeof(uint16_t[TROPH_COUNT]));
    size += sizeof(uint16_t[TROPH_COUNT]);
    memcpy(trophs->threshold, &buf[size], sizeof(uint16_t[TROPH_COUNT]));
    size += sizeof(uint16_t[TROPH_COUNT]);

    memcpy(&trophs->weather, &buf[size], sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&trophs->weather_mul, &buf[size], sizeof(float));
    size += sizeof(float);
    memcpy(&trophs->weather_pass, &buf[size], sizeof(float));
    size += sizeof(float);
    memcpy(&trophs->weather_full, &buf[size], sizeof(float));
    size += sizeof(float);

    size += stepper_deserialize(&trophs->stepper, style, &buf[size]);

    if(style > SERIAL_FULL){ return size; }

    memcpy(trophs->neigh, &buf[size], sizeof(int8_t[8][2]));
    size += sizeof(int8_t[8][2]);
    memcpy(trophs->romu_trio32, &buf[size], sizeof(uint32_t[3]));
    size += sizeof(uint32_t[3]);

    return size;
}

uint32_t habitable_serialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Habitable* habit = (Habitable*)compo;

    memcpy(&buf[size], &habit->accumulator, sizeof(uint16_t));
    size += sizeof(uint16_t);
    memcpy(&buf[size], &habit->output, sizeof(uint16_t));
    size += sizeof(uint16_t);

    return size;
}

uint32_t habitable_deserialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Habitable* habit = (Habitable*)compo;

    memcpy(&habit->accumulator, &buf[size], sizeof(uint16_t));
    size += sizeof(uint16_t);
    memcpy(&habit->output, &buf[size], sizeof(uint16_t));
    size += sizeof(uint16_t);

    return size;
}

uint32_t monolith_serialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Monolith* monol = (Monolith*)compo;

    memcpy(&buf[size], &monol->actual, sizeof(uint16_t));
    size += sizeof(uint16_t);
    memcpy(&buf[size], &monol->length, sizeof(uint16_t));
    size += sizeof(uint16_t);
    memcpy(&buf[size], &monol->accumulator, sizeof(uint16_t));
    size += sizeof(uint16_t);
    memcpy(&buf[size], &monol->threshold, sizeof(uint16_t));
    size += sizeof(uint16_t);

    if(style > SERIAL_FULL){ return size; }

    memcpy(&buf[size], monol->secret, monol->length);
    size += monol->length;

    return size;
}

uint32_t monolith_deserialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Monolith* monol = (Monolith*)compo;

    memcpy(&monol->actual, &buf[size], sizeof(uint16_t));
    size += sizeof(uint16_t);
    memcpy(&monol->length, &buf[size], sizeof(uint16_t));
    size += sizeof(uint16_t);
    memcpy(&monol->accumulator, &buf[size], sizeof(uint16_t));
    size += sizeof(uint16_t);
    memcpy(&monol->threshold, &buf[size], sizeof(uint16_t));
    size += sizeof(uint16_t);

    if(style > SERIAL_FULL){ return size; }

    memcpy(monol->secret, &buf[size], monol->length);
    size += monol->length;

    return size;
}

uint32_t maze_serialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Maze* maze = (Maze*)compo;

    memcpy(&buf[size], &maze->width, sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&buf[size], &maze->stacksize, sizeof(uint16_t));
    size += sizeof(uint16_t);
    memcpy(&buf[size], maze->neigh, sizeof(int8_t[4][2]));
    size += sizeof(int8_t[4][2]);
    memcpy(&buf[size], maze->romu_trio32, sizeof(uint32_t[3]));
    size += sizeof(uint32_t[3]);

    compress_2bitsperbyte(&buf[size], maze->cells,
        maze->width * maze->width);
    size += maze->width * maze->width / 4;

    if(style > SERIAL_FULL){ return size; }

    memcpy(&buf[size], maze->stack, maze->stacksize * sizeof(uint8_t));
    size += maze->stacksize * sizeof(uint8_t);

    return size;
}

uint32_t maze_deserialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Maze* maze = (Maze*)compo;

    memcpy(&maze->width, &buf[size], sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&maze->stacksize, &buf[size], sizeof(uint16_t));
    size += sizeof(uint16_t);
    memcpy(maze->neigh, &buf[size], sizeof(int8_t[4][2]));
    size += sizeof(int8_t[4][2]);
    memcpy(maze->romu_trio32, &buf[size], sizeof(uint32_t[3]));
    size += sizeof(uint32_t[3]);

    decompress_2bitsperbyte(maze->cells, &buf[size],
        maze->width * maze->width / 4);
    size += maze->width * maze->width / 4;

    if(style > SERIAL_FULL){ return size; }

    memcpy(maze->stack, &buf[size], maze->stacksize * sizeof(uint8_t));
    size += maze->stacksize * sizeof(uint8_t);

    return size;
}

uint32_t wormhole_serialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Wormhole* worm = (Wormhole*)compo;

    memcpy(&buf[size], &worm->radius, sizeof(uint32_t));
    size += sizeof(uint32_t);
    memcpy(&buf[size], worm->target, sizeof(uint32_t[3]));
    size += sizeof(uint32_t[3]);
    memcpy(&buf[size], worm->romu_trio32, sizeof(uint32_t[3]));
    size += sizeof(uint32_t[3]);

    return size;
}

uint32_t wormhole_deserialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Wormhole* worm = (Wormhole*)compo;

    memcpy(&worm->radius, &buf[size], sizeof(uint32_t));
    size += sizeof(uint32_t);
    memcpy(worm->target, &buf[size], sizeof(uint32_t[3]));
    size += sizeof(uint32_t[3]);
    memcpy(worm->romu_trio32, &buf[size], sizeof(uint32_t[3]));
    size += sizeof(uint32_t[3]);

    return size;
}

uint32_t barren_serialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Barren* barr = (Barren*)compo;

    memcpy(&buf[size], &barr->richness, sizeof(uint16_t));
    size += sizeof(uint16_t);

    return size;
}

uint32_t barren_deserialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Barren* barr = (Barren*)compo;

    memcpy(&barr->richness, &buf[size], sizeof(uint16_t));
    size += sizeof(uint16_t);

    return size;
}

uint32_t hive_serialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Hive* hive = (Hive*)compo;

    memcpy(&buf[size], hive->romu_trio32, sizeof(uint32_t[3]));
    size += sizeof(uint32_t[3]);
    memcpy(&buf[size], &hive->guarded, sizeof(uint16_t));
    size += sizeof(uint16_t);
    memcpy(&buf[size], hive->barrens, sizeof(uint16_t[BARREN_PER_HIVE]));
    size += sizeof(uint16_t[BARREN_PER_HIVE]);
    memcpy(&buf[size], hive->delays, sizeof(uint16_t[BARREN_PER_HIVE]));
    size += sizeof(uint16_t[BARREN_PER_HIVE]);
    memcpy(&buf[size], hive->cooldowns, sizeof(float[BARREN_PER_HIVE]));
    size += sizeof(float[BARREN_PER_HIVE]);

    return size;
}

uint32_t hive_deserialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Hive* hive = (Hive*)compo;

    memcpy(hive->romu_trio32, &buf[size], sizeof(uint32_t[3]));
    size += sizeof(uint32_t[3]);
    memcpy(&hive->guarded, &buf[size], sizeof(uint16_t));
    size += sizeof(uint16_t);
    memcpy(hive->barrens, &buf[size], sizeof(uint16_t[BARREN_PER_HIVE]));
    size += sizeof(uint16_t[BARREN_PER_HIVE]);
    memcpy(hive->delays, &buf[size], sizeof(uint16_t[BARREN_PER_HIVE]));
    size += sizeof(uint16_t[BARREN_PER_HIVE]);
    memcpy(hive->cooldowns, &buf[size], sizeof(float[BARREN_PER_HIVE]));
    size += sizeof(float[BARREN_PER_HIVE]);

    return size;
}

uint32_t wasp_serialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Wasp* wasp = (Wasp*)compo;

    memcpy(&buf[size], &wasp->state, sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&buf[size], wasp->romu_trio32, sizeof(uint32_t[3]));
    size += sizeof(uint32_t[3]);
    memcpy(&buf[size], &wasp->hive, sizeof(uint16_t));
    size += sizeof(uint16_t);
    memcpy(&buf[size], &wasp->barren, sizeof(uint16_t));
    size += sizeof(uint16_t);
    memcpy(&buf[size], &wasp->timer, sizeof(float));
    size += sizeof(float);
    memcpy(&buf[size], &wasp->ranged, sizeof(float));
    size += sizeof(float);

    return size;
}

uint32_t wasp_deserialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Wasp* wasp = (Wasp*)compo;

    memcpy(&wasp->state, &buf[size], sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(wasp->romu_trio32, &buf[size], sizeof(uint32_t[3]));
    size += sizeof(uint32_t[3]);
    memcpy(&wasp->hive, &buf[size], sizeof(uint16_t));
    size += sizeof(uint16_t);
    memcpy(&wasp->barren, &buf[size], sizeof(uint16_t));
    size += sizeof(uint16_t);
    memcpy(&wasp->timer, &buf[size], sizeof(float));
    size += sizeof(float);
    memcpy(&wasp->ranged, &buf[size], sizeof(float));
    size += sizeof(float);

    return size;
}

uint32_t leviathan_serialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Leviathan* levia = (Leviathan*)compo;

    memcpy(&buf[size], &levia->state, sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&buf[size], levia->romu_trio32, sizeof(uint32_t[3]));
    size += sizeof(uint32_t[3]);
    memcpy(&buf[size], &levia->timer, sizeof(float));
    size += sizeof(float);
    memcpy(&buf[size], &levia->target, sizeof(uint16_t));
    size += sizeof(uint16_t);

    return size;
}

uint32_t leviathan_deserialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Leviathan* levia = (Leviathan*)compo;

    memcpy(&levia->state, &buf[size], sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(levia->romu_trio32, &buf[size], sizeof(uint32_t[3]));
    size += sizeof(uint32_t[3]);
    memcpy(&levia->timer, &buf[size], sizeof(float));
    size += sizeof(float);
    memcpy(&levia->target, &buf[size], sizeof(uint16_t));
    size += sizeof(uint16_t);

    return size;
}

uint32_t missile_serialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Missile* miss = (Missile*)compo;

    memcpy(&buf[size], &miss->state, sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&buf[size], &miss->timer, sizeof(float));
    size += sizeof(float);
    memcpy(&buf[size], &miss->target, sizeof(uint16_t));
    size += sizeof(uint16_t);

    size += type_serialize(&miss->sender, style, &buf[size]);

    return size;
}

uint32_t missile_deserialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Missile* miss = (Missile*)compo;

    memcpy(&miss->state, &buf[size], sizeof(uint8_t));
    size += sizeof(uint8_t);
    memcpy(&miss->timer, &buf[size], sizeof(float));
    size += sizeof(float);
    memcpy(&miss->target, &buf[size], sizeof(uint16_t));
    size += sizeof(uint16_t);

    size += type_deserialize(&miss->sender, style, &buf[size]);

    return size;
}

uint32_t wreckage_serialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Wreckage* wreck = (Wreckage*)compo;

    memcpy(&buf[size], &wreck->timer, sizeof(float));
    size += sizeof(float);

    return size;
}

uint32_t wreckage_deserialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Wreckage* wreck = (Wreckage*)compo;

    memcpy(&wreck->timer, &buf[size], sizeof(float));
    size += sizeof(float);

    return size;
}

uint32_t ruin_serialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Ruin* ruin = (Ruin*)compo;

    memcpy(&buf[size], &ruin->description, sizeof(uint8_t));
    size += sizeof(uint8_t);

    return size;
}

uint32_t ruin_deserialize(void* compo, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    Ruin* ruin = (Ruin*)compo;

    memcpy(&ruin->description, &buf[size], sizeof(uint8_t));
    size += sizeof(uint8_t);

    return size;
}
