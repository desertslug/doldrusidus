#include "sweep.h"

void axis_init(Axis* axis)
{
    axis->size = 0;
}

void axis_add(Axis* axis, uint16_t ent, uint32_t pos)
{
    assert_or_exit(axis->size < TWOPOW16, "axis overfull\n");

    //Find the index of the first entity whose position is greater
    int idx = axis_first_idx_greater(axis, pos);

    //Shift entity indexes up
    for(int i = idx; i < axis->size; ++i){ ++axis->idx[axis->ent[i]]; }

    //Shift data up from the found position
    memmove(&axis->ent[idx + 1], &axis->ent[idx],
        (axis->size - idx) * sizeof(uint16_t));
    memmove(&axis->pos[idx + 1], &axis->pos[idx],
        (axis->size - idx) * sizeof(uint32_t));

    //Insert entity into the found position
    axis->ent[idx] = ent;
    axis->idx[ent] = idx;
    axis->pos[idx] = pos;

    //Increment entity count
    ++axis->size;
}

void axis_rem(Axis* axis, uint16_t ent)
{
    assert_or_exit(axis->size > 0, "axis underfull\n");

    //Get the entity's index
    int idx = axis->idx[ent];

    assert_or_exit(idx < axis->size, "axis removing nonexistent entity\n");

    //Shift entity indexes down
    for(int i = idx + 1; i < axis->size; ++i){ --axis->idx[axis->ent[i]]; }

    //Shift data down into that position
    memmove(&axis->ent[idx], &axis->ent[idx + 1],
        (axis->size - idx - 1) * sizeof(uint16_t));
    memmove(&axis->pos[idx], &axis->pos[idx + 1],
        (axis->size - idx - 1) * sizeof(uint32_t));

    //Decrement entity count
    --axis->size;
}

void axis_sort(Axis* axis)
{
    uint16_t e1, e2;
    uint32_t p1, p2;

    //Bubble entities upwards until the inversion count reaches zero
    int inv = 1;
    while(inv > 0){

        //Start inversion count from zero
        inv = 0;

        //Loop on adjacent elements
        for(int i = 0; i < axis->size - 1; ++i){

            //Swap elements if necessary
            if(axis->pos[i] > axis->pos[i + 1]){

                e1 = axis->ent[i]; e2 = axis->ent[i + 1];
                p1 = axis->pos[i]; p2 = axis->pos[i + 1];

                axis->ent[i] = e2;
                axis->pos[i] = p2;

                axis->ent[i + 1] = e1;
                axis->pos[i + 1] = p1;

                axis->idx[e1] = i + 1;
                axis->idx[e2] = i;

                //Increment inversion count
                ++inv;
            }
        }
    }
}

uint16_t axis_first_idx_greater(Axis* axis, uint32_t pos)
{
    int u = 0;
    int v = axis->size;

    while(u != v){
        int m = (u + v) / 2;
        bool cond = axis->pos[m] <= pos;
        u = cond ? m + 1 : u;
        v = cond ? v : m;
    }

    return u;
}

void sweep_init(Sweep* sweep)
{
    for(int i = 0; i < 3; ++i){ axis_init(&sweep->axes[i]); }
}

void sweep_add(Sweep* sweep, ECS* ecs, uint16_t ent)
{
    //Get position
    const Position* pos = ecs_get_id(ecs, ent, PositionID);
    assert_or_exit(pos, "sweep_add: missing position\n");

    //Add entity on all axes
    for(int i = 0; i < 3; ++i){ axis_add(&sweep->axes[i], ent, pos->pos[i]); }
}

void sweep_rem(Sweep* sweep, uint16_t ent)
{
    //Remove entity from all axes
    for(int i = 0; i < 3; ++i){ axis_rem(&sweep->axes[i], ent); }
}

void sweep_step(Sweep* sweep, ECS* ecs)
{
    Axis* axes = sweep->axes;

    Query q; query_alloc(&q);
    ArchID arch = archid_setbit((ArchID){0}, PositionID);
    query_init(&q, ecs, arch);
    while(query_next(&q)){
        Entity* ents = query_ents(ecs, &q);
        Position* pos = query_term(ecs, &q, PositionID);
        for(int i = 0; i < q.size; ++i){
            
            //Update the position in the sweep axes
            for(int j = 0; j < 3; ++j){
                axes[j].pos[axes[j].idx[ents[i]]] = pos[i].pos[j];
            }
        }
    }
    query_dealloc(&q);

    //Sort all axes
    for(int i = 0; i < 3; ++i){ axis_sort(&sweep->axes[i]); }
}

void sweep_bbox(Sweep* sweep, uint32_t bbox[3][2], uint16_t out[3][2])
{
    for(int i = 0; i < 3; ++i){

        //Find index intervals for the axis
        out[i][0] = axis_first_idx_greater(&sweep->axes[i], bbox[i][0]);
        out[i][1] = axis_first_idx_greater(&sweep->axes[i], bbox[i][1]);

        //Sanitize result range
        out[i][1] = out[i][0] > out[i][1] ? out[i][0] : out[i][1];
    }
}

int sweep_ranges_fewest(Sweep* sweep, uint16_t ranges[3][2])
{
    int ax = 0;
    int min = ranges[ax][1] - ranges[ax][0];
    for(int i = 0; i < 3; ++i){
        int d = ranges[i][1] - ranges[i][0];
        assert(d >= 0);
        if(d < min){ min = d; ax = i; }
    }
    assert(ax >= 0 && ax < 3);
    return ax;
}

int sweep_axis_idx_inside_bbox(Sweep* sweep, int axis, uint16_t idx, uint32_t bbox[3][2])
{
    uint16_t ent = sweep->axes[axis].ent[idx];
    int ax1 = (axis + 1) % 3;
    int ax2 = (axis + 2) % 3;
    uint32_t pos1 = sweep->axes[ax1].pos[sweep->axes[ax1].idx[ent]];
    uint32_t pos2 = sweep->axes[ax2].pos[sweep->axes[ax2].idx[ent]];

    return
        sweep->axes[axis].pos[idx] >= bbox[axis][0] &&
        sweep->axes[axis].pos[idx] <  bbox[axis][1] &&
        pos1 >= bbox[ax1][0] && pos1 < bbox[ax1][1] &&
        pos2 >= bbox[ax2][0] && pos2 < bbox[ax2][1];
}

int sweep_pos_radius_upto(Sweep* sweep, const uint32_t pos[3], uint32_t radius, uint32_t rand, uint16_t* ents, int entsmax)
{
    //Find bounding box
    uint32_t bbox[3][2]; v3uint32_bbox(pos, radius, bbox);

    return sweep_bbox_upto(sweep, bbox, rand, ents, entsmax);
}

int sweep_bbox_upto(Sweep* sweep, uint32_t bbox[3][2], uint32_t rand, uint16_t* ents, int entsmax)
{
    assert_or_exit(entsmax >= 0, "sweep_bbox_upto: entsmax was negative\n");

    //If the maximum number of entities is zero, return early
    if(entsmax == 0){ return 0; }

    //Found number of entities
    int found = 0;

    //Find index intervals for all three axes
    uint16_t range[3][2]; sweep_bbox(sweep, bbox, range);

    //Select the axis with the fewest results
    int ax = sweep_ranges_fewest(sweep, range);
    int min = range[ax][1] - range[ax][0];

    //If the axis with the fewest results had zero, return early
    if(min == 0){ return 0; }

    //Find number of slots, generate a random starting slot
    int slots = ((int)(min - 1) / entsmax) + 1;
    int first = (int)(rand % slots);
    int slotsize = MIN(min, entsmax);

    //Test slots until either:
    //  all slots are exhausted, or
    //  the entity limit is reached.
    int i = 0;
    int slot = first;
    while(i < slots && found < entsmax){

        //Find index bounds of the slot [inclusive, exclusive)
        //uint16_t low = range[ax][0] + slot * min;
        uint16_t low = range[ax][0] + slot * slotsize;
        uint16_t bounds[2] = { low, MIN(range[ax][1], low + min) };

        //Test & add passing entities inside the slot
        for(uint16_t j = bounds[0]; j < bounds[1]; ++j){

            if(sweep_axis_idx_inside_bbox(sweep, ax, j, bbox)){
                ents[found++] = sweep->axes[ax].ent[j];
            }

            //Break out of the loop early if the limit was reached
            if(found >= entsmax){ break; }
        }

        //Update loop variables
        ++i;
        slot = (first + i) % slots;
    }

    assert_or_exit(found <= entsmax, "sweep_bbox_upto: found too many\n");

    //Return number of entities
    return found;
}