#include "random.h"

void hash_step(Hash1* hash1)
{
    hash1->last = fasthash32(hash1->buf, sizeof(uint32_t) * 1, hash1->last);
}

uint32_t hash_next(Hash1* hash1)
{
    hash1->last = fasthash32(hash1->buf, sizeof(uint32_t) * 1, hash1->last);

    return hash1->last;
}

float hash_next_norm(Hash1* hash1)
{
    return hash_next(hash1) / (float)UINT32_MAX;
}

void pos_around_pos_in_radius(Hash1* hash, uint32_t in[3], uint32_t radius, uint32_t out[3])
{
    pos_around_pos_in_kernel(hash, in, 0, radius, out);
}

void pos_around_pos_in_kernel(Hash1* hash, uint32_t in[3], uint32_t rad_min, uint32_t rad_max, uint32_t out[3])
{
    uint32_t bboxmax[3][2]; v3uint32_bbox(in, rad_max, bboxmax);
    uint32_t bboxmin[3][2]; v3uint32_bbox(in, rad_min, bboxmin);
    for(int i = 0; i < 3; ++i){
        if(hash_next_norm(hash) < 0.5f){
            out[i] = glm_lerp(
                bboxmin[i][1], bboxmax[i][1], hash_next_norm(hash));
        } else {
            out[i] = glm_lerp(
                bboxmax[i][0], bboxmin[i][0], hash_next_norm(hash));
        }
    }
}

float romu_trio32_norm(uint32_t romustt[3])
{
    return (romu_trio32_random(romustt) / TWOPOW16F) / TWOPOW16F;
}

uint32_t romu_trio32_range(uint32_t romustt[3], uint32_t min, uint32_t max)
{
    return min + romu_trio32_random(romustt) % (max - min + 1);
}

void romu_trio32_pos_around_pos_in_kernel(uint32_t romustt[3], uint32_t in[3], uint32_t rad_min, uint32_t rad_max, uint32_t out[3])
{
    uint32_t bboxmax[3][2]; v3uint32_bbox(in, rad_max, bboxmax);
    uint32_t bboxmin[3][2]; v3uint32_bbox(in, rad_min, bboxmin);
    for(int i = 0; i < 3; ++i){
        if(romu_trio32_norm(romustt) < 0.5f){
	    out[i] = glm_lerp(
		bboxmin[i][1], bboxmax[i][1], romu_trio32_norm(romustt));
        } else {
	    out[i] = glm_lerp(
		bboxmax[i][0], bboxmin[i][0], romu_trio32_norm(romustt));
        }
    }
}
