#include "world.h"

void age_serialize(struct timespec* age, uint8_t* buf)
{
    uint32_t size = 0;

    uint32_t tv_sec = age->tv_sec;
    uint32_t tv_nsec = age->tv_nsec;
    memcpy(&buf[size], &tv_sec, sizeof(uint32_t));
    size += sizeof(uint32_t);
    memcpy(&buf[size], &tv_nsec, sizeof(uint32_t));
    size += sizeof(uint32_t);
}

void age_deserialize(struct timespec* age, uint8_t* buf)
{
    uint32_t size = 0;

    uint32_t tv_sec;
    uint32_t tv_nsec;
    memcpy(&tv_sec, &buf[size], sizeof(uint32_t));
    size += sizeof(uint32_t);
    memcpy(&tv_nsec, &buf[size], sizeof(uint32_t));
    size += sizeof(uint32_t);
    age->tv_sec = tv_sec;
    age->tv_nsec = tv_nsec;
}

void world_alloc(World* world)
{
    ecs_alloc(&world->ecs);
}

void world_dealloc(World* world)
{
    ecs_dealloc(&world->ecs);
}

void world_init(World* world, uint32_t entsmax)
{
    //Initialize values
    world->age = (struct timespec){0};
    world->delta = (struct timespec){0};

    //Initialize entity component system
    ecs_init(&world->ecs, entsmax, COMPO_COUNT, COMPO_SIZE);
}

void world_stop(World* world)
{

}

void world_step(World* world, struct timespec delta)
{
    //Update world delta
    world->delta = delta;

    //Step age
    world->age = timespec_add(&world->age, &world->delta);

    //Tick the lazily shrinking data structures of the ecs
    ecs_tick(&world->ecs);

    //LOG_WARN_("%li , %li", world->age.tv_sec, world->age.tv_nsec);
}

void world_forget(World* world, Entity* entities, uint32_t entsmax, uint32_t expire_ms, uint32_t fade_ms)
{
    int count = 0;

    Query q; query_alloc(&q);

    //Run query on entity timestamps
    ArchID arch = archid_setbit((ArchID){0}, ChronoID);
    query_init(&q, &world->ecs, arch);
    while(query_next(&q) && count < entsmax){
        Entity* ents = query_ents(&world->ecs, &q);
        Chrono* chronos = (Chrono*)query_term(&world->ecs, &q, ChronoID);
        for(int i = 0; i < q.size; ++i){

            //Convert chrono component into timestamp
            struct timespec stamp =
                { .tv_sec = chronos[i].sec, .tv_nsec = chronos[i].nsec };

            //Queue the entity if it has not received an update in a while
            if(diff_msec(&stamp, &world->age) > expire_ms){

                entities[count++] = ents[i];

                //If enough entities were queued, break inner loop
                if(count >= entsmax){ break; }
            }

            //If the entity is not fading but enough time has passed
            if(!chronos[i].fading && diff_msec(&stamp, &world->age) > fade_ms){

                //Mark the entity as starting to fade
                chronos[i].fading = true;
            }
        }
    }
    query_dealloc(&q);

    //Delete each entity queued for removal
    for(int i = 0; i < count; ++i){
        ecs_entity_rem(&world->ecs, entities[i]);
    }
}

int world_serialize(World* world, FILE* file)
{
    int ret = 0; (void)ret;

    //Serialize age
    uint8_t age_buf[WORLD_AGE_BYTES] = {0};
    age_serialize(&world->age, age_buf);
    if(fwrite(age_buf, WORLD_AGE_BYTES, 1, file) != 1){ return 1; }
    
    if((ret = ecs_serialize(&world->ecs, file))){ return ret; };
    
    return 0;
}

int world_deserialize(World* world, FILE* file)
{
    int ret = 0; (void)ret;

    //Deserialize age
    uint8_t age_buf[WORLD_AGE_BYTES] = {0};
    if(!fread(age_buf, WORLD_AGE_BYTES, 1, file)){ return 1; }
    age_deserialize(&world->age, age_buf);
    
    int result = 0;
    if((ret = ecs_deserialize(&world->ecs, file))){ result = ret; };

    //Actualize the world regardless of how deserialization went
    //world_actualize(world);

    return result;
}

void world_serialize_age_pkt(World* world, void* opkt)
{
    uint8_t buf[WORLD_AGE_BYTES] = {0};
    age_serialize(&world->age, buf);
    pkt_chunk_add(opkt, buf, WORLD_AGE_BYTES, PKT_CHUNK_U8);
}

uint32_t world_deserialize_age_pkt(World* world, void* ipkt, uint8_t* chunk)
{
    uint8_t* buf;
    if(!msg_pkt_chunk_u8_minmax(ipkt, chunk,
        WORLD_AGE_BYTES, WORLD_AGE_BYTES, &buf)){ return 0; }
	age_deserialize(&world->age, buf);
    return pkt_chunk_bytes(ipkt, chunk);
}

void world_serialize_ents_pkt(World* world, Entity* ents, uint32_t count, void* opkt, SerialStyle style)
{
    assert_or_exit(style == SERIAL_NETW || style == SERIAL_NETW_UNRELIABLE,
        "world_serialize_ents_pkt: inappropriate serialization style");

    uint8_t buf[ENTITY_MEMORY_MAX];
    for(int i = 0; i < count; ++i){

        //Serialize to buffer
        uint32_t size =
            ecs_serialize_entity(&world->ecs, ents[i], style, buf);

        //Append buffer to packet
        pkt_chunk_add(opkt, buf, size, PKT_CHUNK_U8);
    }
}

uint32_t world_deserialize_ents_pkt(World* world, void* ipkt, uint8_t* chunk, SerialStyle style)
{
    assert_or_exit(style == SERIAL_NETW || style == SERIAL_NETW_UNRELIABLE,
        "world_deserialize_ents_pkt: inappropriate serialization style");

    uint8_t* chunk_prev = chunk;
    uint8_t* buf;
    while(msg_pkt_chunk_u8_minmax(ipkt, chunk, 1, ENTITY_MEMORY_MAX, &buf)){

        //Deserialize entity
        Entity ent = ecs_deserialize_entity(&world->ecs, style, buf);

        //Set the timestamp of the last update for that entity
        Chrono* chrono = ecs_get_mut_id(&world->ecs, ent, ChronoID);
        chrono->sec = world->age.tv_sec;
        chrono->nsec = world->age.tv_nsec;
        chrono->fading = false;

        chunk += pkt_chunk_bytes(ipkt, chunk);
    }
    return chunk - chunk_prev;
}
