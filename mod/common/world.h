#ifndef WORLD_H
#define WORLD_H

#include <stdint.h>
#include <time.h>

#include "defs.h"
#include "nanosleep.h"
#include "compo.h"
#include "packet.h"
#include "msg.h"

enum { WORLD_AGE_BYTES = 2 * sizeof(uint32_t) };
enum { WORLD_FILEPATH_MAX = 64, };

void age_serialize(struct timespec* age, uint8_t* buf);
void age_deserialize(struct timespec* age, uint8_t* buf);

typedef struct World
{
    struct timespec age;
    struct timespec delta;

    ECS ecs;
} World;

void world_alloc(World* world);
void world_dealloc(World* world);
void world_init(World* world, uint32_t entsmax);
void world_stop(World* world);
void world_step(World* world, struct timespec delta);

void world_forget(World* world, Entity* entities, uint32_t entsmax, uint32_t expire_ms, uint32_t fade_ms);

int world_serialize(World* world, FILE* file);
int world_deserialize(World* world, FILE* file);

void world_serialize_age_pkt(World* world, void* opkt);
uint32_t world_deserialize_age_pkt(World* world, void* ipkt, uint8_t* chunk);
void world_serialize_ents_pkt(World* world, Entity* ents, uint32_t count, void* pkt, SerialStyle style);
uint32_t world_deserialize_ents_pkt(World* world, void* ipkt, uint8_t* chunk, SerialStyle style);

#endif /* WORLD_H */
