#include "memory.h"

void timer_decrease(float* timer, float delta)
{
    *timer = (*timer - delta > 0.0f) ? *timer - delta : 0.0f;
    //*timer = (*timer - delta > 0.0f) * (*timer - delta);
}

void modsession_init(ModSession* session)
{
    //Normal input mode
    session->modality = NormalMode;

    //No focused entity
    session->focusent = -1;

	//No synchronization target entity
	session->syncent = -1;

    //Empty device event buffer
    buffer_reset(&session->ievt);

    //test
    session->radius = 32768 * 65535;
}

void modsession_sent(ModSession* session)
{
    //Reset device event buffer
    buffer_reset(&session->ievt);
}

uint32_t modsession_serialize(ModSession* modsess, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;

    memcpy(&buf[size], modsess->pos, sizeof(uint32_t[3]));
    size += sizeof(uint32_t[3]);
    memcpy(&buf[size], &modsess->radius, sizeof(uint32_t));
    size += sizeof(uint32_t);
    memcpy(&buf[size], &modsess->focusent, sizeof(Entity));
    size += sizeof(Entity);
    memcpy(&buf[size], &modsess->syncent, sizeof(Entity));
    size += sizeof(Entity);

    size += uxn_buffer_serialize(&modsess->ievt, style, &buf[size]);

    assert_or_exit(size <= SESSION_BYTES,
        "modsession_serialize: size exceeds SESSION_BYTES");

    return size; //TODO assert size fits
}

uint32_t modsession_deserialize(ModSession* modsess, SerialStyle style, uint8_t* buf)
{
    uint32_t size = 0;
    
    memcpy(modsess->pos, &buf[size], sizeof(uint32_t[3]));
    size += sizeof(uint32_t[3]);
    memcpy(&modsess->radius, &buf[size], sizeof(uint32_t));
    size += sizeof(uint32_t);
    memcpy(&modsess->focusent, &buf[size], sizeof(Entity));
    size += sizeof(Entity);
    memcpy(&modsess->syncent, &buf[size], sizeof(Entity));
    size += sizeof(Entity);

    size += uxn_buffer_deserialize(&modsess->ievt, style, &buf[size]);

    assert_or_exit(size <= SESSION_BYTES,
        "modsession_serialize: size exceeds SESSION_BYTES");

    return size; //TODO assert size fits
}

int capacity_config_total(CapacityConfig conf, int count)
{
    //Calculate additional capacity
    int more = count > 1 ? (count - 1) * conf.more : 0;

    //Calculate bonus capacity
    int bonus = 0;
    for(int i = 0; i < count; ++i){ bonus += i * conf.bonus; }

    return conf.base + more + bonus;
}

int syncents_add_upto(const SyncEnts* syncents, ECS* ecs, uint16_t* ents, int entsmax)
{
	assert_or_exit(entsmax >= 0, "syncents_add_upto: entsmax was negative\n");

	int found = 0;
	for(int i = 0; i < SYNC_ENT_MAX; ++i){

		//Check that the maximum number of results is not reached
		if(found >= entsmax){ break; }

		//Check that the entity is alive
		if(!ecs_entity_alive(ecs, syncents->ents[i])){ continue; }

		//Add the entity to the result
		ents[found++] = syncents->ents[i];
	}

	return found;
}

uint32_t syncents_serialize_to_signal(SyncEnts* syncents, uint8_t* buf)
{
	uint32_t size = 0;

	buf[size] = NAV_SIG_PROTO_SYNC_ENT;
	size += sizeof(uint8_t);
	buf[size] = 0;
	size += sizeof(uint8_t);

	for(int i = 0; i < SYNC_ENT_MAX; ++i){
		uint16_t u16 = htons(syncents->ents[i]);
		memcpy(&buf[size], &u16, sizeof(uint16_t));
		size += sizeof(uint16_t);
	}

	uint32_t u32 = htonl(syncents->sec);
	memcpy(&buf[size], &u32, sizeof(uint32_t));
	size += sizeof(uint32_t);

	return size;
}

uint32_t syncents_deserialize_from_signal(SyncEnts* syncents, uint8_t* buf)
{
	uint32_t size = 0;

	assert(buf[0] == NAV_SIG_PROTO_SYNC_ENT);

	size += sizeof(uint8_t);
	size += sizeof(uint8_t);

	for(int i = 0; i < SYNC_ENT_MAX; ++i){
		uint16_t u16 = ntohs(*(uint16_t*)(&buf[size]));
		memcpy(&syncents->ents[i], &u16, sizeof(uint16_t));
		size += sizeof(uint16_t);
	}
	
	uint32_t u32 = ntohl(*(uint32_t*)(&buf[size]));
	memcpy(&syncents->sec, &u32, sizeof(uint32_t));
	size += sizeof(uint32_t);

	return size;
}

uint16_t nav_range(const Nav* nav)
{
	return MIN(nav->range, NAV_MAX_RANGE);
}

void nav_set(Nav* nav, uint16_t motion, const uint32_t target[3], uint32_t distance)
{
    //Store navigation parameters
    nav->motion = motion;
    memcpy(nav->target, target, sizeof(uint32_t[3]));
    nav->distance = distance;
}

void nav_step(Nav* nav, Position* pos, Pointphys* pphys, float maxvel, float maxacc, float throttle)
{
    //Run navigation motion for the set motion type
    switch(nav->motion){
    case NAV_MOTION_APPROACH: {

        //Error, the vector from the target
        int32_t err[3]; v3uint32_diff(pos->pos, nav->target, err);
        int32_t errscale[3]; v3int32_scale_as(err, nav->distance, errscale);

        //Target point
        uint32_t target[3]; v3uint32_add(nav->target, errscale, target);

        //Desired velocity change
        int32_t deltavel[3];
        nav_deltav_between_pos_vel(
            pos->pos, pphys->vel,
            target, (int32_t[3]){ 0, 0, 0 },
            throttle * maxvel, throttle * maxacc,
            deltavel);
        vec3 deltav = { deltavel[0], deltavel[1], deltavel[2] };
        float deltav_norm = glm_vec3_norm(deltav);

        //Constrain with maximum acceleration
        float acc_len = MIN(maxacc, deltav_norm);

        //Accumulate only if the desired delta is large enough
        if(deltav_norm > VEL_MINDELTA){
            glm_vec3_scale_as(deltav, acc_len, deltav);
            for(int i = 0; i < 3; ++i){ pphys->acc[i] += deltav[i]; }
        }

    } break;
    case NAV_MOTION_RELATIVE: {

        //Target vector normalized to [-127, 127]
        int32_t veltarg[3];
        for(int i = 0; i < 3; ++i){
            veltarg[i] =
                (((int32_t)nav->target[i] / TWOPOW16) - (UXN_CONT_AXIS_CTR));
        }

        //Scale target vector with maximum velocity
        v3int32_scale_as(veltarg, maxvel * 2.0f, veltarg);

        //Desired velocity change
        int32_t deltavel[3];
        nav_deltav_between_pos_vel(
            pos->pos, pphys->vel,
            pos->pos, veltarg,
            throttle * maxvel, throttle * maxacc,
            deltavel);
        vec3 deltav = { deltavel[0], deltavel[1], deltavel[2] };
        float deltav_norm = glm_vec3_norm(deltav);

        //Constrain with maximum acceleration
        float acc_len = MIN(maxacc, deltav_norm);

        //Accumulate only if the desired delta is large enough
        if(deltav_norm > VEL_MINDELTA){
            glm_vec3_scale_as(deltav, acc_len, deltav);
            for(int i = 0; i < 3; ++i){ pphys->acc[i] += deltav[i]; }
        }

    } break;
    case NAV_MOTION_ORBIT: {

        vec3 vel = { pphys->vel[0], pphys->vel[1], pphys->vel[2] };

        //Error, the vector from the target
        int32_t err[3]; v3uint32_diff(pos->pos, nav->target, err);
        int32_t errscale[3]; v3int32_scale_as(err, nav->distance, errscale);

        //Target point
        uint32_t target[3]; v3uint32_add(nav->target, errscale, target);

        //Calculate orbital plane
        vec3 error = { err[0], err[1], err[2] };
        vec3 plane; glm_vec3_crossn(error, vel, plane);

        //If the orbital plane is ambiguous, choose one arbitrarily
        if(glm_vec3_norm(plane) < 0.5f){
            glm_vec3_copy((vec3){ 0.0f, 0.0f, 1.0f }, plane);
        }

        //Tangential vector
        vec3 tang; glm_vec3_crossn(plane, error, tang);

        //Centrifugal velocity
        float vtarg = powf(maxacc * nav->distance, 0.5f);
        vec3 cenf; glm_vec3_scale(tang, vtarg, cenf);
        int32_t cvel[3]; for(int i = 0; i < 3; ++i){ cvel[i] = cenf[i]; }

        //Desired velocity change
        int32_t deltavel[3];
        nav_deltav_between_pos_vel(
            pos->pos, pphys->vel,
            target, cvel,
            throttle * maxvel, throttle * maxacc,
            deltavel);
        vec3 deltav = { deltavel[0], deltavel[1], deltavel[2] };
        float deltav_norm = glm_vec3_norm(deltav);

        //Constrain with maximum acceleration
        float acc_len = MIN(maxacc, deltav_norm);

        //Accumulate only if the desired delta is large enough
        if(deltav_norm > VEL_MINDELTA){
            glm_vec3_scale_as(deltav, acc_len, deltav);
            for(int i = 0; i < 3; ++i){ pphys->acc[i] += deltav[i]; }
        }

    } break;
    case NAV_MOTION_IMPACT: {

        //Error, the vector from the target
        int32_t err[3]; v3uint32_diff(pos->pos, nav->target, err);
        int32_t errscale[3]; v3int32_scale_as(err, -1 * maxvel, errscale);

        //Desired velocity change
        int32_t deltavel[3];
        nav_deltav_between_pos_vel(
            pos->pos, pphys->vel,
            nav->target, errscale,
            throttle * maxvel, throttle * maxacc,
            deltavel);
        vec3 deltav = { deltavel[0], deltavel[1], deltavel[2] };
        float deltav_norm = glm_vec3_norm(deltav);

        //Constrain with maximum acceleration
        float acc_len = MIN(maxacc, deltav_norm);

        //Accumulate only if the desired delta is large enough
        if(deltav_norm > VEL_MINDELTA){
            glm_vec3_scale_as(deltav, acc_len, deltav);
            for(int i = 0; i < 3; ++i){ pphys->acc[i] += deltav[i]; }
        }

    } break;
    default: {} break;
    }
}

void nav_deltav_between_pos_vel(uint32_t pos1[3], int32_t vel1[3], uint32_t pos2[3], int32_t vel2[3], float maxvel, float maxacc, int32_t deltavel[3])
{
    //Current velocity & position
    vec3 vel = { vel1[0], vel1[1], vel1[2] };

    //Position error
    int32_t perr[3]; v3uint32_diff(pos2, pos1, perr);

    //Velocity error
    int32_t verr[3]; v3int32_diff(vel2, vel1, verr);

    //Ratio between max acceleration and max velocity
    float accpervel = maxacc / maxvel;

    //Aggregate velocity error
    vec3 verror;
    for(int i = 0; i < 3; ++i){
        verror[i] = perr[i] * powf(accpervel, accpervel) + verr[i];
    }

    //Constrain velocity error with maximum velocity
    float verror_len = glm_min(glm_vec3_norm(verror), maxvel);
    vec3 toward; glm_vec3_scale_as(verror, verror_len, toward);

    //Desired velocity delta
    vec3 deltav; glm_vec3_sub(toward, vel, deltav);
    for(int i = 0; i < 3; ++i){ deltavel[i] = deltav[i]; }
}

void pos_pphys_predict_impact(const Position* pos1, const Pointphys* pphys1, float maxvel, const Position* pos2, const Pointphys* pphys2, float delta, uint32_t pos[3])
{
    //Time-to-entity at max velocity
    int32_t diff[3]; v3uint32_diff(pos2->pos, pos1->pos, diff);
    float time2ent = v3int32_norm(diff) / maxvel;

    //Offset time-to-entity by a single timestep
    //time2ent += delta;
    time2ent += 0.1f;

    //Predicted position time-to-entity later
    int32_t fwd[3]; memcpy(fwd, pphys2->vel, sizeof(int32_t[3]));
    v3int32_scale_as(fwd, v3int32_norm(fwd) * time2ent, fwd);
    v3uint32_add(pos2->pos, fwd, pos);
}

void vat_cells_add(Vat* vat, cells_t cells[TROPH_COUNT])
{
    //Assert that the cell counts to be added fit into the capacity
    for(int i = 0; i < TROPH_COUNT; ++i){
        assert_or_exit(vat->cells[i] + cells[i] <= vat->cells_max[i],
            "vat_cells_add: attempt to add too many cells\n");
    }

    //Add cells
    for(int i = 0; i < TROPH_COUNT; ++i){ vat->cells[i] += cells[i]; }
}

void vat_cells_remove(Vat* vat, cells_t cells[TROPH_COUNT])
{
    //Assert that the cell counts to be removed are no greater than available
    for(int i = 0; i < TROPH_COUNT; ++i){
        assert_or_exit(vat->cells[i] >= cells[i],
            "vat_cells_remove: attempt to remove too many cells\n");
    }

    //Remove cells
    for(int i = 0; i < TROPH_COUNT; ++i){ vat->cells[i] -= cells[i]; }
}

void synth_mat_add(Synth* synth, SynthMats mat, mats_t count)
{
    //Assert that the mat count to be added fits into the capacity
    assert_or_exit(synth_mat_fit(synth, mat, count),
        "synth_mat_add: synth_mat_fit false");

    //Add mat
    synth->mats[mat] += count;
}

void synth_mat_del(Synth* synth, SynthMats mat, mats_t count)
{
    //Assert that the mat count to be deleted is available
    assert_or_exit(synth_mat_available(synth, mat, count),
        "synth_mat_del: synth_mat_available false");

    //Delete mat
    synth->mats[mat] -= count;
}

bool synth_mat_fit(Synth* synth, SynthMats mat, mats_t count)
{
    return synth->mats[mat] + count <= synth->mats_max[mat];
}

bool synth_mat_available(Synth* synth, SynthMats mat, mats_t count)
{
    return synth->mats[mat] >= count;
}

void synth_mats_add(Synth* synth, const mats_t mats[SYNTH_MAT_COUNT])
{
    for(int i = 0; i < SYNTH_MAT_COUNT; ++i){
        synth_mat_add(synth, i, mats[i]);
    }
}

void synth_mats_del(Synth* synth, const mats_t mats[SYNTH_MAT_COUNT])
{
    for(int i = 0; i < SYNTH_MAT_COUNT; ++i){
        synth_mat_del(synth, i, mats[i]);
    }
}

bool synth_mats_fit(Synth* synth, const mats_t mats[SYNTH_MAT_COUNT])
{
    bool success = true;
    for(int i = 0; i < SYNTH_MAT_COUNT; ++i){
        success = success && synth_mat_fit(synth, i, mats[i]);
    }
    return success;
}

bool synth_mats_available(Synth* synth, const mats_t mats[SYNTH_MAT_COUNT])
{
    bool success = true;
    for(int i = 0; i < SYNTH_MAT_COUNT; ++i){
        success = success && synth_mat_available(synth, i, mats[i]);
    }
    return success;
}

void hull_damage(Hull* hull, int hit)
{
    //Decrease hitpoints
    hull->hp = MAX(0, hull->hp - hit);
}

void hull_repair(Hull* hull, int hit)
{
    //Increase hitpoints
    hull->hp = MIN(hull->hp_max, hull->hp + hit);
}

bool hull_destroyed(const Hull* hull)
{
    return hull->hp == 0;
}

bool hull_undamaged(const Hull* hull)
{
    return hull->hp == hull->hp_max;
}

void bay_restock(Bay* bay, int ammo)
{
    //Increase ammo
    bay->ammo = MIN(bay->ammo_max, bay->ammo + ammo);
}

bool bay_full(Bay* bay)
{
    return bay->ammo == bay->ammo_max;
}

bool growth_add(Growth* growth, CompoID id, ECS* ecs, Entity ent)
{
    const Type* type = ecs_get_id(ecs, ent, TypeID);

    assert_or_exit(id >= 0 && id < COMPO_COUNT, "growth_add: invalid type\n");
    assert_or_exit(MODULE_GROWABLE[type->type][id], "growth_add: fixed\n");
    assert_or_exit(ecs_get_id(ecs, ent, id), "growth_add: missing\n");

    //Check that the module count fits into the maximum
    if(growth->total >= growth->max){ return false; }

    //Get module count
    uint8_t* count = NULL;
    switch(id){
    case NavID: {
        count = &((Nav*)ecs_get_id(ecs, ent, id))->count; } break;
    case DriveID: {
        count = &((Drive*)ecs_get_id(ecs, ent, id))->count; } break;
    case VatID: {
        count = &((Vat*)ecs_get_id(ecs, ent, id))->count; } break;
    case ExtractID: {
        count = &((Extract*)ecs_get_id(ecs, ent, id))->count; } break;
    case SynthID: {
        count = &((Synth*)ecs_get_id(ecs, ent, id))->count; } break;
    case ComputeID: {
        count = &((Compute*)ecs_get_id(ecs, ent, id))->count; } break;
    case HullID: {
        count = &((Hull*)ecs_get_id(ecs, ent, id))->count; } break;
    case BayID: {
        count = &((Bay*)ecs_get_id(ecs, ent, id))->count; } break;
    default: { assert_or_exit(false, "growth_add"); }
    }

    //Increment module count
    ++(*count);

    //Update growth
    growth_update(growth, ecs, ent);

    return true;
}

bool growth_del(Growth* growth, CompoID id, ECS* ecs, Entity ent)
{
    const Type* type = ecs_get_id(ecs, ent, TypeID);

    assert_or_exit(id >= 0 && id < COMPO_COUNT, "growth_add: invalid type\n");
    assert_or_exit(MODULE_GROWABLE[type->type][id], "growth_add: fixed\n");
    assert_or_exit(ecs_get_id(ecs, ent, id), "growth_add: missing\n");

    //Get module count
    uint8_t* count = NULL;
    switch(id){
    case NavID: {
        count = &((Nav*)ecs_get_id(ecs, ent, id))->count; } break;
    case DriveID: {
        count = &((Drive*)ecs_get_id(ecs, ent, id))->count; } break;
    case VatID: {
        count = &((Vat*)ecs_get_id(ecs, ent, id))->count; } break;
    case ExtractID: {
        count = &((Extract*)ecs_get_id(ecs, ent, id))->count; } break;
    case SynthID: {
        count = &((Synth*)ecs_get_id(ecs, ent, id))->count; } break;
    case ComputeID: {
        count = &((Compute*)ecs_get_id(ecs, ent, id))->count; } break;
    case HullID: {
        count = &((Hull*)ecs_get_id(ecs, ent, id))->count; } break;
    case BayID: {
        count = &((Bay*)ecs_get_id(ecs, ent, id))->count; } break;
    default: { assert_or_exit(false, "growth_add"); }
    }

    //Check that the module count would not fall below the minimum
    if(*count <= COMPO_MIN_COUNT[type->type][id]){ return false; }

    //Increment module count
    --(*count);

    //Update growth
    growth_update(growth, ecs, ent);

    return true;
}

void growth_bonus_add(Growth* growth, uint16_t points)
{
    assert_or_exit(growth->bonus_acc + points <= growth->bonus_max,
        "growth_bonus_add: exceeds upper bound\n");

    growth->bonus_acc += points;
}

int artifact_points(Position* pos1, Position* pos2)
{
    uint32_t points = 0;

    int32_t diff[3]; v3uint32_diff(pos1->pos, pos2->pos, diff);

    //Number of points that an artifact is worth
    points = ((uint32_t)v3int32_norm(diff)) >> 24 >> 1;

    assert_or_exit(points >= 0,
        "artifact_points: negative points");

    return points;
}

const float TROPH_RATIO[TROPH_COUNT] = {
    [TROPH_DEAD] = 0.0f,
    [TROPH_AUTO] = 0.65f,
    [TROPH_HETE] = 0.35f,
};

const float TROPH_WEATH_PARAM[TROPH_WEATH_COUNT][TROPH_PARAM_COUNT] = {
    [TROPH_WEATH_CALM] = { 0.0f, 0.0f, 0.0f, 0.0f },
    [TROPH_WEATH_AUTO_DISEASE] = { 0.02f, 0.06f, 0.0f, 0.0f },
    [TROPH_WEATH_HETE_DISEASE] = { 0.0f, 0.0f, 0.02f, 0.06f },
};

void stepper_init(Stepper* stepper, float frequency)
{
    stepper->wavelength = 1.0f / frequency;
    stepper->remainder = 0.0f;
}

void stepper_stagger(Stepper* stepper, float frequency, float rand_norm)
{
    stepper_init(stepper, frequency);
    stepper->remainder = stepper->wavelength * rand_norm;
}

int stepper_step(Stepper* stepper, float delta)
{
    int ret = 0;

    stepper->remainder += delta;
    while(stepper->remainder > stepper->wavelength){
        ++ret;
        stepper->remainder -= stepper->wavelength;
    }

    return ret;
}

void trophs_initialize(Trophs* trophs, uint8_t width, float params[TROPH_PARAM_COUNT], float params_rand_range[TROPH_PARAM_COUNT], uint32_t seed[3], float weather_mul)
{
    assert_or_exit(width * width <= TROPHS_CELLS_MAX,
        "trophs_initialize: width exceeds allowed maximum");

    //Store parameters
    trophs->width = width;
    trophs->weather_mul = weather_mul;
    memcpy(trophs->romu_trio32, seed, sizeof(uint32_t[3]));

    //Neighborhood offsets
    //[0, 4): Von-Neumann, [0, 8): Moore
    memcpy(trophs->neigh,
        &(int8_t[8][2]){ { -1,  0 }, { +1,  0 }, { 0,  -1 }, { 0,  +1 },
                      { -1, -1 }, { +1, -1 }, { -1, +1 }, { +1, +1 }, },
        sizeof(int8_t[8][2]));

    //Offset neighborhoods by the width so that
    //remainder operators ('%') will acts as modulus
    for(int i = 0; i < 4; ++i){
        trophs->neigh[i][0] += trophs->width;
        trophs->neigh[i][1] += trophs->width;
    }

    //Randomize parameters within the range received
    float param_offset[TROPH_PARAM_COUNT];
    for(int i = 0; i < TROPH_PARAM_COUNT; ++i){
        param_offset[i] = glm_lerp(
            -params_rand_range[i], params_rand_range[i],
            romu_trio32_norm(trophs->romu_trio32)
        );
    }

    //Autotroph & heterotroph, reproduction & death rate parameters
    for(int i = 0; i < TROPH_PARAM_COUNT; ++i){
        trophs->params[i] = params[i] + param_offset[i];
    }
    trophs->hete_hunt = 2;

    //Current grid
    trophs->grid = 0;

    //Initialize cells to dead
    memset(trophs->cells[trophs->grid], TROPH_DEAD, TROPHS_CELLS_MAX);
    trophs_count(trophs);

    //Calculate sampling population thresholds for each cell type
    for(int i = 0; i < TROPH_COUNT; ++i){
        trophs->threshold[i] = trophs->width * trophs->width *
            TROPHS_SAMPLE_THRESHOLD * TROPH_RATIO[i];
    }

    //Weather
    trophs->weather = TROPH_WEATH_CALM;
    trophs->weather_pass = trophs->weather_full = 0.0f;

    //Initialize stepper
    stepper_stagger(&trophs->stepper, TROPHS_STEP_FREQ,
        romu_trio32_norm(trophs->romu_trio32));
}

void trophs_count(Trophs* trophs)
{
    uint8_t* src = trophs->cells[trophs->grid];

    //Aggregate the numbers of each cell type
    for(int i = 0; i < TROPH_COUNT; ++i){ trophs->cellcount[i] = 0; }
    for(int i = 0; i < trophs->width * trophs->width; ++i){
        ++trophs->cellcount[src[i]];
    }
}

void trophs_inject(Trophs* trophs, cells_t counts[TROPH_COUNT], cells_t injected_out[TROPH_COUNT])
{
    //Assert that dead cells will not be injected
    assert_or_exit(counts[TROPH_DEAD] == 0, "trophs_inject: cannot inject dead cells\n");

    //Assert that the cell counts sum up to the total
    int cellcounts = 0;
    for(int i = 0; i < TROPH_COUNT; ++i){ cellcounts += trophs->cellcount[i]; }
    assert_or_exit(cellcounts == trophs->width * trophs->width,
        "trophs_inject: cell counts do not sum up to the total\n");

    //Non-dead cells that could be injected
    int nondead = 0;
    for(int i = 0; i < TROPH_COUNT; ++i){
        nondead += (i != TROPH_DEAD) * counts[i];
    }

    //Amount of cells that will be injected
    int injected = MIN(nondead, trophs->cellcount[TROPH_DEAD]);

    //Amount of each cell that should be injected
    int cells_ideal[TROPH_COUNT];
    for(int i = 0; i < TROPH_COUNT; ++i){
        cells_ideal[i] = TROPH_RATIO[i] * injected;
    }

    //Amount of each cell that will be injected
    int cells_inj[TROPH_COUNT];
    for(int i = 0; i < TROPH_COUNT; ++i){
        cells_inj[i] = MIN(cells_ideal[i], counts[i]);
    }

    //Reset cell count actually injected
    for(int i = 0; i < TROPH_COUNT; ++i){ injected_out[i] = 0; }

    //Inject cells in multiple runs to improve spread
    int runs = 2;
    for(int k = 0; k < runs; ++k){
        for(int i = 0; i < TROPH_COUNT; ++i){

            injected_out[i] +=
                trophs_change_cell(trophs, TROPH_DEAD, i, cells_inj[i] / runs);
        }
    }

    //Update trophs state
    trophs_count(trophs);
}

void trophs_sample(Trophs* trophs, cells_t counts[TROPH_COUNT], cells_t sampled_out[TROPH_COUNT])
{
    //Assert that dead cells will not be sampled
    assert_or_exit(counts[TROPH_DEAD] == 0,
        "trophs_sample: cannot sample dead cells\n");

    //Assert that the cell counts sum up to the total
    int cellcounts = 0;
    for(int i = 0; i < TROPH_COUNT; ++i){ cellcounts += trophs->cellcount[i]; }
    assert_or_exit(cellcounts == trophs->width * trophs->width,
        "trophs_inject: cell counts do not sum up to the total\n");

    //Amount of each cell that could be sampled
    int cells_avail[TROPH_COUNT];
    for(int i = 0; i < TROPH_COUNT; ++i){
        cells_avail[i] = MAX(trophs->cellcount[i] - trophs->threshold[i], 0);
    }

    //Amount of each cell that will be sampled
    int cells_sam[TROPH_COUNT];
    for(int i = 0; i < TROPH_COUNT; ++i){
        cells_sam[i] = MIN(cells_avail[i], counts[i]);
    }

    //Sample cells
    for(int i = 0; i < TROPH_COUNT; ++i){
        sampled_out[i] = trophs_change_cell(trophs, i, TROPH_DEAD, cells_sam[i]);
    }

    //Update trophs state
    trophs_count(trophs);
}

int trophs_change_cell(Trophs* trophs, TrophCell source, TrophCell target, int count)
{
    //Assert that the count of cells to change is nonnegative
    assert_or_exit(count >= 0, "trophs_change_cell: negative cell count\n");

    uint8_t* src = trophs->cells[trophs->grid];

    //Return number of cells actually changed
    int ret = 0;

    int i = 0;
    while(i < trophs->width * trophs->width && ret < count){
        if(src[i] == source){ src[i] = target; ++ret; }
        ++i;
    }

    return ret;
}

void trophs_step(Trophs* trophs, float delta)
{
    //Pointer to cell
    uint8_t* new = trophs->cells[1 - trophs->grid];
    uint8_t* old = trophs->cells[trophs->grid];

    //Keep progressing weather
    trophs->weather_pass += delta;

    //If a weather event has played out in full, get next
    if(trophs->weather_pass >= trophs->weather_full){

        //Randomize next weather
        trophs->weather =
            romu_trio32_random(trophs->romu_trio32) % TROPH_WEATH_COUNT;
        trophs->weather_full = glm_lerp(
            TROPHS_WEATHER_MIN_LEN, TROPHS_WEATHER_MAX_LEN,
            romu_trio32_norm(trophs->romu_trio32)
        );

        //Reset current weather length
        trophs->weather_pass = 0.0f;
    }

    //Stochastic parameters
    float params[TROPH_PARAM_COUNT];
    for(int i = 0; i < TROPH_PARAM_COUNT; ++i){
        params[i] = trophs->params[i] +
            trophs->weather_mul * TROPH_WEATH_PARAM[trophs->weather][i];
    }

    int width = trophs->width;

    //Fill new grid with dead cells initially
    memset(new, TROPH_DEAD, width * width);

    //Autotroph to heterotroph, heterotroph to dead
    for(int i = 0; i < width; ++i){ int row_off = i * width;
    for(int j = 0; j < width; ++j){

        switch(old[row_off + j]){
        case TROPH_AUTO: {

            uint16_t moore = 0;
            for(int k = 0; k < 8; ++k){
                moore += 0x1 << (
                    (old[
                        ((i + trophs->neigh[k][0]) % width) * width +
                        ((j + trophs->neigh[k][1]) % width)
                    ]) * 4
                );
            }
            
            //Autotroph to heterotroph
            if(
                ((moore & 0x0f00) >> 8) >= (uint8_t)trophs->hete_hunt &&
                romu_trio32_norm(trophs->romu_trio32) < params[TROPH_HETE_REPRO]
            ){
                old[row_off + j] = 0;
                new[row_off + j] = TROPH_HETE;
            } else {
                new[row_off + j] = TROPH_AUTO;
            }

        } break;
        case TROPH_HETE: {

            //Heterotroph to dead
            new[row_off + j] =
                romu_trio32_norm(trophs->romu_trio32) <
                params[TROPH_HETE_DEATH] ?
                    TROPH_DEAD : TROPH_HETE;

        } break;
        default: {} break;
        }
    }}

    //Dead to autotroph, autotroph to dead
    for(int i = 0; i < width; ++i){ int row_off = i * width;
    for(int j = 0; j < width; ++j){

        switch(old[row_off + j]){
        case TROPH_AUTO: {

            //Autotroph reproduction
            if(romu_trio32_norm(trophs->romu_trio32) < params[TROPH_AUTO_REPRO]){

                //Get index of random empty neighbor
                int k_off = romu_trio32_random(trophs->romu_trio32) % 4;

                int k = k_off; int kmod = k % 4;
                while(
                    k < k_off + 4 &&
                    old[
                        ((i + trophs->neigh[kmod][0]) % width) * width +
                        ((j + trophs->neigh[kmod][1]) % width)
                    ] != TROPH_DEAD
                ){ ++k; kmod = k % 4; }

                //If there is an empty neighbor, reproduce there
                if(k < k_off + 4){
                    new[
                        ((i + trophs->neigh[kmod][0]) % width) * width +
                        ((j + trophs->neigh[kmod][1]) % width)
                    ] = TROPH_AUTO;
                }
            }

            //Autotroph to dead
            new[row_off + j] =
                romu_trio32_norm(trophs->romu_trio32) <
                params[TROPH_AUTO_DEATH] ?
                    TROPH_DEAD : TROPH_AUTO;

        } break;
        default: {} break;
        }
    }}

    //Set pointer to currently active cell grid
    trophs->grid = 1 - trophs->grid;

    //Update count
    trophs_count(trophs);
}

void trophs_convert_rgba(const Trophs* trophs, uint8_t* dest, vec4 colors[4])
{
    convert_rgba_2bitsperbyte(dest, trophs->cells[trophs->grid],
        trophs->width * trophs->width, colors);
}

void maze_initialize(Maze* maze, Hash1* hash, int width, uint32_t seed[3])
{
    assert_or_exit(width >= 0, "maze_initialize: width negative\n");

    //Check that the grid size is even so that it may be compressed 4x
    assert_or_exit(width % 2 == 0, "maze_initialize: width not even\n");

    //Store parameters
    maze->width = width;
    memcpy(maze->romu_trio32, seed, sizeof(uint32_t[3]));

    //Initialize values
    maze->stacksize = 0;

    //Neighborhood offsets
    //[0, 4): Von-Neumann
    memcpy(maze->neigh,
        &(int8_t[4][2]){ { -1,  0 }, { +1,  0 }, { 0,  -1 }, { 0,  +1 }, },
        sizeof(int8_t[4][2]));

    maze_reset(maze);
}

void maze_reset(Maze* maze)
{
    //Initialize maze to completely blocked
    memset(maze->cells, MAZE_BLOCK, maze->width * maze->width);

    //Push starting cell, mark it as visited
    uint8_t first[2] = { 0, 0 };
    maze_push(maze, first);
    maze_set(maze, maze->cells, first, MAZE_EMPTY);
}

void maze_push(Maze* maze, uint8_t pos[2])
{
    assert(maze->stacksize + 2 <= maze->width * maze->width * 2);

    maze->stack[maze->stacksize++] = pos[0];
    maze->stack[maze->stacksize++] = pos[1];
}

void maze_pop(Maze* maze, uint8_t pos[2])
{
    assert(maze->stacksize - 2 >= 0);

    pos[1] = maze->stack[--maze->stacksize];
    pos[0] = maze->stack[--maze->stacksize];
}

void maze_set(Maze* maze, uint8_t* dest, uint8_t pos[2], uint8_t byte)
{
    assert(pos[0] >= 0 && pos[0] < maze->width);
    assert(pos[1] >= 0 && pos[1] < maze->width);

    dest[pos[0] * maze->width + pos[1]] = byte;
}

void maze_generate(Maze* maze, int steps)
{
    uint8_t curr[2];

    while(maze->stacksize > 0 && steps-- > 0){

        //Pop a cell from the stack, make it the current one
        maze_pop(maze, curr);

        //If the current cell has any unvisited (blocked) neighbors
        int count; uint8_t bneigh[4][2];
        maze_cell_neigh_type_get(maze,
            maze->cells, curr[0], curr[1], MAZE_BLOCK, bneigh, &count);
        if(count > 0){

            //Push the current cell to the stack
            maze_push(maze, curr);

            //Choose one of the unvisited (blocked) neighbors
            int neigh = romu_trio32_random(maze->romu_trio32) % count;

            //Remove wall between current cell and chosen cell
            int between[2] = { (curr[0] + (int)bneigh[neigh][0]) / 2,
                               (curr[1] + (int)bneigh[neigh][1]) / 2, };
            maze_set(maze, maze->cells,
                (uint8_t[2]){ between[0], between[1] }, MAZE_EMPTY);

            //Mark the chosen cell as visited
            maze_set(maze, maze->cells, bneigh[neigh], MAZE_EMPTY);

            //Push the chosen cell to the stack
            maze_push(maze, bneigh[neigh]);
        }
    }
}

void maze_cell_neigh_type_get(Maze* maze, uint8_t* src, uint8_t x, uint8_t y, MazeCell type, uint8_t out[4][2], int* count)
{
    assert(x >= 0 && x < maze->width);
    assert(y >= 0 && y < maze->width);

    uint8_t l = (x > 1) ?
        src[(x - 2) * maze->width + y] : MAZE_COUNT;
    uint8_t r = (x < maze->width - 2) ?
        src[(x + 2) * maze->width + y] : MAZE_COUNT;
    uint8_t t = (y > 1) ?
        src[x * maze->width + (y - 2)] : MAZE_COUNT;
    uint8_t b = (y < maze->width - 2) ?
        src[x * maze->width + (y + 2)] : MAZE_COUNT;

    *count = 0;

    if(l == type){ out[*count][0] = x - 2; out[*count][1] = y; ++*count; }
    if(r == type){ out[*count][0] = x + 2; out[*count][1] = y; ++*count; }
    if(t == type){ out[*count][0] = x; out[*count][1] = y - 2; ++*count; }
    if(b == type){ out[*count][0] = x; out[*count][1] = y + 2; ++*count; }
}

void hive_add_barren(Hive* hive, uint16_t barren, ECS* ecs)
{
    assert_or_exit(hive->guarded < BARREN_PER_HIVE,
        "hive_add_barren: capacity full");
    assert_or_exit(ecs_entity_alive(ecs, barren),
        "hive_add_barren: dead entity");

    const Barren* barr = ecs_get_id(ecs, barren, BarrenID);
    assert_or_exit(barr,
        "hive_add_barren: barren missing component");

    //Add the barren world to the hive
    hive->barrens[hive->guarded] = barren;

    //Launch delay, in seconds
    hive->delays[hive->guarded] =
        (WASP_VISIT_SEC / (float)barr->richness) / HIVE_COVER_RATIO;

    //Launch cooldown
    hive->cooldowns[hive->guarded] = 0.0f;

    ++hive->guarded;
}

void wasp_send_journey(Wasp* wasp, Entity hive, Entity barren, uint32_t seed[3])
{
    wasp->state = WASP_STT_JOURNEY;
    memcpy(wasp->romu_trio32, seed, sizeof(uint32_t[3]));
    wasp->hive = hive;
    wasp->barren = barren;
    wasp->ranged = WASP_RANGED_TIME;
}

void nav_reset(void* compo, ECS* ecs, Entity ent)
{
    Nav* nav = (Nav*)compo;

    const Type* type = ecs_get_id(ecs, ent, TypeID);

    nav->count = COMPO_MIN_COUNT[type->type][NavID];
    nav->motion = NAV_MOTION_NONE;
	nav->range = 0;
    nav->target[0] = nav->target[1] = nav->target[2] = 0;
    memset(nav->signal, 0, SIGNAL_BYTES);
	nav->syncents.sec = 0;
	for(int i = 0; i < SYNC_ENT_MAX; ++i){
		nav->syncents.ents[i] = 0xffff;
	}
}

void nav_update(void* compo, ECS* ecs, Entity ent)
{
	Nav* nav = (Nav*)compo;

	//Calculate capacity
	nav->range = capacity_config_total(NAV_CAPA_CONF, nav->count);
}

void drive_reset(void* compo, ECS* ecs, Entity ent)
{
    Drive* drive = (Drive*)compo;

    const Type* type = ecs_get_id(ecs, ent, TypeID);

    drive->count = COMPO_MIN_COUNT[type->type][DriveID];
    drive->throttle = 0xff;
    drive->twr = 0.0f;
    drive->maxvel = drive->maxacc = 0;
}

void drive_update(void* compo, ECS* ecs, Entity ent)
{
    Drive* drive = (Drive*)compo;

    //Total mass
    uint32_t mass = 0;
    uint8_t counts[COMPO_COUNT];
    entity_count_compos(ecs, ent, counts);
    for(int i = 0; i < COMPO_COUNT; ++i){ mass += counts[i]; }
    
    //Thrust to weight is the total thrust divided by the total mass
    drive->twr = (drive->count * DRIVE_THRUST_UNITS) / mass;

    //Maximum velocity and acceleration
    drive->maxvel = powf(drive->twr, POW_TWR) * VEL_UNITTWR;
    drive->maxacc = drive->twr * ACC_UNITTWR;
}

void vat_reset(void* compo, ECS* ecs, Entity ent)
{
    Vat* vat = (Vat*)compo;

    const Type* type = ecs_get_id(ecs, ent, TypeID);

    vat->count = COMPO_MIN_COUNT[type->type][VatID];
    for(int i = 0; i < TROPH_COUNT; ++i){
        vat->cells[i] = vat->cells_max[i] = 0;
    }
}

void vat_update(void* compo, ECS* ecs, Entity ent)
{
    Vat* vat = (Vat*)compo;

    //Calculate capacity
    for(int i = 0; i < TROPH_COUNT; ++i){
        vat->cells_max[i] =
            capacity_config_total(VAT_CAPA_CONF, vat->count);
    }
}

void extract_reset(void* compo, ECS* ecs, Entity ent)
{
    Extract* ext = (Extract*)compo;

    const Type* type = ecs_get_id(ecs, ent, TypeID);

    ext->count = COMPO_MIN_COUNT[type->type][ExtractID];
    ext->state = EXTRACT_STT_IDLE;
    ext->timer = 0.0f;
}

void synth_reset(void* compo, ECS* ecs, Entity ent)
{
    Synth* synth = (Synth*)compo;

    const Type* type = ecs_get_id(ecs, ent, TypeID);

    synth->count = COMPO_MIN_COUNT[type->type][SynthID];
    synth->state = SYNTH_STT_IDLE;
    synth->timer = 0.0f;
    for(int i = 0; i < SYNTH_MAT_COUNT; ++i){
        synth->mats[i] = synth->mats_max[i] = 0;
    }
}

void synth_update(void* compo, ECS* ecs, Entity ent)
{
    Synth* synth = (Synth*)compo;

    //Calculate capacity
    for(int i = 0; i < SYNTH_MAT_COUNT; ++i){
        synth->mats_max[i] = capacity_config_total(SYNTH_CAPA_CONF, synth->count);
    }
}

void compute_reset(void* compo, ECS* ecs, Entity ent)
{
    Compute* com = (Compute*)compo;

    const Type* type = ecs_get_id(ecs, ent, TypeID);

    com->count = COMPO_MIN_COUNT[type->type][ComputeID];
    com->frequency = com->received = com->received_pre = 0;
}

void compute_update(void* compo, ECS* ecs, Entity ent)
{
    Compute* com = (Compute*)compo;

    //Calculate capacity
    com->frequency = capacity_config_total(COMPUTE_CAPA_CONF, com->count);
}

void hull_reset(void* compo, ECS* ecs, Entity ent)
{
    Hull* hull = (Hull*)compo;
    
    const Type* type = ecs_get_id(ecs, ent, TypeID);

    hull->count = COMPO_MIN_COUNT[type->type][HullID];
    hull->hp = hull->hp_max = 0;
}

void hull_update(void* compo, ECS* ecs, Entity ent)
{
    Hull* hull = (Hull*)compo;

    //Calculate capacity
    hull->hp_max = capacity_config_total(HULL_CAPA_CONF, hull->count);

    //TODO: is this really okay?
    hull->hp = hull->hp_max;
}

void bay_reset(void* compo, ECS* ecs, Entity ent)
{
    Bay* bay = (Bay*)compo;

    const Type* type = ecs_get_id(ecs, ent, TypeID);

    bay->count = COMPO_MIN_COUNT[type->type][BayID];
    bay->state = BAY_STT_IDLE;
    bay->timer = 0.0f;
    bay->ammo = bay->ammo_max = 0;
}

void bay_update(void* compo, ECS* ecs, Entity ent)
{
    Bay* bay = (Bay*)compo;

    //Calculate capacity
    bay->ammo_max = capacity_config_total(BAY_CAPA_CONF, bay->count);
}

void cargo_reset(void* compo, ECS* ecs, Entity ent)
{
    Cargo* cargo = (Cargo*)compo;

    //Reset held artifact
    cargo->artifact = ART_NONE;
}

void cargo_update(void* compo, ECS* ecs, Entity ent)
{
    
}

void growth_reset(void* compo, ECS* ecs, Entity ent)
{
    Growth* growth = (Growth*)compo;

    const Type* type = ecs_get_id(ecs, ent, TypeID);

    //Reset maximum module count
    growth->max = COMPO_MAX_COUNT[type->type];

    //Reset bonus accumulator
    growth->bonus_acc = 0;

    //Increment bonus accumulator to gain one bonus point
    growth->bonus_acc = GROWTH_BONUS_PTS[0];

    //Set maximum counter of points towards bonus
    growth->bonus_max = GROWTH_BONUS_PTS[GROWTH_BONUS_COUNT - 1];
}

void growth_update(void* compo, ECS* ecs, Entity ent)
{
    Growth* growth = (Growth*)compo;

    const Type* type = ecs_get_id(ecs, ent, TypeID);

    //Update maximum module count
    growth->max = COMPO_MAX_COUNT[type->type];
    for(int i = 0; i < GROWTH_BONUS_COUNT; ++i){
        growth->max += (growth->bonus_acc >= GROWTH_BONUS_PTS[i]);
    }

    //Update total module count
    uint8_t counts[COMPO_COUNT]; entity_count_compos(ecs, ent, counts);
    growth->total = 0;
    for(int i = 0; i < COMPO_COUNT; ++i){ growth->total += counts[i]; }
}

Entity wasp_generate(ECS* ecs, uint32_t spawn[3], Entity hive, Entity barren, uint32_t seed[3])
{
    if(ecs->entnext >= ecs->entsmax){ return 0xffff; }

    //Add entity
    Entity ent = ecs_entity_add(ecs);

    //Add & init type component
    Type* typec = ecs_get_mut_id(ecs, ent, TypeID);
    memcpy(typec, &(Type){ .type = ENTTYPE_WASP }, sizeof(Type));

    //Add & init position component
    Position* pos = ecs_get_mut_id(ecs, ent, PositionID);
    memcpy(pos->pos, spawn, sizeof(uint32_t[3]));

    //Add pointphys component
    ecs_get_mut_id(ecs, ent, PointphysID);

    //Add & init wasp component
    Wasp* wasp = ecs_get_mut_id(ecs, ent, WaspID);
    wasp->state = WASP_STT_JOURNEY;
    memcpy(wasp->romu_trio32, seed, sizeof(uint32_t[3]));
    wasp->hive = hive;
    wasp->barren = barren;
    wasp->ranged = WASP_RANGED_TIME;

    //Add components
    ecs_get_mut_id(ecs, ent, NavID);
    ecs_get_mut_id(ecs, ent, DriveID);
    ecs_get_mut_id(ecs, ent, SynthID);
    ecs_get_mut_id(ecs, ent, HullID);

    //Reset & update components
    entity_reset_compos(ecs, ent);
    entity_update_compos(ecs, ent);

    return ent;
}

Entity missile_generate(ECS* ecs, uint32_t spawn[3], int32_t vel[3], uint16_t target, Type sender)
{
    if(ecs->entnext >= ecs->entsmax){ return 0xffff; }

    //Add entity
    Entity ent = ecs_entity_add(ecs);

    //Add & init type component
    Type* typec = ecs_get_mut_id(ecs, ent, TypeID);
    memcpy(typec, &(Type){ .type = ENTTYPE_MISSILE }, sizeof(Type));

    //Add & init position component
    Position* pos = ecs_get_mut_id(ecs, ent, PositionID);
    memcpy(pos->pos, spawn, sizeof(uint32_t[3]));

    //Add pointphys component
    Pointphys* pphys = ecs_get_mut_id(ecs, ent, PointphysID);
    memcpy(pphys->vel, vel, sizeof(int32_t[3]));

    //Add & init missile component
    Missile* miss = ecs_get_mut_id(ecs, ent, MissileID);
    miss->state = MISS_STT_FLY;
    miss->timer = MISS_EXPIRY_TIME;
    miss->target = target;
    memcpy(&miss->sender, &sender, sizeof(Type));

    //Add components
    ecs_get_mut_id(ecs, ent, NavID);
    ecs_get_mut_id(ecs, ent, DriveID);
    ecs_get_mut_id(ecs, ent, HullID);

    //Reset & update components
    entity_reset_compos(ecs, ent);
    entity_update_compos(ecs, ent);

    return ent;
}

Entity wreckage_generate(ECS* ecs, ECS* ecs_orig, Entity orig, uint32_t spawn[3], Type* type, Nav* nav)
{
    if(ecs->entnext >= ecs->entsmax){ return 0xffff; }

    //Add entity
    Entity ent = ecs_entity_add(ecs);

    //Add & init type component
    Type* typec = ecs_get_mut_id(ecs, ent, TypeID);
    memcpy(typec, &(Type){ .type = ENTTYPE_WRECKAGE }, sizeof(Type));

    //Add & init position component
    Position* pos = ecs_get_mut_id(ecs, ent, PositionID);
    memcpy(pos->pos, spawn, sizeof(uint32_t[3]));

    //Add & init wreckage component
    Wreckage* wreck = ecs_get_mut_id(ecs, ent, WreckageID);
    wreck->timer = WRECK_DECAY_TIME;

    //Add & init nav component
    Nav* nav2 = ecs_get_mut_id(ecs, ent, NavID);
    memcpy(nav2, nav, sizeof(Nav));

    //Get component counts
    uint8_t counts[COMPO_COUNT];
    entity_count_compos(ecs_orig, orig, counts);

    //Get module counts above the minimum for that entity
    uint8_t abovecount[COMPO_COUNT];
    for(int i = 0; i < COMPO_COUNT; ++i){
        abovecount[i] = counts[i] - COMPO_MIN_COUNT[type->type][i];
    }

    //Calculate materials required to grow those modules
    mats_t mats[SYNTH_MAT_COUNT] = {0};
    for(int i = 0; i < COMPO_COUNT; ++i){
        for(int j = 0; j < SYNTH_MAT_COUNT; ++j){
            mats[j] += abovecount[i] * COMPO_MAT_COST[i][j];
        }
    }

    //Add & init synth module
    Synth* synth = ecs_get_mut_id(ecs, ent, SynthID);
    synth_reset(synth, ecs, ent);
    for(int i = 0; i < SYNTH_MAT_COUNT; ++i){
        synth->mats_max[i] = mats[i] * MODULE_MAT_ABSORB;
        synth->mats[i] = 0;
    }
    synth_mats_add(synth, synth->mats_max);

    return ent;
}

void entity_count_compos(ECS* ecs, Entity ent, uint8_t counts[COMPO_COUNT])
{
    //Reset module counts to zero initially
    for(int i = 0; i < COMPO_COUNT; ++i){ counts[i] = 0; }

    const Nav* nav = ecs_get_id(ecs, ent, NavID);
    if(nav){ counts[NavID] = nav->count; }

    const Drive* drive = ecs_get_id(ecs, ent, DriveID);
    if(drive){ counts[DriveID] = drive->count; }

    const Vat* vat = ecs_get_id(ecs, ent, VatID);
    if(vat){ counts[VatID] = vat->count; }

    const Extract* ext = ecs_get_id(ecs, ent, ExtractID);
    if(ext){ counts[ExtractID] = ext->count; }

    const Synth* synth = ecs_get_id(ecs, ent, SynthID);
    if(synth){ counts[SynthID] = synth->count; }
    
    const Compute* com = ecs_get_id(ecs, ent, ComputeID);
    if(com){ counts[ComputeID] = com->count; }

    const Hull* hull = ecs_get_id(ecs, ent, HullID);
    if(hull){ counts[HullID] = hull->count; }

    const Bay* bay = ecs_get_id(ecs, ent, BayID);
    if(bay){ counts[BayID] = bay->count; }
}

void entity_reset_compos(ECS* ecs, Entity ent)
{
    Nav* nav = (Nav*)ecs_get_id(ecs, ent, NavID);
    if(nav){ nav_reset(nav, ecs, ent); }

    Vat* vat = (Vat*)ecs_get_id(ecs, ent, VatID);
    if(vat){ vat_reset(vat, ecs, ent); }

    Extract* ext = (Extract*)ecs_get_id(ecs, ent, ExtractID);
    if(ext){ extract_reset(ext, ecs, ent); }

    Synth* synth = (Synth*)ecs_get_id(ecs, ent, SynthID);
    if(synth){ synth_reset(synth, ecs, ent); }
    
    Compute* com = (Compute*)ecs_get_id(ecs, ent, ComputeID);
    if(com){ compute_reset(com, ecs, ent); }

    Hull* hull = (Hull*)ecs_get_id(ecs, ent, HullID);
    if(hull){ hull_reset(hull, ecs, ent); }

    Bay* bay = (Bay*)ecs_get_id(ecs, ent, BayID);
    if(bay){ bay_reset(bay, ecs, ent); }

    Cargo* cargo = (Cargo*)ecs_get_id(ecs, ent, CargoID);
    if(cargo){ cargo_reset(cargo, ecs, ent); }

    Growth* growth = (Growth*)ecs_get_id(ecs, ent, GrowthID);
    if(growth){ growth_reset(growth, ecs, ent); }

    //Update drive last, as it relies on other components
    Drive* drive = (Drive*)ecs_get_id(ecs, ent, DriveID);
    if(drive){ drive_reset(drive, ecs, ent); }
}

void entity_update_compos(ECS* ecs, Entity ent)
{
    Nav* nav = (Nav*)ecs_get_id(ecs, ent, NavID);
    if(nav){ nav_update(nav, ecs, ent); }

    Vat* vat = (Vat*)ecs_get_id(ecs, ent, VatID);
    if(vat){ vat_update(vat, ecs, ent); }

    //Extract* ext = (Extract*)ecs_get_id(ecs, ent, ExtractID);
    //if(ext){ extract_update(ext, ecs, ent); }

    Synth* synth = (Synth*)ecs_get_id(ecs, ent, SynthID);
    if(synth){ synth_update(synth, ecs, ent); }
    
    Compute* com = (Compute*)ecs_get_id(ecs, ent, ComputeID);
    if(com){ compute_update(com, ecs, ent); }

    Hull* hull = (Hull*)ecs_get_id(ecs, ent, HullID);
    if(hull){ hull_update(hull, ecs, ent); }

    Bay* bay = (Bay*)ecs_get_id(ecs, ent, BayID);
    if(bay){ bay_update(bay, ecs, ent); }

    Cargo* cargo = (Cargo*)ecs_get_id(ecs, ent, CargoID);
    if(cargo){ cargo_update(cargo, ecs, ent); }

    Growth* growth = (Growth*)ecs_get_id(ecs, ent, GrowthID);
    if(growth){ growth_update(growth, ecs, ent); }

    //Update drive last, as it relies on other components
    Drive* drive = (Drive*)ecs_get_id(ecs, ent, DriveID);
    if(drive){ drive_update(drive, ecs, ent); }
}

bool check_aggro(ECS* ecs, Entity ent, Entity ent2)
{
    static const uint8_t ENTTYPE_AGGRO[ENTTYPE_COUNT][ENTTYPE_COUNT] = {
        [ENTTYPE_SHIP] = {
            [ENTTYPE_WASP] = 1,
            [ENTTYPE_LEVIATHAN] = 1,
        },
        [ENTTYPE_LEVIATHAN] = {
            [ENTTYPE_SHIP] = 1,
            [ENTTYPE_WASP] = 1,
        },
        [ENTTYPE_WASP] = {
            [ENTTYPE_SHIP] = 1,
            [ENTTYPE_LEVIATHAN] = 1,
        },
    };

    //If either entity is dead, return no aggression
    if(!ecs_entity_alive(ecs, ent)){ return false; }
    if(!ecs_entity_alive(ecs, ent2)){ return false; }

    //Get entity types
    const Type* type1 = ecs_get_id(ecs, ent, TypeID);
    const Type* type2 = ecs_get_id(ecs, ent2, TypeID);

    //Get hull module
    const Hull* hull2 = ecs_get_id(ecs, ent2, HullID);

    //Aggro based on entity type
    bool aggro;
    switch(type1->type){
    case ENTTYPE_MISSILE: {

        const Missile* miss = ecs_get_id(ecs, ent, MissileID);
        aggro = ENTTYPE_AGGRO[miss->sender.type][type2->type];

    } break;
    default: {

        aggro = ENTTYPE_AGGRO[type1->type][type2->type];

    } break;
    }

    return aggro && !hull_destroyed(hull2);
}
