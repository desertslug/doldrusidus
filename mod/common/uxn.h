#ifndef UXN_H
#define UXN_H

#include "helper.h"
//#include "world.h"

enum { UXN_PAGE_PROG = 0x0100, };
enum { UXN_STACK = 255, };
enum { UXN_DEV_COUNT = 16, };
enum { UXN_DEV_BYTES = 16, };
enum { UXN_DEV_BYTES_TOTAL = UXN_DEV_COUNT * UXN_DEV_BYTES, };
enum { UXN_DEV_EVENTS = 128, };
enum { UXN_RAM_BYTES = 0x10000, };
enum { UXN_ROM_BYTES = 0xFF00, };
enum { UXN_WORD_NIBBLES = 4, };

enum { UXN_DEV_SYST = 0x00, };

enum {
    UXN_PORT_SYST_WBYTE = 0x4,
    UXN_PORT_SYST_STATE = 0xf,
};

typedef struct Stack
{
    uint8_t ptr, dat[UXN_STACK];
} Stack;

typedef struct Event
{
    uint8_t devport; uint8_t data;
} Event;

typedef struct Buffer
{
    uint32_t size;
    uint32_t first;
    uint32_t last;
    Event buf[UXN_DEV_EVENTS];
} Buffer;

/* Returns the size of the circular buffer's leading (higher) portion. */
uint32_t buffer_head_size(Buffer* buf);

/* Returns the size of the circular buffer's trailing (lower) portion. */
uint32_t buffer_tail_size(Buffer* buf);

/* Clears the circular buffer. */
void buffer_reset(Buffer* buf);

/* Pushes an event to the circular buffer, overwriting the least recent if full. */
void buffer_push(Buffer* buf, Event event);

/* Pushes an event of (devport, data) to the circular buffer. */
void buffer_push_devportdata(Buffer* buf, uint8_t devport, uint8_t data);

/* Pushes multiple events to the circular buffer (bulk optimization of buffer_push). */
void buffer_push_multi(Buffer* buf, Event* events, uint32_t size);

/* Pushes multiple events of (devport, text[i]) to the circular buffer. */
void buffer_push_text(Buffer* buf, uint8_t devport, const char* text, uint32_t size);

/* Pops and returns the least recent event in the circular buffer. */
Event buffer_pop(Buffer* buf);

typedef struct Uxn
{
    uint8_t halt;
    uint8_t brk;
    uint16_t pc;
    int32_t instr, inspt;
    uint8_t err;
    uint8_t wstptr, rstptr;
    Stack wst, rst;
    Buffer ievt, oevt;
    uint8_t dev[UXN_DEV_BYTES_TOTAL];
    uint8_t ram[UXN_RAM_BYTES];
} Uxn;

#define UXN_DEV_ARGS Uxn* u, uint8_t addr, void* world, uint16_t ent, void* sweep

/* Resets stacks and device event buffers, the program counter and error code.
   Zeroes out the device memory and zero page.
   Hooks in all function pointers for device read/write events.
 */
void uxn_reset(Uxn* u);

uint8_t uxn_dei(UXN_DEV_ARGS);
void uxn_deo(UXN_DEV_ARGS);
void uxn_halt(Uxn* u, uint8_t instr, uint8_t err, uint16_t addr);

uint16_t deo_mask[] = {
    0xffff, 0xffff, 0xffff, 0x0000,
    0x0000, 0x0000, 0x0000, 0x0000,
    0xffff, 0x0000, 0x0000, 0x0000,
    0x0000, 0x0000, 0xffff, 0x0000
};
uint16_t dei_mask[] = {
    0xffff, 0xffff, 0xffff, 0x0000,
    0x0000, 0x0000, 0x0000, 0x0000,
    0xffff, 0x0000, 0x0000, 0x0000,
    0x0000, 0x0000, 0xffff, 0x0000
};

uint32_t uxn_eval(Uxn* u, uint16_t pc, uint32_t ins, void* world, uint16_t ent, void* sweep);

void uxn_run(Uxn* uxn, void* world, uint16_t ent, void* sweep);

void uxn_sync_memory(Uxn* uxn);

#define POKE2(d, v) { (d)[0] = (v) >> 8; (d)[1] = (v); }
#define PEEK2(d) ((d)[0] << 8 | (d)[1])
#define DEO(p, v) { u->dev[p] = v; if((deo_mask[p >> 4] >> (p & 0xf)) & 0x1) uxn_deo(u, (p), world, ent, sweep); }
#define DEI(p) ((dei_mask[(p) >> 4] >> ((p) & 0xf)) & 0x1 ? uxn_dei(u, (p), world, ent, sweep) : u->dev[(p)])

#define HALT(c) { uxn_halt(u, ins, (c), pc - 1); goto err; }
#define FLIP         { s = ins & 0x40 ? &u->wst : &u->rst; }
#define JUMP(x)      { if(m2) pc = (x); else pc += (int8_t)(x); }
#define POKE(x, y)   { if(m2) { POKE2(ram + x, y) } else { ram[(x)] = (y); } }
#define PEEK(o, x)   { if(m2) { o = PEEK2(ram + x); } else o = ram[(x)]; }
#define DEVW(p, y)   { if(m2) { DEO(p, y >> 8) DEO((p + 1), y) } else { DEO(p, y) } }
#define DEVR(o, p)   { if(m2) { o = DEI(p) << 8 | DEI(p + 1); } else { o = DEI(p); } }

#define PUSH1(y)     { if(s->ptr == 0xff) HALT(2) s->dat[s->ptr++] = (y); }
#define PUSH2(y)     { if((tsp = s->ptr) >= 0xfe) HALT(2) t = (y); POKE2(&s->dat[tsp], t); s->ptr = tsp + 2; }
#define PUSHx(y)     { if(m2) { PUSH2(y) } else { PUSH1(y) } }
#define PUSH11(y, z) { if(s->ptr >= 0xfe) HALT(2) s->dat[s->ptr++] = (y); s->dat[s->ptr++] = (z); }
#define PUSH22(y, z) { if((tsp = s->ptr) >= 0xfc) HALT(2) POKE2(&s->dat[tsp], (y)); POKE2(&s->dat[tsp+2], (z)); s->ptr = tsp + 4; }
#define PUSHxx(y, z) { if(m2) { PUSH22(y, z) } else { PUSH11(y, z) } }

#define POP1(o)      { if(*sp == 0x00) HALT(1) o = s->dat[--*sp]; }
#define POP2(o)      { if((tsp = *sp) <= 0x01) HALT(1) o = PEEK2(&s->dat[tsp - 2]); *sp = tsp - 2; }
#define POPx(o)      { if(m2) { POP2(o) } else { POP1(o) } }
#define POP11(o, p)  { if(*sp <= 0x01) HALT(1) o = s->dat[--*sp]; p = s->dat[--*sp]; }
#define POP22(o, p)  { if((tsp = *sp) <= 0x03) HALT(1) o = PEEK2(&s->dat[tsp - 2]); p = PEEK2(&s->dat[tsp - 4]); *sp = tsp - 4; }
#define POPxx(o, p)  { if(m2) { POP22(o, p) } else { POP11(o, p) } }

enum {
    UXN_DEV_CONS = 0x10,
    UXN_DEV_SCRN = 0x20,
    UXN_DEV_CONT = 0x80,
    UXN_DEV_HOST = 0xe0,
};

enum {
    UXN_PORT_CONS_READ = 0x2,
    UXN_PORT_CONS_WRITE = 0x8,
};

enum {
    UXN_PORT_SCRN_EMPTY = 0x2,
};

enum {
    UXN_PORT_CONT_GUIDE = 0x5,
    UXN_PORT_CONT_THUMBS = 0x6,
    UXN_PORT_CONT_SELSTART = 0x7,
    UXN_PORT_CONT_BUTTONS = 0x8,
    UXN_PORT_CONT_BUMPERS = 0x9,
    UXN_PORT_CONT_LEFT_X = 0xa,
    UXN_PORT_CONT_LEFT_Y = 0xb,
    UXN_PORT_CONT_RIGHT_X = 0xc,
    UXN_PORT_CONT_RIGHT_Y = 0xd,
    UXN_PORT_CONT_LEFT_TRIGGER = 0xe,
    UXN_PORT_CONT_RIGHT_TRIGGER = 0xf,
};

enum {
    UXN_PORT_HOST_READ = 0x2,
    UXN_PORT_HOST_WRITE = 0x3,
    UXN_PORT_HOST_RNG_HI = 0x4,
    UXN_PORT_HOST_RNG_LO = 0x5,
    UXN_PORT_HOST_DIRTY = 0x7,
    UXN_PORT_HOST_DATA = 0x8,
};

enum {
    HOST_READ_OK = 0x0,
    HOST_READ_NOK = 0x1,
};

enum {
	HOST_PROTO_MAX = 0xfe,
	HOST_PROTO_SYN_ENT = 0xff,
};

enum {
    HOST_WRITE_NAV_ABS = 0x0,
    HOST_WRITE_NAV_REL = 0x1,
    HOST_WRITE_NAV_ORB = 0x2,
    HOST_WRITE_NAV_IMP = 0x3,
    HOST_WRITE_NAV_ENT_POS = 0x8,
    HOST_WRITE_NAV_ENT_TYP = 0x9,
    HOST_WRITE_NAV_ENT_GET = 0xa,
    HOST_WRITE_NAV_SYN_ENT = 0xb,
    HOST_WRITE_NAV_SIG_SET = 0xc,
    HOST_WRITE_NAV_SIG_GET = 0xd,
	HOST_WRITE_NAV_SYN_SET = 0xe,
	HOST_WRITE_NAV_SYN_GET = 0xf,

    HOST_WRITE_DRV_THR = 0x10,

    HOST_WRITE_VAT_INJ = 0x20,
    HOST_WRITE_VAT_SAM = 0x21,
    HOST_WRITE_VAT_HAR = 0x22,

    HOST_WRITE_EXT_BAR = 0x30,

    HOST_WRITE_SYN_REP = 0x40,
    HOST_WRITE_SYN_RES = 0x41,
    HOST_WRITE_SYN_SAL = 0x42,

    HOST_WRITE_COM_POW =  0x50,

    HOST_WRITE_BAY_LAU = 0x70,

    HOST_WRITE_CRG_GET = 0x80,
    HOST_WRITE_CRG_PUT = 0x81,

    HOST_WRITE_GRO_ADD = 0x90,
    HOST_WRITE_GRO_REM = 0x91,

    HOST_WRITE_RNG = 0xf0,
    HOST_WRITE_RNG2 = 0xf1,
};

enum {
    UXN_CONT_AXIS_CTR = 127,
};

#endif /* UXN_H */
