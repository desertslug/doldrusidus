#ifndef SWEEP_H
#define SWEEP_H

#include <math.h>

#include "defs.h"
#include "helper.h"

typedef struct Axis {
    int32_t size;
    uint16_t ent[TWOPOW16];
    uint16_t idx[TWOPOW16];
    uint32_t pos[TWOPOW16];
} Axis;

void axis_init(Axis* axis);
void axis_add(Axis* axis, uint16_t ent, uint32_t pos);
void axis_rem(Axis* axis, uint16_t ent);
void axis_sort(Axis* axis);
uint16_t axis_first_idx_greater(Axis* axis, uint32_t pos);

typedef struct Sweep {
    Axis axes[3];
} Sweep;

void sweep_init(Sweep* sweep);
void sweep_add(Sweep* sweep, ECS* ecs, uint16_t ent);
void sweep_rem(Sweep* sweep, uint16_t ent);
void sweep_step(Sweep* sweep, ECS* ecs);
void sweep_bbox(Sweep* sweep, uint32_t bbox[3][2], uint16_t out[3][2]);
int sweep_ranges_fewest(Sweep* sweep, uint16_t ranges[3][2]);
int sweep_axis_idx_inside_bbox(Sweep* sweep, int axis, uint16_t idx, uint32_t bbox[3][2]);
int sweep_pos_radius_upto(Sweep* sweep, const uint32_t pos[3], uint32_t radius, uint32_t rand, uint16_t* ents, int entsmax);
int sweep_bbox_upto(Sweep* sweep, uint32_t bbox[3][2], uint32_t rand, uint16_t* ents, int entsmax);

#endif /* SWEEP_H */