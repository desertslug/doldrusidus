#include "uxn.h"

void buffer_reset(Buffer* buf)
{
    buf->size = buf->first = buf->last = 0;
}

uint32_t buffer_head_size(Buffer* buf)
{
    return MIN(buf->size, UXN_DEV_EVENTS - buf->first);
}

uint32_t buffer_tail_size(Buffer* buf)
{
    return buf->first + buf->size > UXN_DEV_EVENTS ? buf->last : 0;
}

void buffer_push(Buffer* buf, Event event)
{
    bool full = buf->size == UXN_DEV_EVENTS;
    buf->first = (full) * ((buf->first + 1) % UXN_DEV_EVENTS) +
                 (!full) * buf->first;
    buf->size += (!full);
    buf->buf[buf->last] = event;
    buf->last = (buf->last + 1) % UXN_DEV_EVENTS;
}

void buffer_push_devportdata(Buffer* buf, uint8_t devport, uint8_t data)
{
    buffer_push(buf, (Event){ .devport = devport, .data = data });
}

void buffer_push_multi(Buffer* buf, Event* events, uint32_t size)
{
    //Get actual copied size, offset event pointer accordingly
    size_t actual = MIN(size, UXN_DEV_EVENTS);
    Event* evts = events + (size > actual ? size - actual : 0);
    
    //Calculate the size of the head & tail portions to be copied
    size_t fst = MIN(UXN_DEV_EVENTS - buf->last, actual);
    size_t snd = actual - fst;

    //Copy head & tail portions
    memcpy(&buf->buf[buf->last], evts, fst * sizeof(Event));
    memcpy(&buf->buf[0], evts + fst, snd * sizeof(Event));

    //Adjust members to reflect the new state
    buf->first = (
        buf->first +
        (actual > (UXN_DEV_EVENTS - buf->size) ?
            actual - (UXN_DEV_EVENTS - buf->size) : 0)
    ) % UXN_DEV_EVENTS;
    buf->size = MIN(buf->size + actual, UXN_DEV_EVENTS);
    buf->last = (buf->last + actual) % UXN_DEV_EVENTS;
}

void buffer_push_text(Buffer* buf, uint8_t devport, const char* text, uint32_t size)
{
    //Get the actual copied size, offset pointer accordingly
    size_t actual = MIN(size, UXN_DEV_EVENTS);
    const char* chars = text + (size > actual ? size - actual : 0);

    //Convert characters into a series of events
    Event events[UXN_DEV_EVENTS];
    for(int i = 0; i < actual; ++i){
        events[i] = (Event){ .devport = devport, .data = chars[i] };
    }

    //Push events
    buffer_push_multi(buf, events, actual);
}

Event buffer_pop(Buffer* buf)
{
    assert_or_exit(buf->size > 0, "attempt to pop from empty Buffer\n");
    --buf->size;
    Event ret = buf->buf[buf->first];
    buf->first = (buf->first + 1) % UXN_DEV_EVENTS;
    return ret;
}

void uxn_reset(Uxn* u)
{
    //Reset stacks & device event buffers
    u->wst.ptr = 0; u->rst.ptr = 0;
    buffer_reset(&u->ievt); buffer_reset(&u->oevt);

    //Set default program counter and clear error code
    u->pc = UXN_PAGE_PROG;
    u->err = 0;

    //Set default instruction budget to zero
    u->inspt = 0;

    //Zero out the zero page
    memset(u->ram, 0, UXN_PAGE_PROG);

    //Zero out device memory
    memset(u->dev, 0, UXN_DEV_BYTES_TOTAL);
}

uint8_t uxn_dei(UXN_DEV_ARGS)
{
    return on_dei(u, addr, world, ent, sweep);
}

void uxn_deo(UXN_DEV_ARGS)
{
    on_deo(u, addr, world, ent, sweep);
}

void uxn_halt(Uxn* u, uint8_t instr, uint8_t err, uint16_t addr)
{
    //Store address
    u->pc = addr;

    //Store error
    u->err = err;

    //Set halt flag
    u->halt = 1;
}

uint32_t uxn_eval(Uxn* u, uint16_t pc, uint32_t instrs, void* world, uint16_t ent, void* sweep)
{
    //If a non-zero byte is present on the system device's state byte,
    //or if the program counter is zero, evaluate zero instructions
    if(!pc || u->dev[UXN_DEV_SYST + UXN_PORT_SYST_STATE]){
        u->brk = 1; return 0;
    }

    uint8_t ins, opc, m2, ksp, tsp, *sp, *ram = u->ram;
    uint16_t a, b, c, t;
    Stack *s;

    uint32_t count = 0;
    while(count < instrs && u->instr > 0 && u->ram[pc]){

        //Deduct instruction from available instructions
        --u->instr;
        ++count;

	ins = ram[pc++];
	/* modes */
	opc = ins & 0x1f;
	m2 = ins & 0x20;
	s = ins & 0x40 ? &u->rst : &u->wst;
	if(ins & 0x80) { ksp = s->ptr; sp = &ksp; } else sp = &s->ptr;
	/* Opcodes */
	switch(opc - (!opc * (ins >> 5))) {
	/* Immediate */
	case -0x0: /* BRK   */ goto brk;
	case -0x1: /* JCI   */ POP1(b) if(!b) { pc += 2; break; } /* else fallthru */
	case -0x2: /* JMI   */ pc += PEEK2(ram + pc) + 2; break;
	case -0x3: /* JSI   */ PUSH2(pc + 2) pc += PEEK2(ram + pc) + 2; break;
	case -0x4: /* LIT   */
	case -0x6: /* LITr  */ PUSH1(ram[pc++]) break;
	case -0x5: /* LIT2  */
	case -0x7: /* LIT2r */ PUSH2(PEEK2(ram + pc)) pc += 2; break;
	/* ALU */
	case 0x01: /* INC */ POPx(a) PUSHx(a + 1) break;
	case 0x02: /* POP */ POPx(a) break;
	case 0x03: /* NIP */ POPxx(a, b) PUSHx(a) break;
	case 0x04: /* SWP */ POPxx(a, b) PUSHxx(a, b) break;
	case 0x05: /* ROT */ POPxx(a, b) POPx(c) PUSHxx(b, a) PUSHx(c) break;
	case 0x06: /* DUP */ POPx(a) PUSHxx(a, a) break;
	case 0x07: /* OVR */ POPxx(a, b) PUSHxx(b, a) PUSHx(b) break;
	case 0x08: /* EQU */ POPxx(a, b) PUSH1(b == a) break;
	case 0x09: /* NEQ */ POPxx(a, b) PUSH1(b != a) break;
	case 0x0a: /* GTH */ POPxx(a, b) PUSH1(b > a) break;
	case 0x0b: /* LTH */ POPxx(a, b) PUSH1(b < a) break;
	case 0x0c: /* JMP */ POPx(a) JUMP(a) break;
	case 0x0d: /* JCN */ POPx(a) POP1(b) if(b) JUMP(a) break;
	case 0x0e: /* JSR */ POPx(a) FLIP PUSH2(pc) JUMP(a) break;
	case 0x0f: /* STH */ POPx(a) FLIP PUSHx(a) break;
	case 0x10: /* LDZ */ POP1(a) PEEK(b, a) PUSHx(b) break;
	case 0x11: /* STZ */ POP1(a) POPx(b) POKE(a, b) break;
	case 0x12: /* LDR */ POP1(a) PEEK(b, pc + (int8_t)a) PUSHx(b) break;
	case 0x13: /* STR */ POP1(a) POPx(b) POKE(pc + (int8_t)a, b) break;
	case 0x14: /* LDA */ POP2(a) PEEK(b, a) PUSHx(b) break;
	case 0x15: /* STA */ POP2(a) POPx(b) POKE(a, b) break;
	case 0x16: /* DEI */ POP1(a) DEVR(b, a) PUSHx(b) break;
	case 0x17: /* DEO */ POP1(a) POPx(b) DEVW(a, b) break;
	case 0x18: /* ADD */ POPxx(a, b) PUSHx(b + a) break;
	case 0x19: /* SUB */ POPxx(a, b) PUSHx(b - a) break;
	case 0x1a: /* MUL */ POPxx(a, b) PUSHx((uint32_t)b * a) break;
	case 0x1b: /* DIV */ POPxx(a, b) if(!a) HALT(3) PUSHx(b / a) break;
	case 0x1c: /* AND */ POPxx(a, b) PUSHx(b & a) break;
	case 0x1d: /* ORA */ POPxx(a, b) PUSHx(b | a) break;
	case 0x1e: /* EOR */ POPxx(a, b) PUSHx(b ^ a) break;
	case 0x1f: /* SFT */ POP1(a) POPx(b) PUSHx(b >> (a & 0xf) << (a >> 4)) break;
	}
    }

brk:
    //If evaluation stopped early because of a BRK
    if(count < instrs && !u->ram[pc]){ u->pc = 0; u->brk = 1; }
    else { u->pc = pc; u->brk = 0; }

err:
    return count;
}

void uxn_run(Uxn* uxn, void* world, uint16_t ent, void* sweep)
{
    //If the machine is halted, return early
    if(uxn->halt){ return; }

    //Reset output event buffer
    buffer_reset(&uxn->oevt);

    //Increase instruction budget if it's below the maximum
    if(uxn->instr < uxn->inspt){ uxn->instr += uxn->inspt; }

    //Run while there are instructions left,
    //and the machine hasn't hit a BRK with no remaining input events
    while(uxn->instr > 0 && !(uxn->brk > 0 && uxn->ievt.size == 0)){

        //Serve input event if the machine has hit a BRK
        if(uxn->brk > 0 && uxn->ievt.size > 0){

            //Get event & device
            Event evt = buffer_pop(&uxn->ievt);
            uint8_t* dev = &uxn->dev[(evt.devport >> 4) << 4];

            //Write data to port
            dev[evt.devport & 0xf] = evt.data;

            //Set program counter, unset BRK
            uxn->pc = (dev[0] << 8) + dev[1];
            uxn->brk = 0;
            
            //Write the port number onto the system device
            uxn->dev[UXN_DEV_SYST + UXN_PORT_SYST_WBYTE] = evt.devport & 0xf;
        }

        //Evaluate machine, decrement instruction budget
	uxn_eval(uxn, uxn->pc, uxn->instr, world, ent, sweep);

        //Note working and return stack sizes for display purposes
        uxn->wstptr = uxn->wst.ptr; uxn->rstptr = uxn->rst.ptr;
    }
}

void uxn_sync_memory(Uxn* uxn)
{
    //test
    uxn->inspt = 100;

    /*
    Compute* compute = memory_get_module(memory, MODULE_COMPUTE);
    if(compute){

        //Compute modules can generate as many instructions:
        //as they generate by default, plus what they've received
        uxn->inspt = compute->generated + compute->received;

        //Store what was received for debug purposes, reset received
        compute->received_pre = compute->received;
        compute->received = 0;
    }
    */
}
