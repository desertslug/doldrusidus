# FORNAX

This implementation is the one shipped along with the engine by default. The universe it simulates is called Fornax, named after the constellation near Eridanus.

# Configuration

The parameters of server and client modifications are intended to be stored and carried in `mod/srv/srvconf.h` and `mod/cli/cliconf.h`, respectively.

# Commands

These commands are only processed for authenticated sessions.

The client stores the most recent first argument for each command, with leading and trailing whitespace included. Right after typing a command's name or shorthand, press `TB_KEY_TAB`, and the client will insert the most recent first argument for it.

The commands below are specififed in the format: `name[|shorthand] [args] ...`.

Commands that expect an entity ID can receive it:
- as a quart (four hexadecimal digits) (e.g. `0000`),
- as a proquint (consonant-vowel-consonant-vowel-consonant) (e.g. `babab`).

`save <name>` serializes the world into `<name>.bin`.

`load <name>` deserializes the world from `<name>.bin`.

`generate <seed>` starts world generation from the specified seed.

`test <name>` generates a test world identified by the specified name.

`addship` adds a ship to be piloted by the invoking user.

`delship <entity>` deleted a ship piloted by the invoking user.

`ships` lists the ships piloted by the invoking user.

`uxn-ram|! <entity> <path>` loads the rom from `<path>` to the specified entity, resets its uxn machine, and boots it.

`uxn-boot <entity>` resets the entity's uxn machine, and boots it.

`uxn-halt <entity>` sets the halt flag of entity's uxn machine.

`uxn-resume <entity>` unsets the halt flag of entity's uxn machine.

`cons|. <entity> <text>` feeds input events to the entity's uxn machine.

`ow` opens the graphical window for the client.

`cw` closes the graphical window for the client.

`focus|> entity` sets the focused entity.

`unfocus|<` unsets the focused entity.

# Input handling

Currently, the client supports two input modes, `NormalMode` for keyboards & mice, and `ControllerMode` for joysticks. The currently selected modality is shown on the prompt as `NOR` or `CON`, respectively.

## Input cross-mapping

Additionally, in each of these modes, the other input device is mapped to the selected one before it is processed. See `map_kbmouse_to_gamepad` and `map_gamepad_to_kbmouse` for the exact mappings.

## Input remapping

You can map your desired keys and mouse buttons to the accepted ones by modifying `KEY_MAPS` and `BUTTON_MAPS` upon initialization in `client.c`.

## Input bindings

### Keyboard & mouse bindings

`GL_KEY_V` changes the input mode to `NormalMode`.

`GL_KEY_C` changes the input mode to `ControllerMode`.

`GL_KEY_R` focuses the entity currently hovered over.

`GL_KEY_R` defocuses any entity, if none are being hovered over.

`GL_MOUSE_BUTTON_2` offsets the camera's azimuth & altitude using the cursor's motion on the x & y axes.

`GL_KEY_E` / `GL_KEY_Q` zooms the camera closer in or farther out.

`GL_MOUSE_BUTTON_1 + GL_MOUSE_BUTTON_2` starts zooming the camera using the cursor's motion on the y axis.

`GL_KEY_A` / `GL_KEY_D` moves the camera horizontally to its relative left/right.

`GL_KEY_S` / `GL_KEY_W` moves the camera horizontally to its relative backward/forward.

`GL_KEY_LEFT_CONTROL` / `GL_KEY_SPACE` lowers or raises the camera vertically.

`GL_KEY_LEFT_SHIFT` jumps to the position of a hovered entity.

`GL_KEY_F` starts following the entity currently hovered over.

`GL_KEY_F` stops following an entity, if none are being hovered over.

`GL_KEY_P` toggles the display of identifiers between proquints and hexadecimal quartets.

`GL_KEY_M` cycles between entity synchronization modes.

### Gamepad bindings

`GL_GAMEPAD_BUTTON_BACK` changes the input mode to `NormalMode`.

`GL_GAMEPAD_BUTTON_START` changes the input mode to `ControllerMode`.

`GL_GAMEPAD_BUTTON_DPAD_DOWN` focuses the entity at the center of the screen.

`GL_GAMEPAD_BUTTON_DPAD_DOWN` defocuses any entity, if none are being looked at.

`GL_GAMEPAD_AXIS_RIGHT_X` / `GL_GAMEPAD_AXIS_RIGHT_Y` offsets the camera's azimuth and altitude.

`GL_GAMEPAD_BUTTON_RIGHT_BUMPER` / `GL_GAMEPAD_BUTTON_LEFT_BUMPER` zooms the camera closer in or farther out.

`GL_GAMEPAD_AXIS_LEFT_TRIGGER` / `GL_GAMEPAD_AXIS_RIGHT_TRIGGER` lowers or raises the camera vertically.

`GL_GAMEPAD_BUTTON_DPAD_UP` jumps to the position of the entity at the center of the screen.

`GL_GAMEPAD_BUTTON_DPAD_LEFT` starts following the entity at the center of the screen.

`GL_GAMEPAD_BUTTON_DPAD_RIGHT` stops following an entity.

`GL_GAMEPAD_BUTTON_X` toggles the display of identifiers between proquints and hexadecimal quartets.

`GL_GAMEPAD_BUTTON_B` cycles between entity synchronization modes.

# World

A `World` is where all game state is stored. The age of each world is stored as a second/nanosecond pair of `uint32_t`'s.

The simulation of a world is stepped by a delta `timespec` with an accuracy of one microsecond. The variable-length delta time in microseconds is calculated roughly in the following form.

```c
uint32_t srv_tick_usec = 1000000 / srv_tps;
uint32_t delta_usec = (srv_tps_pass * srv_tick_usec) / srv_tps;
```

`WARNING:` A zero value in `srv_tps_pass`, currently, has no special meaning to the core engine. It is the responsibility of whomever scripts the world in Lua to ensure that the simulation freezes completely when the delta time is zero.

## Serialization

The binary format for storing a world is the following.

```
uint32_t /* world age, seconds */
uint32_t /* world age, remaining nanoseconds */
[ /* entity component system */ ]
```

# Entity component system

The ECS is based on an archetype structure. The archetype of an entity is the set of components that are attached to it, therefore, two entities have the same archetype only if they hold the same set of components. Entities of differing archetypes can be stored in different locations in memory, contiguously, since all of them in the same location will have the same set of components, which helps with cache locality and vectorization. This is why queries also have to iterate the different archetypes that hold at least those components that were queried for.

## Serialization

The binary format for storing entities is the following.

```
[
    uint32_t /* size of following data in bytes */
    uint16_t /* entity ID */
    uint8_t  /* number of following components */
    [
        uint8_t /* component ID */
        uint8_t[] /* component data */
    ] ... /* repeat for each component */
] ... /* repeat for each entity */
```

## Synchronization

The server uses an authenticated client's access level and currently selected synchronization mode to determine which entities it should serialize and synchronize (send) to it. The currently supported two modes are `SyncModeCamera` and `SyncModeWeighted`.

The latter mode behaves the same at all access levels, and it will request only those entities to be synchronized which are perceived by the user's piloted ships. Also, it will request synchronization more often for those entities that are closer to the camera's origin, according to a weighted random selection, configurable by `ENTITY_SYNC_WEIGHT_PCT_MIN` and `ENTITY_SYNC_WEIGHT_PCT_MAX`.

The former mode requests only entities within the camera's bounding box to be synchronized. At root access level, this synchronizes all entities within that bounding box. Below root access level, it uses the weighted selection logic, with the exception that it filters the selectable entities to those within the bounding box.

A shorthand for the currently active mode is displayed at the bottom after `SYN`. To find the keyboard and gamepad bindings to cycle between modes, please refer to the `## Input bindings` section.

# Entity types

The convention here is to start the structure definition of every entity type by the same structure, so that the `Memory` component's `dat` member can be casted to it to know the entity's type. For the current list of entity types, see `mod/common/compo.h`.

## Ships

Starships that can execute uxn code. They can be equipped with multiple modules, which expand their capabilities.

## Stars

Colossal blobs of matter under fusion.

## Oases

Rare bodies where the environmental conditions allow stable growth of nonsentient organic life. Some of them already host life, while others are only capable of doing so, but happen to be empty.

## Habitable worlds

Fairly common bodies where the environmental conditions sometimes allow growth of nonsentient organic life. Their growth parameters vary randomly, and are further affected by changing weather conditions. Ecosystems may or may not survive harsh weather.

As long as the ecosystem it hosts remains stable and diverse, it will continue accumulating organic raw materials. These materials can then be harvested, but this will harm populations in the process.

## Monoliths

Presumably ancient structures holding texts. They can be deciphered one character at a time by donating instructions to them, to progressively map out their internal structure. Whenever the next character in sequence is deciphered, it will be received by the next donator. Upon reaching the end of the text, the next character will loop around to the first.

## Wormholes

Warps and folds in the curvature of spacetime, a pair of which connect two distant points in space. Draws in entities inside its sphere of influence, and warps them below a threshold distance to a target position which lies outside the influence of the wormhole on the other side.

## Barren worlds

Common bodies where the harsh environmental conditions are unsuitable for the growth of organic life. However, they contain vast amounts of inorganic raw materials, which makes them quite valuable in the eyes of interstellar entities.

## Hives

Ancient, sprawling structures found at regular intervals, buzzing with calculated self-replication. They send wasps towards barren worlds to secure and exploit inorganic raw materials.

## Wasps

Non-sentient forms of machine life assembled in hives. A wasp flies out towards a barren world to guard and extract inorganic raw materials from it. Launches missiles at hostile entities that come into the range of its sensors, but will not deviate otherwise from its purpose.

## Leviathans

Immortal creatures of the void exhibiting deep-space gigantism. Suspected to be non-sentient, they are hostile to all other known forms of life. They wander the universe randomly, and they avoid deep gravity wells. If their hull is destroyed, they fall into a dormant state to regenerate it.

## Missiles

Projectile weapons that damage the targeted entity's hull upon impact. They expire and self-destruct if they do not reach their target in time.

## Wreckages

The remains of a destroyed ship. Contains a portion of the materials the originating entity used to grow beyond its minimum set of modules. Repeats the signal of the originating entity.

## Civilizations

Biologically evolved, sentient species living on their home planets. They create and appreciate cultural artifacts. When receiving an artifact, they share their fascination with whoever delivered it. They find artifacts from more distant civilizations more fascinating.

## Ruins

The remains of fallen civilizations.

# Mechanics

## Ship modules

Ships start with a minimal set of modules.

```c
const uint32_t COMPO_MIN_COUNT[ENTTYPE_COUNT][COMPO_COUNT] = {
    [ENTTYPE_SHIP] = {
        [NavID] = 1,
        [DriveID] = 1,
        [VatID] = 1,
        [ExtractID] = 1,
        [SynthID] = 1,
        [ComputeID] = 1,
        [HullID] = 1,
        [BayID] = 1,
    },
};
```

Ships can grow more modules until the total number of modules they can hold is reached.

```c
const mats_t COMPO_MAT_COST[COMPO_COUNT][SYNTH_MAT_COUNT] = {
    [NavID]     = { [SYNTH_MAT_ORGANIC] = 16, [SYNTH_MAT_INORGANIC] = 16, },
    [DriveID]   = { [SYNTH_MAT_ORGANIC] = 16, [SYNTH_MAT_INORGANIC] = 16, },
    [VatID]     = { [SYNTH_MAT_ORGANIC] = 16, [SYNTH_MAT_INORGANIC] = 16, },
    [ExtractID] = { [SYNTH_MAT_ORGANIC] = 16, [SYNTH_MAT_INORGANIC] = 16, },
    [SynthID]   = { [SYNTH_MAT_ORGANIC] = 16, [SYNTH_MAT_INORGANIC] = 16, },
    [ComputeID] = { [SYNTH_MAT_ORGANIC] = 16, [SYNTH_MAT_INORGANIC] = 16, },
    [HullID]    = { [SYNTH_MAT_ORGANIC] = 16, [SYNTH_MAT_INORGANIC] = 16, },
    [BayID]     = { [SYNTH_MAT_ORGANIC] = 16, [SYNTH_MAT_INORGANIC] = 16, },
};
```

Ship can also reabsorb these modules, to regain some of the materials they required.

```c
const float MODULE_MAT_ABSORB = 0.5f;
```

### Navigation

Responsible for the ship's guidance and communications. It can guide a ship towards a direction, or to approach, orbit, or impact a coordinate. Allows the ship to perceive nearby entities. Allows the ship to broadcast a short signal, and to read the signals of nearby ships.

### Drive

Propels a ship. More drives give a ship a higher thrust-to-weight ratio, which determine its maximum velocity and acceleration. Its thrust can be throttled, which will affect the maximum velocity of movement modes.

### Vats

Stores samples of nonsentient organic life (autotrophs and heterotrophs).

### Extractor

Accumulates and converts inorganic raw materials.

### Synthesis

Stores raw materials to be used later in synthesis processes.

### Compute

Generates machine instructions. Also, it can donate unused instructions to other machines for computation, or to monoliths for deciphering.

### Hull

Provides a defensive structure.

### Bay

Stores missiles, and can fire them towards entities.

### Cargo

Stores cultural artifacts.

### Growth

Responsible for guiding a ship's growth. Grants space for further modules by accumulating fascination. As fascination increases towards its upper bound, the growth module grants further module space progressively.

## Autotroph-heterotroph simulations

A sort of probabilistic cellular automata inspired by predator-prey simulations (heterotroph-autotroph here, respectively).

Each simulated step consists of two phases:
- heterotroph phase, where autotrophs become heterotrophs (according to the heterotroph reproduction rate, if they have at least two heterotroph neighbors), and heterotrophs become dead cells (according to the heterotroph death rate),
- autotroph phase, where autotrophs can reproduce into a random neighboring dead cell (according to the autotroph birth rate), or become a dead cell (according to the autotroph death rate).

Trophs may be `injected` into a simulation according to a specific ratio (which currently favors autotrophs).

Trophs may be `sampled` from a simulation. Only those that exceed a certain population threshold are sampled (to protect against extinction due to oversampling), and this threshold is applied to each cell type according to a specific ratio.

Optionally, simulations can be affected by varying weather conditions, which will change the stochastic parameters slightly. Weather events are chosen from a set list with equal likelihood, and last between a lower and upper bound of time.

## Mazes

Generated by a random depth-first search, using explicit stacks.

## Uxn devices

```c
enum {
    UXN_DEV_CONS = 0x1,
    UXN_DEV_SCRN = 0x2,
    UXN_DEV_CONT = 0x8,
    UXN_DEV_HOST = 0xe,
};

enum {
    UXN_PORT_CONS_READ = 0x2,
    UXN_PORT_CONS_WRITE = 0x8,
};

enum {
    UXN_PORT_SCRN_EMPTY = 0x2,
};

enum {
    UXN_PORT_CONT_GUIDE = 0x5,
    UXN_PORT_CONT_THUMBS = 0x6,
    UXN_PORT_CONT_SELSTART = 0x7,
    UXN_PORT_CONT_BUTTONS = 0x8,
    UXN_PORT_CONT_BUMPERS = 0x9,
    UXN_PORT_CONT_LEFT_X = 0xa,
    UXN_PORT_CONT_LEFT_Y = 0xb,
    UXN_PORT_CONT_RIGHT_X = 0xc,
    UXN_PORT_CONT_RIGHT_Y = 0xd,
    UXN_PORT_CONT_LEFT_TRIGGER = 0xe,
    UXN_PORT_CONT_RIGHT_TRIGGER = 0xf,
};

enum {
    UXN_PORT_HOST_READ = 0x2,
    UXN_PORT_HOST_WRITE = 0x3,
    UXN_PORT_HOST_RNG_HI = 0x4,
    UXN_PORT_HOST_RNG_LO = 0x5,
    UXN_PORT_HOST_DIRTY = 0x7,
    UXN_PORT_HOST_DATA = 0x8,
};
```

## Uxn commands

Accessible to uxn machines through the host device, `UXN_DEV_HOST`, `0xe0`.

Machines can invoke these commands by writing the index of the command to the `UXN_PORT_HOST_WRITE` port of the device, and data exchange happens from `UXN_PORT_HOST_DATA` and upwards, on the `0xe8 - 0xef` range of ports. Commands that return a status code write it to the `UXN_PORT_HOST_READ` port. Commands will return a non-zero status if their execution fails at any point, or a zero status on success.

```c
enum {
    HOST_READ_OK = 0x0,
    HOST_READ_NOK = 0x1,
};

enum {
    HOST_WRITE_NAV_ABS = 0x0,
    HOST_WRITE_NAV_REL = 0x1,
    HOST_WRITE_NAV_ORB = 0x2,
    HOST_WRITE_NAV_IMP = 0x3,
    HOST_WRITE_NAV_ENT_POS = 0x8,
    HOST_WRITE_NAV_ENT_TYP = 0x9,
    HOST_WRITE_NAV_ENT_GET = 0xa,
    HOST_WRITE_NAV_SYN_ENT = 0xb,
    HOST_WRITE_NAV_SIG_SET = 0xc,
    HOST_WRITE_NAV_SIG_GET = 0xd,
	HOST_WRITE_NAV_SYN_SET = 0xe,
	HOST_WRITE_NAV_SYN_GET = 0xf,

    HOST_WRITE_DRV_THR = 0x10,

    HOST_WRITE_VAT_INJ = 0x20,
    HOST_WRITE_VAT_SAM = 0x21,
    HOST_WRITE_VAT_HAR = 0x22,

    HOST_WRITE_EXT_BAR = 0x30,

    HOST_WRITE_SYN_REP = 0x40,
    HOST_WRITE_SYN_RES = 0x41,
    HOST_WRITE_SYN_SAL = 0x42,

    HOST_WRITE_COM_POW =  0x50,

    HOST_WRITE_BAY_LAU = 0x70,

    HOST_WRITE_CRG_GET = 0x80,
    HOST_WRITE_CRG_PUT = 0x81,

    HOST_WRITE_GRO_ADD = 0x90,
    HOST_WRITE_GRO_REM = 0x91,

    HOST_WRITE_RNG = 0xf0,
    HOST_WRITE_RNG2 = 0xf1,
};
```

The logic behind the execution of these commands varies in complexity, this is simulated by subtracting the cycle cost of a command from the number of remaining instructions. This is the penalty that allows servers to provide more complex interfaces to the world while keeping performance needs low (at the expense of latency from the perspective of machines).

```c
const int HOST_WRITE_CYCLES[UXN_DEV_BYTES_TOTAL] = {
    [HOST_WRITE_NAV_ABS] = 1,
    [HOST_WRITE_NAV_REL] = 1,
    [HOST_WRITE_NAV_ORB] = 1,
    [HOST_WRITE_NAV_IMP] = 1,
    [HOST_WRITE_NAV_ENT_POS] = 10,
    [HOST_WRITE_NAV_ENT_GET] = 25,
    [HOST_WRITE_NAV_SYN_ENT] = 25,
    [HOST_WRITE_NAV_SIG_SET] = 1,
    [HOST_WRITE_NAV_SIG_GET] = 1,
    [HOST_WRITE_NAV_SYN_SET] = 10,
    [HOST_WRITE_NAV_SYN_GET] = 10,

    [HOST_WRITE_DRV_THR] = 1,

    [HOST_WRITE_VAT_INJ] = 10,
    [HOST_WRITE_VAT_SAM] = 10,
    [HOST_WRITE_VAT_HAR] = 10,

    [HOST_WRITE_EXT_BAR] = 10,

    [HOST_WRITE_SYN_REP] = 5,
    [HOST_WRITE_SYN_RES] = 5,
    [HOST_WRITE_SYN_SAL] = 10,

    [HOST_WRITE_COM_POW] =  1,

    [HOST_WRITE_BAY_LAU] =  5,

    [HOST_WRITE_CRG_GET] = 10,
    [HOST_WRITE_CRG_PUT] = 10,

    [HOST_WRITE_GRO_ADD] = 10,
    [HOST_WRITE_GRO_REM] = 10,

    [HOST_WRITE_RNG] = 10,
    [HOST_WRITE_RNG2] = 10,
};
```

The commands are specified in the following format:

- `host-write-byte` is the value written to the write port that invokes the command,
- `data-port-contents` the values on the host data port and upwards, before and after,
- `host-read-byte` is the return status that can be read from the read port afterwards.

```
host-write-byte : data-port-contents -> host-read-byte : data-port-contents
```

`HOST_WRITE_NAV_ABS : &x $2 &y $2 &z $2 &dist $2 ->`

Sets navigation to approach the specified coordinates at a distance.

`HOST_WRITE_NAV_REL : &x $1 &y $1 &z $1 ->`

Sets thrusters to push towards the specified direction. The coordinates are integers in [0, 255] representing a floating point number in [-1, 1), with 127, `0x7f` mapping to zero.

`HOST_WRITE_NAV_ORB : &x $2 &y $2 &z $2 &dist $2 ->`

Sets navigation to orbit the specified coordinates at a distance.

`HOST_WRITE_NAV_IMP : &x $2 &y $2 &z $2 ->`

Sets navigation to impact the specified coordinates.

`HOST_WRITE_NAV_ENT_POS : &entity $2 -> &entity-exists $1 : &x $2 &y $2 &z $2`

Attempts to query the position of an entity.

`HOST_WRITE_NAV_ENT_TYP : &entity $2 -> &entity-exists $1 : &type $1`

Attempts to query the type of an entity.

`HOST_WRITE_NAV_ENT_GET : &radius $2 -> &entity-exists $1 : &entity $2`

Attempts to query an entity within a specified radius, up to the navigation range.

`HOST_WRITE_NAV_SYN_ENT : &radius $2 -> &entity-exists $1 : &signal $16`

Generates a signal containing entities to be synchronized, up to the navigation range.

`HOST_WRITE_NAV_SIG_SET : &signal $16 ->`

Sets the signal of the navigation module.

`HOST_WRITE_NAV_SIG_GET : &entity $2 -> &entity-readable $1 : &signal $16`

Attempts to read the signal of the specified entity's navigation module. The distance between the two entities must be less than the sum of their navigation ranges.

`HOST_WRITE_NAV_SYN_SET : &signal $16 -> &signal-valid $1 :`

Stores a signal containing entities to be synchronized into a buffer.

`HOST_WRITE_NAV_SYN_GET : -> : &signal $16`

Retrieves a signal containing entities to be synchronized from a buffer. 

`HOST_WRITE_DRV_THR : &throttle $1 ->`

Throttles drives between zero thrust and full thrust with `0x00 - 0xff`.

`HOST_WRITE_VAT_INJ : &entity $2 ->`

Injects possibly all available autotrophs and heterotrophs into an entity that can host them. The entity must be no farther than `TROPHS_INTERACTION_UNITS`.

`HOST_WRITE_VAT_SAM : &entity $2 ->`

Samples autotrophs and heterotrophs from an entity that can host them. The entity must be no farther than `TROPHS_INTERACTION_UNITS`.

`HOST_WRITE_VAT_HAR : &entity $2 ->`

Harvests organic raw materials from a troph simulation that accumulates them. Populations are reset to zero in the process. The entity must be no farther than `TROPHS_INTERACTION_UNITS`.

`HOST_WRITE_EXT_BAR : &entity $2 ->`

Extracts inorganic raw materials from barren worlds. The entity must be no farther than `BARREN_INTERACTION_UNITS`.

`HOST_WRITE_SYN_REP : &entity $2 ->`

If the synth module is idle, sets it to repair an entity's hull, and sets the specified entity as its target. If the entity is farther than `SYNTH_INTERACTION_UNITS`, the synth module will return to idling.

`HOST_WRITE_SYN_RES : &entity $2 ->`

If the synth module is idle, sets it to restock an entity's bay, and sets the specified entity as its target. If the entity is farther than `SYNTH_INTERACTION_UNITS`, the synth module will return to idling.

`HOST_WRITE_SYN_SAL : &entity $2 ->`

Salvages raw materials from wreckages. The entity must be no farther than `SYNTH_INTERACTION_UNITS`.

`HOST_WRITE_COM_POW : &entity $2 -> &monolith-deciphered $1 : &char $1`

Donates remaining instructions to an entity that can use them. Ships can use received instructions for computation or they can pass them on. Monoliths use these instructions in their deciphering process.

`HOST_WRITE_BAY_LAU : &entity $2 ->`

If the bay module is idle, sets it to launch a missile towards the specified entity. If no missile is available, the bay module will return to idling.

`HOST_WRITE_CRG_GET : &entity $2 ->`

Takes an artifact from a civilization and puts it into an empty cargo hold. The entity must be no farther than `CIVIL_INTERACTION_UNITS`.

`HOST_WRITE_CRG_PUT : &entity $2 ->`

Takes an artifact from the cargo hold and gives it to a civilization. The entity must be no farther than `CIVIL_INTERACTION_UNITS`.

`HOST_WRITE_GRO_ADD : &module-type $1 ->`

Grows an additional module, consuming the materials it requires in the process. Needs at least one free module space. The module index starts from zero at the nav component.

`HOST_WRITE_GRO_REM : &module-type $1 ->`

Absorbs an existing module, regaining some portion of the materials it required in the process. Needs at least one more module of that type than is required by minimum. The module index starts from zero at the nav component.

`HOST_WRITE_RNG : ->`

Generates a random byte, and writes it to the `UXN_PORT_HOST_RNG_LO` port of the host device. Writes no other ports.

`HOST_WRITE_RNG2 : ->`

Generates a random short, and writes it to the `UXN_PORT_HOST_RNG_HI - UXN_PORT_HOST_RNG_LO` ports of the host device. Writes no other ports.

# Transpiling C to uxntal with chibicc

Any emulator-specific comforts for uxntal code transpiled by `chibicc` against the emulator of `FORNAX` are provided in `tal/include/fornax.h`. If you would like to use these, make sure to pass the path to the directory containing the header with the `-I<path>` option.

Currently, there are no device vectors that don't conform to varvara, so even though it should be easy to alter chibicc to specify a different set of device name and port pairs (to let it know which hooks it should generate), it is not necessary at this time.

A convenience script to preprocess, transpile and assemble C files into roms is provided in `tal/chibi.sh`. If the invocations of `chibicc` or `uxnasm` differ on your system from the defaults, you can override them with environment variables.

```
$ UXNASM=<path> CHIBICC=<path> ./chibi.sh <input>.c <output>.rom
```
