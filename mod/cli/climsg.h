#ifndef CLIMSG_H
#define CLIMSG_H

#include "defs.h"
#include "modmsg.h"
#include "auth.h"
#include "session.h"
#include "memory.h"

uint8_t* file_get_contents(const char* path, size_t max, uint32_t* size_out);

size_t str_process_to_cmd(rtcp_uvec_t* cmds, const char* str, size_t size, int* cmdidx);

void str_process_to_msg(rtcp_uvec_t* cmds, char* str, int size, void* pkt, ModClient* modcli, ModSession* modsess, rtcp_uvec_t* firstargs);

void pkt_process(void* ipkt, void* opkt, ModClient* modcli, ModSession* modsess);

#endif /* CLIMSG_H */
