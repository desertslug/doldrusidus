#ifndef CLICONF_H
#define CLICONF_H

#include "compo.h"

/* Number of lines in the terminal ui's history. */
enum { TERMUI_HISTORY = 1024, };

/* Milliseconds to wait for a connection initially */
enum { CONN_WAIT_MSEC = 2000, };

/* Number of maximum rendered entities. */
enum { ENTITY_RENDER_MAX = 2048, };

/* Number of maximum rendered particles. */
enum { PARTICLE_RENDER_MAX = 1024, };

/* Number of maximum rendered characters in a panel */
enum { PANEL_CHAR_RENDER_MAX = 4096, };

/* Fade time of an entity that has not received updates. */
enum { ENTITY_SYNC_FADE_MS = 4000, };

/* Expiration time of an entity that has not received updates. */
enum { ENTITY_SYNC_EXPIRE_MS = 10000, };

/* The number of entities that can expire in a single pass. */
enum { ENTITY_SYNC_EXPIRE_MAX = 256, };

/* Minimum weight while selecting between piloted entities. */
enum { ENTITY_SYNC_WEIGHT_PCT_MIN = 10, };

/* Maximum weight while selecting between piloted entities. */
enum { ENTITY_SYNC_WEIGHT_PCT_MAX = 90, };

/* Cursor disabling. Zero to disable cursor disabling, enable it otherwise. */
enum { CLI_CURSOR_DISABLING = 0, };

/* Initial window size. */
enum { CLI_WINDOW_X = 1280, };
enum { CLI_WINDOW_Y = 720, };

/* Index of the glyph in the font symbolizing the zeroeth entity type. */
enum { FONT_ENTTYPE_ZERO_CHAR = 193, };

/* Index of the glyph in the font symbolizing the piloted entity marker. */
enum { FONT_PILOTMARKER_CHAR = 223, };

/* How close in angle does the mouse need to point to an entity for it to be resolved (considered a valid candidate for mouse selection). */
#define CLI_ENTITY_RESOLVE_ANGLE_DEG 5.0f

/* Joystick deadzone, below which input is ignored. */
#define CLI_JOYSTICK_DEADZONE 0.1f

/* Joystick binary activation threshold. */
#define CLI_JOYSTICK_THRESHOLD 0.4f

/* Joystick delta sensitvity, smaller differences will not be updated. */
#define CLI_JOYSTICK_SENSITIVITY 0.025f

/* Joystick as mouse cursor sensitivity multiplier. */
#define CLI_JOYSTICK_CURSOR_SENS 100.0f

/* Camera zoom sensitivity to keyboard and mouse. */
#define CAM_ZOOM_SENS_KBMOUSE 0.25f

/* Camera zoom sensitivity to gamepad. */
#define CAM_ZOOM_SENS_GAMEPAD 1.5f

/* Camera orbit sensitivity to keyboard and mouse. */
#define CAM_ORBIT_X_SENS_KBMOUSE (-0.2f)
#define CAM_ORBIT_Y_SENS_KBMOUSE (0.2f)

/* Camera orbit sensitivity to gamepad. */
#define CAM_ORBIT_X_SENS_GAMEPAD (-2.5f)
#define CAM_ORBIT_Y_SENS_GAMEPAD (2.5f)

enum {
    BG_BLACK = 0,
    BG_RED,
    BG_GREEN,
    BG_YELLOW,
    BG_BLUE,
    BG_MAGENTA,
    BG_CYAN,
    BG_WHITE,

    FG_BLACK,
    FG_RED,
    FG_GREEN,
    FG_YELLOW,
    FG_BLUE,
    FG_MAGENTA,
    FG_CYAN,
    FG_WHITE,
};

enum { COL_BG_BLACK   = 0x141110, };
enum { COL_BG_RED     = 0xde4e4e, };
enum { COL_BG_GREEN   = 0xb0c63f, };
enum { COL_BG_YELLOW  = 0xfea63c, };
enum { COL_BG_BLUE    = 0x679ab3, };
enum { COL_BG_MAGENTA = 0xd6155f, };
enum { COL_BG_CYAN    = 0x3faf98, };
enum { COL_BG_WHITE   = 0xa0a0a0, };

enum { COL_FG_BLACK   = 0x707070, };
enum { COL_FG_RED     = 0xb83232, };
enum { COL_FG_GREEN   = 0xbde077, };
enum { COL_FG_YELLOW  = 0xffe863, };
enum { COL_FG_BLUE    = 0x3a5bd6, };
enum { COL_FG_MAGENTA = 0xe5537b, };
enum { COL_FG_CYAN    = 0x2a968a, };
enum { COL_FG_WHITE   = 0xdfdfdf, };

/* Entity colors. */

const float ENTITY_COLORS[ENTTYPE_COUNT][4] = {
    { ((COL_FG_WHITE & 0xff0000)>>16) / 255.0f,
      ((COL_FG_WHITE & 0x00ff00)>>8)  / 255.0f,
      ((COL_FG_WHITE & 0x0000ff)>>0)  / 255.0f, 1.0f }, //ENTTYPE_NONE
    { ((COL_FG_WHITE & 0xff0000)>>16) / 255.0f,
      ((COL_FG_WHITE & 0x00ff00)>>8)  / 255.0f,
      ((COL_FG_WHITE & 0x0000ff)>>0)  / 255.0f, 1.0f }, //ENTTYPE_SHIP
    { ((COL_BG_YELLOW & 0xff0000)>>16) / 255.0f,
      ((COL_BG_YELLOW & 0x00ff00)>>8)  / 255.0f,
      ((COL_BG_YELLOW & 0x0000ff)>>0)  / 255.0f, 1.0f }, //ENTTYPE_STAR
    { ((COL_BG_GREEN & 0xff0000)>>16) / 255.0f,
      ((COL_BG_GREEN & 0x00ff00)>>8)  / 255.0f,
      ((COL_BG_GREEN & 0x0000ff)>>0)  / 255.0f, 1.0f }, //ENTTYPE_OASIS
    { ((COL_BG_GREEN & 0xff0000)>>16) / 255.0f,
      ((COL_BG_GREEN & 0x00ff00)>>8)  / 255.0f,
      ((COL_BG_GREEN & 0x0000ff)>>0)  / 255.0f, 1.0f }, //ENTTYPE_HABITABLE
    { ((COL_BG_MAGENTA & 0xff0000)>>16) / 255.0f,
      ((COL_BG_MAGENTA & 0x00ff00)>>8)  / 255.0f,
      ((COL_BG_MAGENTA & 0x0000ff)>>0)  / 255.0f, 1.0f }, //ENTTYPE_MONOLITH
    { ((COL_BG_MAGENTA & 0xff0000)>>16) / 255.0f,
      ((COL_BG_MAGENTA & 0x00ff00)>>8)  / 255.0f,
      ((COL_BG_MAGENTA & 0x0000ff)>>0)  / 255.0f, 1.0f }, //ENTTYPE_WORMHOLE
    { ((COL_BG_RED & 0xff0000)>>16) / 255.0f,
      ((COL_BG_RED & 0x00ff00)>>8)  / 255.0f,
      ((COL_BG_RED & 0x0000ff)>>0)  / 255.0f, 1.0f }, //ENTTYPE_BARREN
    { ((COL_BG_MAGENTA & 0xff0000)>>16) / 255.0f,
      ((COL_BG_MAGENTA & 0x00ff00)>>8)  / 255.0f,
      ((COL_BG_MAGENTA & 0x0000ff)>>0)  / 255.0f, 1.0f }, //ENTTYPE_HIVE
    { ((COL_BG_RED & 0xff0000)>>16) / 255.0f,
      ((COL_BG_RED & 0x00ff00)>>8)  / 255.0f,
      ((COL_BG_RED & 0x0000ff)>>0)  / 255.0f, 1.0f }, //ENTTYPE_WASP
    { ((COL_BG_RED & 0xff0000)>>16) / 255.0f,
      ((COL_BG_RED & 0x00ff00)>>8)  / 255.0f,
      ((COL_BG_RED & 0x0000ff)>>0)  / 255.0f, 1.0f }, //ENTTYPE_LEVIATHAN
    { ((COL_FG_WHITE & 0xff0000)>>16) / 255.0f,
      ((COL_FG_WHITE & 0x00ff00)>>8)  / 255.0f,
      ((COL_FG_WHITE & 0x0000ff)>>0)  / 255.0f, 1.0f }, //ENTTYPE_MISSILE
    { ((COL_FG_WHITE & 0xff0000)>>16) / 255.0f,
      ((COL_FG_WHITE & 0x00ff00)>>8)  / 255.0f,
      ((COL_FG_WHITE & 0x0000ff)>>0)  / 255.0f, 1.0f }, //ENTTYPE_WRECKAGE
    { ((COL_BG_BLUE & 0xff0000)>>16) / 255.0f,
      ((COL_BG_BLUE & 0x00ff00)>>8)  / 255.0f,
      ((COL_BG_BLUE & 0x0000ff)>>0)  / 255.0f, 1.0f }, //ENTTYPE_CIVILIZATION
    { ((COL_BG_WHITE & 0xff0000)>>16) / 255.0f,
      ((COL_BG_WHITE & 0x00ff00)>>8)  / 255.0f,
      ((COL_BG_WHITE & 0x0000ff)>>0)  / 255.0f, 1.0f }, //ENTTYPE_RUIN
};

#endif /* CLICONF_H */
