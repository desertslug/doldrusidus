#include "particle.h"

uint32_t PART_SIZE[PART_COUNT] = {
    [PartPositionID] = sizeof(Position),
    [PartPointphysID] = sizeof(Pointphys),
    [PartLifetimeID] = sizeof(Lifetime),
    [PartColorID] = sizeof(Color),
    [PartSpriteID] = sizeof(Sprite),
};

int particle_freq_to_count(float freq, float delta, float randnorm)
{
    //Particles inside the delta time
    float indelta = freq * delta;

    //Random number of particles that roughly reflects
    int ret = indelta;
    ret += randnorm < (indelta - ret);

    return ret;
}

void romu_trio32_vec3_rand(uint32_t romustt[3], float* out)
{
    //Random angles
    float pi2 = 6.28318530f;
    float theta = romu_trio32_norm(romustt) * pi2;
    float phi = romu_trio32_norm(romustt) * pi2;

    out[0] = cos(theta) * cos(phi);
    out[1] = cos(theta) * sin(phi);
    out[2] = sin(theta);
}

void parts_alloc(Particles* parts)
{
    ecs_alloc(&parts->ecs);
}

void parts_dealloc(Particles* parts)
{
    ecs_dealloc(&parts->ecs);
}

void parts_init(Particles* parts, uint32_t seed[3])
{
    ecs_init(&parts->ecs, PARTICLE_RENDER_MAX, PART_COUNT, PART_SIZE);

    memcpy(parts->romu_trio32, seed, sizeof(uint32_t[3]));
}

void parts_step(Particles* parts, struct timespec age, struct timespec delta, Camera* camera)
{
    ecs_tick(&parts->ecs);

    //Forget
    parts_forget(parts, age);

    //Systems
    Query q; query_alloc(&q);
    {
		float fdelta = timespec_float(&delta);
		partsystem_pos_pphys_integrate(&q, &parts->ecs, fdelta);
    }
    query_dealloc(&q);
}

void parts_render(Particles* parts, struct timespec age, Camera* camera)
{
    ArchID arch = {0};
    ECS* ecs = &parts->ecs;
    SpriteInstance insts[PARTICLE_RENDER_MAX];

    int count = 0;

    arch = archid_setbit(arch, PartPositionID);
    arch = archid_setbit(arch, PartSpriteID);
    arch = archid_setbit(arch, PartColorID);
    Query q; query_alloc(&q); query_init(&q, ecs, arch);
    while(query_next(&q) && count < PARTICLE_RENDER_MAX){
        Position* pos = query_term(ecs, &q, PartPositionID);
		Sprite* spr = query_term(ecs, &q, PartSpriteID);
		Lifetime* life = query_term(ecs, &q, PartLifetimeID);
		Color* col = query_term(ecs, &q, PartColorID);

		for(int i = 0; i < q.size; ++i){

			//Position
			vec3 wpos; uint32_to_vec3(pos[i].pos, wpos);

			//Compose drawn instance
			insts[count] = (SpriteInstance){
				{ wpos[0], wpos[1], wpos[2] },
				spr[i].idx / (float)PART_SPRITE_COUNT + 0.5f / (float)PART_SPRITE_COUNT,
				{ 0.0f, 0.0f, 0.0f, 0.0f },
				{ 0, 0 },
				{ 1, 1 },
			};

			//Color
			int colidx = life[i].percent > 0.5f;
			float percent = (life[i].percent - colidx * 0.5f) * 2.0f;
			glm_vec4_lerp(col[i].cols[colidx], col[i].cols[colidx+1],
			    percent, insts[count].col);

			++count;
		}
    }
    query_dealloc(&q);

    spritesrender_render_3d(&parts->sprites, insts, count, camera->viewpos);
}

void parts_forget(Particles* parts, struct timespec age)
{
    ArchID arch = {0};
    ECS* ecs = &parts->ecs;

    //Entities to be deleted
    int count = 0;
    Entity entities[PARTICLE_RENDER_MAX];

    arch = archid_setbit(arch, PartLifetimeID);
    Query q; query_alloc(&q); query_init(&q, ecs, arch);
    while(query_next(&q) && count < PARTICLE_RENDER_MAX){
		Entity* ents = query_ents(ecs, &q);
		Lifetime* life = query_term(ecs, &q, PartLifetimeID);

		for(int i = 0; i < q.size; ++i){

			//Calculate percentage of lifetime lived
			life[i].percent = diff_msec(&life[i].start, &age) /
							  (1000.0f * timespec_float(&life[i].length));
			
			//If the particle has exceeded its lifetime
			if(life[i].percent >= 1.0f){ entities[count++] = ents[i]; }
		}
    }
    query_dealloc(&q);

    //Delete each particle queued for removal
    for(int i = 0; i < count; ++i){
		ecs_entity_rem(ecs, entities[i]);
    }
}

void parts_spawn_pos_vel_life_col_spr(Particles* parts, uint32_t pos[3], int32_t vel[3], struct timespec* start, float life_sec, float cols[3][4], uint8_t spr_idx)
{
    static ArchID arch = {0};
    ECS* ecs = &parts->ecs;

    //Configure the desired archetype
    static bool init = false;
    if(!init){
		arch = archid_setbit(arch, PartPositionID);
		arch = archid_setbit(arch, PartPointphysID);
		arch = archid_setbit(arch, PartLifetimeID);
		arch = archid_setbit(arch, PartColorID);
		arch = archid_setbit(arch, PartSpriteID);
    }

    //Caller must ensure that the ECS can fit one more particle
    Entity ent = ecs_entity_add(ecs);

    //Expand archetype in a single operation
    ecs_add_ids(ecs, ent, arch);

    Position* posi = ecs_get_mut_id(ecs, ent, PartPositionID);
    memcpy(posi->pos, pos, sizeof(uint32_t[3]));

    Pointphys* pphysi = ecs_get_mut_id(ecs, ent, PartPointphysID);
    memcpy(pphysi->vel, vel, sizeof(int32_t[3]));

    Lifetime* life = ecs_get_mut_id(ecs, ent, PartLifetimeID);
    memcpy(&life->start, start, sizeof(struct timespec));
    life->length = (struct timespec){
		(int)life_sec, (life_sec - (int)life_sec) / (float)1e9
    };

    Color* col = ecs_get_mut_id(ecs, ent, PartColorID);
    memcpy(col->cols, cols, sizeof(float[3][4]));

    Sprite* spr = ecs_get_mut_id(ecs, ent, PartSpriteID);
    spr->idx = spr_idx;
}

bool parts_full(Particles* parts)
{
    return parts->ecs.entnext >= parts->ecs.entsmax;
}

void partsystem_pos_pphys_integrate(Query* q, ECS* ecs, float delta)
{
    ArchID arch = {0};
    arch = archid_setbit(arch, PartPositionID);
    arch = archid_setbit(arch, PartPointphysID);
    query_init(q, ecs, arch);
    while(query_next(q)){
        Position* pos = query_term(ecs, q, PartPositionID);
		Pointphys* pphys = query_term(ecs, q, PartPointphysID);

		for(int i = 0; i < q->size; ++i){

			for(int j = 0; j < 3; ++j){

				//Integrate position by velocity
				pos[i].pos[j] += delta * pphys[i].vel[j];

				//Integrate velocity by acceleration, reset the latter
				pphys[i].vel[j] += delta * pphys[i].acc[j];
				pphys[i].acc[j] = 0;
			}
		}
    }
}

void wind_init(Wind* wind, uint32_t romustt[3])
{
    romu_trio32_vec3_rand(romustt, wind->dir);
}

void wind_step(Wind* wind, struct timespec delta, uint32_t romustt[3])
{
    float fdelta = timespec_float(&delta);

    //Blend old direction with a new, random direction
    vec3 new; romu_trio32_vec3_rand(romustt, new);
    glm_vec3_lerp(wind->dir, new, fdelta, wind->dir);
    glm_vec3_normalize(wind->dir);
}

void wind_spawn(Wind* wind, Particles* parts, struct timespec age, struct timespec delta, Camera* camera)
{
    //General
    vec4 color; hex_rgba_to_vec4_norm(COL_BG_WHITE << 8 | 0x00, color);
    uint32_t radius = MIN(camera->zoom_pos * TWOPOW16F * 2, 0x7fffffff);
    float fdelta = timespec_float(&delta);
    ECS* ecs = &parts->ecs;

    //Particle data
    uint32_t position[3];
    int32_t velocity[3];
    float life_sec;
    float colors[3][4];
    uint8_t spr_idx;

    //Spawn particles
    int spawned = particle_freq_to_count(
	30.0f, fdelta, romu_trio32_norm(parts->romu_trio32));
    for(int i = 0; i < spawned; ++i){

		if(ecs->entnext >= ecs->entsmax){ break; }

		//Position
		romu_trio32_pos_around_pos_in_kernel(
			parts->romu_trio32, camera->pos, 0x0, radius, position);
		
		//Pointphys
		vec3 vel; glm_vec3_scale(wind->dir, 2000 * camera->zoom_pos, vel);
		vec3_to_int32(vel, velocity);
		
		//Lifetime
		life_sec = 6.0f;

		//Color
		for(int k = 0; k < 3; ++k){ glm_vec4_ucopy(color, colors[k]); }
		colors[1][3] = 0.6f;
		
		//Sprite
		spr_idx = PART_DOT1 +
			romu_trio32_random(parts->romu_trio32) % (PART_DOT2 - PART_DOT1 + 1);

		//Spawn particle
		parts_spawn_pos_vel_life_col_spr(parts,
			position, velocity, &age, life_sec, colors, spr_idx);
    }
}

void solar_spawn(ECS* ecs0, Query* q, Particles* parts, struct timespec age, struct timespec delta, Camera* camera)
{
    //General
    vec4 color; hex_rgba_to_vec4_norm(COL_BG_YELLOW << 8 | 0x00, color);
    float fdelta = timespec_float(&delta);
    ECS* ecs = &parts->ecs;

    //Particle data
    uint32_t position[3];
    int32_t velocity[3];
    float life_sec;
    float colors[3][4];
    uint8_t spr_idx;

    ArchID arch = {0};
    arch = archid_setbit(arch, PositionID);
    arch = archid_setbit(arch, StarID);

    query_init(q, ecs0, arch);
    while(query_next(q)){
		Position* pos = query_term(ecs0, q, PositionID);
		Star* star = query_term(ecs0, q, StarID);

		if(ecs->entnext >= ecs->entsmax){ break; }

        for(int i = 0; i < q->size; ++i){

			if(ecs->entnext >= ecs->entsmax){ break; }

    //Spawn particles
    int spawned = particle_freq_to_count(
	4.0f, fdelta, romu_trio32_norm(parts->romu_trio32));
    for(int j = 0; j < spawned; ++j){

		if(ecs->entnext >= ecs->entsmax){ break; }

		vec3 dir; romu_trio32_vec3_rand(parts->romu_trio32, dir);
		uint32_t dist = (romu_trio32_random(parts->romu_trio32) % 0x40) << 16;
		dist *= star[i].mass;
		vec3 off; glm_vec3_scale(dir, dist, off);

		//Position
		v3uint32_add(pos[i].pos, (int32_t[3]){ off[0], off[1], off[2] }, position);

		//Pointphys
		glm_vec3_scale(dir, star[i].mass * (0x10 << 16), dir);
		vec3_to_int32(dir, velocity);

		//Lifetime
		life_sec = 4.0f;

		//Color
		for(int k = 0; k < 3; ++k){ glm_vec4_ucopy(color, colors[k]); }
		colors[1][3] = 0.8f;
		
		//Sprite
		spr_idx = PART_DOT1 +
			romu_trio32_random(parts->romu_trio32) % (PART_DOT2 - PART_DOT1 + 1);

		//Spawn particle
		parts_spawn_pos_vel_life_col_spr(parts,
			position, velocity, &age, life_sec, colors, spr_idx);
    }
		}
    }
}

void drive_spawn(ECS* ecs0, Query* q, Particles* parts, struct timespec age, struct timespec delta, Camera* camera)
{
    //General
    float fdelta = timespec_float(&delta);
    ECS* ecs = &parts->ecs;

    //Particle data
    uint32_t position[3];
    int32_t velocity[3];
    float life_sec;
    float colors[3][4];
    uint8_t spr_idx;

    ArchID arch = {0};
    arch = archid_setbit(arch, TypeID);
    arch = archid_setbit(arch, PositionID);
    arch = archid_setbit(arch, PointphysID);
    arch = archid_setbit(arch, DriveID);

    query_init(q, ecs0, arch);
    while(query_next(q)){
		Type* type = query_term(ecs0, q, TypeID);
		Position* pos = query_term(ecs0, q, PositionID);
		Pointphys* pphys = query_term(ecs0, q, PointphysID);
		Drive* drive = query_term(ecs0, q, DriveID);

		if(ecs->entnext >= ecs->entsmax){ break; }

        for(int i = 0; i < q->size; ++i){

			if(ecs->entnext >= ecs->entsmax){ break; }

			//Perceived throttle
			float throttle = v3int32_norm(pphys[i].acc) / drive[i].maxacc;

    //Spawn particles
    int spawned = particle_freq_to_count(
	20.0f * throttle, fdelta, romu_trio32_norm(parts->romu_trio32));
    for(int j = 0; j < spawned; ++j){

		if(ecs->entnext >= ecs->entsmax){ break; }

		//Position
		memcpy(position, pos[i].pos, sizeof(uint32_t[3]));
		
		//Pointphys
		for(int k = 0; k < 3; ++k){
			velocity[k] = pphys[i].vel[k] - 2 * pphys[i].acc[k];
		}

		//Lifetime
		life_sec = 3.0f;

		//Color
		for(int k = 0; k < 3; ++k){
            glm_vec4_ucopy((float*)ENTITY_COLORS[type[i].type], colors[k]);
		}
		colors[0][3] = 0.5f; colors[1][3] = 0.7f; colors[2][3] = 0.0f;
		
		//Sprite
		spr_idx = PART_DOT1 +
			romu_trio32_random(parts->romu_trio32) % (PART_DOT2 - PART_DOT1 + 1);

		//Spawn particle
		parts_spawn_pos_vel_life_col_spr(parts,
			position, velocity, &age, life_sec, colors, spr_idx);
    }
		}
    }
}

void oasis_spawn(ECS* ecs0, Query* q, Particles* parts, struct timespec age, struct timespec delta, Camera* camera)
{
    //General
    vec4 color; hex_rgba_to_vec4_norm(COL_BG_GREEN << 8 | 0x00, color);
    float fdelta = timespec_float(&delta);
    ECS* ecs = &parts->ecs;

    //Particle data
    uint32_t position[3];
    int32_t velocity[3];
    float life_sec;
    float colors[3][4];
    uint8_t spr_idx;

    ArchID arch = {0};
    arch = archid_setbit(arch, PositionID);
    arch = archid_setbit(arch, TrophsID);

    query_init(q, ecs0, arch);
    while(query_next(q)){
		Position* pos = query_term(ecs0, q, PositionID);
		Trophs* trophs = query_term(ecs0, q, TrophsID);
		(void)trophs;

		if(ecs->entnext >= ecs->entsmax){ break; }

        for(int i = 0; i < q->size; ++i){

			if(ecs->entnext >= ecs->entsmax){ break; }

    //Spawn particles
    int spawned = particle_freq_to_count(
	10.0f * (trophs[i].weather_mul == 0.0f),
	fdelta, romu_trio32_norm(parts->romu_trio32));
    for(int j = 0; j < spawned; ++j){

		if(ecs->entnext >= ecs->entsmax){ break; }

		//Position
		vec3 off; romu_trio32_vec3_rand(parts->romu_trio32, off);
		glm_vec3_scale(off,
			romu_trio32_range(parts->romu_trio32, 0x0, 0x400) << 16, off);
		v3uint32_add(pos[i].pos, (int32_t[3]){ off[0], off[1], off[2] }, position);
		
		//Pointphys
		vec3 dir; romu_trio32_vec3_rand(parts->romu_trio32, dir);
		glm_vec3_scale(dir, 1000 * camera->zoom_pos, dir);
		vec3_to_int32(dir, velocity);

		//Lifetime
		life_sec = 4.0f;

		//Color
		for(int k = 0; k < 3; ++k){ glm_vec4_ucopy(color, colors[k]); }
		colors[1][3] = 0.8f;
		
		//Sprite
		spr_idx = PART_DOT1 +
			romu_trio32_random(parts->romu_trio32) % (PART_DOT2 - PART_DOT1 + 1);

		//Spawn particle
		parts_spawn_pos_vel_life_col_spr(parts,
			position, velocity, &age, life_sec, colors, spr_idx);
    }
		}
    }
}

void monolith_spawn(ECS* ecs0, Query* q, Particles* parts, struct timespec age, struct timespec delta, Camera* camera)
{
    //General
    vec4 color; hex_rgba_to_vec4_norm(COL_BG_MAGENTA << 8 | 0x00, color);
    float fdelta = timespec_float(&delta);
    ECS* ecs = &parts->ecs;

    //Particle data
    uint32_t position[3];
    int32_t velocity[3];
    float life_sec;
    float colors[3][4];
    uint8_t spr_idx;

    ArchID arch = {0};
    arch = archid_setbit(arch, PositionID);
    arch = archid_setbit(arch, MonolithID);

    query_init(q, ecs0, arch);
    while(query_next(q)){
		Position* pos = query_term(ecs0, q, PositionID);
		Monolith* monol = query_term(ecs0, q, MonolithID);
		(void)monol;

		if(ecs->entnext >= ecs->entsmax){ break; }

        for(int i = 0; i < q->size; ++i){

			if(ecs->entnext >= ecs->entsmax){ break; }

    //Spawn particles
    int spawned = particle_freq_to_count(
	10.0f, fdelta, romu_trio32_norm(parts->romu_trio32));
    for(int j = 0; j < spawned; ++j){

		if(ecs->entnext >= ecs->entsmax){ break; }

		//Position
		vec3 off; romu_trio32_vec3_rand(parts->romu_trio32, off);
		glm_vec3_scale(off,
			romu_trio32_range(parts->romu_trio32, 0x0, 0x400) << 16, off);
		v3uint32_add(pos[i].pos, (int32_t[3]){ off[0], off[1], off[2] }, position);
		
		//Pointphys
		vec3 dir; romu_trio32_vec3_rand(parts->romu_trio32, dir);
		glm_vec3_scale(dir, 1000 * camera->zoom_pos, dir);
		vec3_to_int32(dir, velocity);

		//Lifetime
		life_sec = 4.0f;

		//Color
		for(int k = 0; k < 3; ++k){ glm_vec4_ucopy(color, colors[k]); }
		colors[1][3] = 0.8f;
		
		//Sprite
		spr_idx = PART_DOT1 +
			romu_trio32_random(parts->romu_trio32) % (PART_DOT2 - PART_DOT1 + 1);

		//Spawn particle
		parts_spawn_pos_vel_life_col_spr(parts,
			position, velocity, &age, life_sec, colors, spr_idx);
    }
		}
    }
}

void wormhole_spawn(ECS* ecs0, Query* q, Particles* parts, struct timespec age, struct timespec delta, Camera* camera)
{
    //General
    vec4 color; hex_rgba_to_vec4_norm(COL_BG_MAGENTA << 8 | 0x00, color);
    float fdelta = timespec_float(&delta);
    ECS* ecs = &parts->ecs;

    //Particle data
    uint32_t position[3];
    int32_t velocity[3];
    float life_sec;
    float colors[3][4];
    uint8_t spr_idx;

    ArchID arch = {0};
    arch = archid_setbit(arch, PositionID);
    arch = archid_setbit(arch, WormholeID);

    query_init(q, ecs0, arch);
    while(query_next(q)){
		Position* pos = query_term(ecs0, q, PositionID);
		Wormhole* worm = query_term(ecs0, q, WormholeID);
		(void)worm;

		if(ecs->entnext >= ecs->entsmax){ break; }

        for(int i = 0; i < q->size; ++i){

			if(ecs->entnext >= ecs->entsmax){ break; }

    //Spawn particles
    int spawned = particle_freq_to_count(
	20.0f,
	fdelta, romu_trio32_norm(parts->romu_trio32));
    for(int j = 0; j < spawned; ++j){

		if(ecs->entnext >= ecs->entsmax){ break; }

		vec3 dir; romu_trio32_vec3_rand(parts->romu_trio32, dir);
		uint32_t dist = (0x100 + romu_trio32_random(parts->romu_trio32) % 0x100) << 16;
		vec3 off; glm_vec3_scale(dir, dist, off);

		//Position
		v3uint32_add(pos[i].pos, (int32_t[3]){ off[0], off[1], off[2] }, position);

		//Pointphys
		glm_vec3_scale(dir, -1 * (0x40 << 16), dir);
		vec3_to_int32(dir, velocity);

		//Lifetime
		life_sec = 4.0f;

		//Color
		for(int k = 0; k < 3; ++k){ glm_vec4_ucopy(color, colors[k]); }
		colors[1][3] = 0.8f;
		
		//Sprite
		spr_idx = PART_DOT1 +
			romu_trio32_random(parts->romu_trio32) % (PART_DOT2 - PART_DOT1 + 1);

		//Spawn particle
		parts_spawn_pos_vel_life_col_spr(parts,
			position, velocity, &age, life_sec, colors, spr_idx);
    }
		}
    }
}

