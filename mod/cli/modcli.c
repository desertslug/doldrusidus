#include "modcli.h"

void modclient_start(ModClient* modcli, void* client)
{
    //Store
    modcli->client = client;

    //Initialize world
    world_alloc(&modcli->world);
    world_init(&modcli->world, 0xffff /* TODO? */);

	//Allocate piloted entity query results
	rtcp_uvec_malloc(&modcli->piloted, 1, sizeof(Piloted));
	rtcp_uvec_init(&modcli->piloted);

	//Initialize random number generator
	modcli->rand.buf[0] = 1;
	modcli->rand.last = 0x8badf00d;

    //Reset input event buffer
    buffer_reset(&modcli->ievt);

    //Initialize commands
    rtcp_uvec_malloc(&modcli->cmds, 1, sizeof(Command));
    rtcp_uvec_init(&modcli->cmds);
    cmds_append_all(&modcli->cmds);

    //Initialize first argument storage for commands
    rtcp_uvec_malloc(&modcli->firstargs, 1, sizeof(lstr_t));
    rtcp_uvec_init(&modcli->firstargs);
    for(int i = 0; i < modcli->cmds.size; ++i){
        lstr_t lstr; lstr_malloc(&lstr, 1); lstr_init(&lstr);
        rtcp_uvec_add(&modcli->firstargs, &lstr);
    }

    //Initialize camera
    camera_init(&modcli->camera,
        (vec3){ 0.0f, 0.0f, 1.0f },
        (uint32_t[3]){ 0x80000000, 0x80000000, 0x80000000 },
        (vec3){ 1.0f, 1.0f, 1.0f },
        0.0f,
        0.0f,
        TWOPOW16F
    );

    //Initialize particle system
    parts_alloc(&modcli->parts);
    parts_init(&modcli->parts, (uint32_t[3]){ 0x8badf00d, 0xabadc0de, 0x1ee7cafe });

    //Initialize cosmic wind
    wind_init(&modcli->wind, (uint32_t[3]){ 0x8badf00d, 0xabadc0de, 0x1ee7cafe });
}

void modclient_stop(ModClient* modcli)
{
    //Deallocate first argument storage
    for(int i = 0; i < modcli->firstargs.size; ++i){
        lstr_free(rtcp_uvec_get(&modcli->firstargs, i));
    }
    rtcp_uvec_free(&modcli->firstargs);

    //Deallocate world
    world_dealloc(&modcli->world);

	//Deallocate piloted entity query results
	rtcp_uvec_free(&modcli->piloted);

    //Deallocate commands
    rtcp_uvec_free(&modcli->cmds);

    //Deallocate particle system
    parts_dealloc(&modcli->parts);
}

int pilotent_cmp(void* a, void* b)
{
	Piloted* lhs = (Piloted*)a;
	Piloted* rhs = (Piloted*)b;
	
	return lhs->dist > rhs->dist;
}

void modclient_pilotents_ordered(ModClient* modcli, PilotedFilter filter)
{
    World* world = &modcli->world;
	ECS* ecs = &world->ecs;
	Camera* camera = &modcli->camera;

	uint32_t bbox[3][2];

	switch(filter){
	case PilotedAll: {} break;
	case PilotedWithinLookAt: {

		//Find bounding box
		uint32_t zoom = camera_get_zoom_u32(camera);
		v3uint32_bbox(camera->pos, zoom, bbox);

	} break;
	default: { assert_or_exit(0, "modclient_pilotents_ordered\n"); } break;
	}


	//Attempt to get an authenticated username
	const char* uname = client_get_uname(modcli->client);
	if(!uname){ return; }

	//Re-initialize piloted entities
    rtcp_uvec_init(&modcli->piloted);

    //Run query on components
    ArchID arch = (ArchID){0};
    arch = archid_setbit(arch, PositionID);
    arch = archid_setbit(arch, PilotID);
    Query q; query_alloc(&q); query_init(&q, ecs, arch);
    while(query_next(&q)){
        Entity* ents = query_ents(ecs, &q);
		Position* pos = query_term(ecs, &q, PositionID);
		Pilot* pilot = query_term(ecs, &q, PilotID);

		for(int i = 0; i < q.size; ++i){
			if(strcmp(pilot[i].uname, uname) == 0){

				switch(filter){
				case PilotedWithinLookAt: {

					//Check that the entity is in the lookat bbox
					if(!v3uint32_in_bbox(pos[i].pos, bbox)){ continue; }

				} break;
				default: {} break;
				}

				//Calculate distance to camera origin
				float dist = v3uint32_dist(camera->pos, pos[i].pos);
				Piloted pild = (Piloted){ ents[i], dist / TWOPOW16F };
				rtcp_uvec_add(&modcli->piloted, &pild);
			}
		}
	}
    query_dealloc(&q);

	//Insertion sort piloted entities by distance, ascending
	Piloted swap;
	rtcp_uvec_insertsort(&modcli->piloted, pilotent_cmp, &swap);
}

Entity modclient_pilotent_weighted(ModClient* modcli, PilotedFilter filter)
{
	rtcp_uvec_t* pild = &modcli->piloted;

	//Build cache of piloted entities again
	modclient_pilotents_ordered(modcli, filter);

	//If there are no piloted entities stored, return extremal ID
	if(pild->size < 1){ return 0xffff; }

	//Assign decreasing weights, and compute total weight
	const int min = ENTITY_SYNC_WEIGHT_PCT_MIN;
	const int max = ENTITY_SYNC_WEIGHT_PCT_MAX;
	int sum = 0;
	for(int i = 0; i < pild->size; ++i){

		//Interpolate weight based on position in ordering
		Piloted* p = rtcp_uvec_get(pild, i);
		p->weight = glm_lerp(max, min, i / (float)pild->size);

		//Increase total weight
		sum += p->weight;
	}

	//Roll target weight randomly up to the total weight
	int target = glm_lerp(0, sum, hash_next_norm(&modcli->rand));

	//Find entity that increases the running total beyond the target
	int i = 0;
	int runtotal = 0;
	while(i < pild->size && runtotal <= target){
		Piloted* p = rtcp_uvec_get(pild, i);
		runtotal += p->weight;
		++i;
	}

	Piloted* select = rtcp_uvec_get(pild, i - 1);
	return select->ent;
}

Entity modclient_get_sync_entity(ModClient* modcli, SyncModality syncmode)
{
	switch(syncmode){
	case SyncModeCamera: {
	
		switch(client_get_access(modcli->client)){
		case AccessGuest: {

			//Randomize a piloted entity within the lookat bbox
			return modclient_pilotent_weighted(modcli, PilotedWithinLookAt);

		} break;
		default: {

			//Extremal entity ID means no specified entity here
			return 0xffff;

		} break;
		}

	} break;
	case SyncModeWeighted: {

		//Randomize a piloted entity from a weighted ordering
		return modclient_pilotent_weighted(modcli, PilotedAll);

	} break;
	default: { assert_or_exit(0, "modclient_get_sync_entity"); } break;
	}

	return 0xffff; //unreachable
}

void cmds_append_all(rtcp_uvec_t* cmds)
{
    int i = 0;

    assert_or_exit(i == MSG_LOGIN, "cmds_append_all\n"); ++i;
    rtcp_uvec_add(cmds, &(Command){
        "login", "", });
    assert_or_exit(i == MSG_VERIFY, "cmds_append_all\n"); ++i;
    rtcp_uvec_add(cmds, &(Command){
        "", "", });
    assert_or_exit(i == MSG_LOGOUT, "cmds_append_all\n"); ++i;
    rtcp_uvec_add(cmds, &(Command){
        "logout", "", });
    assert_or_exit(i == MSG_CHANGEPW, "cmds_append_all\n"); ++i;
    rtcp_uvec_add(cmds, &(Command){
        "changepw", "", });
    assert_or_exit(i == MSG_KICK, "cmds_append_all\n"); ++i;
    rtcp_uvec_add(cmds, &(Command){
        "kick", "", });
    assert_or_exit(i == MSG_REGISTER, "cmds_append_all\n"); ++i;
    rtcp_uvec_add(cmds, &(Command){
        "register", "", });
    assert_or_exit(i == MSG_UNREGISTER, "cmds_append_all\n"); ++i;
    rtcp_uvec_add(cmds, &(Command){
        "unregister", "", });
    assert_or_exit(i == MSG_CHANGEACC, "cmds_append_all\n"); ++i;
    rtcp_uvec_add(cmds, &(Command){
        "changeacc", "", });
    assert_or_exit(i == MSG_GETAUTHS, "cmds_append_all\n"); ++i;
    rtcp_uvec_add(cmds, &(Command){
        "auths", "", });
    assert_or_exit(i == MSG_GETSESSIONS, "cmds_append_all\n"); ++i;
    rtcp_uvec_add(cmds, &(Command){
        "sessions", "", });
    assert_or_exit(i == MSG_SETTPS, "cmds_append_all\n"); ++i;
    rtcp_uvec_add(cmds, &(Command){
        "settps", "", });
    assert_or_exit(i == MSG_SETTPSPASS, "cmds_append_all\n"); ++i;
    rtcp_uvec_add(cmds, &(Command){
        "settpspass", "", });
    assert_or_exit(i == MSG_LOG_REOPEN, "cmds_append_all\n"); ++i;
    rtcp_uvec_add(cmds, &(Command){
        "logreopen", "", });
    assert_or_exit(i == MSG_CHATGLOBAL, "cmds_append_all\n"); ++i;
    rtcp_uvec_add(cmds, &(Command){
        "say", ";", });
    assert_or_exit(i == MSG_CHATWHISPER, "cmds_append_all\n"); ++i;
    rtcp_uvec_add(cmds, &(Command){
        "tell", ",", });
    assert_or_exit(i == MOD_MSG_SAVEWORLD, "cmds_append_all\n"); ++i;
    rtcp_uvec_add(cmds, &(Command){
        "save", "", });
    assert_or_exit(i == MOD_MSG_LOADWORLD, "cmds_append_all\n"); ++i;
    rtcp_uvec_add(cmds, &(Command){
        "load", "", });
    assert_or_exit(i == MOD_MSG_GENWORLD, "cmds_append_all\n"); ++i;
    rtcp_uvec_add(cmds, &(Command){
        "generate", "", });
    assert_or_exit(i == MOD_MSG_TESTWORLD, "cmds_append_all\n"); ++i;
    rtcp_uvec_add(cmds, &(Command){
        "test", "", });
    assert_or_exit(i == MOD_MSG_ADDSHIP, "cmds_append_all\n"); ++i;
    rtcp_uvec_add(cmds, &(Command){
        "addship", "", });
    assert_or_exit(i == MOD_MSG_DELSHIP, "cmds_append_all\n"); ++i;
    rtcp_uvec_add(cmds, &(Command){
        "delship", "", });
    assert_or_exit(i == MOD_MSG_LISTSHIP, "cmds_append_all\n"); ++i;
    rtcp_uvec_add(cmds, &(Command){
        "ships", "", });
    assert_or_exit(i == MOD_MSG_UXN_RAM, "cmds_append_all\n"); ++i;
    rtcp_uvec_add(cmds, &(Command){
        "uxn-ram", "!", });
    assert_or_exit(i == MOD_MSG_UXN_BOOT, "cmds_append_all\n"); ++i;
    rtcp_uvec_add(cmds, &(Command){
        "uxn-boot", "", });
    assert_or_exit(i == MOD_MSG_UXN_HALT, "cmds_append_all\n"); ++i;
    rtcp_uvec_add(cmds, &(Command){
        "uxn-halt", "", });
    assert_or_exit(i == MOD_MSG_UXN_RESUME, "cmds_append_all\n"); ++i;
    rtcp_uvec_add(cmds, &(Command){
        "uxn-resume", "", });
    assert_or_exit(i == MOD_MSG_UXN_FEED, "cmds_append_all\n"); ++i;
    rtcp_uvec_add(cmds, &(Command){
        "cons", ".", });
    assert_or_exit(i == MOD_MSG_SYNC_ECS, "cmds_append_all\n"); ++i;
    rtcp_uvec_add(cmds, &(Command){
        "", "", });
    assert_or_exit(i == MOD_MSG_SYNC_ECS_ENT, "cmds_append_all\n"); ++i;
    rtcp_uvec_add(cmds, &(Command){
        "", "", });
    assert_or_exit(i == MOD_MSG_SYNC_ECS_DELETE, "cmds_append_all\n"); ++i;
    rtcp_uvec_add(cmds, &(Command){
        "", "", });
    assert_or_exit(i == MOD_MSG_SYNC_SESSION, "cmds_append_all\n"); ++i;
    rtcp_uvec_add(cmds, &(Command){
        "", "", });
    assert_or_exit(i == MOD_MSG_WINDOW_OPEN, "cmds_append_all\n"); ++i;
    rtcp_uvec_add(cmds, &(Command){
        "ow", "", });
    assert_or_exit(i == MOD_MSG_WINDOW_CLOSE, "cmds_append_all\n"); ++i;
    rtcp_uvec_add(cmds, &(Command){
        "cw", "", });
    assert_or_exit(i == MOD_MSG_ENT_FOCUS, "cmds_append_all\n"); ++i;
    rtcp_uvec_add(cmds, &(Command){
        "focus", ">", });
    assert_or_exit(i == MOD_MSG_ENT_UNFOCUS, "cmds_append_all\n"); ++i;
    rtcp_uvec_add(cmds, &(Command){
        "unfocus", "<", });
}
