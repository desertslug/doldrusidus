#ifndef TERMUI_H
#define TERMUI_H

#include "uvec.h"
#include "lstr.h"
#include "climsg.h"

enum { TUI_PRINT_MAX = 0xffff, };
char TUI_PRINT[TUI_PRINT_MAX];

typedef struct termui_t
{
    int vcursor[2];
    int vidx;

    bool inputdelta;
    int inrows;
    lstr_t input;
    lstr_t prompt;

    rtcp_uvec_t inbuf;

    bool historydelta;
    int historylast;
    int historyscroll;
    rtcp_uvec_t history;

} termui_t;

void termui_init(termui_t* tui, int history);
void termui_stop(termui_t* tui);

lstr_t* termui_history_get_curr(termui_t* tui);
lstr_t* termui_history_get_next(termui_t* tui);

void termui_virtcursor_next_offset(termui_t* tui, int off, int pos_out[2]);
void termui_input_key(termui_t* tui, uint16_t key, rtcp_uvec_t* cmds, rtcp_uvec_t* firstargs);

void termui_input_char(termui_t* tui, uint32_t ch, rtcp_uvec_t* cmds, rtcp_uvec_t* firstargs);
void termui_input_draw(termui_t* tui);

void termui_update(termui_t* tui, rtcp_uvec_t* cmds, rtcp_uvec_t* firstargs);

#endif /* TERMUI_H */
