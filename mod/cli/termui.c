#include "termui.h"

void termui_init(termui_t* tui, int history)
{
    //Allocate
    lstr_malloc(&tui->input, 1);
    lstr_init(&tui->input);
    lstr_malloc(&tui->prompt, 1);
    lstr_init(&tui->prompt);
    rtcp_uvec_malloc(&tui->inbuf, 1, sizeof(lstr_t));
    rtcp_uvec_init(&tui->inbuf);
    rtcp_uvec_malloc(&tui->history, 1, sizeof(lstr_t));
    rtcp_uvec_init(&tui->history);
    for(int i = 0; i < history; ++i){
        lstr_t lstr; lstr_malloc(&lstr, 1); lstr_init(&lstr);
        rtcp_uvec_add(&tui->history, &lstr);
    }

    //Initialize values
    tui->vcursor[0] = 0;
    tui->vcursor[1] = 0;
    tui->vidx = 0;
    tui->inputdelta = true;
    tui->inrows = 1;
    tui->historydelta = true;
    tui->historylast = 0;
    tui->historyscroll = 0;

    //Initialize termbox
    assert_or_exit(tb_init() == 0, "termui_init: tb_init() failed\n");
}

void termui_stop(termui_t* tui)
{
    //Deallocate
    for(int i = tui->inbuf.size - 1; i >= 0; --i){
        lstr_free(rtcp_uvec_get(&tui->inbuf, i));
        rtcp_uvec_del(&tui->inbuf, i);
    }
    rtcp_uvec_free(&tui->inbuf);
    for(int i = tui->history.size - 1; i >= 0; --i){
        lstr_free(rtcp_uvec_get(&tui->history, i));
        rtcp_uvec_del(&tui->history, i);
    }
    rtcp_uvec_free(&tui->history);
    lstr_free(&tui->input);
    lstr_free(&tui->prompt);
}

lstr_t* termui_history_get_curr(termui_t* tui)
{
    //Get next string, increment last (exclusive) index, mark as changed
    int idx = (tui->historylast - 1 + tui->history.size)
        % tui->history.size;
    lstr_t* lstr = rtcp_uvec_get(&tui->history, idx);
    tui->historydelta = true;

    return lstr;
}

lstr_t* termui_history_get_next(termui_t* tui)
{
    //Get next string, increment last (exclusive) index, mark as changed
    lstr_t* lstr = rtcp_uvec_get(&tui->history, tui->historylast);
    tui->historylast = (tui->historylast + 1) % tui->history.size;
    tui->historydelta = true;

    return lstr;
}

void termui_virtcursor_next_offset(termui_t* tui, int off, int pos_out[2])
{
    int w = tb_width(); int h = tb_height();

    //Add the offset, clamp to first position and after the last
    tui->vidx = clamp(tui->vidx + off, 0, lstr_len(&tui->input));

    //Calculate the true index, combining the prompt and input
    int vidx = tui->vidx + lstr_len(&tui->prompt);

    //Calculate the proper y and x offsets, assuming the row starts at zero
    pos_out[0] = vidx % w;
    pos_out[1] = h - tui->inrows + vidx / w;
}

void termui_input_key(termui_t* tui, uint16_t key, rtcp_uvec_t* cmds, rtcp_uvec_t* firstargs)
{
    int w = tb_width();

    switch(key){
    case TB_KEY_TAB: {

        //Separate leading whitespace in input
        const char* input = lstr_get(&tui->input);
        int curr = strspn(input, WHITESPACE);
        int next = curr;

        //Attempt to parse command
        int cmdidx;
        next += str_process_to_cmd(cmds,
            &input[curr], lstr_len(&tui->input) - curr, &cmdidx);
        if(next == curr){ break; }

        //Ignore whitespace
        next += strspn(&input[next], WHITESPACE);

        //Check that only the command was typed
        if(next != lstr_len(&tui->input)){ break; }

        //Append most recent first argument stored for the command
        lstr_t* firstarg = rtcp_uvec_get(firstargs, cmdidx);
        lstr_cat_str(&tui->input, lstr_get(firstarg));

        //Move virtual cursor
        termui_virtcursor_next_offset(tui, lstr_len(firstarg), tui->vcursor);
        tb_set_cursor(tui->vcursor[0], tui->vcursor[1]);

    } break;
    case TB_KEY_ENTER: {
        
        //Display input in the history
        chprintf("%s\n", lstr_get(&tui->input));

        //Push input to queue
        lstr_t lstr; lstr_malloc(&lstr, 1); lstr_init(&lstr);
        lstr_cat_str(&lstr, lstr_get(&tui->input));
        rtcp_uvec_add(&tui->inbuf, &lstr);

        //Reset input field
        lstr_clear(&tui->input);

        //Move virtual cursor
        termui_virtcursor_next_offset(tui, 0, tui->vcursor);
        tb_set_cursor(tui->vcursor[0], tui->vcursor[1]);
        
    } break;
    case TB_KEY_BACKSPACE: case TB_KEY_BACKSPACE2:
    case TB_KEY_DELETE: {

        //Do nothing if the input buffer is already empty
        if(lstr_len(&tui->input) == 0){ break; }

        //The index of the character to delete
        int idx_del = key == TB_KEY_DELETE ? tui->vidx : tui->vidx - 1;

        //Remove the character from the input buffer
        if(idx_del >= 0 && idx_del < lstr_len(&tui->input)){

            lstr_del_pos(&tui->input, idx_del);

            //Move virtual cursor
            termui_virtcursor_next_offset(tui,
                key == TB_KEY_DELETE ? 0 : -1, tui->vcursor);
            tb_set_cursor(tui->vcursor[0], tui->vcursor[1]);
        }

    } break;
    case TB_KEY_PGUP: {

        //Increment scroll offset if it does not exceed the history size
        if(tui->historyscroll < tui->history.size - 1){
            ++tui->historyscroll;
            tui->historydelta = true;
        }

    } break;
    case TB_KEY_PGDN: {

        //Decrement scroll offset until it reaches zero
        if(tui->historyscroll > 0){
            --tui->historyscroll;
            tui->historydelta = true;
        }

    } break;
    case TB_KEY_HOME: case TB_KEY_END:
    case TB_KEY_ARROW_UP:   case TB_KEY_ARROW_DOWN:
    case TB_KEY_ARROW_LEFT: case TB_KEY_ARROW_RIGHT:
    {
        //Calculate offset
        int offset = 0;
        switch(key){
            case TB_KEY_HOME : offset = -1 * lstr_len(&tui->input); break;
            case TB_KEY_END  : offset = lstr_len(&tui->input); break;
            case TB_KEY_ARROW_UP   : offset = -1 * w; break;
            case TB_KEY_ARROW_DOWN : offset = w; break;
            case TB_KEY_ARROW_LEFT : offset = -1; break;
            case TB_KEY_ARROW_RIGHT: offset = +1; break;
        }

        //Move virtual cursor
        termui_virtcursor_next_offset(tui, offset, tui->vcursor);
        tb_set_cursor(tui->vcursor[0], tui->vcursor[1]);

    } break;
    default: {} break;
    }
}

void termui_input_char(termui_t* tui, uint32_t ch, rtcp_uvec_t* cmds, rtcp_uvec_t* firstargs)
{
    switch(ch){
    case '\n' : {

        termui_input_key(tui, TB_KEY_ENTER, cmds, firstargs);

    } break;
    default: {

        //Check whether character is printable ASCII
        if(ch >= 32 && ch <= 127){

            //Insert into the input
            lstr_insert_str(&tui->input, tui->vidx, (char[2]){ ch, '\0' });
            
            //Move virtual cursor
            termui_virtcursor_next_offset(tui, 1, tui->vcursor);
            tb_set_cursor(tui->vcursor[0], tui->vcursor[1]);
        }

    } break;
    }
}

void termui_input_draw(termui_t* tui)
{
    int w = tb_width(); int h = tb_height();

    //Clear the previous starting line
    clr_to_eol(0, h - tui->inrows);

    //Draw prompt
    curse_str_end_at_row(
        h - 1 - (lstr_len(&tui->input) + lstr_len(&tui->prompt)) / w,
        0, lstr_get(&tui->prompt), lstr_len(&tui->prompt));

    //Draw input
    tui->inrows = curse_str_end_at_row(
	    h - 1, lstr_len(&tui->prompt),
        lstr_get(&tui->input), lstr_len(&tui->input));

    //Update the virtual & real cursor positions
    termui_virtcursor_next_offset(tui, 0, tui->vcursor);
    tb_set_cursor(tui->vcursor[0], tui->vcursor[1]);
}

void termui_update(termui_t* tui, rtcp_uvec_t* cmds, rtcp_uvec_t* firstargs)
{
    bool delta = false;

    //Process events
    struct tb_event evt;
    int ret = tb_peek_event(&evt, 0);
    if(ret != TB_ERR_NO_EVENT && ret != TB_ERR_POLL){
        switch(evt.type){
        case TB_EVENT_KEY: {

            //Note input field size & virtual cursor pos
            int prev_size = lstr_len(&tui->input);
            int vcursor[2]; memcpy(vcursor, tui->vcursor, sizeof(int[2]));

            termui_input_char(tui, evt.ch, cmds, firstargs);
            termui_input_key(tui, evt.key, cmds, firstargs);

            //If the input size changed, or the virtual cursor moved, mark delta
            if(
                prev_size != lstr_len(&tui->input) ||
                vcursor[0] != tui->vcursor[0] ||
                vcursor[1] != tui->vcursor[1]
            ){ tui->inputdelta = true; }

            //Send interrupt signal upon ^C
            if(evt.ch == 0 && evt.key == TB_KEY_CTRL_C){ raise(SIGINT); }

        } break;
        case TB_EVENT_RESIZE: {

            //If the window dimensions have changed, redraw or delta all fields
            //tui->inputdelta = tui->recvstrdelta = tui->diagstrdelta = true;
            tui->inputdelta = tui->historydelta = true;
            
        } break;
        case TB_EVENT_MOUSE: {
            
        } break;
        default: {} break;
        }
    }

    //Redraw input field if it is changed
    if(tui->inputdelta){
        termui_input_draw(tui);
        tui->inputdelta = false;
        delta = true;
    }

    //Redraw text history if it has changed, or if the previous fields have
    if(delta || tui->historydelta){

        int w = tb_width(); int h = tb_height();
        //lstr_t* last = rtcp_uvec_get(&tui->history, tui->historylast);
        lstr_t* last = termui_history_get_curr(tui);
        curse_struvec_end_at_row(
            h - (lstr_len(&tui->prompt) + lstr_len(&tui->input)) / w - 2,
            &tui->history,
            (
                tui->historylast - 1 - tui->historyscroll
                - (lstr_len(last) == 0) + tui->history.size
            ) % tui->history.size);

        tui->historydelta = false;
        delta = true;
    }

    //Synchronize the internal back buffer with the terminal
    if(delta){ tb_present(); }
}

void tui_vhprintf(termui_t* tui, const char* fmt, va_list vl, uint16_t focusent)
{
    int bytes = vsnprintf(TUI_PRINT, TUI_PRINT_MAX, fmt, vl);

    //If output was truncated, null-terminate manually
    if(bytes >= TUI_PRINT_MAX){ TUI_PRINT[TUI_PRINT_MAX - 1] = '\0'; }

    //Separate lines to print them individually
    lstr_t* lstr = termui_history_get_curr(tui);
    char* curr; char* next = TUI_PRINT;
    while(true){

        //Attempt to find the next newline
        curr = next; next = strchr(curr, '\n');
        
        //If a newline was found
        if(next){

            //Zero terminate, then write
            next[0] = '\0';

            //If there is no focused entity
            if(focusent == 0xffff){

                //Write
                lstr_cat_str(lstr, curr);

                //Get next string & clear it
                lstr = termui_history_get_next(tui); lstr_clear(lstr);
            }

        } else {

            //Concatenate, finish printing
            lstr_cat_str(lstr, curr);
            break;
        }
    }
}
