#ifndef CURSE_H
#define CURSE_H

#undef TB_IMPL
#include "termbox2.h"

#include "uvec.h"

void clr_to_eol(int cx, int cy);

void tb_print_wrap(int x, int y, uintattr_t fg, uintattr_t bg, const char* str, int size);

int str_display_lines(int startoff, const char* str, size_t sz);

int curse_str_end_at_row(int lastrow, int startoff, const char* str, int size);

void curse_struvec_end_at_row(int lastrow, rtcp_uvec_t* uvec, int lastidx);

#endif /* CURSE_H */
