#include "camera.h"

void camera_init(Camera* camera, vec3 up, uint32_t* pos, vec3 dir, float azi, float alt, float zoom_pos)
{
    //Store initial values
    glm_vec3_copy(up, camera->worldup);
    memcpy(camera->pos, pos, sizeof(uint32_t) * 3);
    glm_vec3_copy(dir, camera->dir);
    camera->azi = azi;
    camera->alt = alt;
    camera->zoom_pos = zoom_pos;

    //Update right & up vectors
    camera_dir_right_up(camera);

    //Unset cursor disabling as its starting state
    camera->kbm_cursor_disabled = false;

    //Set followed entity to an invalid value
    camera->followent = 0xffff; //TODO

    //Set non-proquint display by default
    camera->proquint = false;

	//Set camera-based sync modality by default
	camera->syncmode = SyncModeCamera;

    //Inherit configuration
    camera->kbm_zoom_sens = CAM_ZOOM_SENS_KBMOUSE;
    camera->gmp_zoom_sens = CAM_ZOOM_SENS_GAMEPAD;
    camera->kbm_orbit_sens[0] = CAM_ORBIT_X_SENS_KBMOUSE;
    camera->kbm_orbit_sens[1] = CAM_ORBIT_Y_SENS_KBMOUSE;
    camera->gmp_orbit_sens[0] = CAM_ORBIT_X_SENS_GAMEPAD;
    camera->gmp_orbit_sens[1] = CAM_ORBIT_Y_SENS_GAMEPAD;

    camera->zoom_vel = 0.0f;
    camera->vel[0] = camera->vel[1] = camera->vel[2] = 0;
}

void camera_dir_right_up(Camera* camera)
{
    //Direction
    camera->dir[0] = cos(glm_rad(camera->azi)) * cos(glm_rad(camera->alt));
    camera->dir[1] = sin(glm_rad(camera->azi)) * cos(glm_rad(camera->alt));
    camera->dir[2] = sin(glm_rad(camera->alt));
    glm_vec3_normalize(camera->dir);

    //Right
    glm_vec3_cross(camera->dir, camera->worldup, camera->right);
    glm_vec3_normalize(camera->right);

    //Up
    glm_vec3_cross(camera->right, camera->dir, camera->up);
    glm_vec3_normalize(camera->up);
}

void camera_process_kbmouse(Camera* camera, KBMouse* kbm, GLFWwindow* window, ECS* ecs, Entity resolved)
{
    //Extract azimuth & altitude angle offsets
    if(
        !kbm->buttons_delta[GL_MOUSE_BUTTON_1] &&
        kbm->buttons_act[GL_MOUSE_BUTTON_1] == GL_RELEASE_ &&
        kbm->buttons_delta[GL_MOUSE_BUTTON_2] &&
        kbm->buttons_act[GL_MOUSE_BUTTON_2] > GL_RELEASE_
    ){
        //Update azimuth & altitude angles
        camera->azi += camera->kbm_orbit_sens[0] *
            kbm->cursor_rel[0];
        camera->alt += camera->kbm_orbit_sens[1] *
            kbm->cursor_rel[1];

        //Constrain azimuth & altitude angles
        camera->azi = fmodf(camera->azi, glm_deg(M_PI * 2.0f));
        camera->alt = fclamp(camera->alt,
            -1.0f * CAMERA_ALTITUDE_MAX, CAMERA_ALTITUDE_MAX);

        //Update right & up vectors
        camera_dir_right_up(camera);
    }

    //Extract zoom offset from pressed keys
    float zoom_off = 0.0f;
    if (kbm->keys_delta[GL_KEY_E] &&
        kbm->keys_act[GL_KEY_E] > GL_RELEASE_)
    { zoom_off -= 1.0f; }
    if (kbm->keys_delta[GL_KEY_Q] &&
        kbm->keys_act[GL_KEY_Q] > GL_RELEASE_)
    { zoom_off += 1.0f; }

    //Extract zoom offset from mouse
    if(
        kbm->buttons_delta[GL_MOUSE_BUTTON_1] &&
        kbm->buttons_act[GL_MOUSE_BUTTON_1] > GL_RELEASE_ &&
        kbm->buttons_delta[GL_MOUSE_BUTTON_2] &&
        kbm->buttons_act[GL_MOUSE_BUTTON_2] > GL_RELEASE_
    ){
        zoom_off += camera->kbm_zoom_sens * kbm->cursor_rel[1];
    }

    //Set camera zoom velocity
    camera->zoom_vel = zoom_off * camera->zoom_pos;

    //Extract position offset from pressed keys
    vec3 pos_off = { 0.0f, 0.0f, 0.0f };
    if (kbm->keys_delta[GL_KEY_D] &&
        kbm->keys_act[GL_KEY_D] > GL_RELEASE_)
    { pos_off[0] += 1.0f; }
    if (kbm->keys_delta[GL_KEY_A] &&
        kbm->keys_act[GL_KEY_A] > GL_RELEASE_)
    { pos_off[0] -= 1.0f; }
    if (kbm->keys_delta[GL_KEY_S] &&
        kbm->keys_act[GL_KEY_S] > GL_RELEASE_)
    { pos_off[1] -= 1.0f; }
    if (kbm->keys_delta[GL_KEY_W] &&
        kbm->keys_act[GL_KEY_W] > GL_RELEASE_)
    { pos_off[1] += 1.0f; }
    if (kbm->keys_delta[GL_KEY_SPACE] &&
        kbm->keys_act[GL_KEY_SPACE] > GL_RELEASE_)
    { pos_off[2] += 1.0f; }
    if (kbm->keys_delta[GL_KEY_LEFT_CONTROL] &&
        kbm->keys_act[GL_KEY_LEFT_CONTROL] > GL_RELEASE_)
    { pos_off[2] -= 1.0f; }

    //Rotate offset by azimuthal angle
    glm_vec3_normalize(pos_off);
    glm_vec3_rotate(pos_off, glm_rad(camera->azi + 90.0f), (vec3){ 0.0f, 0.0f, 1.0f });

    //Scale vector by camera speed
    glm_vec3_scale(pos_off, camera->zoom_pos * 0.25f, pos_off);

    //Set camera velocity
    for(int i = 0; i < 3; ++i){
        camera->vel[i] = TWOPOW16 * (int32_t)pos_off[i];
    }

    //If left shift was pressed, attempt to jump to a resolved entity
    if(
        kbm->keys_delta[GL_KEY_LEFT_SHIFT] &&
        kbm->keys_act[GL_KEY_LEFT_SHIFT] == GL_PRESS_ &&
        resolved != 0xffff && ecs_entity_alive(ecs, resolved)
    ){
        //Retrieve position
        const Position* pos = ecs_get_id(ecs, resolved, PositionID);
        if(pos){

            //Set the camera's position to the entity's
            memcpy(camera->pos, pos->pos, sizeof(uint32_t) * 3);
        }
    }
    
    //If F was pressed, attempt to start following an entity
    if(
        kbm->keys_delta[GL_KEY_F] &&
        kbm->keys_act[GL_KEY_F] == GL_PRESS_ &&
        resolved != 0xffff && ecs_entity_alive(ecs, resolved) &&
        ecs_get_id(ecs, resolved, PositionID)
    ){
        camera->followent = resolved;
    }
    
    //If F was pressed, and there is no resolved entity, unfollow any entity
    if(
        kbm->keys_delta[GL_KEY_F] &&
        kbm->keys_act[GL_KEY_F] == GL_PRESS_ &&
        resolved == 0xffff
    ){
        camera->followent = 0xffff;
    }

    //Cursor grabbing & hiding state machine
    if(CLI_CURSOR_DISABLING){
        if(
            !camera->kbm_cursor_disabled &&
            glfwGetInputMode(window, GLFW_CURSOR) != GLFW_CURSOR_DISABLED &&
            (kbm->buttons_delta[GL_MOUSE_BUTTON_2] &&
            kbm->buttons_act[GL_MOUSE_BUTTON_2] > GL_RELEASE_)
        ){
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
            camera->kbm_cursor_disabled = true;
        }
        if(
            camera->kbm_cursor_disabled &&
            glfwGetInputMode(window, GLFW_CURSOR) == GLFW_CURSOR_DISABLED &&
            !(kbm->buttons_delta[GL_MOUSE_BUTTON_2] &&
            kbm->buttons_act[GL_MOUSE_BUTTON_2] > GL_RELEASE_)
        ){
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
            camera->kbm_cursor_disabled = false;
        }
    }

    //Proquint or hex quart display
    if(kbm->keys_delta[GL_KEY_P] && kbm->keys_act[GL_KEY_P] == GL_PRESS_){
        camera->proquint = !camera->proquint;
    }

	//Cycle sync modality
    if(kbm->keys_delta[GL_KEY_M] && kbm->keys_act[GL_KEY_M] == GL_PRESS_){
		camera->syncmode = (camera->syncmode + 1) % SyncModeCount;
	}
}

void camera_process_gamepad(Camera* camera, Gamepad* gmp, GLFWwindow* window, ECS* ecs, Entity resolved)
{
    //Extract azimuth & altitude angle offsets
    //Rely on values being reset to 0.0f inside the deadzone
    if(
        gmp->axes_delta[GL_GAMEPAD_AXIS_RIGHT_X] ||
        gmp->axes_delta[GL_GAMEPAD_AXIS_RIGHT_Y]
    ){
        //Update azimuth & altitude angles
        camera->azi += camera->gmp_orbit_sens[0] *
            gmp->axes_val[GL_GAMEPAD_AXIS_RIGHT_X];
        camera->alt += camera->gmp_orbit_sens[1] *
            gmp->axes_val[GL_GAMEPAD_AXIS_RIGHT_Y];

        //Constrain azimuth & altitude angles
        camera->azi = fmodf(camera->azi, glm_deg(M_PI * 2.0f));
        camera->alt = fclamp(camera->alt,
            -1.0f * CAMERA_ALTITUDE_MAX, CAMERA_ALTITUDE_MAX);

        //Update right & up vectors
        camera_dir_right_up(camera);
    }

    //Extract zoom offset
    float zoom_off = 0.0f;
    if(
        gmp->buttons_delta[GL_GAMEPAD_BUTTON_RIGHT_BUMPER] &&
        gmp->buttons_act[GL_GAMEPAD_BUTTON_RIGHT_BUMPER] > GL_RELEASE_
    ){
        zoom_off -= 1.0f;
    }
    if(
        gmp->buttons_delta[GL_GAMEPAD_BUTTON_LEFT_BUMPER] &&
        gmp->buttons_act[GL_GAMEPAD_BUTTON_LEFT_BUMPER] > GL_RELEASE_
    ){
        zoom_off += 1.0f;
    }
    
    //Set camera zoom velocity
    camera->zoom_vel = zoom_off * camera->zoom_pos;

    //Extract position offset
    vec3 pos_off = { 0.0f, 0.0f, 0.0f };
    if(gmp->axes_delta[GL_GAMEPAD_AXIS_LEFT_X]){
        pos_off[0] += gmp->axes_val[GL_GAMEPAD_AXIS_LEFT_X];
    }
    if(gmp->axes_delta[GL_GAMEPAD_AXIS_LEFT_Y]){
        pos_off[1] -= gmp->axes_val[GL_GAMEPAD_AXIS_LEFT_Y];
    }
    if(gmp->axes_delta[GL_GAMEPAD_AXIS_LEFT_TRIGGER]){
        pos_off[2] -= gmp->axes_val[GL_GAMEPAD_AXIS_LEFT_TRIGGER];
    }
    if(gmp->axes_delta[GL_GAMEPAD_AXIS_RIGHT_TRIGGER]){
        pos_off[2] += gmp->axes_val[GL_GAMEPAD_AXIS_RIGHT_TRIGGER];
    }

    //Rotate offset by azimuthal angle
    glm_vec3_rotate(pos_off, glm_rad(camera->azi + 90.0f), (vec3){ 0.0f, 0.0f, 1.0f });

    //Scale vector by camera speed
    glm_vec3_scale(pos_off, camera->zoom_pos * 0.25f, pos_off);

    //Set camera velocity
    for(int i = 0; i < 3; ++i){
        camera->vel[i] = TWOPOW16 * (int32_t)pos_off[i];
    }

    //If up was pressed, attempt to jump to a resolved entity
    if(
        gmp->buttons_delta[GL_GAMEPAD_BUTTON_DPAD_UP] &&
        gmp->buttons_act[GL_GAMEPAD_BUTTON_DPAD_UP] == GL_PRESS_ &&
        gmp->buttons_pre[GL_GAMEPAD_BUTTON_DPAD_UP] != GL_PRESS_ &&
        resolved != 0xffff && ecs_entity_alive(ecs, resolved)
    ){
        //Retrieve position
        const Position* pos = ecs_get_id(ecs, resolved, PositionID);
        if(pos){

            //Set the camera's position to the entity's
            memcpy(camera->pos, pos->pos, sizeof(uint32_t) * 3);
        }
    }
    
    //If left was pressed, attempt to start following an entity
    if(
        gmp->buttons_delta[GL_GAMEPAD_BUTTON_DPAD_LEFT] &&
        gmp->buttons_act[GL_GAMEPAD_BUTTON_DPAD_LEFT] == GL_PRESS_ &&
        gmp->buttons_pre[GL_GAMEPAD_BUTTON_DPAD_LEFT] != GL_PRESS_ &&
        resolved != 0xffff && ecs_entity_alive(ecs, resolved) &&
        ecs_get_id(ecs, resolved, PositionID)
    ){
        camera->followent = resolved;
    }
    
    //If right was pressed, unfollow any entity
    if(
        gmp->buttons_delta[GL_GAMEPAD_BUTTON_DPAD_RIGHT] &&
        gmp->buttons_act[GL_GAMEPAD_BUTTON_DPAD_RIGHT] == GL_PRESS_ &&
        gmp->buttons_pre[GL_GAMEPAD_BUTTON_DPAD_RIGHT] != GL_PRESS_
    ){
        camera->followent = 0xffff;
    }

    //Proquint or hex quart display
    if(
        gmp->buttons_delta[GL_GAMEPAD_BUTTON_X] &&
        gmp->buttons_act[GL_GAMEPAD_BUTTON_X] == GL_PRESS_ &&
        gmp->buttons_pre[GL_GAMEPAD_BUTTON_X] != GL_PRESS_
    ){
        camera->proquint = !camera->proquint;
    }

	//Cycle sync modality
    if(
        gmp->buttons_delta[GL_GAMEPAD_BUTTON_B] &&
        gmp->buttons_act[GL_GAMEPAD_BUTTON_B] == GL_PRESS_ &&
        gmp->buttons_pre[GL_GAMEPAD_BUTTON_B] != GL_PRESS_
    ){
		camera->syncmode = (camera->syncmode + 1) % SyncModeCount;
	}
}

void camera_step(Camera* camera, ECS* ecs, float delta)
{
    //Integrate zoom by velocity
    camera->zoom_pos = glm_clamp(
        camera->zoom_pos + camera->zoom_vel * delta,
        4.0f / TWOPOW16F, 3.0f * TWOPOW16F);

    //While following an entity, calculate position differently
    if(camera->followent != 0xffff && ecs_entity_alive(ecs, camera->followent)){
        
        //Retrieve entity position
        const Position* pos = ecs_get_id(ecs, camera->followent, PositionID);
        if(pos){
            memcpy(camera->pos, pos->pos, sizeof(uint32_t) * 3);
        }

    } else {

        //Integrate position by velocity
        for(int i = 0; i < 3; ++i){
            camera->pos[i] += camera->vel[i] * delta;
        }
    }

    //Update view position
    vec3 camorig; uint32_to_vec3(camera->pos, camorig);
    glm_vec3_copy(camorig, camera->viewpos);
    glm_vec3_muladds(camera->dir, camera->zoom_pos, camera->viewpos);
}

uint32_t camera_get_zoom_u32(Camera* camera)
{
	float f = glm_clamp(camera->zoom_pos, 4.0f / TWOPOW16F, TWOPOW16F - 1.0f);
	return ((uint32_t)(f * 256)) << 8;
}
