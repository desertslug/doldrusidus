#include "climsg.h"

uint8_t* file_get_contents(const char* path, size_t max, uint32_t* size_out)
{
    //Attempt to open the file for reading
    FILE* file = fopen(path, "r");
    if(!file){
        LOG_WARN_("file_append_to_pkt: could not open file \"%s\"", path);
        LOG_WARN_("could not open file, strerror: %s\n", strerror(errno));
        return NULL;
    }

    //Get the size of the file
    fseek(file, 0, SEEK_END);
    size_t size = ftell(file);

    //Check that the file is nonempty
    if(size == 0){
        LOG_WARN_("file_append_to_pkt: empty file");
        fclose(file); return NULL;
    }

    //Check that the file is not too large
    if(size > max){
        LOG_WARN_("file_append_to_pkt: file larger than %lu bytes", max);
        fclose(file); return NULL;
    }

    //Allocate space for file contents
    uint8_t* content = malloc(size);
    if(!content){
        LOG_WARN_("file_append_to_pkt: malloc failed");
        fclose(file); return NULL;
    }

    //Read in file
    fseek(file, 0, SEEK_SET);
    size_t ret = fread(content, 1, size, file);

	//Check that the number of bytes read matches the file size
	if(ret != size){
        LOG_WARN_("file_append_to_pkt: fread return mismatch");
        fclose(file); free(content); return NULL;
	}

    //Attempt to close the file
    if(fclose(file)){
        LOG_WARN_("file_process_to_msg could not close file");
    }

    *size_out = size;
    return content;
}

size_t str_process_to_cmd(rtcp_uvec_t* cmds, const char* str, size_t size, int* cmdidx)
{
    int curr = 0; int next = 0;

    //Skip whitespace
    curr = next; next += strspn(&str[curr], WHITESPACE);

    //Separate out first string until whitespace
    curr = next; next += strcspn(&str[curr], WHITESPACE);

    //Attempt to match against a known command
    int i = 0; while(i < cmds->size){
        Command* cmd = rtcp_uvec_get(cmds, i);

        //Attempt to match against command shorthand
        int len = strlen(cmd->shortname);
        if(len && len <= size && strncmp(cmd->shortname, &str[curr], len) == 0){

            //Set parsing to immediately after the shorthand,
            //as it does not need to be followed by whitespace
            next = curr + len; break;
        }

        //Attempt to match against full command name
        len = strlen(cmd->name);
        if(
            len && len == next - curr &&
            strncmp(cmd->name, &str[curr], len) == 0
        ){ break; }

        ++i;
    }

    //If no string was matched, return zero parsed chars
    if(i >= cmds->size){ return 0; }
    //Otherwise, set command index and return its length
    else { *cmdidx = i; return next - curr; }
}

void str_process_to_msg(rtcp_uvec_t* cmds, char* str, int size, void* pkt, ModClient* modcli, ModSession* modsess, rtcp_uvec_t* firstargs)
{
    int curr = 0; int next = 0;

    //Attempt to parse a command
    int cmdidx;
    next += str_process_to_cmd(cmds, &str[curr], size, &cmdidx);
    if(next == curr){ return; }

    assert_or_exit(cmdidx >= 0 && cmdidx <= 0xffff,
        "str_process_to_msg: invalid command");

    //Attempt to separate out an argument with leading & trailing whitespace
    int arg_next = next;
    arg_next += strspn(&str[arg_next], WHITESPACE);
    arg_next += strcspn(&str[arg_next], WHITESPACE);
    arg_next += strspn(&str[arg_next], WHITESPACE);

    //If the argument was nonempty, store it for completion
    if(next != arg_next){
        lstr_t* firstarg = rtcp_uvec_get(firstargs, cmdidx);
        lstr_clear(firstarg);
        lstr_cat_strn(firstarg, &str[next], arg_next - next);
    }

    //Command index
    uint16_t idx = cmdidx;
    
    //Skip whitespace
    curr = next; next += strspn(&str[curr], WHITESPACE);

    //Command* cmd = rtcp_uvec_get(cmds, cmdidx);

    //Parse command
    switch(cmdidx){
    case MSG_LOGIN: {

        //Username
        curr = next; next += strcspn(&str[curr], WHITESPACE);
        if(curr == next){ break; }
        const char* uname = &str[curr]; str[next++] = '\0';
        curr = next; next += strspn(&str[curr], WHITESPACE);

        //Password
        curr = next; next += strcspn(&str[curr], WHITESPACE);
        if(curr == next){ break; }
        const char* pword = &str[curr]; str[next++] = '\0';
        curr = next; next += strspn(&str[curr], WHITESPACE);

        //Start authentication
        const unsigned char* bytes_A; int len_A;
        client_login_start_auth(modcli->client, uname, pword, &bytes_A, &len_A);

        pkt_chunk_add(pkt, (uint8_t*)&idx, sizeof(uint16_t), PKT_CHUNK_U16);

        //Append chunk: username\0
        pkt_chunk_add(pkt, (uint8_t*)uname, strlen(uname) + 1, PKT_CHUNK_U8);

        //Append chunk: bytes_A
        pkt_chunk_add(pkt, (uint8_t*)bytes_A, len_A, PKT_CHUNK_U8);

    } break;
    case MSG_LOGOUT: {

        //End authentication
        client_logout_end_auth(modcli->client);

        on_client_update_prompt();

        pkt_chunk_add(pkt, (uint8_t*)&idx, sizeof(uint16_t), PKT_CHUNK_U16);

    } break;
    case MSG_CHANGEPW: {

        //Password
        curr = next; next += strcspn(&str[curr], WHITESPACE);
        if(curr == next){ break; }
        const char* pword = &str[curr]; str[next++] = '\0';
        curr = next; next += strspn(&str[curr], WHITESPACE);

        //Check that the client's user is authenticated
        const char* uname = client_get_uname(modcli->client);
        if(!uname){ break; }
        
        //Small salt 's' & host password verifier 'v'
        const unsigned char* bytes_s = 0; int len_s = 0;
        const unsigned char* bytes_v = 0; int len_v = 0;

        //Create salt & verification key for the user's password
        srp_create_salted_verification_key(
            SRP_HASH, SRP_NG,
            uname, (const unsigned char*)pword, strlen(pword),
            &bytes_s, &len_s, &bytes_v, &len_v, 0, 0);

        pkt_chunk_add(pkt, (uint8_t*)&idx, sizeof(uint16_t), PKT_CHUNK_U16);

        //Append chunk: bytes_s & bytes_v
        pkt_chunk_add(pkt, (uint8_t*)bytes_s, len_s, PKT_CHUNK_U8);
        pkt_chunk_add(pkt, (uint8_t*)bytes_v, len_v, PKT_CHUNK_U8);

        //Free bytes_s & bytes_v
        free((char*)bytes_s);
        free((char*)bytes_v);

    } break;
    case MSG_KICK: {

        //Username
        curr = next; next += strcspn(&str[curr], WHITESPACE);
        if(curr == next){ break; }
        const char* uname = &str[curr]; str[next++] = '\0';
        curr = next; next += strspn(&str[curr], WHITESPACE);

        pkt_chunk_add(pkt, (uint8_t*)&idx, sizeof(uint16_t), PKT_CHUNK_U16);

        //Append chunk: username\0
        pkt_chunk_add(pkt, (uint8_t*)uname, strlen(uname) + 1, PKT_CHUNK_U8);

    } break;
    case MSG_REGISTER: {

        //Username
        curr = next; next += strcspn(&str[curr], WHITESPACE);
        if(curr == next){ break; }
        const char* uname = &str[curr]; str[next++] = '\0';
        curr = next; next += strspn(&str[curr], WHITESPACE);

        //Password
        curr = next; next += strcspn(&str[curr], WHITESPACE);
        if(curr == next){ break; }
        const char* pword = &str[curr]; str[next++] = '\0';
        curr = next; next += strspn(&str[curr], WHITESPACE);

        //Small salt 's' & host password verifier 'v'
        const unsigned char* bytes_s = 0; int len_s = 0;
        const unsigned char* bytes_v = 0; int len_v = 0;

        //Create salt & verification key for the user's password
        srp_create_salted_verification_key(
            SRP_HASH, SRP_NG,
            uname, (const unsigned char*)pword, strlen(pword),
            &bytes_s, &len_s, &bytes_v, &len_v, 0, 0);

        pkt_chunk_add(pkt, (uint8_t*)&idx, sizeof(uint16_t), PKT_CHUNK_U16);

        //Append chunk: username\0
        pkt_chunk_add(pkt, (uint8_t*)uname, strlen(uname) + 1, PKT_CHUNK_U8);

        //Append chunk: bytes_s & bytes_v
        pkt_chunk_add(pkt, (uint8_t*)bytes_s, len_s, PKT_CHUNK_U8);
        pkt_chunk_add(pkt, (uint8_t*)bytes_v, len_v, PKT_CHUNK_U8);

        //Free bytes_s & bytes_v
        free((char*)bytes_s);
        free((char*)bytes_v);

    } break;
    case MSG_UNREGISTER: {

        //Username
        curr = next; next += strcspn(&str[curr], WHITESPACE);
        if(curr == next){ break; }
        const char* uname = &str[curr]; str[next++] = '\0';
        curr = next; next += strspn(&str[curr], WHITESPACE);

        pkt_chunk_add(pkt, (uint8_t*)&idx, sizeof(uint16_t), PKT_CHUNK_U16);

        //Append chunk: username\0
        pkt_chunk_add(pkt, (uint8_t*)uname, strlen(uname) + 1, PKT_CHUNK_U8);

    } break;
    case MSG_CHANGEACC: {

        //Username
        curr = next; next += strcspn(&str[curr], WHITESPACE);
        if(curr == next){ break; }
        const char* uname = &str[curr]; str[next++] = '\0';
        curr = next; next += strspn(&str[curr], WHITESPACE);

        //Access level
        curr = next; next += strcspn(&str[curr], WHITESPACE);
        if(curr == next){ break; }
        const char* access = &str[curr]; str[next++] = '\0';
        curr = next; next += strspn(&str[curr], WHITESPACE);

        //Attempt to parse access level
        uint16_t acclev;
        if(sscanf(access, "%hu", &acclev) != 1){ break; }

        pkt_chunk_add(pkt, (uint8_t*)&idx, sizeof(uint16_t), PKT_CHUNK_U16);

        //Append chunk: username\0
        pkt_chunk_add(pkt, (uint8_t*)uname, strlen(uname) + 1, PKT_CHUNK_U8);

        //Append chunk: access level
        pkt_chunk_add(pkt, (uint8_t*)&acclev, sizeof(uint16_t), PKT_CHUNK_U16);

    } break;
    case MSG_GETAUTHS: {

        pkt_chunk_add(pkt, (uint8_t*)&idx, sizeof(uint16_t), PKT_CHUNK_U16);

    } break;
    case MSG_GETSESSIONS: {

        pkt_chunk_add(pkt, (uint8_t*)&idx, sizeof(uint16_t), PKT_CHUNK_U16);

    } break;
    case MSG_SETTPS: {

        //Ticks per second
        curr = next; next += strcspn(&str[curr], WHITESPACE);
        if(curr == next){ break; }
        const char* ticks = &str[curr]; str[next++] = '\0';
        curr = next; next += strspn(&str[curr], WHITESPACE);

        //Attempt to parse ticks per second
        uint16_t tps;
        if(sscanf(ticks, "%hu", &tps) != 1){ break; }

        pkt_chunk_add(pkt, (uint8_t*)&idx, sizeof(uint16_t), PKT_CHUNK_U16);

        //Append chunk: ticks per second
        pkt_chunk_add(pkt, (uint8_t*)&tps, sizeof(uint16_t), PKT_CHUNK_U16);

    } break;
    case MSG_SETTPSPASS: {

        //Ticks per second
        curr = next; next += strcspn(&str[curr], WHITESPACE);
        if(curr == next){ break; }
        const char* ticks = &str[curr]; str[next++] = '\0';
        curr = next; next += strspn(&str[curr], WHITESPACE);

        //Attempt to parse ticks per second
        uint16_t tps;
        if(sscanf(ticks, "%hu", &tps) != 1){ break; }

        pkt_chunk_add(pkt, (uint8_t*)&idx, sizeof(uint16_t), PKT_CHUNK_U16);

        //Append chunk: ticks per second
        pkt_chunk_add(pkt, (uint8_t*)&tps, sizeof(uint16_t), PKT_CHUNK_U16);

    } break;
    case MSG_LOG_REOPEN: {

        pkt_chunk_add(pkt, (uint8_t*)&idx, sizeof(uint16_t), PKT_CHUNK_U16);

    } break;
    case MSG_CHATGLOBAL: {

        //Text
        curr = next; next += strcspn(&str[curr], "");
        if(curr == next){ break; }
        const char* text = &str[curr]; str[next++] = '\0';

        pkt_chunk_add(pkt, (uint8_t*)&idx, sizeof(uint16_t), PKT_CHUNK_U16);

        //Append chunk: text
        pkt_chunk_add(pkt, (uint8_t*)text, strlen(text) + 1, PKT_CHUNK_U8);

    } break;
    case MSG_CHATWHISPER: {

        //Username
        curr = next; next += strcspn(&str[curr], WHITESPACE);
        if(curr == next){ break; }
        const char* uname = &str[curr]; str[next++] = '\0';
        curr = next; next += strspn(&str[curr], WHITESPACE);

        //Text
        curr = next; next += strcspn(&str[curr], "");
        if(curr == next){ break; }
        const char* text = &str[curr]; str[next++] = '\0';

        pkt_chunk_add(pkt, (uint8_t*)&idx, sizeof(uint16_t), PKT_CHUNK_U16);

        //Append chunk: username\0
        pkt_chunk_add(pkt, (uint8_t*)uname, strlen(uname) + 1, PKT_CHUNK_U8);

        //Append chunk: text
        pkt_chunk_add(pkt, (uint8_t*)text, strlen(text) + 1, PKT_CHUNK_U8);

    } break;
    case MOD_MSG_SAVEWORLD: {

        //Path
        curr = next; next += strcspn(&str[curr], WHITESPACE);
        if(curr == next){ break; }
        const char* path = &str[curr]; str[next++] = '\0';
        curr = next; next += strspn(&str[curr], WHITESPACE);

        pkt_chunk_add(pkt, (uint8_t*)&idx, sizeof(uint16_t), PKT_CHUNK_U16);

        //Append chunk: path\0
        pkt_chunk_add(pkt, (uint8_t*)path, strlen(path) + 1, PKT_CHUNK_U8);

    } break;
    case MOD_MSG_LOADWORLD: {

        //Path
        curr = next; next += strcspn(&str[curr], WHITESPACE);
        if(curr == next){ break; }
        const char* path = &str[curr]; str[next++] = '\0';
        curr = next; next += strspn(&str[curr], WHITESPACE);

        pkt_chunk_add(pkt, (uint8_t*)&idx, sizeof(uint16_t), PKT_CHUNK_U16);

        //Append chunk: path\0
        pkt_chunk_add(pkt, (uint8_t*)path, strlen(path) + 1, PKT_CHUNK_U8);

    } break;
    case MOD_MSG_GENWORLD: {

        //Seed
        curr = next; next += strcspn(&str[curr], WHITESPACE);
        if(curr == next){ break; }
        const char* text = &str[curr]; str[next++] = '\0';
        curr = next; next += strspn(&str[curr], WHITESPACE);

        uint32_t seed;
        if(sscanf(text, "%u", &seed) != 1){ break; }

        pkt_chunk_add(pkt, (uint8_t*)&idx, sizeof(uint16_t), PKT_CHUNK_U16);

        pkt_chunk_add(pkt, (uint8_t*)&seed, sizeof(uint32_t), PKT_CHUNK_U32);

    } break;
    case MOD_MSG_TESTWORLD: {
	
		//Name
        curr = next; next += strcspn(&str[curr], WHITESPACE);
        if(curr == next){ break; }
        const char* text = &str[curr]; str[next++] = '\0';
        curr = next; next += strspn(&str[curr], WHITESPACE);

        pkt_chunk_add(pkt, (uint8_t*)&idx, sizeof(uint16_t), PKT_CHUNK_U16);

        //Append chunk: text
        pkt_chunk_add(pkt, (uint8_t*)text, strlen(text) + 1, PKT_CHUNK_U8);

    } break;
    case MOD_MSG_ADDSHIP: {

        pkt_chunk_add(pkt, (uint8_t*)&idx, sizeof(uint16_t), PKT_CHUNK_U16);

    } break;
    case MOD_MSG_DELSHIP: {

        //Entity
        curr = next; next += strcspn(&str[curr], WHITESPACE);
        if(curr == next){ break; }
        const char* entid = &str[curr]; str[next++] = '\0';
        curr = next; next += strspn(&str[curr], WHITESPACE);

        //Attempt to parse entity
        Entity entity; if(!quartorquint2short(entid, &entity)){ break; }

        pkt_chunk_add(pkt, (uint8_t*)&idx, sizeof(uint16_t), PKT_CHUNK_U16);

        //Append chunk: entity
        pkt_chunk_add(pkt, (uint8_t*)&entity, sizeof(uint16_t), PKT_CHUNK_U16);

    } break;
    case MOD_MSG_LISTSHIP: {

        pkt_chunk_add(pkt, (uint8_t*)&idx, sizeof(uint16_t), PKT_CHUNK_U16);

    } break;
    case MOD_MSG_UXN_RAM: {

        //Entity
        curr = next; next += strcspn(&str[curr], WHITESPACE);
        if(curr == next){ break; }
        const char* entid = &str[curr]; str[next++] = '\0';
        curr = next; next += strspn(&str[curr], WHITESPACE);

        //Path
        curr = next; next += strcspn(&str[curr], WHITESPACE);
        if(curr == next){ break; }
        const char* path = &str[curr]; str[next++] = '\0';
        curr = next; next += strspn(&str[curr], WHITESPACE);

        //Attempt to parse entity
        Entity entity; if(!quartorquint2short(entid, &entity)){ break; }

        //Attempt to read contents of file
        uint32_t file_sz;
        uint8_t* content = file_get_contents(path, UXN_ROM_BYTES, &file_sz);
        if(!content){ break; }

        pkt_chunk_add(pkt, (uint8_t*)&idx, sizeof(uint16_t), PKT_CHUNK_U16);

        //Append chunk: entity
        pkt_chunk_add(pkt, (uint8_t*)&entity, sizeof(uint16_t), PKT_CHUNK_U16);

        //Add chunk: file contents
        pkt_chunk_add(pkt, (uint8_t*)content, file_sz, PKT_CHUNK_U8);

        //Free file contents
        free(content);

    } break;
    case MOD_MSG_UXN_BOOT:
    case MOD_MSG_UXN_HALT:
    case MOD_MSG_UXN_RESUME: {

        //Entity
        curr = next; next += strcspn(&str[curr], WHITESPACE);
        if(curr == next){ break; }
        const char* entid = &str[curr]; str[next++] = '\0';
        curr = next; next += strspn(&str[curr], WHITESPACE);

        //Attempt to parse entity
        Entity entity; if(!quartorquint2short(entid, &entity)){ break; }

        pkt_chunk_add(pkt, (uint8_t*)&idx, sizeof(uint16_t), PKT_CHUNK_U16);

        //Append chunk: entity
        pkt_chunk_add(pkt, (uint8_t*)&entity, sizeof(uint16_t), PKT_CHUNK_U16);

    } break;
    case MOD_MSG_UXN_FEED: {

        //Entity
        curr = next; next += strcspn(&str[curr], WHITESPACE);
        if(curr == next){ break; }
        const char* entid = &str[curr]; str[next++] = '\0';
        curr = next; next += strspn(&str[curr], WHITESPACE);

        //Text
        curr = next; next += strcspn(&str[curr], "");
        if(curr == next){ break; }
        const char* text = &str[curr]; str[next++] = '\0';

        //Attempt to parse entity
        Entity entity; if(!quartorquint2short(entid, &entity)){ break; }

        //Check the length of the text
        int text_len = strlen(text);
        if(text_len > UXN_DEV_EVENTS - 1){ break; }

        pkt_chunk_add(pkt, (uint8_t*)&idx, sizeof(uint16_t), PKT_CHUNK_U16);

        //Append chunk: entity
        pkt_chunk_add(pkt, (uint8_t*)&entity, sizeof(uint16_t), PKT_CHUNK_U16);

        //Convert text into event buffer contents
        Buffer buf; buffer_reset(&buf);
        buffer_push_text(&buf, UXN_DEV_CONS + UXN_PORT_CONS_READ, text, text_len);
        buffer_push_text(&buf, UXN_DEV_CONS + UXN_PORT_CONS_READ, "\n", 1);

        //Append chunk: buffer events
        pkt_chunk_add(pkt, (uint8_t*)buf.buf, (text_len + 1) * sizeof(Event), PKT_CHUNK_U8);

    } break;
    case MOD_MSG_WINDOW_OPEN: {

        on_client_window_open(modcli);

    } break;
    case MOD_MSG_WINDOW_CLOSE: {

        on_client_window_close(modcli);

    } break;
    case MOD_MSG_ENT_FOCUS: {

        //Entity
        curr = next; next += strcspn(&str[curr], WHITESPACE);
        if(curr == next){ break; }
        const char* entid = &str[curr]; str[next++] = '\0';
        curr = next; next += strspn(&str[curr], WHITESPACE);

        //Attempt to parse entity
        Entity entity; if(!quartorquint2short(entid, &entity)){ break; }

        //Set focused entity
        modsess->focusent = entity;

        on_client_update_prompt();

    } break;
    case MOD_MSG_ENT_UNFOCUS: {

        //Set focused entity to invalid
        modsess->focusent = -1;

        on_client_update_prompt();

    } break;
    default: { assert_or_exit(false, "str_process_to_msg: cmdidx\n"); };
    }
}

void pkt_process(void* ipkt, void* opkt, ModClient* modcli, ModSession* modsess)
{
    uint8_t* chunk = pkt_data(ipkt);
    if(!pkt_chunk_check(ipkt, chunk)){ return; }

    //Attempt to get message index
    uint16_t cmdidx;
    if(!msg_pkt_chunk_u16_single(ipkt, chunk, &cmdidx)){ return; }
    chunk += pkt_chunk_bytes(ipkt, chunk);

    switch(cmdidx){
    case MSG_VERIFY: {

        on_client_update_prompt();

    } break;
    case MSG_LOGOUT: {

        //End authentication
        client_logout_end_auth(modcli->client);

        on_client_update_prompt();

    } break;
    case MSG_GETAUTHS:
    case MSG_GETSESSIONS: {

        char* uname;
        while(msg_pkt_chunk_ztprintascii(ipkt, chunk,
                UNAME_MIN + 1, UNAME_MAX, &uname))
        {
            chprintf("\"%s\"\n", uname);
            chunk += pkt_chunk_bytes(ipkt, chunk);
        }

    } break;
    case MSG_CHATGLOBAL: {

        //Attempt to get username
        char* uname;
        if(!msg_pkt_chunk_ztprintascii(ipkt, chunk,
            UNAME_MIN + 1, UNAME_MAX, &uname)){ break; }
        chunk += pkt_chunk_bytes(ipkt, chunk);

        //Attempt to get text
        char* text;
        if(!msg_pkt_chunk_ztprintascii(ipkt, chunk,
            1, CHAT_TEXT_MAX, &text)){ break; }
        chunk += pkt_chunk_bytes(ipkt, chunk);

        chprintf("[%s] %s\n", uname, text);
    } break;
    case MSG_CHATWHISPER: {

        //Attempt to get username
        char* uname0;
        if(!msg_pkt_chunk_ztprintascii(ipkt, chunk,
            UNAME_MIN + 1, UNAME_MAX, &uname0)){ break; }
        chunk += pkt_chunk_bytes(ipkt, chunk);

        //Attempt to get username
        char* uname;
        if(!msg_pkt_chunk_ztprintascii(ipkt, chunk,
            UNAME_MIN + 1, UNAME_MAX, &uname)){ break; }
        chunk += pkt_chunk_bytes(ipkt, chunk);

        //Attempt to get text
        char* text;
        if(!msg_pkt_chunk_ztprintascii(ipkt, chunk,
            1, CHAT_TEXT_MAX, &text)){ break; }
        chunk += pkt_chunk_bytes(ipkt, chunk);

        chprintf("[%s -> %s] %s\n", uname0, uname, text);
    } break;
    case MOD_MSG_LISTSHIP: {

        Entity ent;
        while(msg_pkt_chunk_u16_single(ipkt, chunk, &ent)){
            chprintf("\"%04hx\"\n", ent);
            chunk += pkt_chunk_bytes(ipkt, chunk);
        }

    } break;
    case MOD_MSG_SYNC_ECS: {

        uint16_t style;
        if(!msg_pkt_chunk_u16_single(ipkt, chunk, &style)){ break; }
        chunk += pkt_chunk_bytes(ipkt, chunk);

        chunk += world_deserialize_age_pkt(&modcli->world, ipkt, chunk);
        chunk += world_deserialize_ents_pkt(&modcli->world, ipkt, chunk, style);

		//Select an entity as the synchronization target entity
		modsess->syncent = modclient_get_sync_entity(modcli, modcli->camera.syncmode);
        modsess->delta = true;

        //Synchronize session in turn, this is a very specific protocol

		//Currently, the modsession delta is vestigial, and always set
        if(!modsess->delta){ break; }
        modsess->delta = false;

        uint16_t idx = MOD_MSG_SYNC_SESSION;
        pkt_chunk_add(opkt, (uint8_t*)&idx, sizeof(uint16_t), PKT_CHUNK_U16);

        uint8_t bytes[SESSION_BYTES];
        uint32_t bytes_len = modsession_serialize(modsess, 0, bytes);

        pkt_chunk_add(opkt, bytes, bytes_len, PKT_CHUNK_U8);

        //Indicate to the session that it has been sent
        modsession_sent(modsess);

    } break;
    case MOD_MSG_SYNC_ECS_DELETE: {
        
        Entity ent;
        while(msg_pkt_chunk_u16_single(ipkt, chunk, &ent)){
            
            //Delete entity
            if(ecs_entity_alive(&modcli->world.ecs, ent)){
                ecs_entity_rem(&modcli->world.ecs, ent);
            }

            chunk += pkt_chunk_bytes(ipkt, chunk);
        }

    } break;
    default: {} break;
    }
}
