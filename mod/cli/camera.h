#ifndef CAMERA_H
#define CAMERA_H

#include "input.h"
#include "ivec.h"

#include "cglm/cam.h"
#include "cglm/vec3.h"

#define CAMERA_ALTITUDE_MAX 89.0f

typedef enum SyncModality
{
	SyncModeCamera = 0,
	SyncModeWeighted,
	SyncModeCount,
} SyncModality;

typedef struct Camera
{
    vec3 worldup;
    vec3 dir;
    vec3 right;
    vec3 up;

    float zoom_pos;
    float zoom_vel;

    float kbm_zoom_sens;
    float gmp_zoom_sens;

    uint32_t pos[3];
    int32_t vel[3];
    float azi;
    float alt;
    vec3 viewpos;

    float kbm_orbit_sens[2];
    float gmp_orbit_sens[2];

    bool kbm_cursor_disabled;

    Entity followent;

    bool proquint;

	SyncModality syncmode;

} Camera;

void camera_init(Camera* camera, vec3 up, uint32_t* pos, vec3 dir, float azi, float alt, float zoom_pos);

void camera_dir_right_up(Camera* camera);

void camera_process_kbmouse(Camera* camera, KBMouse* kbm, GLFWwindow* window, ECS* ecs, Entity resolved);
void camera_process_gamepad(Camera* camera, Gamepad* gmp, GLFWwindow* window, ECS* ecs, Entity resolved);

void camera_step(Camera* camera, ECS* ecs, float delta);

uint32_t camera_get_zoom_u32(Camera* camera);

#endif /* CAMERA_H */
