#ifndef INPUT_H
#define INPUT_H

#include <math.h>

#include "cglm/vec2.h"

#include "memory.h"

typedef struct ModClient ModClient;

enum
{
    GL_KEY_UNKNOWN = -1,
    GL_KEY_SPACE = 32,
    GL_KEY_APOSTROPHE = 39,
    GL_KEY_COMMA = 44,
    GL_KEY_MINUS = 45,
    GL_KEY_PERIOD = 46,
    GL_KEY_SLASH = 47,
    GL_KEY_0 = 48,
    GL_KEY_1 = 49,
    GL_KEY_2 = 50,
    GL_KEY_3 = 51,
    GL_KEY_4 = 52,
    GL_KEY_5 = 53,
    GL_KEY_6 = 54,
    GL_KEY_7 = 55,
    GL_KEY_8 = 56,
    GL_KEY_9 = 57,
    GL_KEY_SEMICOLON = 59,
    GL_KEY_EQUAL = 61,
    GL_KEY_A = 65,
    GL_KEY_B = 66,
    GL_KEY_C = 67,
    GL_KEY_D = 68,
    GL_KEY_E = 69,
    GL_KEY_F = 70,
    GL_KEY_G = 71,
    GL_KEY_H = 72,
    GL_KEY_I = 73,
    GL_KEY_J = 74,
    GL_KEY_K = 75,
    GL_KEY_L = 76,
    GL_KEY_M = 77,
    GL_KEY_N = 78,
    GL_KEY_O = 79,
    GL_KEY_P = 80,
    GL_KEY_Q = 81,
    GL_KEY_R = 82,
    GL_KEY_S = 83,
    GL_KEY_T = 84,
    GL_KEY_U = 85,
    GL_KEY_V = 86,
    GL_KEY_W = 87,
    GL_KEY_X = 88,
    GL_KEY_Y = 89,
    GL_KEY_Z = 90,
    GL_KEY_LEFT_BRACKET = 91,
    GL_KEY_BACKSLASH = 92,
    GL_KEY_RIGHT_BRACKET = 93,
    GL_KEY_GRAVE_ACCENT = 96,
    GL_KEY_WORLD_1 = 161,
    GL_KEY_WORLD_2 = 162,
    GL_KEY_ESCAPE = 256,
    GL_KEY_ENTER = 257,
    GL_KEY_TAB = 258,
    GL_KEY_BACKSPACE = 259,
    GL_KEY_INSERT = 260,
    GL_KEY_DELETE = 261,
    GL_KEY_RIGHT = 262,
    GL_KEY_LEFT = 263,
    GL_KEY_DOWN = 264,
    GL_KEY_UP = 265,
    GL_KEY_PAGE_UP = 266,
    GL_KEY_PAGE_DOWN = 267,
    GL_KEY_HOME = 268,
    GL_KEY_END = 269,
    GL_KEY_CAPS_LOCK = 280,
    GL_KEY_SCROLL_LOCK = 281,
    GL_KEY_NUM_LOCK = 282,
    GL_KEY_PRINT_SCREEN = 283,
    GL_KEY_PAUSE = 284,
    GL_KEY_F1 = 290,
    GL_KEY_F2 = 291,
    GL_KEY_F3 = 292,
    GL_KEY_F4 = 293,
    GL_KEY_F5 = 294,
    GL_KEY_F6 = 295,
    GL_KEY_F7 = 296,
    GL_KEY_F8 = 297,
    GL_KEY_F9 = 298,
    GL_KEY_F10 = 299,
    GL_KEY_F11 = 300,
    GL_KEY_F12 = 301,
    GL_KEY_F13 = 302,
    GL_KEY_F14 = 303,
    GL_KEY_F15 = 304,
    GL_KEY_F16 = 305,
    GL_KEY_F17 = 306,
    GL_KEY_F18 = 307,
    GL_KEY_F19 = 308,
    GL_KEY_F20 = 309,
    GL_KEY_F21 = 310,
    GL_KEY_F22 = 311,
    GL_KEY_F23 = 312,
    GL_KEY_F24 = 313,
    GL_KEY_F25 = 314,
    GL_KEY_KP_0 = 320,
    GL_KEY_KP_1 = 321,
    GL_KEY_KP_2 = 322,
    GL_KEY_KP_3 = 323,
    GL_KEY_KP_4 = 324,
    GL_KEY_KP_5 = 325,
    GL_KEY_KP_6 = 326,
    GL_KEY_KP_7 = 327,
    GL_KEY_KP_8 = 328,
    GL_KEY_KP_9 = 329,
    GL_KEY_KP_DECIMAL = 330,
    GL_KEY_KP_DIVIDE = 331,
    GL_KEY_KP_MULTIPLY = 332,
    GL_KEY_KP_SUBTRACT = 333,
    GL_KEY_KP_ADD = 334,
    GL_KEY_KP_ENTER = 335,
    GL_KEY_KP_EQUAL = 336,
    GL_KEY_LEFT_SHIFT = 340,
    GL_KEY_LEFT_CONTROL = 341,
    GL_KEY_LEFT_ALT = 342,
    GL_KEY_LEFT_SUPER = 343,
    GL_KEY_RIGHT_SHIFT = 344,
    GL_KEY_RIGHT_CONTROL = 345,
    GL_KEY_RIGHT_ALT = 346,
    GL_KEY_RIGHT_SUPER = 347,
    GL_KEY_MENU = 348,
};

enum
{
    GL_MOUSE_BUTTON_1 = 0,
    GL_MOUSE_BUTTON_2 = 1,
    GL_MOUSE_BUTTON_3 = 2,
    GL_MOUSE_BUTTON_4 = 3,
    GL_MOUSE_BUTTON_5 = 4,
    GL_MOUSE_BUTTON_6 = 5,
    GL_MOUSE_BUTTON_7 = 6,
    GL_MOUSE_BUTTON_8 = 7,
};

enum { GL_RELEASE_ = 0, };
enum { GL_PRESS_ = 1, };
enum { GL_REPEAT_ = 2, };

enum { GL_CONNECTED = 0x00040001, };
enum { GL_DISCONNECTED = 0x00040002, };

enum
{
    GL_GAMEPAD_BUTTON_A = 0,
    GL_GAMEPAD_BUTTON_B = 1,
    GL_GAMEPAD_BUTTON_X = 2,
    GL_GAMEPAD_BUTTON_Y = 3,
    GL_GAMEPAD_BUTTON_LEFT_BUMPER = 4,
    GL_GAMEPAD_BUTTON_RIGHT_BUMPER = 5,
    GL_GAMEPAD_BUTTON_BACK = 6,
    GL_GAMEPAD_BUTTON_START = 7,
    GL_GAMEPAD_BUTTON_GUIDE = 8,
    GL_GAMEPAD_BUTTON_LEFT_THUMB = 9,
    GL_GAMEPAD_BUTTON_RIGHT_THUMB = 10,
    GL_GAMEPAD_BUTTON_DPAD_UP = 11,
    GL_GAMEPAD_BUTTON_DPAD_RIGHT = 12,
    GL_GAMEPAD_BUTTON_DPAD_DOWN = 13,
    GL_GAMEPAD_BUTTON_DPAD_LEFT = 14,
};

enum
{
    GL_GAMEPAD_AXIS_LEFT_X = 0,
    GL_GAMEPAD_AXIS_LEFT_Y = 1,
    GL_GAMEPAD_AXIS_RIGHT_X = 2,
    GL_GAMEPAD_AXIS_RIGHT_Y = 3,
    GL_GAMEPAD_AXIS_LEFT_TRIGGER = 4,
    GL_GAMEPAD_AXIS_RIGHT_TRIGGER = 5,
};

enum { GL_KEY_LAST = 348, };
enum { GL_KEY_COUNT = GL_KEY_LAST + 1, };

enum { GL_MOUSE_BUTTON_LAST = 7, };
enum { GL_MOUSE_BUTTON_COUNT = GL_MOUSE_BUTTON_LAST + 1, };

typedef struct KBMouse
{
    uint8_t keys_act[GL_KEY_COUNT];
    uint8_t keys_pre[GL_KEY_COUNT];
    uint8_t keys_delta[GL_KEY_COUNT];
    uint8_t keys_mods[GL_KEY_COUNT];

    uint8_t buttons_act[GL_MOUSE_BUTTON_COUNT];
    uint8_t buttons_pre[GL_MOUSE_BUTTON_COUNT];
    uint8_t buttons_delta[GL_MOUSE_BUTTON_COUNT];
    uint8_t buttons_mods[GL_MOUSE_BUTTON_COUNT];

    double cursor_abs[2];
    double cursor_rel[2];
    double cursor_pre[2];

} KBMouse;

void kbmouse_step(KBMouse* kbm);
void kbmouse_process(KBMouse* kbm, ModSession* modsession, ModClient* modcli);

enum { GL_GAMEPAD_AXIS_LAST = 5, };
enum { GL_GAMEPAD_AXIS_COUNT = GL_GAMEPAD_AXIS_LAST + 1, };

enum { GL_GAMEPAD_BUTTON_LAST = 14, };
enum { GL_GAMEPAD_BUTTON_COUNT = GL_GAMEPAD_BUTTON_LAST + 1, };

/* non-conflicting definition identical to GLFWgamepadstate */
typedef struct GamepadState
{
    unsigned char buttons[15];
    float axes[6];
} GamepadState;

typedef struct Gamepad
{
    GamepadState state;

    uint8_t buttons_act[GL_GAMEPAD_BUTTON_COUNT];
    uint8_t buttons_pre[GL_GAMEPAD_BUTTON_COUNT];
    uint8_t buttons_delta[GL_GAMEPAD_BUTTON_COUNT];
    
    uint8_t axes_act[GL_GAMEPAD_AXIS_COUNT];
    float   axes_val[GL_GAMEPAD_AXIS_COUNT];
    float   axes_cur[GL_GAMEPAD_AXIS_COUNT];
    float   axes_pre[GL_GAMEPAD_AXIS_COUNT];
    uint8_t axes_delta[GL_GAMEPAD_AXIS_COUNT];

} Gamepad;

void gamepad_step(Gamepad* gmp, float sensitivity);
void gamepad_process(Gamepad* gmp, ModSession* modsession, ModClient* modcli, float deadzone, float azimuth);
void gamepad_update(Gamepad* gmp, float deadzone);
void gamepad_events_to_buffer(Gamepad* gmp, void* cli, Buffer* buf);

void map_kbmouse_to_gamepad(KBMouse* kbm, Gamepad* gmp);
void map_gamepad_to_kbmouse(Gamepad* gmp, KBMouse* kbm, float threshold, float sensitivity);

typedef struct InputInfo
{
    KBMouse kbmouse;
    Gamepad gamepad;
    int windsize[2];
} InputInfo;

#endif /* INPUT_H */
