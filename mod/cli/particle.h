#ifndef PARTICLE_H
#define PARTICLE_H

#include "cliconf.h"
#include "compo.h"
#include "gfx.h"
#include "camera.h"
#include "system.h"

enum { PART_COLORS = 2, };

typedef struct Lifetime
{
    struct timespec start;
    struct timespec length;
    float percent;
} Lifetime;

typedef struct Color
{
    float cols[3][4];
} Color;

typedef struct Sprite
{
    uint8_t idx;
} Sprite;

typedef enum PartID
{
    PartPositionID = 0,
    PartPointphysID,
    PartLifetimeID,
    PartColorID,
    PartSpriteID,
    PART_COUNT,
} PartID;

uint32_t PART_SIZE[PART_COUNT];

int particle_freq_to_count(float freq, float delta, float randnorm);

void romu_trio32_vec3_rand(uint32_t romustt[3], float* out);

typedef struct Particles
{
    ECS ecs;
    SpritesRender sprites;
    uint32_t romu_trio32[3];
} Particles;

void parts_alloc(Particles* parts);
void parts_dealloc(Particles* parts);
void parts_init(Particles* parts, uint32_t seed[3]);
void parts_step(Particles* parts, struct timespec age, struct timespec delta, Camera* camera);
void parts_render(Particles* parts, struct timespec age, Camera* camera);
void parts_forget(Particles* parts, struct timespec age);
void parts_spawn_pos_vel_life_col_spr(Particles* parts, uint32_t pos[3], int32_t vel[3], struct timespec* start, float life_sec, float cols[3][4], uint8_t spr_idx);
bool parts_full(Particles* parts);

void partsystem_pos_pphys_integrate(Query* q, ECS* ecs, float delta);

typedef struct Wind
{
    vec3 dir;
} Wind;

void wind_init(Wind* wind, uint32_t romustt[3]);
void wind_step(Wind* wind, struct timespec delta, uint32_t romustt[3]);
void wind_spawn(Wind* wind, Particles* parts, struct timespec age, struct timespec delta, Camera* camera);

enum {
    PART_DOT1 = 0,
    PART_DOT2,
    PART_SPRITE_COUNT,
};

void solar_spawn(ECS* ecs0, Query* q, Particles* parts, struct timespec age, struct timespec delta, Camera* camera);
void drive_spawn(ECS* ecs0, Query* q, Particles* parts, struct timespec age, struct timespec delta, Camera* camera);
void oasis_spawn(ECS* ecs0, Query* q, Particles* parts, struct timespec age, struct timespec delta, Camera* camera);
void monolith_spawn(ECS* ecs0, Query* q, Particles* parts, struct timespec age, struct timespec delta, Camera* camera);
void wormhole_spawn(ECS* ecs0, Query* q, Particles* parts, struct timespec age, struct timespec delta, Camera* camera);

#endif /* PARTICLE_H */
