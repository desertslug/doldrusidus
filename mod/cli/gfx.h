#ifndef GFX_H
#define GFX_H

#include <signal.h>
#include <assert.h>

#include "glfw.h"
#include "shader.h"

#include "cglm/vec3.h"
#include "cglm/vec4.h"

#include "stb_image.h"

#include "defs.h"
#include "helper.h"
#include "camera.h"
//#include "memory.h"

void hex_rgba_to_vec4(uint32_t rgba, vec4 out);
void hex_rgba_to_vec4_norm(uint32_t rgba, vec4 out);

void vec3_parallel_to(vec3 a, vec3 dir, vec3 dest);

typedef struct Vertex3PC
{
    float pos[3];
    float col[4];
} Vertex3PC;

typedef struct LineRender
{
    GLuint program;
    GLuint uniloc_mat_mv;
    GLuint uniloc_mat_pr;
    GLuint vao;
    GLuint vbo;
    int verts;
} LineRender;

void linerender_alloc(LineRender* lines, int verts);
void linerender_dealloc(LineRender* lines);

void linerender_setup(LineRender* lines, mat4 mv, mat4 pr);
void linerender_render_3d(LineRender* lines, GLenum mode, Vertex3PC* pos, int verts, float width, uint16_t stipple, vec3 cam_pos);

typedef struct Vertex2PT
{
    float pos[2];
    float tex[2];
} Vertex2PT;

typedef struct SpriteInstance
{
    float pos[3];
    float type;
    float col[4];
    int32_t off[2];
    int32_t scale[2];
} SpriteInstance;

typedef struct SpritesRender
{
    GLuint program;
    GLuint uniloc_mat_mv;
    GLuint uniloc_mat_pr;
    GLuint uniloc_color_in;
    GLuint uniloc_maxdim;
    GLuint uniloc_window;
    GLuint vao;
    GLuint vbo;
    GLuint instance_vbo;
    GLuint texture;
    int verts;
    int instances;
    int maxdim[2];
} SpritesRender;

void spritesrender_alloc(SpritesRender* sprites, int instances);
void spritesrender_dealloc(SpritesRender* sprites);

void spritesrender_load_file(SpritesRender* sprites, const char* base_path, int count);
void spritesrender_load_bytes(SpritesRender* sprites, const uint8_t* src, int (*dims)[2]);
void spritesrender_setup(SpritesRender* sprites, mat4 mv, mat4 pr, int* windowsizes);
void spritesrender_render_2d(SpritesRender* sprites, SpriteInstance* insts, int instances);
void spritesrender_render_3d(SpritesRender* sprites, SpriteInstance* insts, int instances, vec3 cam_pos);

typedef struct BoardInstance
{
    float pos[3];
    int32_t code;
    float col[3];
    int32_t off[2];
} BoardInstance;

typedef struct BoardRender
{
    GLuint program;
    GLuint uniloc_mat_mv;
    GLuint uniloc_mat_pr;
    GLuint uniloc_bbox;
    GLuint uniloc_chardim;
    GLuint uniloc_window;
    GLuint vao;
    GLuint vbo;
    GLuint instance_vbo;
    GLuint texture;
    int bbox[2];
    int chardim[2];
    int imgdim[2];
    int imgchan;
    int verts;
    int instances;
} BoardRender;

void boardrender_alloc(BoardRender* boards, int instances, const char* path, int fontw, int fonth);
void boardrender_dealloc(BoardRender* boards);

void boardrender_load(BoardRender* boards, const char* path, int fontw, int fonth);
void boardrender_setup(BoardRender* boards, mat4 mv, mat4 pr, int* windowsizes);
void boardrender_render_2d(BoardRender* boards, BoardInstance* insts, int instances);
void boardrender_render_3d(BoardRender* boards, BoardInstance* insts, int instances, vec3 cam_pos);

typedef struct PanelRender {
    SpritesRender sprites;
    BoardInstance* insts;
    int instances;
    int chardim[2];
    char* scratch;

    int columns;
    int offset[2];
    vec3 pos;
    int padding;

    int maxcol;
    int col;
    int row;
    int length;
} PanelRender;

void panelrender_alloc(PanelRender* panels, int instances, int fontw, int fonth);
void panelrender_dealloc(PanelRender* panels);
void panelrender_typeset_reset(PanelRender* panels, int columns, vec3 pos, int padding);
void panelrender_typeset_append(PanelRender* panels, vec4 color, const char* fmt, ...);
void panelrender_render_2d(PanelRender* panels, BoardRender* boards, int* windowsizes, mat4 mv, mat4 pr, int center[2]);

#endif /* GFX_H */
