#ifndef GRAPHICS_H
#define GRAPHICS_H

#include "proquint.h"

#include "gfx.h"

typedef struct EntityInstance
{
    uint32_t typei;    
    uint16_t ent;
    char fading;
} EntityInstance;

Entity resolve_entity(Camera* camera, EntityInstance* entities, SpriteInstance* instances, int count, float windnorm[2], int windsize[2]);

void linerender_world_border(LineRender* lines, Camera* camera);
void linerender_horizontal_axes(LineRender* lines, Camera* camera);
void linerender_entity_verticals(LineRender* lines, SpriteInstance* instances, int count, Camera* camera);
void linerender_entity_nav(LineRender* lines, EntityInstance* entities, SpriteInstance* instances, int count, ECS* ecs, Camera* camera);
//void linerender_missile(LineRender* lines, ECS* ecs, Camera* camera);

int spritesrender_entities(SpritesRender* sprites, EntityInstance* entities, SpriteInstance* instances, SpriteInstance* instances_xform, ECS* ecs, Camera* camera);
void spritesrender_entity_modules(SpritesRender* sprites, SpriteInstance* instances, int count, int spritew, int spriteh, ECS* ecs, Camera* camera);
void spritesrender_resolved_entity(SpritesRender* sprites, SpritesRender* sprites_bg, int* windowsizes, ECS* ecs, Entity ent, mat4 view, mat4 proj, Camera* camera);

void boardrender_world_cardinals(BoardRender* boards, Camera* camera);
void boardrender_entity_id(BoardRender* boards, EntityInstance* entities, SpriteInstance* instances, int count, int spritew, int spriteh, Camera* camera);
void boardrender_entity_distance(BoardRender* boards, SpriteInstance* instances, int count, int spritew, int spriteh, Camera* camera);
void boardrender_resolved_entity(BoardRender* boards, ECS* ecs, Entity ent, int spritew, int spriteh, Camera* camera);
void boardrender_entity_pilotmarker(BoardRender* boards, ECS* ecs, int spritew, int spriteh, Camera* camera, const char* uname);

void panelrender_age(PanelRender* panels, BoardRender* boards, struct timespec age, int* windowsizes);
void panelrender_camera(PanelRender* panels, BoardRender* boards, Camera* camera, int* windowsizes);

void panelrender_pilot(PanelRender* panels, const Pilot* pilot);
void panelrender_nav_range(PanelRender* panels, const Nav* nav);
void panelrender_pilot_and_nav_range(PanelRender* panels, const Pilot* pilot, const Nav* nav);
void panelrender_uxn(PanelRender* panels, const Uxn* uxn);
void panelrender_position(PanelRender* panels, const Position* pos);
void panelrender_pointphys(PanelRender* panels, const Pointphys* pphys);
void panelrender_star(PanelRender* panels, const Star* star);
void panelrender_trophs(PanelRender* panels, const Trophs* trophs);
void panelrender_habitable(PanelRender* panels, const Habitable* habit);
void panelrender_monolith(PanelRender* panels, const Monolith* monol);
void panelrender_wormhole(PanelRender* panels, const Wormhole* worm);
void panelrender_barren(PanelRender* panels, const Barren* barr);
void panelrender_hive(PanelRender* panels, const Hive* hive);
void panelrender_wasp(PanelRender* panels, const Wasp* wasp);
void panelrender_leviathan(PanelRender* panels, const Leviathan* levia, Camera* camera);
void panelrender_missile(PanelRender* panels, const Missile* miss, Camera* camera);
void panelrender_wreckage(PanelRender* panels, const Wreckage* wreck);
void panelrender_ruin(PanelRender* panels, const Ruin* ruin);
//void panelrender_maze(PanelRender* panels, const Maze* maze);
void panelrender_nav(PanelRender* panels, const Nav* nav);
void panelrender_drive(PanelRender* panels, const Drive* drive);
void panelrender_vat(PanelRender* panels, const Vat* vat);
void panelrender_extract(PanelRender* panels, const Extract* ext);
void panelrender_synth(PanelRender* panels, const Synth* synth);
void panelrender_compute(PanelRender* panels, const Compute* compute);
void panelrender_hull(PanelRender* panels, const Hull* hull);
void panelrender_bay(PanelRender* panels, const Bay* bay);
void panelrender_growth(PanelRender* panels, const Growth* growth);
void panelrender_cargo(PanelRender* panels, const Cargo* cargo);

void panelrender_resolved_entity(PanelRender* panels, BoardRender* boards, int* windowsizes, ECS* ecs, Entity ent, mat4 view, mat4 proj, Camera* camera);

#endif /* GRAPHICS_H */
