#include "curse.h"

void clr_to_eol(int cx, int cy)
{
    int w = tb_width();
    for(int i = cx; i < w; ++i){
        tb_set_cell(i, cy, ' ', 0, 0);
    }
}

void tb_print_wrap(int x, int y, uintattr_t fg, uintattr_t bg, const char* str, int size)
{
    int w = tb_width();
    int col = x; int row = y;

    for(int i = 0; i < size; ++i){
        tb_set_cell(col, row, str[i], fg, bg);
        ++col; if(col >= w){ col = 0; ++row; }
    }
}

int str_display_lines(int startoff, const char* str, size_t sz)
{
    int w = tb_width();

    //Starting out, an empty string takes 1 line an 'startoff' chars to display
    int lines = 1; int chars = startoff;

    //Examine string until its terminator or a set maximum size
    int i = 0; while(str[i] != '\0' && i < sz){

        //Newlines always take one more line to draw
	    if(str[i++] == '\n'){ chars = 0; ++lines; }
        //Otherwise, increment lines if the last column was reached
	    else { ++chars; if(chars == w){ chars = 0; ++lines; } }
    }

    return lines;
}

int curse_str_end_at_row(int lastrow, int startoff, const char* str, int size)
{
    int w = tb_width();

    //Calculate number of lines needed to display the string
    int lines = str_display_lines(startoff, str, size);

    //Clear last row manually
    clr_to_eol(lines > 1 ? 0 : startoff, lastrow);

    //Print the line at a position that, when wrapped, makes it fit exactly
    //tb_printf(startoff % w, lastrow - lines + 1, 0, 0, "%.*s", size, str);
    tb_print_wrap(startoff % w, lastrow - lines + 1, 0, 0, str, size);

    //Clear the rest of the line
    //TODO why

    return lines;
}

void curse_struvec_end_at_row(int lastrow, rtcp_uvec_t* uvec, int lastidx)
{
    //Draw lines from the bottom up until the next one wouldn't appear at all
    int idx = lastidx;
    while(lastrow >= 0){

        lstr_t* lstr = rtcp_uvec_get(uvec, idx);
        lastrow -= curse_str_end_at_row(
            lastrow, 0, lstr_get(lstr), lstr_len(lstr));
        idx = (idx - 1 + uvec->size) % uvec->size;
    }
}
