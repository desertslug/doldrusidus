#include "gfx.h"

void hex_rgba_to_vec4(uint32_t rgba, vec4 out)
{
    out[0] = ((rgba & 0xff000000) >> 24);
    out[1] = ((rgba & 0x00ff0000) >> 16);
    out[2] = ((rgba & 0x0000ff00) >> 8);
    out[3] = ((rgba & 0x000000ff) >> 0);
}

void hex_rgba_to_vec4_norm(uint32_t rgba, vec4 out)
{
    hex_rgba_to_vec4(rgba, out);
    glm_vec4_scale(out, 1.0f / 255.0f, out);
}

void vec3_parallel_to(vec3 a, vec3 dir, vec3 dest)
{
    //Normalize the original and the direction vectors
    vec3 a_n; glm_vec3_normalize_to(a, a_n);
    vec3 d_n; glm_vec3_normalize_to(dir, d_n);

    //Scale the dir vector by the dot product times the scale of the original
    glm_vec3_scale_as(dir, glm_vec3_dot(a_n, d_n) * glm_vec3_norm(a), dest);
}

void linerender_alloc(LineRender* lines, int verts)
{
    lines->verts = verts;

    //Load shader program
    ShaderInfo shaders[2];
    shaderinfo_init(&shaders[0], GL_VERTEX_SHADER, "res/shd/lines.vert");
    shaderinfo_init(&shaders[1], GL_FRAGMENT_SHADER, "res/shd/lines.frag");
    if(shaders_compile_program(shaders, 2, &lines->program)){
        LOG_ERROR_("failed to compile shaders into program");
        raise(SIGINT);
    }

    //Get uniform locations
    lines->uniloc_mat_mv = glGetUniformLocation(lines->program, "mat_mv");
    lines->uniloc_mat_pr = glGetUniformLocation(lines->program, "mat_pr");

    //Generate & configure VAO
    glGenVertexArrays(1, &lines->vao);
    glBindVertexArray(lines->vao);

    //Generate & configure VBO
    glGenBuffers(1, &lines->vbo);
    glBindBuffer(GL_ARRAY_BUFFER, lines->vbo);
    glBufferData(GL_ARRAY_BUFFER, verts * sizeof(Vertex3PC), NULL, GL_DYNAMIC_DRAW);

    //Specify vertex attribute layout
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex3PC),
        (GLvoid*)(offsetof(Vertex3PC, pos)));
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex3PC),
        (GLvoid*)(offsetof(Vertex3PC, col)));

    //Enable generic vertex attribute arrays
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
}

void linerender_dealloc(LineRender* lines)
{
    //Delete VBO
    glDeleteBuffers(1, &lines->vbo);

    //Delete VAO
    glDeleteVertexArrays(1, &lines->vao);

    //Delete shader program
    glDeleteProgram(lines->program);
}

void linerender_setup(LineRender* lines, mat4 mv, mat4 pr)
{
    //Change rendering program
    glUseProgram(lines->program);

    //Set model-view and projection matrices
    glUniformMatrix4fv(lines->uniloc_mat_mv, 1, GL_FALSE, (const GLfloat*)mv);
    glUniformMatrix4fv(lines->uniloc_mat_pr, 1, GL_FALSE, (const GLfloat*)pr);
}

void linerender_render_3d(LineRender* lines, GLenum mode, Vertex3PC* pos, int verts, float width, uint16_t stipple, vec3 cam_pos)
{
    //Assert that the vertex data fits into the allocated buffer
    if(verts > lines->verts){
        LOG_ERROR_("linerender_render: too much vertex data");
        raise(SIGINT);
    }

    //Transform vertices into the format expected by the shader
    for(int i = 0; i < verts; ++i){
        glm_vec3_sub(pos[i].pos, cam_pos, pos[i].pos);
    }

    //Set line width
    glLineWidth(width);

    //Set stipple pattern
    glLineStipple(1, stipple);

    //Bind VAO & VBO to be modified
    glBindVertexArray(lines->vao);
    glBindBuffer(GL_ARRAY_BUFFER, lines->vbo);

    //Load the data into the buffer
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(Vertex3PC) * verts, pos);

    //Draw vertex array
    glDrawArrays(mode, 0, verts);
}

void spritesrender_alloc(SpritesRender* sprites, int instances)
{
    //Store parameters
    sprites->instances = instances;

    //Load shader program
    ShaderInfo shaders[2];
    shaderinfo_init(&shaders[0], GL_VERTEX_SHADER, "res/shd/sprites.vert");
    shaderinfo_init(&shaders[1], GL_FRAGMENT_SHADER, "res/shd/sprites.frag");
    if(shaders_compile_program(shaders, 2, &sprites->program)){
        LOG_ERROR_("failed to compile shaders into program");
        raise(SIGINT);
    }

    //Get uniform locations
    sprites->uniloc_mat_mv = glGetUniformLocation(sprites->program, "mat_mv");
    sprites->uniloc_mat_pr = glGetUniformLocation(sprites->program, "mat_pr");
    sprites->uniloc_maxdim = glGetUniformLocation(sprites->program, "maxdim");
    sprites->uniloc_window = glGetUniformLocation(sprites->program, "window");

    //Generate & configure VAO
    glGenVertexArrays(1, &sprites->vao);
    glBindVertexArray(sprites->vao);

    //Generate & configure VBO
    glGenBuffers(1, &sprites->vbo);
    glBindBuffer(GL_ARRAY_BUFFER, sprites->vbo);

    //Specify the vertex attribute of position
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE,
        sizeof(Vertex2PT), (GLvoid*)(offsetof(Vertex2PT, pos)));
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE,
        sizeof(Vertex2PT), (GLvoid*)(offsetof(Vertex2PT, tex)));

    //Fill the buffer with vertex data
    sprites->verts = 4;
    Vertex2PT* verts = malloc(sprites->verts * sizeof(Vertex2PT));
    verts[0] = (Vertex2PT){ {  1.0f,  1.0f }, { 1.0f,  1.0f, } };
    verts[1] = (Vertex2PT){ {  1.0f, -1.0f }, { 1.0f,  0.0f, } };
    verts[3] = (Vertex2PT){ { -1.0f, -1.0f }, { 0.0f,  0.0f, } };
    verts[2] = (Vertex2PT){ { -1.0f,  1.0f }, { 0.0f,  1.0f, } };
    glBufferData(GL_ARRAY_BUFFER, sprites->verts * sizeof(Vertex2PT), verts, GL_STATIC_DRAW);
    free(verts);

    //Enable generic vertex attribute arrays
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    //Generate & configure instance VBO
    glGenBuffers(1, &sprites->instance_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, sprites->instance_vbo);
    glBufferData(GL_ARRAY_BUFFER, sprites->instances * sizeof(SpriteInstance), NULL, GL_DYNAMIC_DRAW);

    //Specify the instance attribute layout
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE,
        sizeof(SpriteInstance), (GLvoid*)(offsetof(SpriteInstance, pos)));
    glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE,
        sizeof(SpriteInstance), (GLvoid*)(offsetof(SpriteInstance, type)));
    glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE,
        sizeof(SpriteInstance), (GLvoid*)(offsetof(SpriteInstance, col)));
    glVertexAttribIPointer(5, 2, GL_INT,
        sizeof(SpriteInstance), (GLvoid*)(offsetof(SpriteInstance, off)));
    glVertexAttribIPointer(6, 2, GL_INT,
        sizeof(SpriteInstance), (GLvoid*)(offsetof(SpriteInstance, scale)));

    //Enable instance attribute arrays, updating them every 1 instance
    glVertexAttribDivisor(2, 1); glEnableVertexAttribArray(2);
    glVertexAttribDivisor(3, 1); glEnableVertexAttribArray(3);
    glVertexAttribDivisor(4, 1); glEnableVertexAttribArray(4);
    glVertexAttribDivisor(5, 1); glEnableVertexAttribArray(5);
    glVertexAttribDivisor(6, 1); glEnableVertexAttribArray(6);

    //Generate name for a texture
    glGenTextures(1, &sprites->texture);
    glBindTexture(GL_TEXTURE_3D, sprites->texture);

    //Set texture filtering parameters
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
}

void spritesrender_dealloc(SpritesRender* sprites)
{
    //Delete VBO
    glDeleteBuffers(1, &sprites->vbo);
    glDeleteBuffers(1, &sprites->instance_vbo);

    //Delete textures
    glDeleteTextures(1, &sprites->texture);

    //Delete VAO
    glDeleteVertexArrays(1, &sprites->vao);

    //Delete shader program
    glDeleteProgram(sprites->program);
}

void spritesrender_load_file(SpritesRender* sprites, const char* base_path, int count)
{
    //Path to image
    char path[512]; path[0] = '\0';
    strcpy(path, base_path);
    int baselen = strlen(path);

    //Image data
    int dims[count][2];
    int chans[count];
    uint8_t* datas[count];

    //Set stb_image to flip images vertically on load
    stbi_set_flip_vertically_on_load(true); 

    //Load one icon for each entity type
    for(int i = 0; i < count; ++i){

        //Compose path to the corresponding image
        sprintf(&path[baselen], "%d", i);
        strcat(path, ".png");

        //Attempt to load image
        datas[i] = (uint8_t*)
            stbi_load(path, &dims[i][0], &dims[i][1], &chans[i], 0);
        if(!datas[i]){
            LOG_ERROR_("spritesrender_load_file: failed to load \"%s\"", path);
            raise(SIGINT);
        }
        if(chans[i] != 4){
            LOG_ERROR_("expecting 4 color channels, got %d", chans[i]);
            raise(SIGINT);
        }
    }

    //Search for the maximum dimensions
    int maxd[2] = { 0, 0 };
    for(int i = 0; i < count; ++i){
        for(int j = 0; j < 2; ++j){
            maxd[j] = dims[i][j] > maxd[j] ? dims[i][j] : maxd[j];
        }
    }

    //Store maximum dimensions
    sprites->maxdim[0] = maxd[0];
    sprites->maxdim[1] = maxd[1];

    //Configure texture
    glBindTexture(GL_TEXTURE_3D, sprites->texture);
    glTexImage3D(
        GL_TEXTURE_3D,
        0, //level-of-detail number
        GL_RGBA8,
        maxd[0], maxd[1], count,
        0,
        GL_RGBA,
        GL_UNSIGNED_BYTE,
        NULL
    );

    //Zero out textures
    uint8_t* empty = malloc(maxd[0] * maxd[1]);
    memset(empty, 0, maxd[0] * maxd[1]);
    for(int i = 0; i < count; ++i){
        glTexSubImage3D(
            GL_TEXTURE_3D,
            0, //level-of-detail number
            0, 0, i,
            maxd[0], maxd[1], 1,
            GL_RGBA,
            GL_UNSIGNED_BYTE,
            empty
        );
    }
    free(empty);

    //Initialize textures
    for(int i = 0; i < count; ++i){
        glTexSubImage3D(
            GL_TEXTURE_3D,
            0, //level-of-detail number
            (maxd[0] - dims[i][0]) / 2, (maxd[1] - dims[i][1]) / 2, i,
            dims[i][0], dims[i][1], 1,
            GL_RGBA,
            GL_UNSIGNED_BYTE,
            datas[i]
        );
    }

    //Free image data pointers
    for(int i = 0; i < count; ++i){
        stbi_image_free(datas[i]);
    }
}

void spritesrender_load_bytes(SpritesRender* sprites, const uint8_t* src, int (*dims)[2])
{
    //Image data
    const uint8_t* datas[1] = { src };

    //Search for the maximum dimensions
    int maxd[2] = { dims[0][0], dims[0][1] };

    //Store maximum dimensions
    sprites->maxdim[0] = maxd[0];
    sprites->maxdim[1] = maxd[1];

    //Configure texture
    glBindTexture(GL_TEXTURE_3D, sprites->texture);
    glTexImage3D(
        GL_TEXTURE_3D,
        0, //level-of-detail number
        GL_RGBA8,
        maxd[0], maxd[1], 1,
        0,
        GL_RGBA,
        GL_UNSIGNED_BYTE,
        NULL
    );

    //Initialize textures
    for(int i = 0; i < 1; ++i){
        glTexSubImage3D(
            GL_TEXTURE_3D,
            0, //level-of-detail number
            (maxd[0] - dims[i][0]) / 2, (maxd[1] - dims[i][1]) / 2, i,
            dims[i][0], dims[i][1], 1,
            GL_RGBA,
            GL_UNSIGNED_BYTE,
            datas[i]
        );
    }
}

void spritesrender_setup(SpritesRender* sprites, mat4 mv, mat4 pr, int* windowsizes)
{
    //Change rendering program
    glUseProgram(sprites->program);

    //Set model-view and projection matrices
    glUniformMatrix4fv(sprites->uniloc_mat_mv, 1, GL_FALSE, (const GLfloat*)mv);
    glUniformMatrix4fv(sprites->uniloc_mat_pr, 1, GL_FALSE, (const GLfloat*)pr);

    //Set maximal dimensions
    glUniform2iv(sprites->uniloc_maxdim, 1, (const GLint*)sprites->maxdim);

    //Set window size
    glUniform2iv(sprites->uniloc_window, 1, (const GLint*)windowsizes);

    //chprintf(client, "glGetError(): %d", glGetError());
}

void spritesrender_render_2d(SpritesRender* sprites, SpriteInstance* insts, int instances)
{
    //Assert that the instance data fits into the allocated buffer
    if(instances > sprites->instances){
        LOG_ERROR_("spritesrender_render_2d: too much instance data");
        raise(SIGINT);
    }

    //Bind vertex & instance array to be modified
    glBindVertexArray(sprites->vao);
    glBindBuffer(GL_ARRAY_BUFFER, sprites->instance_vbo);
    glBufferSubData(GL_ARRAY_BUFFER, 0, instances * sizeof(SpriteInstance), insts);

    //Bind the texture to be used for the draw call
    glBindTexture(GL_TEXTURE_3D, sprites->texture);

    //Draw instances
    glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, sprites->verts, instances);
}

void spritesrender_render_3d(SpritesRender* sprites, SpriteInstance* insts, int instances, vec3 cam_pos)
{
    //Assert that the instance data fits into the allocated buffer
    if(instances > sprites->instances){
        LOG_ERROR_("spritesrender_render_3d: too much instance data");
        raise(SIGINT);
    }

    //Transform instances into the format expected by the shader
    for(int i = 0; i < instances; ++i){
        glm_vec3_sub(insts[i].pos, cam_pos, insts[i].pos);
    }

    //Bind vertex & instance array to be modified
    glBindVertexArray(sprites->vao);
    glBindBuffer(GL_ARRAY_BUFFER, sprites->instance_vbo);
    glBufferSubData(GL_ARRAY_BUFFER, 0, instances * sizeof(SpriteInstance), insts);

    //Bind the texture to be used for the draw call
    glBindTexture(GL_TEXTURE_3D, sprites->texture);

    //Draw instances
    glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, sprites->verts, instances);
}

void boardrender_alloc(BoardRender* boards, int instances, const char* path, int fontw, int fonth)
{
    //Store parameteres
    boards->instances = instances;

    //Load shader program
    ShaderInfo shaders[2];
    shaderinfo_init(&shaders[0], GL_VERTEX_SHADER, "res/shd/boards.vert");
    shaderinfo_init(&shaders[1], GL_FRAGMENT_SHADER, "res/shd/boards.frag");
    if(shaders_compile_program(shaders, 2, &boards->program)){
        LOG_ERROR_("failed to compile shaders into program");
        raise(SIGINT);
    }

    //Get uniform locations
    boards->uniloc_mat_mv = glGetUniformLocation(boards->program, "mat_mv");
    boards->uniloc_mat_pr = glGetUniformLocation(boards->program, "mat_pr");
    boards->uniloc_bbox = glGetUniformLocation(boards->program, "bbox");
    boards->uniloc_chardim = glGetUniformLocation(boards->program, "chardim");
    boards->uniloc_window = glGetUniformLocation(boards->program, "window");

    //Generate & configure VAO
    glGenVertexArrays(1, &boards->vao);
    glBindVertexArray(boards->vao);

    //Generate & configure VBO
    glGenBuffers(1, &boards->vbo);
    glBindBuffer(GL_ARRAY_BUFFER, boards->vbo);

    //Specify the vertex attribute of position
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE,
        sizeof(Vertex2PT), (GLvoid*)(offsetof(Vertex2PT, pos)));
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE,
        sizeof(Vertex2PT), (GLvoid*)(offsetof(Vertex2PT, tex)));

    //Fill the buffer with vertex data
    boards->verts = 4;
    Vertex2PT* verts = malloc(boards->verts * sizeof(Vertex2PT));
    verts[0] = (Vertex2PT){ {  1.0f,  1.0f }, { 1.0f,  1.0f, } };
    verts[1] = (Vertex2PT){ {  1.0f, -1.0f }, { 1.0f,  0.0f, } };
    verts[3] = (Vertex2PT){ { -1.0f, -1.0f }, { 0.0f,  0.0f, } };
    verts[2] = (Vertex2PT){ { -1.0f,  1.0f }, { 0.0f,  1.0f, } };
    glBufferData(GL_ARRAY_BUFFER, boards->verts * sizeof(Vertex2PT), verts, GL_STATIC_DRAW);
    free(verts);

    //Enable generic vertex attribute arrays
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    //Generate & configure instance VBO
    glGenBuffers(1, &boards->instance_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, boards->instance_vbo);
    glBufferData(GL_ARRAY_BUFFER, boards->instances * sizeof(BoardInstance), NULL, GL_DYNAMIC_DRAW);

    //Specify the instance attribute layout
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE,
        sizeof(BoardInstance), (GLvoid*)(offsetof(BoardInstance, pos)));
    glVertexAttribIPointer(3, 1, GL_INT,
        sizeof(BoardInstance), (GLvoid*)(offsetof(BoardInstance, code)));
    glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE,
        sizeof(BoardInstance), (GLvoid*)(offsetof(BoardInstance, col)));
    glVertexAttribIPointer(5, 2, GL_INT,
        sizeof(BoardInstance), (GLvoid*)(offsetof(BoardInstance, off)));

    //Enable instance attribute arrays, updating them every 1 instance
    glVertexAttribDivisor(2, 1); glEnableVertexAttribArray(2);
    glVertexAttribDivisor(3, 1); glEnableVertexAttribArray(3);
    glVertexAttribDivisor(4, 1); glEnableVertexAttribArray(4);
    glVertexAttribDivisor(5, 1); glEnableVertexAttribArray(5);

    //Generate name for a texture
    glGenTextures(1, &boards->texture);

    //Fill texture with font
    boardrender_load(boards, path, fontw, fonth);
}

void boardrender_dealloc(BoardRender* boards)
{
    //Delete VBO
    glDeleteBuffers(1, &boards->vbo);
    glDeleteBuffers(1, &boards->instance_vbo);

    //Delete textures
    glDeleteTextures(1, &boards->texture);

    //Delete VAO
    glDeleteVertexArrays(1, &boards->vao);

    //Delete shader program
    glDeleteProgram(boards->program);
}

void boardrender_load(BoardRender* boards, const char* path, int fontw, int fonth)
{
    //Set stb_image to flip images vertically on load
    stbi_set_flip_vertically_on_load(true); 

    //Attempt to load image
    uint8_t* data = (uint8_t*) stbi_load(path, &boards->imgdim[0], &boards->imgdim[1], &boards->imgchan, 0);
    if(!data){
        LOG_ERROR_("boardrender_load: failed to load \"%s\"", path);
        raise(SIGINT);
    }
    if(boards->imgchan != 1){
        LOG_ERROR_("expecting 1 color channel, got %d", boards->imgchan);
        raise(SIGINT);
    }

    //Calculate number of glyph rows & columns in the image
    boards->bbox[0] = fontw;
    boards->bbox[1] = fonth;
    boards->chardim[0] = boards->imgdim[0] / boards->bbox[0];
    boards->chardim[1] = boards->imgdim[1] / boards->bbox[1];

    //Configure texture
    glBindTexture(GL_TEXTURE_2D, boards->texture);
    glTexImage2D(
        GL_TEXTURE_2D,
        0, //level-of-detail number
        GL_R8,
        boards->imgdim[0], boards->imgdim[1],
        0,
        GL_RED,
        GL_UNSIGNED_BYTE,
        data
    );

    //Set texture filtering parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    //Free image data
    free(data);
}

void boardrender_setup(BoardRender* boards, mat4 mv, mat4 pr, int* windowsizes)
{
    //Change rendering program
    glUseProgram(boards->program);

    //Set model-view and projection matrices
    glUniformMatrix4fv(boards->uniloc_mat_mv, 1, GL_FALSE, (const GLfloat*)mv);
    glUniformMatrix4fv(boards->uniloc_mat_pr, 1, GL_FALSE, (const GLfloat*)pr);

    //Set font bounding box
    glUniform2iv(boards->uniloc_bbox, 1, (const GLint*)boards->bbox);

    //Set row & column count for glyphs in the texture
    glUniform2iv(boards->uniloc_chardim, 1, (const GLint*)boards->chardim);

    //Set window size
    glUniform2iv(boards->uniloc_window, 1, (const GLint*)windowsizes);

    //chprintf(client, "glGetError(): %d", glGetError());
}

void boardrender_render_2d(BoardRender* boards, BoardInstance* insts, int instances)
{
    //Assert that the instance data fits into the allocated buffer
    if(instances > boards->instances){
        LOG_ERROR_("boardrender_render_2d: too much instance data");
        raise(SIGINT);
    }

    //Bind vertex & instance array to be modified
    glBindVertexArray(boards->vao);
    glBindBuffer(GL_ARRAY_BUFFER, boards->instance_vbo);
    glBufferSubData(GL_ARRAY_BUFFER, 0, instances * sizeof(BoardInstance), insts);

    //Bind the texture to be used for the draw call
    glBindTexture(GL_TEXTURE_2D, boards->texture);

    //Draw instances
    glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, boards->verts, instances);
}

void boardrender_render_3d(BoardRender* boards, BoardInstance* insts, int instances, vec3 cam_pos)
{
    //Assert that the instance data fits into the allocated buffer
    if(instances > boards->instances){
        LOG_ERROR_("boardrender_render_3d: too much instance data");
        raise(SIGINT);
    }

    //Transform instances into the format expected by the shader
    for(int i = 0; i < instances; ++i){
        glm_vec3_sub(insts[i].pos, cam_pos, insts[i].pos);
    }

    //Bind vertex & instance array to be modified
    glBindVertexArray(boards->vao);
    glBindBuffer(GL_ARRAY_BUFFER, boards->instance_vbo);
    glBufferSubData(GL_ARRAY_BUFFER, 0, instances * sizeof(BoardInstance), insts);

    //Bind the texture to be used for the draw call
    glBindTexture(GL_TEXTURE_2D, boards->texture);

    //Draw instances
    glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, boards->verts, instances);
}

void panelrender_alloc(PanelRender* panels, int instances, int fontw, int fonth)
{
    //Store parameters
    panels->instances = instances;
    memcpy(panels->chardim, &(int[2]){ fontw, fonth }, sizeof(int[2]));

    //Allocate instance data
    panels->insts = malloc(sizeof(BoardInstance) * panels->instances);
    panels->scratch = malloc(sizeof(char) * panels->instances);

    //Allocate renderer for background sprite
    spritesrender_alloc(&panels->sprites, 1);
    spritesrender_load_file(&panels->sprites, "res/tex/panel_fg1_100_", 1);
}

void panelrender_dealloc(PanelRender* panels)
{
    //Deallocate instance data
    free(panels->insts);
    free(panels->scratch);

    //Deallocate renderer for background sprite
    spritesrender_dealloc(&panels->sprites);
}

void panelrender_typeset_reset(PanelRender* panels, int columns, vec3 pos, int padding)
{
    //Store parameters
    panels->columns = columns;
    glm_vec3_copy(pos, panels->pos);
    panels->padding = padding;

    //Reset row & column indices
    panels->col = panels->row = 0;

    //Reset character count
    panels->length = 0;

    //Reset maximum column
    panels->maxcol = 0;
}

void panelrender_typeset_append(PanelRender* panels, vec4 color, const char* fmt, ...)
{
    //Print the string into scratch space
    va_list vl; va_start(vl, fmt);
    vsprintf(panels->scratch, fmt, vl);
    va_end(vl);

    //Assert that there are no more characters than boards
    int length = strlen(panels->scratch);
    if(panels->length + length > panels->instances){
        LOG_ERROR_("panelrender_typeset: too many characters");
        raise(SIGINT);
    }

    int i = 0;
    while(i < length){
        
        switch(panels->scratch[i]){
        case '\n': {
            
            //Step panels->row & column indices to next line
            panels->col = 0; ++panels->row;

            panels->insts[panels->length + i] = (BoardInstance){
                { panels->pos[0], panels->pos[1], panels->pos[2] },
                ' ',
                { color[0], color[1], color[2] },
                { panels->chardim[0] * panels->col,
                  0 - panels->chardim[1] * panels->row },
            };
            ++i;

        } break;
        default: {

            //Find the beginning and ending indices of the token
            int pos_next = i + strcspn(&panels->scratch[i], " \n");
            pos_next += strspn(&panels->scratch[pos_next], " ");

            //If the token would exceed the columns, go to the next line
            if(panels->col + (pos_next - i) > panels->columns){
                panels->col = 0; ++panels->row;
            }

            //Render each character of the token
            while(i < pos_next){
                panels->insts[panels->length + i] = (BoardInstance){
                    { panels->pos[0], panels->pos[1], panels->pos[2] },
                    panels->scratch[i],
                    { color[0], color[1], color[2] },
                    { panels->chardim[0] * panels->col,
                      0 - panels->chardim[1] * panels->row },
                };
                ++i;
                ++panels->col;
            }

            //Note the maximum column index
            panels->maxcol = MAX(panels->maxcol, panels->col);

            //Step panels->row & column indices
            if(panels->col >= panels->columns){ panels->col = 0; ++panels->row; }

        } break;
        }
    }

    //Increase character count
    panels->length += length;
}

void panelrender_render_2d(PanelRender* panels, BoardRender* boards, int* windowsizes, mat4 mv, mat4 pr, int center[2])
{
    //Calculate background width & height
    int dim_bg[2] = { panels->maxcol * panels->chardim[0],
                      panels->row    * panels->chardim[1] };

    //Background sprite instance
    SpriteInstance bg_inst = (SpriteInstance){
        { panels->pos[0], panels->pos[1], panels->pos[2] },
	1.0f,
        { PAL[BG_BLACK][0], PAL[BG_BLACK][1],
	  PAL[BG_BLACK][2], 0.9f },
        { center[0], center[1] },
        { dim_bg[0] + panels->padding * 2, dim_bg[1] + panels->padding * 2 },
    };

    //Render background
    spritesrender_setup(&panels->sprites, mv, pr, windowsizes);
    spritesrender_render_2d(&panels->sprites, &bg_inst, 1);

    //Offset panel instances by the center point, as well as:
    //  horizontally by half of the maximal column index
    //  vertically by half of the maximal row index
    for(int i = 0; i < panels->length; ++i){
        panels->insts[i].off[0] +=
            center[0] - (panels->maxcol - 1) * panels->chardim[0] / 2;
        panels->insts[i].off[1] +=
            center[1] + (panels->row - 1) * panels->chardim[1] / 2;
    }

    //Render text
    boardrender_setup(boards, mv, pr, windowsizes);
    boardrender_render_2d(boards, panels->insts, panels->length);
}

