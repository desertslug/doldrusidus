#define TB_IMPL
#include "termbox2.h"

#define PLANCKLOG_FILE
#define PLANCKLOG_HEADER
#include "plancklog.h"

#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include "srp.h"
#include "helper.h"
#include "lstr.h"
#include "uvec.h"
#include "glfw.h"

#include "cglm/vec4.h"

#define SRP_HASH SRP_SHA1
#define SRP_NG SRP_NG_2048

#include "cliconf.h"

#include "curse.c"
#include "world.c"
#include "modcli.c"
#include "memory.c"

ModSession modsession;
ModClient modcli;
GLFWwindow* gui;
termui_t tui;

//Global color palette, normalized, 4 channel
vec4 PAL[16];

int KEY_MAPS[GL_KEY_COUNT];
int BUTTON_MAPS[GL_MOUSE_BUTTON_COUNT];

void chprintf(const char* fmt, ...);
void on_client_window_open(ModClient* modcli);
void on_client_window_close(ModClient* modcli);
void on_client_update_prompt();
uint8_t on_dei(UXN_DEV_ARGS);
void on_deo(UXN_DEV_ARGS);

#include "termui.c"
#include "climsg.c"
#include "ecs.c"
#include "compo.c"
#include "uxn.c"
#include "dev.c"
#include "random.c"
#include "romu.c"
#include "gfx.c"
#include "graphics.c"
#include "shader.c"
#include "input.c"
#include "camera.c"
#include "ivec.c"
#include "sweep.c"
#include "system.c"
#include "artifact.c"
#include "particle.c"

void chprintf(const char* fmt, ...)
{
    va_list vl;
    va_start(vl, fmt);

    tui_vhprintf(&tui, fmt, vl, modsession.focusent);

    va_end(vl);
}

void error_callback(int error, const char* description)
{
    LOG_ERROR_("glfw error_callback: %s", description);
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    InputInfo* ii = &modcli.inputinfo;
    int mapkey = KEY_MAPS[key];

    ii->kbmouse.keys_act[mapkey] = action;
    ++ii->kbmouse.keys_delta[mapkey];
    ii->kbmouse.keys_mods[mapkey] |= mods;
}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
    InputInfo* ii = &modcli.inputinfo;
    int mapbut = BUTTON_MAPS[button];

    ii->kbmouse.buttons_act[mapbut] = action;
    ++ii->kbmouse.buttons_delta[mapbut];
    ii->kbmouse.buttons_mods[mapbut] |= mods;
}

void cursorpos_callback(GLFWwindow* window, double xpos, double ypos)
{
    InputInfo* ii = &modcli.inputinfo;

    ii->kbmouse.cursor_rel[0] += xpos - ii->kbmouse.cursor_pre[0];
    ii->kbmouse.cursor_rel[1] += ypos - ii->kbmouse.cursor_pre[1];
    ii->kbmouse.cursor_pre[0] = xpos;
    ii->kbmouse.cursor_pre[1] = ypos;
}

void joystick_callback(int jid, int event)
{
    /*chprintf(cli, "jid: %d, event: %s",
        jid,
        (event == GL_CONNECTED) ? "GL_CONNECTED" :
            ((event == GL_DISCONNECTED) ? "GL_DISCONNECTED" : "UNKNOWN"));*/
}

void window_size_callback(GLFWwindow* window, int width, int height)
{
    InputInfo* ii = &modcli.inputinfo;

    ii->windsize[0] = width;
    ii->windsize[1] = height;
}

void on_client_window_open(ModClient* modcli)
{
    //If the client window is already open, return early silently
    if(gui){ return; }

    //Get configuration
    StartConfig* config = client_get_config(modcli->client);
    if(!config){ return; }

    //Open and store pointer to GLFW window
    gui = glfwwindow_initialize(config, "cli-doldrusidus", modcli->client,
        error_callback, key_callback, mouse_button_callback,
        cursorpos_callback, joystick_callback, window_size_callback);
    if(!gui){ return; }

    //Enable blending
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    //Enable stippled lines
    glEnable(GL_LINE_STIPPLE);

    //Enable depth testing
    glEnable(GL_DEPTH_TEST);

    //Enable 3D texturing
    glEnable(GL_TEXTURE_3D);

    //Color palette
    hex_rgba_to_vec4_norm(COL_BG_BLACK   << 8 | 0xff, PAL[BG_BLACK]);
    hex_rgba_to_vec4_norm(COL_BG_RED     << 8 | 0xff, PAL[BG_RED]);
    hex_rgba_to_vec4_norm(COL_BG_GREEN   << 8 | 0xff, PAL[BG_GREEN]);
    hex_rgba_to_vec4_norm(COL_BG_YELLOW  << 8 | 0xff, PAL[BG_YELLOW]);
    hex_rgba_to_vec4_norm(COL_BG_BLUE    << 8 | 0xff, PAL[BG_BLUE]);
    hex_rgba_to_vec4_norm(COL_BG_MAGENTA << 8 | 0xff, PAL[BG_MAGENTA]);
    hex_rgba_to_vec4_norm(COL_BG_CYAN    << 8 | 0xff, PAL[BG_CYAN]);
    hex_rgba_to_vec4_norm(COL_BG_WHITE   << 8 | 0xff, PAL[BG_WHITE]);

    hex_rgba_to_vec4_norm(COL_FG_BLACK   << 8 | 0xff, PAL[FG_BLACK]);
    hex_rgba_to_vec4_norm(COL_FG_RED     << 8 | 0xff, PAL[FG_RED]);
    hex_rgba_to_vec4_norm(COL_FG_GREEN   << 8 | 0xff, PAL[FG_GREEN]);
    hex_rgba_to_vec4_norm(COL_FG_YELLOW  << 8 | 0xff, PAL[FG_YELLOW]);
    hex_rgba_to_vec4_norm(COL_FG_BLUE    << 8 | 0xff, PAL[FG_BLUE]);
    hex_rgba_to_vec4_norm(COL_FG_MAGENTA << 8 | 0xff, PAL[FG_MAGENTA]);
    hex_rgba_to_vec4_norm(COL_FG_CYAN    << 8 | 0xff, PAL[FG_CYAN]);
    hex_rgba_to_vec4_norm(COL_FG_WHITE   << 8 | 0xff, PAL[FG_WHITE]);

    //Allocate renderers
    linerender_alloc(&modcli->lines, ENTITY_RENDER_MAX * 4);
    spritesrender_alloc(&modcli->sprites, ENTITY_RENDER_MAX);
    spritesrender_load_file(&modcli->sprites, "res/tex/ent15_", ENTTYPE_COUNT);
    boardrender_alloc(&modcli->boards, ENTITY_RENDER_MAX * PROQUINT_CHARS,
        "res/font/tamsynmod8x15r_alt.tga", 8, 15);
    panelrender_alloc(&modcli->panels, PANEL_CHAR_RENDER_MAX, 8, 15);
    spritesrender_alloc(&modcli->sprites_resolved, 1);
    spritesrender_alloc(&modcli->sprites_bg, 1);
    spritesrender_load_file(&modcli->sprites_bg, "res/tex/panel_fg1_100_", 1);
    spritesrender_alloc(&modcli->sprites_fg, ENTITY_RENDER_MAX);
    spritesrender_load_file(&modcli->sprites_fg, "res/tex/panel_fg1_100_", 1);
    spritesrender_alloc(&modcli->parts.sprites, PARTICLE_RENDER_MAX);
    spritesrender_load_file(&modcli->parts.sprites, "res/tex/particle_", PART_SPRITE_COUNT);

    //Set initial window size
    InputInfo* ii = &modcli->inputinfo;
    ii->windsize[0] = CLI_WINDOW_X;
    ii->windsize[1] = CLI_WINDOW_Y;
    glfwSetWindowSize(gui, ii->windsize[0], ii->windsize[1]);

    //Initialize key maps
    for(int i = 0; i < GL_KEY_COUNT; ++i){
        KEY_MAPS[i] = i;
    }

    //Initialize button maps
    for(int i = 0; i < GL_MOUSE_BUTTON_COUNT; ++i){
        BUTTON_MAPS[i] = i;
    }
}

void on_client_window_close(ModClient* modcli)
{
    //If the client window is already closed, return early silently
    if(!gui){ return; }

    //Deallocate renderers
    linerender_dealloc(&modcli->lines);
    spritesrender_dealloc(&modcli->sprites);
    boardrender_dealloc(&modcli->boards);
    panelrender_dealloc(&modcli->panels);
    spritesrender_dealloc(&modcli->sprites_resolved);
    spritesrender_dealloc(&modcli->sprites_bg);
    spritesrender_dealloc(&modcli->sprites_fg);
    spritesrender_dealloc(&modcli->parts.sprites);

    //Close window, reset pointer
    glfwwindow_terminate(gui);
    gui = NULL;
}

void on_client_update_prompt()
{
    const char* modality_names[ModalityCount] = {
        [NormalMode] = "NOR",
        [ControllerMode] = "CON"
    };

    lstr_init(&tui.prompt);

    //Input modality
    lstr_cat_str(&tui.prompt, "[");
    lstr_cat_str(&tui.prompt, modality_names[modsession.modality]);
    lstr_cat_str(&tui.prompt, "]");

    //Focused entity
    if(modsession.focusent != 0xffff){
        char pq[6] = {0}; short2quint(modsession.focusent, pq);
        char id[5] = {0}; short2quart(modsession.focusent, id);
        lstr_cat_str(&tui.prompt, "<");
        lstr_cat_str(&tui.prompt, id);
        lstr_cat_str(&tui.prompt, " ");
        lstr_cat_str(&tui.prompt, pq);
        lstr_cat_str(&tui.prompt, ">");
    }

    //Username
    const char* uname = client_get_uname(modcli.client);
    if(uname){
        lstr_cat_str(&tui.prompt, "[");
        lstr_cat_str(&tui.prompt, uname);
        lstr_cat_str(&tui.prompt, "]");
    }

    lstr_cat_str(&tui.prompt, "> ");

    tui.inputdelta = true;
}

void on_client_start(void* cli)
{
    if(sizeof(ModSession) > SESSION_BYTES){
        LOG_ERROR_("modsession_init: size exceeds SESSION_BYTES");
        raise(SIGINT);
    }

    modclient_start(&modcli, cli);

    modsession_init(&modsession);

    //Start without a window
    gui = NULL;

    termui_init(&tui, TERMUI_HISTORY);

    //Prompt
    on_client_update_prompt();

    //Wait for the connection to succeed
    if(client_connect_wait(modcli.client, CONN_WAIT_MSEC)){
        ENetPeer* peer = client_get_peer(modcli.client);
        if(peer){
            uint32_t host = peer->address.host;
            chprintf("client connected to %hhu.%hhu.%hhu.%hhu:%hu\n",
                (host >> 0) & 0xff, (host >> 8) & 0xff,
                (host >> 16) & 0xff, (host >> 24) & 0xff,
                peer->address.port);
        }
    }
}

void on_client_stop()
{
    modclient_stop(&modcli);

    termui_stop(&tui);

    //Shutdown termbox
    tb_shutdown();
}

void process_input_buffer(rtcp_uvec_t* inbuf, rtcp_uvec_t* cmds)
{
    //If there are no strings in the buffer, return early
    if(inbuf->size == 0){ return; }

    //If the client started authenticating, but is not finished yet, return
    if(client_auth_started(modcli.client) && !client_get_uname(modcli.client))
    { return; }

    //Get input
    lstr_t* lstr = rtcp_uvec_get(inbuf, 0);

    //Attempt to process input
    ENetPacket* opkt = pkt_create(NULL, 0, ENET_PACKET_FLAG_RELIABLE);
    str_process_to_msg(cmds, lstr_get_mut(lstr), lstr_len(lstr), opkt,
        &modcli, &modsession, &modcli.firstargs);

    //Send nonempty output packet
    ENetPeer* peer = client_get_peer(modcli.client);
    if(pkt_len(opkt) && peer){ enet_peer_send(peer, 0, opkt); }
    else { pkt_destroy(opkt); }

    //Pop input
    lstr_free(lstr);
    rtcp_uvec_pop(inbuf, 0);
}

void on_world_step(World* world, struct timespec delta)
{
    world_step(world, delta);
    Entity ents[ENTITY_SYNC_EXPIRE_MAX];
    world_forget(world, ents, ENTITY_SYNC_EXPIRE_MAX,
        ENTITY_SYNC_EXPIRE_MS, ENTITY_SYNC_FADE_MS);

    ArchID arch; (void)arch;
    Query q; query_alloc(&q);

    //Run systems
    //system_nav_drive_pos_pphys(world, &q);
    system_pos_pphys_integrate(world, &q);

    query_dealloc(&q);
}

void client_swap(GLFWwindow* window)
{
    //Handle GLFW window if there is one
    if(window){
        glfwSwapBuffers(window);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }
}

void client_poll(void* cli, GLFWwindow* window)
{
    //Silently return early if the window is not opened
    if(!window){ return; }

    KBMouse* kbm = &modcli.inputinfo.kbmouse;
    Gamepad* gmp = &modcli.inputinfo.gamepad;

    //Poll GLFW events
    glfwPollEvents();
    
    //Check joystick presence, and whether they're gamepads
    for(int i = 0; i <= GLFW_JOYSTICK_LAST; ++i){
        if(glfwJoystickIsGamepad(i)){

            //Fetch gamepad state
            GLFWgamepadstate state;
            glfwGetGamepadState(i, (GLFWgamepadstate*)&state);
            memcpy(&gmp->state, &state, sizeof(GamepadState));
        }
    }

    Camera* cam = &modcli.camera;

    //Perform cross-mapping between devices based on input modality
    if(modsession.modality == NormalMode){
        gamepad_process(gmp, &modsession, &modcli,
            CLI_JOYSTICK_DEADZONE, cam->azi);
        map_gamepad_to_kbmouse(gmp, kbm,
            CLI_JOYSTICK_THRESHOLD, CLI_JOYSTICK_CURSOR_SENS);
        kbmouse_process(kbm, &modsession, &modcli);
    } else
    if(modsession.modality == ControllerMode){
        kbmouse_process(kbm, &modsession, &modcli);
        map_kbmouse_to_gamepad(kbm, gmp);
        gamepad_process(gmp, &modsession, &modcli,
            CLI_JOYSTICK_DEADZONE, cam->azi);
    }

    ECS* ecs = &modcli.world.ecs;

    //Process input before it is stepped and reset
    switch(modsession.modality){
    case NormalMode: {
        camera_process_kbmouse(cam, kbm, gui, ecs, modsession.resolvent);
    } break;
    case ControllerMode: {
        camera_process_gamepad(cam, gmp, gui, ecs, modsession.resolvent);
    } break;
    case ModalityCount: {} break;
    }

    //Step input devices
    kbmouse_step(kbm);
    gamepad_step(gmp, CLI_JOYSTICK_SENSITIVITY);
}

void client_draw(ModClient* modcli)
{
    InputInfo* ii = &modcli->inputinfo;
    Camera* cam = &modcli->camera;
    World* world = &modcli->world;
    KBMouse* kbm = &modcli->inputinfo.kbmouse;
    //Gamepad* gmp = &modcli->inputinfo.gamepad;

    //Clear color & depth buffers
    glClearColor(
	PAL[BG_BLACK][0], PAL[BG_BLACK][1], PAL[BG_BLACK][2], PAL[BG_BLACK][3]);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //Calculate aspect ratio
    float aspect = ii->windsize[0] / (float)ii->windsize[1];

    //Set the affine transformation for the viewport
    glViewport(0, 0, ii->windsize[0], ii->windsize[1]);

    //Calculate properties of third-person camera
    vec3 campos; uint32_to_vec3(cam->pos, campos);
    vec3 offset;
    glm_vec3_scale(cam->dir, cam->zoom_pos, offset);
    glm_vec3_add(campos, offset, campos);
    vec3 camdir; glm_vec3_scale(cam->dir, -1.0f * cam->zoom_pos, camdir);

    //Construct view matrix
    mat4 view;
    glm_look(GLM_VEC3_ZERO, camdir, cam->worldup, view);

    //Construct projection matrix
    mat4 perspective;
    glm_perspective(glm_rad(90.0f), aspect,
        cam->zoom_pos / 256.0f, 4 * TWOPOW16F, perspective);

    //Draw particle system
    glDisable(GL_DEPTH_TEST);
    {
	spritesrender_setup(&modcli->parts.sprites, view, perspective, ii->windsize);
	parts_render(&modcli->parts, modcli->world.age, cam);
    }
    glEnable(GL_DEPTH_TEST);

    //Draw sprites
    spritesrender_setup(&modcli->sprites, view, perspective, ii->windsize);

    EntityInstance entity_inst[ENTITY_RENDER_MAX];
    SpriteInstance sprite_inst[ENTITY_RENDER_MAX];
    SpriteInstance sprite_inst_xform[ENTITY_RENDER_MAX];
    int entities = spritesrender_entities(&modcli->sprites,
        entity_inst, sprite_inst, sprite_inst_xform, &world->ecs, cam);


    //Attempt to resolve an entity
    switch(modsession.modality){
    case NormalMode: { //Resolve from mouse coordinates
        modsession.resolvent = resolve_entity(
	    cam, entity_inst, sprite_inst, entities,
            (float[2]){
                glm_lerp(-1.0f, 1.0f, kbm->cursor_abs[0] / ii->windsize[0]),
                glm_lerp(-1.0f, 1.0f, (ii->windsize[1] - kbm->cursor_abs[1]) / ii->windsize[1])
            },
            ii->windsize
        );
    } break;
    case ControllerMode: { //Resolve from the center of the screen
        modsession.resolvent = resolve_entity(
	    cam, entity_inst, sprite_inst, entities,
            (float[2]){ 0.0f, 0.0f }, ii->windsize);
    } break;
    case ModalityCount: {} break;
    }

    //Draw lines
    linerender_setup(&modcli->lines, view, perspective);
    
    linerender_world_border(&modcli->lines, cam);

    linerender_horizontal_axes(&modcli->lines, cam);

    linerender_entity_verticals(&modcli->lines, sprite_inst, entities, cam);

    linerender_entity_nav(&modcli->lines, entity_inst, sprite_inst, entities, &world->ecs, cam);

    //Draw sprites
    spritesrender_setup(&modcli->sprites_fg, view, perspective, ii->windsize);

    spritesrender_entity_modules(&modcli->sprites_fg, sprite_inst, entities,
        modcli->sprites.maxdim[0], modcli->sprites.maxdim[1], &world->ecs, cam);

    //Draw boards
    boardrender_setup(&modcli->boards, view, perspective, ii->windsize);

    boardrender_world_cardinals(&modcli->boards, cam);

    boardrender_entity_id(&modcli->boards, entity_inst, sprite_inst, entities,
        modcli->sprites.maxdim[0], modcli->sprites.maxdim[1], cam);

    boardrender_entity_distance(&modcli->boards, sprite_inst, entities,
        modcli->sprites.maxdim[0], modcli->sprites.maxdim[1], cam);

	boardrender_entity_pilotmarker(&modcli->boards, &world->ecs,
		modcli->sprites.maxdim[0], modcli->sprites.maxdim[1], cam,
		client_get_uname(modcli->client));

    if(modsession.resolvent != 0xffff){
        boardrender_resolved_entity(
            &modcli->boards, &world->ecs, modsession.resolvent,
            modcli->sprites.maxdim[0], modcli->sprites.maxdim[1], cam);
    }

    //Disable depth testing to draw menu-like elements on top
    glDisable(GL_DEPTH_TEST);
    {
        //Show the age of the world at the top
        panelrender_age(&modcli->panels, &modcli->boards, world->age, ii->windsize);

		//Show camera properties at the bottom
        panelrender_camera(&modcli->panels, &modcli->boards, cam, ii->windsize);
        
        //If an entity was resolved, render an information panel for it
        if(modsession.resolvent != 0xffff){
            panelrender_resolved_entity(&modcli->panels, &modcli->boards,
                ii->windsize, &world->ecs, modsession.resolvent,
                view, perspective, cam);
            spritesrender_resolved_entity(&modcli->sprites_resolved,
                &modcli->sprites_bg, ii->windsize,
                &world->ecs, modsession.resolvent, view, perspective, cam);
        }
    }
    glEnable(GL_DEPTH_TEST);
}

void modsession_step(ModSession* session, Camera* camera)
{
    //Store position in session
    for(int i = 0; i < 3; ++i){
        session->pos[i] = camera->pos[i];
    }
    
    //Store zoom position as radius in session
	session->radius = camera_get_zoom_u32(camera);

    session->delta = true; //teststuff
}

void on_client_step(struct timespec delta)
{
    //Swap front & back buffers
    client_swap(gui);

    //Poll glfw
    client_poll(modcli.client, gui);

    //Close opened client window if the "should close" flag is set
    if(gui && glfwWindowShouldClose(gui)){
        on_client_window_close(&modcli);
    }

    //Handle focused entity device output
    ECS* ecs = &modcli.world.ecs;
    ModSession* modsess = &modsession;
    if(modsess->focusent != 0xffff && ecs_entity_alive(ecs, modsess->focusent)){
        
        Uxn* uxn = (Uxn*)ecs_get_id(ecs, modsess->focusent, UxnID);
        if(uxn){
            while(uxn->oevt.size > 0){
                Event evt = buffer_pop(&uxn->oevt);
                switch(evt.devport){
                case UXN_DEV_CONS + UXN_PORT_CONS_WRITE: {
                    chprintf("%c", evt.data);
                } break;
                }
            }
        }
    }

    //Keep processing any input
    process_input_buffer(&tui.inbuf, &modcli.cmds);

    //Update terminal
    termui_update(&tui, &modcli.cmds, &modcli.firstargs);

    //World step
    on_world_step(&modcli.world, delta);

    //Step camera
    camera_step(&modcli.camera, &modcli.world.ecs, timespec_float(&delta));

    //Step modsession
    modsession_step(&modsession, &modcli.camera);

    //Step particle system
    parts_step(&modcli.parts, modcli.world.age, delta, &modcli.camera);

    //Step cosmic wind
    wind_step(&modcli.wind, delta, modcli.parts.romu_trio32);

    //Particle spawning
    if(gui && !parts_full(&modcli.parts)){

        //Cosmic wind particles
        wind_spawn(&modcli.wind,
            &modcli.parts, modcli.world.age, delta, &modcli.camera);

        //Particles from the regular ECS
        Query q; query_alloc(&q);
        {
            solar_spawn(&modcli.world.ecs,
                &q, &modcli.parts, modcli.world.age, delta, &modcli.camera);
            drive_spawn(&modcli.world.ecs,
                &q, &modcli.parts, modcli.world.age, delta, &modcli.camera);
            oasis_spawn(&modcli.world.ecs,
                &q, &modcli.parts, modcli.world.age, delta, &modcli.camera);
            monolith_spawn(&modcli.world.ecs,
                &q, &modcli.parts, modcli.world.age, delta, &modcli.camera);
            wormhole_spawn(&modcli.world.ecs,
                &q, &modcli.parts, modcli.world.age, delta, &modcli.camera);
        }
        query_dealloc(&q);
    }

    //Render
    if(gui){ client_draw(&modcli); }
}

void on_client_recv(ENetPeer* peer, void* pkt)
{
    assert_or_exit(peer, "on_client_recv: peer was NULL\n");

    //Attempt to process packet
    ENetPacket* opkt = pkt_create(NULL, 0, ENET_PACKET_FLAG_RELIABLE);
    pkt_process(pkt, opkt, &modcli, &modsession);

    //Send nonempty output packet
    if(pkt_len(opkt)){ enet_peer_send(peer, 0, opkt); }
    else { pkt_destroy(opkt); }
}

void on_client_args(int argc, char** argv)
{
    int idx = 0;
    while(idx < argc){
        char* arg = argv[idx];

        //Command to be passed to the interpreter
        if(strcmp(arg, "-c") == 0 && argc - idx > 1){
            char* nextarg = argv[idx + 1];
            
            //Pass every character of the command
            int len = strlen(nextarg);
            for(int k = 0; k < len; ++k){
                termui_input_char(&tui,
					nextarg[k], &modcli.cmds, &modcli.firstargs);
            }
            
            ++idx;
        }

        //Command file to be passed to the interpreter
        if(strcmp(arg, "--cmds") == 0 && argc - idx > 1){
            char* nextarg = argv[idx + 1];

			//Open command file
			FILE* cmds = fopen(nextarg, "r");
			if(cmds){

				//Pass every character of the command file
				char c;
				while(fread(&c, 1, 1, cmds) == 1){
					termui_input_char(&tui,
						c, &modcli.cmds, &modcli.firstargs);
				}

				fclose(cmds);
			}
            
            ++idx;
        }

        //Start in windowed mode
        if(strcmp(arg, "-w") == 0 && argc - idx > 0){
            on_client_window_open(&modcli);
        }

        ++idx;
    }
}
