#ifndef DEO_H
#define DEO_H

#include "defs.h"
#include "uxn.h"
#include "memory.h"

uint8_t on_dei(UXN_DEV_ARGS);
void on_deo(UXN_DEV_ARGS);

#endif /* DEO_H */