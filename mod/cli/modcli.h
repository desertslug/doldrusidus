#ifndef MODCLI_H
#define MODCLI_H

#include "glfw.h"
#include "gfx.h"
#include "termui.h"
#include "world.h"
#include "random.h"
#include "uvec.h"
#include "modmsg.h"
#include "camera.h"
#include "input.h"
#include "particle.h"

typedef struct Piloted
{
	Entity ent;
	float dist;
	int weight;
} Piloted;

typedef enum PilotedFilter
{
	PilotedAll = 0,
	PilotedWithinLookAt,
} PilotedFilter;

typedef struct ModClient
{
    void* client;
    World world;
	rtcp_uvec_t piloted;
	Hash1 rand;
    rtcp_uvec_t cmds;
    rtcp_uvec_t firstargs;
    Camera camera;
    InputInfo inputinfo;
    Buffer ievt;
    Particles parts;
    Wind wind;
    LineRender lines;
    SpritesRender sprites;
    BoardRender boards;
    PanelRender panels;
    SpritesRender sprites_resolved;
    SpritesRender sprites_fg;
    SpritesRender sprites_bg;
} ModClient;

void modclient_start(ModClient* modcli, void* client);
void modclient_stop(ModClient* modcli);
Entity modclient_get_sync_entity(ModClient* modcli, SyncModality syncmode);

void cmds_append_all(rtcp_uvec_t* cmds);

#endif /* MODCLI_H */
