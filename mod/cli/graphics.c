#include "graphics.h"

const char* RUIN_DESC[RUIN_LAST] = {
    [RUIN_NONE] = "NONE\n",
    [RUIN_SPIRES] = "Where spires once stretched skywards, only their crumbling husks remain.\n",
    [RUIN_GLASS] = "Dry riverbeds, cracked earth. The surface of deserts turned into molten glass.\n",
    [RUIN_SCORCHED] = "Infertile, scorched ground. The winds carry heavy dust.\n",
    [RUIN_ASHES] = "Crawling dunes of ash. Rivers of rock, no longer molten.\n",
    [RUIN_DIRIGIBLES] = "Byzantine frames of dirigibles dot the dry lakebeds, waiting to wake from their timeless rot.\n",
    [RUIN_DERELICTS] = "Hulking derelicts of reinforced concrete continue to be worn away by time.\n",
    [RUIN_BRIDGES] = "Countless bridges and dams easing slowly into the blankets of the surface below.\n",
    [RUIN_RADIOS] = "Time measured in the snapping of tension cables. Some sturdy radio towers stand unpowered, listening to the silence between.\n",
};

Entity resolve_entity(Camera* camera, EntityInstance* entities, SpriteInstance* instances, int count, float windnorm[2], int windsize[2])
{
    //Camera direction
    vec3 camdir; glm_vec3_scale(camera->dir, -1.0f * camera->zoom_pos, camdir);

    //Calculate aspect ratio
    float aspect = windsize[0] / (float) windsize[1];

    //Construct view matrix
    mat4 view;
    glm_look(camera->viewpos, camdir, camera->worldup, view);

    //Construct projection matrix
    mat4 perspective;
    glm_perspective(glm_rad(90.0f), aspect, 1.0f * 4, TWOPOW16F * 2, perspective);

    //Clip space coordinates
    vec4 clip = { windnorm[0], windnorm[1], 0.0f, 1.0f };

    //Get matrix that will convert clip space to world space
    mat4 conv; glm_mat4_mul(perspective, view, conv);
    glm_mat4_inv(conv, conv);

    //World space vector
    vec4 world; glm_mat4_mulv(conv, clip, world);

    //Perspective division
    world[0] /= world[3]; world[1] /= world[3]; world[2] /= world[3];

    //Get vector from the camera to the point in the world
    vec3 camtopoint;
    glm_vec3_sub((vec3){ world[0], world[1], world[2] },
        camera->viewpos, camtopoint);
    glm_vec3_normalize(camtopoint);

    //Get the entity with the highest value for the heuristic
    vec3 camtoent;
    float dot;
    float highestdot = -1.0f;
    int highestent = -1;
    for(int i = 0; i < count; ++i){
        
        //Get vector from the camera to the entity
        glm_vec3_sub(instances[i].pos, camera->viewpos, camtoent);
        glm_vec3_normalize(camtoent);

        //Dot product between the two vectors
        dot = glm_clamp(glm_vec3_dot(camtopoint, camtoent), -1.0f, 1.0f);
        highestent = dot > highestdot ? entities[i].ent : highestent;
        highestdot = dot > highestdot ? dot : highestdot;
    }

    //If there was a valid candidate
    if(highestent >= 0){

        //Return candidate if the angle to it was close enough
        return glm_deg(acos(highestdot)) < CLI_ENTITY_RESOLVE_ANGLE_DEG ?
            highestent : 0xffff;
    } else {
        return 0xffff;
    }
}

void linerender_world_border(LineRender* lines, Camera* camera)
{
    //Rendering style
    float width = 1.0f;
    uint16_t stipple = 0xffff;

    //Rendered data
    Vertex3PC verts[24] = {
        (Vertex3PC){ { 0.0f, 0.0f, 0.0f } },
        (Vertex3PC){ { TWOPOW16F, 0.0f, 0.0f } },
        (Vertex3PC){ { TWOPOW16F, 0.0f, 0.0f } },
        (Vertex3PC){ { TWOPOW16F, 0.0f, TWOPOW16F } },
        (Vertex3PC){ { TWOPOW16F, 0.0f, TWOPOW16F } },
        (Vertex3PC){ { 0.0f, 0.0f, TWOPOW16F } },

        (Vertex3PC){ { TWOPOW16F, 0.0f, 0.0f } },
        (Vertex3PC){ { TWOPOW16F, TWOPOW16F, 0.0f } },
        (Vertex3PC){ { TWOPOW16F, TWOPOW16F, 0.0f } },
        (Vertex3PC){ { TWOPOW16F, TWOPOW16F, TWOPOW16F } },
        (Vertex3PC){ { TWOPOW16F, TWOPOW16F, TWOPOW16F } },
        (Vertex3PC){ { TWOPOW16F, 0.0f, TWOPOW16F } },

        (Vertex3PC){ { TWOPOW16F, TWOPOW16F, 0.0f } },
        (Vertex3PC){ { 0.0f, TWOPOW16F, 0.0f } },
        (Vertex3PC){ { 0.0f, TWOPOW16F, 0.0f } },
        (Vertex3PC){ { 0.0f, TWOPOW16F, TWOPOW16F } },
        (Vertex3PC){ { 0.0f, TWOPOW16F, TWOPOW16F } },
        (Vertex3PC){ { TWOPOW16F, TWOPOW16F, TWOPOW16F } },

        (Vertex3PC){ { 0.0f, TWOPOW16F, 0.0f } },
        (Vertex3PC){ { 0.0f, 0.0f, 0.0f } },
        (Vertex3PC){ { 0.0f, 0.0f, 0.0f } },
        (Vertex3PC){ { 0.0f, 0.0f, TWOPOW16F } },
        (Vertex3PC){ { 0.0f, 0.0f, TWOPOW16F } },
        (Vertex3PC){ { 0.0f, TWOPOW16F, TWOPOW16F } },
    };
    
    for(int i = 0; i < 24; ++i){ glm_vec4_ucopy(PAL[FG_BLACK], verts[i].col); }

    linerender_render_3d(lines, GL_LINES, verts, 24, width, stipple, camera->viewpos);
}

void linerender_horizontal_axes(LineRender* lines, Camera* camera)
{
    //Rendering style
    GLenum mode = GL_LINES;
    vec4 fg_white_00; hex_rgba_to_vec4_norm(COL_FG_WHITE << 8 | 0x00, fg_white_00);
    float width = 1.0f;
    uint16_t stipple = 0xffff;

    //Camera origin position
    vec3 campos; uint32_to_vec3(camera->pos, campos);

    //Rendered data
    Vertex3PC verts[12];
    for(int i = 0; i < 12; ++i){ glm_vec3_copy(campos, verts[i].pos); }
    
    verts[1].pos[0] = TWOPOW16F; verts[3].pos[0] = 0.0f;
    verts[5].pos[1] = TWOPOW16F; verts[7].pos[1] = 0.0f;
    verts[9].pos[2] = TWOPOW16F; verts[11].pos[2] = 0.0f;
    
    for(int i = 0; i < 12; i += 4){
        glm_vec4_ucopy(PAL[FG_WHITE], verts[i+0].col);
        glm_vec4_ucopy(fg_white_00, verts[i+1].col);
        glm_vec4_ucopy(PAL[BG_WHITE], verts[i+2].col);
        glm_vec4_ucopy(fg_white_00, verts[i+3].col);
    }

    //Offset center points
    float offset = camera->zoom_pos * 0.4f;
    verts[0].pos[0] = MIN(verts[0].pos[0] + offset, TWOPOW16F);
    verts[2].pos[0] = MAX(verts[2].pos[0] - offset, 0.0f);
    verts[4].pos[1] = MIN(verts[4].pos[1] + offset, TWOPOW16F);
    verts[6].pos[1] = MAX(verts[6].pos[1] - offset, 0.0f);
    verts[8].pos[2] = MIN(verts[8].pos[2] + offset, TWOPOW16F);
    verts[10].pos[2] = MAX(verts[10].pos[2] - offset, 0.0f);

    //Offset far points
    offset = camera->zoom_pos * 0.04f;
    verts[1].pos[0] = MAX(verts[1].pos[0] - offset, verts[0].pos[0]);
    verts[3].pos[0] = MIN(verts[3].pos[0] + offset, verts[2].pos[0]);
    verts[5].pos[1] = MAX(verts[5].pos[1] - offset, verts[4].pos[1]);
    verts[7].pos[1] = MIN(verts[7].pos[1] + offset, verts[6].pos[1]);
    verts[9].pos[2] = MAX(verts[9].pos[2] - offset, verts[8].pos[2]);
    verts[11].pos[2] = MIN(verts[11].pos[2] + offset, verts[10].pos[2]);
    
    linerender_render_3d(lines, mode, verts, 8, width, stipple, camera->viewpos);

    for(int i = 0; i < 12; ++i){ glm_vec3_copy(campos, verts[i].pos); }
    
    verts[1].pos[0] = TWOPOW16F; verts[3].pos[0] = 0.0f;
    verts[5].pos[1] = TWOPOW16F; verts[7].pos[1] = 0.0f;
    verts[9].pos[2] = TWOPOW16F; verts[11].pos[2] = 0.0f;
    
    for(int i = 0; i < 12; i += 4){
        glm_vec4_ucopy(PAL[FG_WHITE], verts[i+0].col);
        glm_vec4_ucopy(fg_white_00, verts[i+1].col);
        glm_vec4_ucopy(PAL[BG_WHITE], verts[i+2].col);
        glm_vec4_ucopy(fg_white_00, verts[i+3].col);
    }

    //Offset center points
    offset = camera->zoom_pos * 0.4f;
    verts[0].pos[0] = MIN(verts[0].pos[0] + offset, TWOPOW16F);
    verts[2].pos[0] = MAX(verts[2].pos[0] - offset, 0.0f);
    verts[4].pos[1] = MIN(verts[4].pos[1] + offset, TWOPOW16F);
    verts[6].pos[1] = MAX(verts[6].pos[1] - offset, 0.0f);
    verts[8].pos[2] = MIN(verts[8].pos[2] + offset, TWOPOW16F);
    verts[10].pos[2] = MAX(verts[10].pos[2] - offset, 0.0f);

    //Offset far points
    offset = camera->zoom_pos * 0.1f;
    verts[1].pos[0] = verts[0].pos[0] - offset;
    verts[3].pos[0] = verts[2].pos[0] + offset;
    verts[5].pos[1] = verts[4].pos[1] - offset;
    verts[7].pos[1] = verts[6].pos[1] + offset;
    verts[9].pos[2] = verts[8].pos[2] - offset;
    verts[11].pos[2] = verts[10].pos[2] + offset;
    
    linerender_render_3d(lines, mode, verts, 8, width, stipple, camera->viewpos);
}

void linerender_entity_verticals(LineRender* lines, SpriteInstance* instances, int count, Camera* camera)
{
    //Rendering style
    float width = 1.0f;

    //Camera positions (origin & third-person position)
    vec3 camorig; uint32_to_vec3(camera->pos, camorig);

    //Vector used for computing constant-screen-size offsets
    vec3 diff = { 0.0f, 0.0f, 0.0f };
    float offset = 0.0f;

    int above = 0;
    Vertex3PC verts_above[ENTITY_RENDER_MAX][2];
    int below = 0;
    Vertex3PC verts_below[ENTITY_RENDER_MAX][2];
    Vertex3PC verts_bot[ENTITY_RENDER_MAX][2];

    for(int i = 0; i < count; ++i){

        vec3 color; glm_vec3_copy(instances[i].col, color);

        //Select based on whether the entity is above or below the orbital plane
        bool higher = instances[i].pos[2] > camorig[2];
        Vertex3PC* verts_select = higher ?
            &verts_above[above][0] : &verts_below[below][0];

        //Increment relevant counter
        int* count_select = higher ? &above : &below;
        ++(*count_select);

        //Line that is offset from entity position
        glm_vec3_sub(instances[i].pos, camera->viewpos, diff);
        offset = glm_vec3_norm(diff) * 0.025f;
        float inst_z = instances[i].pos[2];
        verts_select[0] = (Vertex3PC){
            {
                instances[i].pos[0], instances[i].pos[1],
                fclamp(
                    inst_z - ((inst_z - camorig[2] > 0) * 2 - 1) * offset,
                    inst_z - camorig[2] > 0 ? camorig[2] : inst_z,
                    inst_z - camorig[2] > 0 ? inst_z : camorig[2]
                )
            },
            { color[0], color[1], color[2], 0.5f },
        };
        verts_select[1] = (Vertex3PC){
            { instances[i].pos[0], instances[i].pos[1], camorig[2] },
            { color[0], color[1], color[2], 1.0f },
        };

        //Line that points towards the camera origin
        glm_vec3_sub(verts_select[1].pos, camera->viewpos, diff);
        offset = glm_vec3_norm(diff) * 0.025f;
        glm_vec3_sub(verts_select[1].pos, camorig, diff);
        glm_vec3_scale_as(diff, offset, diff);
        verts_bot[i][0] = (Vertex3PC){
            {
                verts_select[1].pos[0],
                verts_select[1].pos[1],
                verts_select[1].pos[2],
            },
            { color[0], color[1], color[2], 1.0f },
        };
        verts_bot[i][1] = (Vertex3PC){
            {
                verts_bot[i][0].pos[0] - diff[0],
                verts_bot[i][0].pos[1] - diff[1],
                verts_bot[i][0].pos[2] - diff[2],
            },
            { color[0], color[1], color[2], 1.0f },
        };
    }

    linerender_render_3d(lines, GL_LINES,
        &verts_above[0][0], above*2, width, 0xffff, camera->viewpos);
    linerender_render_3d(lines, GL_LINES,
        &verts_below[0][0], below*2, width, 0x03ff, camera->viewpos);
    linerender_render_3d(lines, GL_LINES,
        &verts_bot[0][0], count*2, width, 0xffff, camera->viewpos);
}

void linerender_entity_nav(LineRender* lines, EntityInstance* entities, SpriteInstance* instances, int count, ECS* ecs, Camera* camera)
{
    //Rendering style
    float width = 1.0f;

    vec4 color_ff;
    vec4 color_00;

    int rendercount = 0;
    Vertex3PC verts[ENTITY_RENDER_MAX][4];

    for(int i = 0; i < count; ++i){

        switch(entities[i].typei){
        case ENTTYPE_SHIP: {
            hex_rgba_to_vec4_norm(COL_FG_WHITE << 8 | 0xff, color_ff);
            hex_rgba_to_vec4_norm(COL_FG_WHITE << 8 | 0x00, color_00);
        } break;
        case ENTTYPE_WASP:
        case ENTTYPE_LEVIATHAN:
        {
            hex_rgba_to_vec4_norm(COL_BG_RED << 8 | 0xff, color_ff);
            hex_rgba_to_vec4_norm(COL_BG_RED << 8 | 0x00, color_00);
        } break;
        case ENTTYPE_MISSILE: {
            hex_rgba_to_vec4_norm(COL_FG_WHITE << 8 | 0xff, color_ff);
            hex_rgba_to_vec4_norm(COL_FG_WHITE << 8 | 0x00, color_00);
        } break;
        /*case ENTTYPE_MISSILE: {
            Missile* miss = (Missile*)ecs_get_id(ecs, instances[i].ent, MissileID);
            glm_vec3_copy((float*)ENTITY_COLORS[miss->sender.type], color_ff);
            color_ff[3] = 1.0f;
            glm_vec3_copy((float*)ENTITY_COLORS[miss->sender.type], color_00);
            color_00[3] = 0.0f;
        } break;*/
        }

        switch(entities[i].typei){
        case ENTTYPE_SHIP:
        case ENTTYPE_WASP:
        case ENTTYPE_LEVIATHAN:
        case ENTTYPE_MISSILE:
        {
            //Get navigation module
            if(!ecs_entity_alive(ecs, entities[i].ent)){ break; }
            const Nav* nav = ecs_get_id(ecs, entities[i].ent, NavID);
            if(!nav){ break; }

            switch(nav->motion){
            case NAV_MOTION_APPROACH:
            case NAV_MOTION_ORBIT:
            case NAV_MOTION_IMPACT: {

                vec3 target; uint32_to_vec3(nav->target, target);
                vec3 between; glm_vec3_lerp(instances[i].pos, target, 0.5f, between);

                Vertex3PC* vert = verts[rendercount++];

                glm_vec3_copy(instances[i].pos, vert[0].pos);
                glm_vec4_ucopy(color_00, vert[0].col);

                glm_vec3_copy(between, vert[1].pos);
                glm_vec4_ucopy(color_ff, vert[1].col);

                glm_vec3_copy(between, vert[2].pos);
                glm_vec4_ucopy(color_ff, vert[2].col);

                glm_vec3_copy(target, vert[3].pos);
                glm_vec4_ucopy(color_00, vert[3].col);

            } break;
            }

        } break;
        case ENTTYPE_WORMHOLE: {

            if(!ecs_entity_alive(ecs, entities[i].ent)){ break; }
            const Wormhole* worm = ecs_get_id(ecs, entities[i].ent, WormholeID);
            if(!worm){ break; }

            vec4 bg_magenta_ff;
            hex_rgba_to_vec4_norm(COL_BG_MAGENTA << 8 | 0xff, bg_magenta_ff);
            vec4 bg_magenta_00;
            hex_rgba_to_vec4_norm(COL_BG_MAGENTA << 8 | 0x80, bg_magenta_00);

            vec3 target; uint32_to_vec3(worm->target, target);
            vec3 between; glm_vec3_lerp(instances[i].pos, target, 0.5f, between);

            Vertex3PC* vert = verts[rendercount++];

            glm_vec3_copy(instances[i].pos, vert[0].pos);
            glm_vec4_ucopy(bg_magenta_00, vert[0].col);

            glm_vec3_copy(between, vert[1].pos);
            glm_vec4_ucopy(bg_magenta_ff, vert[1].col);

            glm_vec3_copy(between, vert[2].pos);
            glm_vec4_ucopy(bg_magenta_ff, vert[2].col);

            glm_vec3_copy(target, vert[3].pos);
            glm_vec4_ucopy(bg_magenta_00, vert[3].col);

        } break;
        };
    }

    linerender_render_3d(lines, GL_LINES,
        &verts[0][0], rendercount * 4, width, 0xffff, camera->viewpos);
}

int spritesrender_entities(SpritesRender* sprites, EntityInstance* entities, SpriteInstance* instances, SpriteInstance* instances_xform, ECS* ecs, Camera* camera)
{
    int count = 0;

    //Fade color
    vec4 col_fade; hex_rgba_to_vec4_norm(COL_BG_WHITE << 8 | 0xff, col_fade);

    //Camera positions (origin & third-person position)
    vec3 camorig; uint32_to_vec3(camera->pos, camorig);

    //Run query on memory components
    ArchID arch = archid_setbit((ArchID){0}, TypeID);
    arch = archid_setbit(arch, PositionID);
    arch = archid_setbit(arch, ChronoID);
    Query q; query_alloc(&q); query_init(&q, ecs, arch);
    while(query_next(&q) && count < sprites->instances){
        Entity* ents = query_ents(ecs, &q);
        Type* types = query_term(ecs, &q, TypeID);
        Position* pos = query_term(ecs, &q, PositionID);
        Chrono* chronos = query_term(ecs, &q, ChronoID);

        for(int i = 0; i < q.size; ++i){

            //Get color
            vec4 col;
            for(int j = 0; j < 4; ++j){
                col[j] = chronos[i].fading * col_fade[j] +
                    (!chronos[i].fading) * ENTITY_COLORS[types[i].type][j];
            }

			//Compose entity instance
			entities[count] = (EntityInstance){
				types[i].type, ents[i], chronos[i].fading,
			};

            //Compose drawn instance       
            vec3 wpos; uint32_to_vec3(pos[i].pos, wpos);
            instances[count] = (SpriteInstance){
                { wpos[0], wpos[1], wpos[2] },
                types[i].type / (float)ENTTYPE_COUNT + 0.5f / (float)ENTTYPE_COUNT,
                { col[0], col[1], col[2], col[3] },
                { 0, 0 },
                { 1, 1 },
            };

            //If the entity limit was reached, skip the rest of the query
            if(++count >= sprites->instances){ break; }
        }
    }

    query_dealloc(&q);

    //Copy the instance calls into an array that rendering can freely mutate
    memcpy(instances_xform, instances, sizeof(SpriteInstance) * count);

    spritesrender_render_3d(sprites, instances_xform, count, camera->viewpos);

    //Return number of instances drawn
    return count;
}

void spritesrender_entity_modules(SpritesRender* sprites, SpriteInstance* instances, int count, int spritew, int spriteh, ECS* ecs, Camera* camera)
{
    SpriteInstance sprite_inst[ENTITY_RENDER_MAX][1];

    //Camera positions (origin & third-person position)
    vec3 camorig; uint32_to_vec3(camera->pos, camorig);

    //Hitpoint bar dimensions
    int hp_dim[2] = { 32, 9 };

    //Starting offset
    int off[2] = { 0, hp_dim[1] / 2 + ((spriteh / 2) + 1) + 2 };

    vec4 entcol;

    int total;

    ArchID arch;
    Query q; query_alloc(&q);

    arch = archid_setbit((ArchID){0}, TypeID);
    arch = archid_setbit(arch, PositionID);
    arch = archid_setbit(arch, HullID);

    total = 0;
    query_init(&q, ecs, arch);
    while(query_next(&q)){
        Type* type = query_term(ecs, &q, TypeID);
        Position* pos = query_term(ecs, &q, PositionID);
        //Hull* hull = query_term(ecs, &q, HullID);
        for(int i = 0; i < q.size; ++i){

            glm_vec4_ucopy((float*)ENTITY_COLORS[type[i].type], entcol);
            sprite_inst[total++][0] = (SpriteInstance){
                { pos[i].pos[0] / TWOPOW16F,
                  pos[i].pos[1] / TWOPOW16F,
                  pos[i].pos[2] / TWOPOW16F },
                0.0f,
                { entcol[0], entcol[1], entcol[2], entcol[3] },
                { off[0], off[1] },
                { hp_dim[0], hp_dim[1] },
            };
        }
    }

    glDisable(GL_DEPTH_TEST);
    spritesrender_render_3d(sprites, &sprite_inst[0][0], total, camera->viewpos);
    glEnable(GL_DEPTH_TEST);

    total = 0;
    query_init(&q, ecs, arch);
    while(query_next(&q)){
        //Type* type = query_term(ecs, &q, TypeID);
        Position* pos = query_term(ecs, &q, PositionID);
        //Hull* hull = query_term(ecs, &q, HullID);
        for(int i = 0; i < q.size; ++i){

            sprite_inst[total++][0] = (SpriteInstance){
                { pos[i].pos[0] / TWOPOW16F,
                  pos[i].pos[1] / TWOPOW16F,
                  pos[i].pos[2] / TWOPOW16F },
                0.0f,
                { PAL[BG_BLACK][0], PAL[BG_BLACK][1],
				  PAL[BG_BLACK][2], PAL[BG_BLACK][3] },
                { off[0], off[1] },
                { hp_dim[0] - 2, hp_dim[1] - 2 },
            };
        }
    }

    glDisable(GL_DEPTH_TEST);
    spritesrender_render_3d(sprites, &sprite_inst[0][0], total, camera->viewpos);
    glEnable(GL_DEPTH_TEST);

    total = 0;
    query_init(&q, ecs, arch);
    while(query_next(&q)){
        Type* type = query_term(ecs, &q, TypeID);
        Position* pos = query_term(ecs, &q, PositionID);
        Hull* hull = query_term(ecs, &q, HullID);
        for(int i = 0; i < q.size; ++i){

            float fullness = hull[i].hp / (float)hull[i].hp_max;
            int pixels = hp_dim[0] - 6;
            int filled = pixels * fullness;
            if(filled % 2){ filled = filled - 1; }

            //At nonzero hp, keep at least 2 pixels
            if(hull[i].hp > 0){ filled = MAX(filled, 2); }

            glm_vec4_ucopy((float*)ENTITY_COLORS[type[i].type], entcol);
            sprite_inst[total++][0] = (SpriteInstance){
                { pos[i].pos[0] / TWOPOW16F,
                  pos[i].pos[1] / TWOPOW16F,
                  pos[i].pos[2] / TWOPOW16F },
                0.0f,
                { entcol[0], entcol[1], entcol[2], entcol[3] },
                { off[0] - (pixels - filled) / 2, off[1] },
                { filled, hp_dim[1] - 6 },
            };
        }
    }

    glDisable(GL_DEPTH_TEST);
    spritesrender_render_3d(sprites, &sprite_inst[0][0], total, camera->viewpos);
    glEnable(GL_DEPTH_TEST);

    query_dealloc(&q);
}

void spritesrender_resolved_entity(SpritesRender* sprites, SpritesRender* sprites_bg, int* windowsizes, ECS* ecs, Entity ent, mat4 view, mat4 proj, Camera* camera)
{
    if(!ecs_entity_alive(ecs, ent)){ return; }

    //Attempt to get position, skip if unavailable
    const Position* pos = ecs_get_id(ecs, ent, PositionID);
    if(!pos){ return; }
    vec3 wpos; uint32_to_vec3(pos->pos, wpos);

    //Position offset by the camera position
    vec3 wposrel; glm_vec3_sub(wpos, camera->viewpos, wposrel);

    //Rendering style
    vec4 bg_black_00;
    hex_rgba_to_vec4_norm(COL_BG_BLACK << 8 | 0x00, bg_black_00);
    vec4 palette[4];

    //Render troph simulation
    const Trophs* trophs = ecs_get_id(ecs, ent, TrophsID);
    if(trophs){
        
        glm_vec4_ucopy(bg_black_00, palette[0]);
        glm_vec4_ucopy(PAL[BG_YELLOW], palette[1]);
        glm_vec4_ucopy(PAL[BG_MAGENTA], palette[2]);

        //Sprite instance
        SpriteInstance inst = (SpriteInstance){
            { wpos[0], wpos[1], wpos[2] },
            0.0f,
            { 1.0f, 1.0f, 1.0f, 0.9f },
            { 0, (trophs->width / 2 + 8) * 4 },
            { 4, -4 },
        };

        //Background instance
        SpriteInstance bg_inst = inst;
        bg_inst.scale[0] = bg_inst.scale[1] = trophs->width * 4 + 32;
        memcpy(bg_inst.col, PAL[BG_BLACK], sizeof(float[3]));

        //Convert trophs cells into image data
        uint8_t img[TROPHS_CELLS_MAX * 4];
        trophs_convert_rgba(trophs, img, palette);

        //Load bytes into sprite renderer
        spritesrender_load_bytes(sprites, img,
            &((int[2]){ trophs->width, trophs->width }));

        //Setup & call renderer
        spritesrender_setup(sprites_bg, view, proj, windowsizes);
        spritesrender_render_3d(sprites_bg, &bg_inst, 1, camera->viewpos);
        spritesrender_setup(sprites, view, proj, windowsizes);
        spritesrender_render_3d(sprites, &inst, 1, camera->viewpos);
    }

    //Render maze
    const Maze* maze = ecs_get_id(ecs, ent, MazeID);
    if(maze){
        
        glm_vec4_ucopy(bg_black_00, palette[0]);
        glm_vec4_ucopy(PAL[BG_MAGENTA], palette[1]);

        //Sprite instance
        SpriteInstance inst = (SpriteInstance){
            { wpos[0], wpos[1], wpos[2] },
            0.0f,
            { 1.0f, 1.0f, 1.0f, 0.9f },
            { 0, (maze->width / 2 + 8) * 4 },
            { 4, -4 },
        };

        //Background instance
        SpriteInstance bg_inst = inst;
        bg_inst.scale[0] = bg_inst.scale[1] = maze->width * 4 + 32;
        memcpy(bg_inst.col, PAL[BG_BLACK], sizeof(float[3]));

        //Convert maze cells into image data
        uint8_t img[MAZE_CELLS_MAX * 4];
        convert_rgba_2bitsperbyte(img,
            maze->cells, maze->width * maze->width, palette);

        //Load bytes into sprite renderer
        spritesrender_load_bytes(sprites, img,
            &((int[2]){ maze->width, maze->width }));

        //Setup & call renderer
        spritesrender_setup(sprites_bg, view, proj, windowsizes);
        spritesrender_render_3d(sprites_bg, &bg_inst, 1, camera->viewpos);
        spritesrender_setup(sprites, view, proj, windowsizes);
        spritesrender_render_3d(sprites, &inst, 1, camera->viewpos);
    }
}

void boardrender_world_cardinals(BoardRender* boards, Camera* camera)
{
    //Camera positions (origin & third-person position)
    vec3 camorig; uint32_to_vec3(camera->pos, camorig);

    //Instances
    char cardinals[6] = { 'E', 'W', 'N', 'S', 'Z', 'N' };
    BoardInstance board_inst[6];
    for(int i = 0; i < 6; ++i){
        board_inst[i] = (BoardInstance){
            { camorig[0], camorig[1], camorig[2] },
            cardinals[i],
            { PAL[FG_WHITE][0], PAL[FG_WHITE][1], PAL[FG_WHITE][2] },
            { 0, 0 }
        };
    }

    board_inst[0].pos[0] = TWOPOW16F;
    board_inst[1].pos[0] = 0.0f;
    board_inst[2].pos[1] = TWOPOW16F;
    board_inst[3].pos[1] = 0.0f;
    board_inst[4].pos[2] = TWOPOW16F;
    board_inst[5].pos[2] = 0.0f;

    boardrender_render_3d(boards, board_inst, 6, camera->viewpos);
}

void boardrender_entity_id(BoardRender* boards, EntityInstance* entities, SpriteInstance* instances, int count, int spritew, int spriteh, Camera* camera)
{
    //Fade color
    vec4 col_fade; hex_rgba_to_vec4_norm(COL_BG_WHITE << 8 | 0xff, col_fade);

    //Board instances
    BoardInstance board_inst[ENTITY_RENDER_MAX][PROQUINT_CHARS];

    //Starting offset
    int off[2] = { -1 * boards->bbox[0] * 4 - boards->bbox[0] / 2 - spritew / 2 - 2, 0 };

    for(int i = 0; i < count; ++i){

        //Get color
        vec3 col;
        for(int j = 0; j < 3; ++j){
            col[j] = entities[i].fading * col_fade[j] +
                (!entities[i].fading) * PAL[FG_WHITE][j];
        }

        //Convert entity ID to text
        char id[PROQUINT_CHARS]; id[0] = ' ';
        if(camera->proquint){ short2quint(entities[i].ent, id); }
        else                { short2quart(entities[i].ent, id + 1); }

        for(int j = 0; j < PROQUINT_CHARS; ++j){
            board_inst[i][j] = (BoardInstance){
                { instances[i].pos[0], instances[i].pos[1], instances[i].pos[2] },
                id[j],
                { col[0], col[1], col[2] },
                { off[0] + boards->bbox[0] * j, off[1] },
            };
        }
    }

    boardrender_render_3d(boards, &board_inst[0][0], count * PROQUINT_CHARS, camera->viewpos);
}

void boardrender_entity_distance(BoardRender* boards, SpriteInstance* instances, int count, int spritew, int spriteh, Camera* camera)
{
    //Board instances
    BoardInstance board_inst[ENTITY_RENDER_MAX][4];

    //Camera positions (origin & third-person position)
    vec3 camorig; uint32_to_vec3(camera->pos, camorig);

    //Starting offset
    int off[2] = { boards->bbox[0] / 2 + spritew / 2 + 2, 0 };

    int dig = 0;
    float distf;
    int nybble;
    for(int i = 0; i < count; ++i){

        //Calculate distance to camera origin
        distf = glm_vec3_distance(camorig, instances[i].pos); 
        distf = MIN(distf, 65535.5f);

        //Convert distance to 4 hex characters
        for(int j = 0; j < 4; ++j){
            float* color = j < 2 ? PAL[FG_WHITE] : PAL[BG_WHITE];
            nybble = (3 - j) * 4;
            dig = ((((int16_t)distf) & (0xf << nybble)) >> (nybble));
            board_inst[i][j] = (BoardInstance){
                { instances[i].pos[0], instances[i].pos[1], instances[i].pos[2] },
                dig + (dig > 9 ? 87 : '0'),
                { color[0], color[1], color[2] },
                { off[0] + boards->bbox[0] * j, off[1] },
            };
        }
    }

    boardrender_render_3d(boards, &board_inst[0][0], count * 4, camera->viewpos);
}

void boardrender_resolved_entity(BoardRender* boards, ECS* ecs, Entity ent, int spritew, int spriteh, Camera* camera)
{
    if(!ecs_entity_alive(ecs, ent)){ return; }

    //Attempt to get position, skip if unavailable
    const Position* pos = ecs_get_id(ecs, ent, PositionID);
    if(!pos){ return; }
    vec3 wpos; uint32_to_vec3(pos->pos, wpos);

    //Board instances
    BoardInstance board_inst[2];

    board_inst[0] = (BoardInstance){
        { wpos[0], wpos[1], wpos[2] },
        '<',
        { PAL[FG_WHITE][0], PAL[FG_WHITE][1], PAL[FG_WHITE][2] },
        { (camera->proquint ? -5 : -4) * boards->bbox[0] -
          boards->bbox[0] / 2 - spritew / 2 - 2, 0 }
    };
    board_inst[1] = (BoardInstance){
        { wpos[0], wpos[1], wpos[2] },
        '>',
        { PAL[FG_WHITE][0], PAL[FG_WHITE][1], PAL[FG_WHITE][2] },
        { 4 * boards->bbox[0] +
          boards->bbox[0] / 2 + spritew / 2 + 2, 0 }
    };

    boardrender_render_3d(boards, &board_inst[0], 2, camera->viewpos);
}

void boardrender_entity_pilotmarker(BoardRender* boards, ECS* ecs, int spritew, int spriteh, Camera* camera, const char* uname)
{
    int count = 0;

	//If the username is null, no piloted markers need to be rendered
	if(!uname){ return; }

    //Fade color
    vec4 col_fade; hex_rgba_to_vec4_norm(COL_BG_WHITE << 8 | 0xff, col_fade);

    //Board instances
    BoardInstance board_inst[ENTITY_RENDER_MAX];

    //Camera positions (origin & third-person position)
    vec3 camorig; uint32_to_vec3(camera->pos, camorig);

    //Run query on memory components
    ArchID arch = archid_setbit((ArchID){0}, TypeID);
    arch = archid_setbit(arch, PositionID);
    arch = archid_setbit(arch, ChronoID);
    arch = archid_setbit(arch, PilotID);
    Query q; query_alloc(&q); query_init(&q, ecs, arch);
    while(query_next(&q) && count < boards->instances){
        Type* types = query_term(ecs, &q, TypeID);
        Position* pos = query_term(ecs, &q, PositionID);
        Chrono* chronos = query_term(ecs, &q, ChronoID);
        Pilot* pilot = query_term(ecs, &q, PilotID);

        for(int i = 0; i < q.size; ++i){

			if(strcmp(uname, pilot[i].uname) != 0){ continue; }

            //Get color
            vec3 col;
            for(int j = 0; j < 3; ++j){
                col[j] = chronos[i].fading * col_fade[j] +
                    (!chronos[i].fading) * ENTITY_COLORS[types[i].type][j];
            }

			//Convert entity ID to text
			vec3 wpos; uint32_to_vec3(pos[i].pos, wpos);
			board_inst[count] = (BoardInstance){
				{ wpos[0], wpos[1], wpos[2] },
				FONT_PILOTMARKER_CHAR,
				{ col[0], col[1], col[2] },
				{ 0, -spriteh },
			};

            //If the entity limit was reached, skip the rest of the query
            if(++count >= boards->instances){ break; }
        }
    }

    query_dealloc(&q);

    boardrender_render_3d(boards, &board_inst[0], count, camera->viewpos);
}

void panelrender_age(PanelRender* panels, BoardRender* boards, struct timespec age, int* windowsizes)
{
    //Start typesetting
    panelrender_typeset_reset(panels, 18, GLM_VEC3_ZERO, panels->chardim[0]);

    //Convert age into text
    panelrender_typeset_append(panels, PAL[BG_BLUE], "EPOCH ");
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%08lx\n", age.tv_sec);
    
    //Render panel
    panelrender_render_2d(panels, boards,
        windowsizes, GLM_MAT4_IDENTITY, GLM_MAT4_IDENTITY,
        (int[2]){
            0,
            windowsizes[1] / 2 - panels->chardim[1] * 2 +
            ((panels->row - 1) * panels->chardim[1]) / 2
        }
    );
}

void panelrender_camera(PanelRender* panels, BoardRender* boards, Camera* camera, int* windowsizes)
{
	const char* syncmodestr[SyncModeCount] = {
		[SyncModeCamera] = "CAM",
		[SyncModeWeighted] = "WEI",
	};

    //Start typesetting
    panelrender_typeset_reset(panels, 60, GLM_VEC3_ZERO, panels->chardim[0]);

    panelrender_typeset_append(panels, PAL[BG_BLUE], "POS ");
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%04hx %04hx %04hx",
        camera->pos[0] >> 16,
        camera->pos[1] >> 16,
        camera->pos[2] >> 16);
    panelrender_typeset_append(panels, PAL[FG_WHITE], " | ");
    panelrender_typeset_append(panels, PAL[BG_BLUE], "OCT ");
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%c%c%c",
        camera->pos[1] >= 0x80000000 ? 'N' : 'S',
        camera->pos[0] >= 0x80000000 ? 'E' : 'W',
        camera->pos[2] >= 0x80000000 ? 'Z' : 'N');
    panelrender_typeset_append(panels, PAL[FG_WHITE], " | ");
    panelrender_typeset_append(panels, PAL[BG_BLUE], "RAD ");
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%04hx",
        (uint16_t)glm_clamp(camera->zoom_pos, 0.0f, TWOPOW16F - 1.0f));
    panelrender_typeset_append(panels, PAL[FG_WHITE], " | ");
    panelrender_typeset_append(panels, PAL[BG_BLUE], "SYN ");
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%s\n",
		syncmodestr[camera->syncmode]);
    
    //Render panel
    panelrender_render_2d(panels, boards,
        windowsizes, GLM_MAT4_IDENTITY, GLM_MAT4_IDENTITY,
        (int[2]){
            0,
            0 - windowsizes[1] / 2 + panels->chardim[1] * 2 +
            ((panels->row - 1) * panels->chardim[1]) / 2
        }
    );
}

void panelrender_pilot(PanelRender* panels, const Pilot* pilot)
{
    panelrender_typeset_append(panels, PAL[BG_BLUE], "PILOT ");
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%s",
        pilot->uname);
}

void panelrender_nav_range(PanelRender* panels, const Nav* nav)
{
    panelrender_typeset_append(panels, PAL[BG_BLUE], "RANGE ");
    panelrender_typeset_append(panels, PAL[BG_WHITE], "0x");
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%04hx\n",
        nav_range(nav));
}

void panelrender_pilot_and_nav_range(PanelRender* panels, const Pilot* pilot, const Nav* nav)
{
	panelrender_pilot(panels, pilot);
    panelrender_typeset_append(panels, PAL[FG_WHITE], " ");
	panelrender_nav_range(panels, nav);
}

void panelrender_uxn(PanelRender* panels, const Uxn* uxn)
{
    panelrender_typeset_append(panels, PAL[BG_YELLOW], "UXN ");

    panelrender_typeset_append(panels, PAL[BG_BLUE], "HALT");
    panelrender_typeset_append(panels, PAL[FG_WHITE], " %c ",
        uxn->halt ? '1' : '0');

    panelrender_typeset_append(panels, PAL[BG_BLUE], "BREAK");
    panelrender_typeset_append(panels, PAL[FG_WHITE], " %c\n",
        uxn->brk ? '1' : '0');

    //TODO
    panelrender_typeset_append(panels, PAL[BG_BLUE], "ERR");
    panelrender_typeset_append(panels, PAL[FG_WHITE], " ??? ");
    /*panelrender_typeset_append(panels, PAL[FG_WHITE], " %s\n",
        UXN_ERR_STR[uxn->err]);*/

    panelrender_typeset_append(panels, PAL[BG_BLUE], "WST ");
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%02hhx ",
        uxn->wstptr);

    panelrender_typeset_append(panels, PAL[BG_BLUE], "RST ");
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%02hhx\n",
        uxn->rstptr);

    panelrender_typeset_append(panels, PAL[BG_BLUE], "PC 0x");
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%04hx ",
        uxn->pc);

    panelrender_typeset_append(panels, PAL[BG_BLUE], "INS ");
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%4d / %4d\n",
        uxn->instr, uxn->inspt);
}

void panelrender_position(PanelRender* panels, const Position* pos)
{
    panelrender_typeset_append(panels, PAL[BG_BLUE], "POS ");
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%04hx %04hx %04hx\n",
        pos->pos[0] >> 16, pos->pos[1] >> 16, pos->pos[2] >> 16);
}

void panelrender_pointphys(PanelRender* panels, const Pointphys* pphys)
{
    panelrender_typeset_append(panels, PAL[BG_BLUE], "VEL ");
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%.2f\n",
        v3int32_norm(pphys->vel) / TWOPOW16F);
}

void panelrender_star(PanelRender* panels, const Star* star)
{
    panelrender_typeset_append(panels, PAL[BG_BLUE], "MASS ");
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%d\n",
	star->mass);
}

void panelrender_trophs(PanelRender* panels, const Trophs* trophs)
{
    panelrender_typeset_append(panels, PAL[BG_YELLOW], "TROPHS\n");
    panelrender_typeset_append(panels, PAL[BG_BLUE], "CAPACITY ");
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%d x %d\n",
        trophs->width, trophs->width);
    panelrender_typeset_append(panels, PAL[BG_YELLOW], "AUT ");
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%+2.2f / %+2.2f %4d (%+4d)\n",
        trophs->params[TROPH_AUTO_REPRO], trophs->params[TROPH_AUTO_DEATH],
        trophs->cellcount[TROPH_AUTO],
        trophs->cellcount[TROPH_AUTO] - trophs->threshold[TROPH_AUTO]);
    panelrender_typeset_append(panels, PAL[BG_MAGENTA], "HET ");
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%+2.2f / %+2.2f %4d (%+4d)\n",
        trophs->params[TROPH_HETE_REPRO], trophs->params[TROPH_HETE_DEATH],
        trophs->cellcount[TROPH_HETE],
        trophs->cellcount[TROPH_HETE] - trophs->threshold[TROPH_HETE]);

    panelrender_typeset_append(panels, PAL[BG_BLUE], "WEATHER ");
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%.1f / %.1f\n",
        trophs->weather_pass, trophs->weather_full);

    panelrender_typeset_append(panels, PAL[BG_YELLOW], "AUT ");
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%+2.2f / %+2.2f\n",
        TROPH_WEATH_PARAM[trophs->weather][TROPH_AUTO_REPRO] * trophs->weather_mul,
        TROPH_WEATH_PARAM[trophs->weather][TROPH_AUTO_DEATH] * trophs->weather_mul);
    panelrender_typeset_append(panels, PAL[BG_MAGENTA], "HET ");
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%+2.2f / %+2.2f\n",
        TROPH_WEATH_PARAM[trophs->weather][TROPH_HETE_REPRO] * trophs->weather_mul,
        TROPH_WEATH_PARAM[trophs->weather][TROPH_HETE_DEATH] * trophs->weather_mul);
}

void panelrender_habitable(PanelRender* panels, const Habitable* habit)
{
    panelrender_typeset_append(panels, PAL[BG_BLUE], "ACCUMULATOR ");
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%5hu / %5hu\n",
        habit->accumulator, HABITABLE_ACC_LIMIT);
    panelrender_typeset_append(panels, PAL[BG_BLUE], "ORGANIC RAW ");
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%5hu / %5hu\n",
        habit->output, HABITABLE_OUT_LIMIT);
}

void panelrender_monolith(PanelRender* panels, const Monolith* monol)
{
    panelrender_typeset_append(panels, PAL[BG_BLUE], "ACTUAL ");
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%u / %u\n",
        monol->actual, monol->length);
    panelrender_typeset_append(panels, PAL[BG_BLUE], "ACCUMULATOR ");
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%4u / %4u\n",
        monol->accumulator, monol->threshold);
}

void panelrender_wormhole(PanelRender* panels, const Wormhole* worm)
{
    panelrender_typeset_append(panels, PAL[BG_BLUE], "RAD ");
        panelrender_typeset_append(panels, PAL[FG_WHITE], "%04hx\n",
            worm->radius >> 16);
        panelrender_typeset_append(panels, PAL[BG_BLUE], "TAR ");
        panelrender_typeset_append(panels, PAL[FG_WHITE], "%04hx %04hx %04hx\n",
            worm->target[0] >> 16,
            worm->target[1] >> 16,
            worm->target[2] >> 16);
}

void panelrender_barren(PanelRender* panels, const Barren* barr)
{
    panelrender_typeset_append(panels, PAL[BG_BLUE], "RICHNESS ");
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%hu\n",
        barr->richness);
}

void panelrender_hive(PanelRender* panels, const Hive* hive)
{
    panelrender_typeset_append(panels, PAL[BG_BLUE], "GUARDED ");
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%hu\n",
        hive->guarded);

    for(int i = 0; i < hive->guarded; ++i){
        panelrender_typeset_append(panels, PAL[FG_WHITE], "%04hx %3hu",
            hive->barrens[i], hive->delays[i]);
        panelrender_typeset_append(panels, PAL[BG_BLUE], "s");

        if(i % 2 == 0){
            panelrender_typeset_append(panels, PAL[FG_WHITE], " | ");
        }
        if(i % 2 == 1){
            panelrender_typeset_append(panels, PAL[FG_WHITE], "\n");
        }
    }
}

void panelrender_wasp(PanelRender* panels, const Wasp* wasp)
{
    panelrender_typeset_append(panels, PAL[BG_BLUE], "STT ");

    switch(wasp->state){
    case WASP_STT_DEATH: {
        panelrender_typeset_append(panels, PAL[FG_WHITE], "DEATH ");
    } break;
    case WASP_STT_JOURNEY: {
        panelrender_typeset_append(panels, PAL[FG_WHITE], "JOURNEY ");
    } break;
    case WASP_STT_GUARD_BARREN: {
        panelrender_typeset_append(panels, PAL[FG_WHITE], "GUARD_BARREN ");
        panelrender_typeset_append(panels, PAL[FG_WHITE], "%.2f\n",
            wasp->timer);
    } break;
    case WASP_STT_RETURN: {
        panelrender_typeset_append(panels, PAL[FG_WHITE], "RETURN ");
    } break;
    }
    panelrender_typeset_append(panels, PAL[FG_WHITE], "\n");
}

void panelrender_leviathan(PanelRender* panels, const Leviathan* levia, Camera* camera)
{
    panelrender_typeset_append(panels, PAL[BG_BLUE], "STT ");

    switch(levia->state){
    case LEVIA_STT_DORMANT: {
        panelrender_typeset_append(panels, PAL[FG_WHITE], "DORMANT ");
        panelrender_typeset_append(panels, PAL[FG_WHITE], "%.2f\n",
            levia->timer);
    } break;
    case LEVIA_STT_WANDER: {
        panelrender_typeset_append(panels, PAL[FG_WHITE], "WANDER ");
    } break;
    case LEVIA_STT_ESCAPE: {
        panelrender_typeset_append(panels, PAL[FG_WHITE], "ESCAPE ");
    } break;
    case LEVIA_STT_AGGRO: {
        panelrender_typeset_append(panels, PAL[FG_WHITE], "AGGRO ");
        char id[PROQUINT_CHARS + 1] = {0};
        if(camera->proquint){ short2quint(levia->target, id); }
        else                { short2quart(levia->target, id); }
        panelrender_typeset_append(panels, PAL[FG_WHITE], "%s ", id);
    } break;
    }
    panelrender_typeset_append(panels, PAL[FG_WHITE], "\n");
}

void panelrender_missile(PanelRender* panels, const Missile* miss, Camera* camera)
{
    panelrender_typeset_append(panels, PAL[BG_BLUE], "STT ");

    switch(miss->state){
    case MISS_STT_DEATH: {

    } break;
    case MISS_STT_FLY: {
        panelrender_typeset_append(panels, PAL[FG_WHITE], "FLY ");
        char id[PROQUINT_CHARS + 1] = {0};
        if(camera->proquint){ short2quint(miss->target, id); }
        else                { short2quart(miss->target, id); }
        panelrender_typeset_append(panels, PAL[FG_WHITE], "%s ", id);
    } break;
    }
    panelrender_typeset_append(panels, PAL[FG_WHITE], "\n");
}

void panelrender_wreckage(PanelRender* panels, const Wreckage* wreck)
{
    panelrender_typeset_append(panels, PAL[BG_BLUE], "DECAY ");
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%.1f\n", wreck->timer);
}

void panelrender_ruin(PanelRender* panels, const Ruin* ruin)
{
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%s",
        RUIN_DESC[ruin->description]);
}

void panelrender_nav(PanelRender* panels, const Nav* nav)
{
    panelrender_typeset_append(panels, PAL[BG_BLUE], "NAV ");
    switch(nav->motion){
    case NAV_MOTION_NONE: {
        panelrender_typeset_append(panels, PAL[BG_BLUE], "NONE ");
    } break;
    case NAV_MOTION_APPROACH: {
        panelrender_typeset_append(panels, PAL[BG_BLUE], "ABS ");
    } break;
    case NAV_MOTION_RELATIVE: {
        panelrender_typeset_append(panels, PAL[BG_BLUE], "REL ");
    } break;
    case NAV_MOTION_ORBIT: {
        panelrender_typeset_append(panels, PAL[BG_BLUE], "ORB ");
    } break;
    case NAV_MOTION_IMPACT: {
        panelrender_typeset_append(panels, PAL[BG_BLUE], "IMP ");
    } break;
    default: {
        panelrender_typeset_append(panels, PAL[BG_BLUE], "N/A ");
    } break;
    }
    switch(nav->motion){
    case NAV_MOTION_APPROACH:
    case NAV_MOTION_ORBIT: {
        panelrender_typeset_append(panels, PAL[FG_WHITE],
            "%04hx %04hx %04hx %04hx\n",
                nav->target[0] >> 16,
                nav->target[1] >> 16,
                nav->target[2] >> 16,
                nav->distance  >> 16);
    } break;
    case NAV_MOTION_RELATIVE: {
        panelrender_typeset_append(panels, PAL[FG_WHITE],
            "%04hx %04hx %04hx\n",
                nav->target[0] >> 16,
                nav->target[1] >> 16,
                nav->target[2] >> 16);
    } break;
    case NAV_MOTION_IMPACT: {
        panelrender_typeset_append(panels, PAL[FG_WHITE],
            "%04hx %04hx %04hx\n",
                nav->target[0] >> 16,
                nav->target[1] >> 16,
                nav->target[2] >> 16);
    } break;
    default: {
        panelrender_typeset_append(panels, PAL[FG_WHITE], "\n");
    } break;
    }

    panelrender_typeset_append(panels, PAL[BG_YELLOW], "  SIG ");
    for(int i = 0; i < SIGNAL_BYTES; ++i){
		if(i != 0 && i % 8 == 0){
			panelrender_typeset_append(panels, PAL[BG_YELLOW], "      ");
		}
        panelrender_typeset_append(panels, PAL[FG_WHITE], "%02hhx ",
            nav->signal[i]);
    }
    panelrender_typeset_append(panels, PAL[FG_WHITE], "\n");
}

void panelrender_drive(PanelRender* panels, const Drive* drive)
{
    panelrender_typeset_append(panels, PAL[BG_BLUE], "DRV ");
    panelrender_typeset_append(panels, PAL[BG_YELLOW], "THR ");
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%02hhx ",
        drive->throttle);
    panelrender_typeset_append(panels, PAL[BG_YELLOW], "TWR ");
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%4.2f\n",
        drive->twr);
}

void panelrender_vat(PanelRender* panels, const Vat* vat)
{
    panelrender_typeset_append(panels, PAL[BG_BLUE], "VAT ");
    panelrender_typeset_append(panels, PAL[BG_YELLOW], "AUT ");
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%d / %d ",
        vat->cells[TROPH_AUTO], vat->cells_max[TROPH_AUTO]);
    panelrender_typeset_append(panels, PAL[BG_MAGENTA], "HET ");
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%d / %d\n",
        vat->cells[TROPH_HETE], vat->cells_max[TROPH_HETE]);
}

void panelrender_extract(PanelRender* panels, const Extract* ext)
{
    panelrender_typeset_append(panels, PAL[BG_BLUE], "EXT ");

    panelrender_typeset_append(panels, PAL[BG_YELLOW], "STT ");
    switch(ext->state){
    case EXTRACT_STT_IDLE: {
        panelrender_typeset_append(panels, PAL[FG_WHITE], "IDLE ");
    } break;
    case EXTRACT_STT_COOL: {
        panelrender_typeset_append(panels, PAL[FG_WHITE], "COOLDOWN ");
        panelrender_typeset_append(panels, PAL[FG_WHITE], "%.1f s",
            ext->timer);
    } break;
    case EXTRACT_STT_PULL: {
        panelrender_typeset_append(panels, PAL[FG_WHITE], "PULL ");
    } break;
    default: {} break;
    }
    panelrender_typeset_append(panels, PAL[FG_WHITE], "\n");
}

void panelrender_synth(PanelRender* panels, const Synth* synth)
{
    panelrender_typeset_append(panels, PAL[BG_BLUE], "SYN ");
    panelrender_typeset_append(panels, PAL[BG_YELLOW], "ORG ");
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%d / %d ",
        synth->mats[SYNTH_MAT_ORGANIC],
        synth->mats_max[SYNTH_MAT_ORGANIC]);
    panelrender_typeset_append(panels, PAL[BG_YELLOW], "INO ");
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%d / %d\n",
        synth->mats[SYNTH_MAT_INORGANIC],
        synth->mats_max[SYNTH_MAT_INORGANIC]);

    panelrender_typeset_append(panels, PAL[BG_YELLOW], "  STT ");
    switch(synth->state){
    case SYNTH_STT_IDLE: {
        panelrender_typeset_append(panels, PAL[FG_WHITE], "IDLE ");
    } break;
    case SYNTH_STT_COOL: {
        panelrender_typeset_append(panels, PAL[FG_WHITE], "COOLDOWN ");
        panelrender_typeset_append(panels, PAL[FG_WHITE], "%.1f s",
            synth->timer);
    } break;
    case SYNTH_STT_REPAIR: {
        panelrender_typeset_append(panels, PAL[FG_WHITE], "REPAIR ");
    } break;
    case SYNTH_STT_RESTOCK: {
        panelrender_typeset_append(panels, PAL[FG_WHITE], "RESTOCK ");
    } break;
    default: {} break;
    }
    panelrender_typeset_append(panels, PAL[FG_WHITE], "\n");
}

void panelrender_compute(PanelRender* panels, const Compute* compute)
{
    panelrender_typeset_append(panels, PAL[BG_BLUE], "COM ");
    panelrender_typeset_append(panels, PAL[BG_YELLOW], "FRQ ");
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%d Hz ",
        compute->frequency);
    panelrender_typeset_append(panels, PAL[BG_YELLOW], "REC ");
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%4d\n",
        compute->received_pre);
}

void panelrender_hull(PanelRender* panels, const Hull* hull)
{
    panelrender_typeset_append(panels, PAL[BG_BLUE], "HUL ");
    panelrender_typeset_append(panels, PAL[BG_YELLOW], "HIT ");
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%d / %d\n",
        hull->hp, hull->hp_max);
}

void panelrender_bay(PanelRender* panels, const Bay* bay)
{
    panelrender_typeset_append(panels, PAL[BG_BLUE], "BAY ");
    panelrender_typeset_append(panels, PAL[BG_YELLOW], "AMMO ");
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%d / %d\n",
        bay->ammo, bay->ammo_max);
    
    panelrender_typeset_append(panels, PAL[BG_YELLOW], "  STT ");
    switch(bay->state){
    case BAY_STT_IDLE: {
        panelrender_typeset_append(panels, PAL[FG_WHITE], "IDLE ");
    } break;
    case BAY_STT_FIRE: {
        panelrender_typeset_append(panels, PAL[FG_WHITE], "FIRE ");
    } break;
    case BAY_STT_COOL: {
        panelrender_typeset_append(panels, PAL[FG_WHITE], "COOLDOWN ");
        panelrender_typeset_append(panels, PAL[FG_WHITE], "%.1f s",
            bay->timer);
    } break;
    default: {} break;
    }
    panelrender_typeset_append(panels, PAL[FG_WHITE], "\n");
}

void panelrender_growth(PanelRender* panels, const Growth* growth)
{
    panelrender_typeset_append(panels, PAL[BG_BLUE], "GRO ");
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%d / %d ",
        growth->total, growth->max);

    panelrender_typeset_append(panels, PAL[BG_BLUE], "FSC ");
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%d / %d\n",
        growth->bonus_acc, growth->bonus_max);
}

void panelrender_cargo(PanelRender* panels, const Cargo* cargo)
{
    panelrender_typeset_append(panels, PAL[BG_BLUE], "CRG ");
    
    panelrender_typeset_append(panels, PAL[BG_YELLOW], "ITEM ");
    panelrender_typeset_append(panels,
        cargo->artifact == ART_NONE ? PAL[BG_WHITE] : PAL[FG_WHITE],
        "%04hx ", cargo->artifact);

    panelrender_typeset_append(panels, PAL[FG_WHITE], "FROM ");
    panelrender_typeset_append(panels,
        cargo->originator == ART_NONE ? PAL[BG_WHITE] : PAL[FG_WHITE],
        "%04hx ", cargo->originator);
    
    panelrender_typeset_append(panels, PAL[FG_WHITE], "\n");
}

void panelrender_resolved_entity(PanelRender* panels, BoardRender* boards, int* windowsizes, ECS* ecs, Entity ent, mat4 view, mat4 proj, Camera* camera)
{
    if(!ecs_entity_alive(ecs, ent)){ return; }

    const Type* type = ecs_get_id(ecs, ent, TypeID);
    if(!type){ return; }
    const Position* pos = ecs_get_id(ecs, ent, PositionID);
    if(!pos){ return; }
    vec3 wpos; uint32_to_vec3(pos->pos, wpos);

    //Position offset by the camera position
    vec3 wposrel; glm_vec3_sub(wpos, camera->viewpos, wposrel);

    //Start typesetting
    panelrender_typeset_reset(panels, 32, wposrel, panels->chardim[0]);

    //Entity ID & icon
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%04hx ", ent);
    char pq[5]; short2quint(ent, pq);
    panelrender_typeset_append(panels, PAL[FG_WHITE], "%.*s ", 5, pq);
    panelrender_typeset_append(panels,
        (float*)ENTITY_COLORS[type->type],
        "%c ", FONT_ENTTYPE_ZERO_CHAR + type->type);

    switch(type->type){
    case ENTTYPE_SHIP: {
        panelrender_typeset_append(panels, PAL[FG_WHITE], "SHIP\n");
    } break;
    case ENTTYPE_STAR: {
        panelrender_typeset_append(panels, PAL[FG_WHITE], "STAR\n");
    } break;
    case ENTTYPE_OASIS: {
        panelrender_typeset_append(panels, PAL[FG_WHITE], "OASIS\n");
    } break;
    case ENTTYPE_HABITABLE: {
        panelrender_typeset_append(panels, PAL[FG_WHITE], "HABITABLE WORLD\n");
    } break;
    case ENTTYPE_MONOLITH: {
        panelrender_typeset_append(panels, PAL[FG_WHITE], "MONOLITH\n");
    } break;
    case ENTTYPE_WORMHOLE: {
        panelrender_typeset_append(panels, PAL[FG_WHITE], "WORMHOLE\n");
    } break;
    case ENTTYPE_BARREN: {
        panelrender_typeset_append(panels, PAL[FG_WHITE], "BARREN WORLD\n");
    } break;
    case ENTTYPE_HIVE: {
        panelrender_typeset_append(panels, PAL[FG_WHITE], "HIVE\n");
    } break;
    case ENTTYPE_WASP: {
        panelrender_typeset_append(panels, PAL[FG_WHITE], "WASP\n");
    } break;
    case ENTTYPE_LEVIATHAN: {
        panelrender_typeset_append(panels, PAL[FG_WHITE], "LEVIATHAN\n");
    } break;
    case ENTTYPE_MISSILE: {
        panelrender_typeset_append(panels, PAL[FG_WHITE], "MISSILE\n");
    } break;
    case ENTTYPE_WRECKAGE: {
        panelrender_typeset_append(panels, PAL[FG_WHITE], "WRECKAGE\n");
    } break;
    case ENTTYPE_CIVILIZATION: {
        panelrender_typeset_append(panels, PAL[FG_WHITE], "CIVILIZATION\n");
    } break;
    case ENTTYPE_RUIN: {
        panelrender_typeset_append(panels, PAL[FG_WHITE], "RUIN\n");
    } break;
    default: {} break;
    }

    panelrender_position(panels, pos);
    const Pointphys* pphys = ecs_get_id(ecs, ent, PointphysID);
    if(pphys){ panelrender_pointphys(panels, pphys); }

	{
		const Nav* nav = ecs_get_id(ecs, ent, NavID);
		const Pilot* pilot = ecs_get_id(ecs, ent, PilotID);
		if(pilot && !nav){
			panelrender_pilot(panels, pilot);
		} else
		if(pilot && nav){
			panelrender_pilot_and_nav_range(panels, pilot, nav);
		}
		panelrender_typeset_append(panels, PAL[FG_WHITE], "\n");
	}

    const Uxn* uxn = ecs_get_id(ecs, ent, UxnID);
    if(uxn){
        panelrender_uxn(panels, uxn);
        panelrender_typeset_append(panels, PAL[FG_WHITE], "\n");
    }

    const Nav* nav = ecs_get_id(ecs, ent, NavID);
    if(nav){
        panelrender_typeset_append(panels, PAL[FG_WHITE], "%d ", nav->count);
        panelrender_nav(panels, nav);
    }

    const Drive* drive = ecs_get_id(ecs, ent, DriveID);
    if(drive){
        panelrender_typeset_append(panels, PAL[FG_WHITE], "%d ", drive->count);
        panelrender_drive(panels, drive);
    }

    const Vat* vat = ecs_get_id(ecs, ent, VatID);
    if(vat){
        panelrender_typeset_append(panels, PAL[FG_WHITE], "%d ", vat->count);
        panelrender_vat(panels, vat);
    }

    const Extract* ext = ecs_get_id(ecs, ent, ExtractID);
    if(ext){
        panelrender_typeset_append(panels, PAL[FG_WHITE], "%d ", vat->count);
        panelrender_extract(panels, ext);
    }

    const Synth* synth = ecs_get_id(ecs, ent, SynthID);
    if(synth){
        panelrender_typeset_append(panels, PAL[FG_WHITE], "%d ", synth->count);
        panelrender_synth(panels, synth);
    }

    const Compute* com = ecs_get_id(ecs, ent, ComputeID);
    if(com){
        panelrender_typeset_append(panels, PAL[FG_WHITE], "%d ", com->count);
        panelrender_compute(panels, com);
    }

    const Hull* hull = ecs_get_id(ecs, ent, HullID);
    if(hull){
        panelrender_typeset_append(panels, PAL[FG_WHITE], "%d ", hull->count);
        panelrender_hull(panels, hull);
    }

    const Bay* bay = ecs_get_id(ecs, ent, BayID);
    if(bay){
        panelrender_typeset_append(panels, PAL[FG_WHITE], "%d ", bay->count);
        panelrender_bay(panels, bay);
    }

    const Cargo* cargo = ecs_get_id(ecs, ent, CargoID);
    if(cargo){
        panelrender_typeset_append(panels, PAL[BG_WHITE], "- ");
        panelrender_cargo(panels, cargo);
    }

    const Growth* growth = ecs_get_id(ecs, ent, GrowthID);
    if(growth){
        panelrender_typeset_append(panels, PAL[BG_WHITE], "- ");
        panelrender_growth(panels, growth);
    }

    const Star* star = ecs_get_id(ecs, ent, StarID);
    if(star){ panelrender_star(panels, star); }

    const Trophs* trophs = ecs_get_id(ecs, ent, TrophsID);
    if(trophs){ panelrender_trophs(panels, trophs); }

    const Habitable* habit = ecs_get_id(ecs, ent, HabitableID);
    if(habit){
        panelrender_typeset_append(panels, PAL[FG_WHITE], "\n");
        panelrender_habitable(panels, habit);
    }

    const Monolith* monol = ecs_get_id(ecs, ent, MonolithID);
    if(monol){ panelrender_monolith(panels, monol); }

    const Wormhole* worm = ecs_get_id(ecs, ent, WormholeID);
    if(worm){ panelrender_wormhole(panels, worm); }

    const Barren* barr = ecs_get_id(ecs, ent, BarrenID);
    if(barr){ panelrender_barren(panels, barr); }

    const Hive* hive = ecs_get_id(ecs, ent, HiveID);
    if(hive){
        panelrender_typeset_append(panels, PAL[FG_WHITE], "\n");
        panelrender_hive(panels, hive);
    }

    const Wasp* wasp = ecs_get_id(ecs, ent, WaspID);
    if(wasp){
        panelrender_typeset_append(panels, PAL[FG_WHITE], "\n");
        panelrender_wasp(panels, wasp);
    }

    const Leviathan* levia = ecs_get_id(ecs, ent, LeviathanID);
    if(levia){
        panelrender_typeset_append(panels, PAL[FG_WHITE], "\n");
        panelrender_leviathan(panels, levia, camera);
    }

    const Missile* miss = ecs_get_id(ecs, ent, MissileID);
    if(miss){
        panelrender_typeset_append(panels, PAL[FG_WHITE], "\n");
        panelrender_missile(panels, miss, camera);
    }

    const Wreckage* wreck = ecs_get_id(ecs, ent, WreckageID);
    if(wreck){
        panelrender_typeset_append(panels, PAL[FG_WHITE], "\n");
        panelrender_wreckage(panels, wreck);
    }

    const Ruin* ruin = ecs_get_id(ecs, ent, RuinID);
    if(ruin){
        panelrender_typeset_append(panels, PAL[FG_WHITE], "\n");
        panelrender_ruin(panels, ruin);
    }

    //Render panel
    panelrender_render_2d(panels, boards,
        windowsizes, view, proj,
        (int[2]){
            0,
            0 - panels->chardim[1] * 2 -
            ((panels->row - 1) * panels->chardim[1]) / 2
        }
    );

    //Above
    switch(type->type){
    case ENTTYPE_SHIP: {

        //If a cargo module was found previously
        if(cargo && cargo->artifact != ART_NONE){

            panelrender_typeset_reset(panels, 31, wposrel, panels->chardim[0]);

            panelrender_typeset_append(panels, PAL[BG_BLUE], "ARTIFACT ");
            panelrender_typeset_append(panels, PAL[FG_WHITE], "%04hx\n",
                cargo->artifact);

            panelrender_typeset_append(panels, PAL[FG_WHITE], "%s\n\n",
                ART_NAME[cargo->artifact]);

            panelrender_typeset_append(panels, PAL[FG_WHITE], "%s\n",
                ART_DESC[cargo->artifact]);

            panelrender_render_2d(panels, boards,
                windowsizes, view, proj,
                (int[2]){
                    0,
                    0 + panels->chardim[1] * 3 +
                    ((panels->row - 1) * panels->chardim[1]) / 2
                }
            );
        }

    } break;
    default: {} break;
    }
}
