#include "input.h"

void kbmouse_step(KBMouse* kbm)
{
    //Save the key & button states as the previous states
    for(int i = 0; i < GL_KEY_COUNT; ++i){
        kbm->keys_pre[i] = kbm->keys_act[i];
    }
    for(int i = 0; i < GL_MOUSE_BUTTON_COUNT; ++i){
        kbm->keys_pre[i] = kbm->keys_act[i];
    }

    //Step the absolute cursor position with the relative
    //TODO: check if this is always correct, and how to reset it if not
    kbm->cursor_abs[0] = kbm->cursor_pre[0];
    kbm->cursor_abs[1] = kbm->cursor_pre[1];

    //Autorepeat pressed keys & buttons, release others
    for(int i = 0; i < GL_KEY_COUNT; ++i){
        if(kbm->keys_delta[i] > 0 && kbm->keys_act[i] > GL_RELEASE_){
            kbm->keys_delta[i] = 1;
            kbm->keys_act[i] = GL_REPEAT_;
        } else {
            kbm->keys_delta[i] = 0;
        }
    }
    for(int i = 0; i < GL_MOUSE_BUTTON_COUNT; ++i){
        if(kbm->buttons_delta[i] > 0 && kbm->buttons_act[i] > GL_RELEASE_){
            kbm->buttons_delta[i] = 1;
            kbm->buttons_act[i] = GL_REPEAT_;
        } else {
            kbm->buttons_delta[i] = 0;
        }
    }

    //Reset key & button modifiers
    memset(&kbm->keys_mods, 0, GL_KEY_COUNT);
    memset(&kbm->buttons_mods, 0, GL_MOUSE_BUTTON_COUNT);

    //Reset relative cursor motion
    kbm->cursor_rel[0] = 0.0f;
    kbm->cursor_rel[1] = 0.0f;
}

void kbmouse_process(KBMouse* kbm, ModSession* modsession, ModClient* modcli)
{
    //Close the window if ESC was pressed
    if(
        kbm->keys_delta[GL_KEY_ESCAPE] > 0 &&
        kbm->keys_act[GL_KEY_ESCAPE] == GL_PRESS_
    ){
        on_client_window_close(modcli);
    }

    //Change input modality to NormalMode if 'V' was pressed
    if(kbm->keys_delta[GL_KEY_V] > 0 && kbm->keys_act[GL_KEY_V] == GL_PRESS_){
        modsession->modality = NormalMode;
        on_client_update_prompt();
    }

    //Change input modality to ControllerMode if 'C' was pressed
    if(kbm->keys_delta[GL_KEY_C] > 0 && kbm->keys_act[GL_KEY_C] == GL_PRESS_){
        modsession->modality = ControllerMode;
        on_client_update_prompt();
    }

    //Focus resolved entity if 'R' was pressed
    if(
        kbm->keys_delta[GL_KEY_R] > 0 &&
        kbm->keys_act[GL_KEY_R] == GL_PRESS_ &&
        modsession->resolvent >= 0
    ){
        modsession->focusent = modsession->resolvent;
        on_client_update_prompt();
    }

    //Unfocus any entity if none was resolved and 'R' was pressed
    if(
        kbm->keys_delta[GL_KEY_R] > 0 &&
        kbm->keys_act[GL_KEY_R] == GL_PRESS_ &&
        modsession->resolvent < 0
    ){
        modsession->focusent = -1;
        on_client_update_prompt();
    }
}

void gamepad_step(Gamepad* gmp, float sensitivity)
{
    //Save the button & axis states as the previous states
    for(int i = 0; i < GL_GAMEPAD_BUTTON_COUNT; ++i){
        gmp->buttons_pre[i] = gmp->buttons_act[i];
    }
    for(int i = 0; i < GL_GAMEPAD_AXIS_COUNT; ++i){
        gmp->axes_pre[i] = gmp->axes_cur[i];
    }

    //Update axis states if:
    //either the input delta exceeds the sensitivity,
    //or the new value is zero
    for(int i = 0; i < GL_GAMEPAD_AXIS_COUNT; ++i){
        if(
            (fabs(gmp->axes_cur[i] - gmp->axes_val[i]) > sensitivity) ||
            (gmp->axes_val[i] == 0.0f)
        ){
            gmp->axes_cur[i] = gmp->axes_val[i];
        }
    }

    //Reset button & axis deltas
    memset(&gmp->buttons_delta, 0, GL_GAMEPAD_BUTTON_COUNT);
    memset(&gmp->axes_delta, 0, GL_GAMEPAD_AXIS_COUNT);

    //Reset gamepad triggers to their true zero
    //TODO: why is this necessary
    gmp->state.axes[GL_GAMEPAD_AXIS_LEFT_TRIGGER] = -1.0f;
    gmp->state.axes[GL_GAMEPAD_AXIS_RIGHT_TRIGGER] = -1.0f;
}

void gamepad_process(Gamepad* gmp, ModSession* modsession, ModClient* modcli, float deadzone, float azimuth)
{
    //Update gamepad state
    gamepad_update(gmp, deadzone);

    //Rotate left axes by camera azimuth
    vec2 val = { gmp->axes_cur[GL_GAMEPAD_AXIS_LEFT_X],
                 -gmp->axes_cur[GL_GAMEPAD_AXIS_LEFT_Y] };
    glm_vec2_rotate(val, glm_rad(azimuth + 90.0f), val);
    glm_vec2_scale_as(val, MIN(glm_vec2_norm(val), 1.0f), val);
    gmp->axes_cur[GL_GAMEPAD_AXIS_LEFT_X] = val[0];
    gmp->axes_cur[GL_GAMEPAD_AXIS_LEFT_Y] = val[1];

    //Change input modality to NormalMode if 'BACK' was pressed
    if(
        gmp->buttons_delta[GL_GAMEPAD_BUTTON_BACK] > 0 &&
        gmp->buttons_act[GL_GAMEPAD_BUTTON_BACK] == GL_PRESS_
    ){
        modsession->modality = NormalMode;
        on_client_update_prompt();
    }

    //Change input modality to ControllerMode if 'START' was pressed
    if(
        gmp->buttons_delta[GL_GAMEPAD_BUTTON_START] > 0 &&
        gmp->buttons_act[GL_GAMEPAD_BUTTON_START] == GL_PRESS_
    ){
        modsession->modality = ControllerMode;
        on_client_update_prompt();
    }

    //Focus resolved entity if 'DPAD_DOWN' was pressed
    if(
        gmp->buttons_delta[GL_GAMEPAD_BUTTON_DPAD_DOWN] > 0 &&
        gmp->buttons_act[GL_GAMEPAD_BUTTON_DPAD_DOWN] == GL_PRESS_ &&
        modsession->resolvent >= 0
    ){
        modsession->focusent = modsession->resolvent;
        on_client_update_prompt();
    }

    //Unfocus any entity if none was resolved and 'DPAD_DOWN' was pressed
    if(
        gmp->buttons_delta[GL_GAMEPAD_BUTTON_DPAD_DOWN] > 0 &&
        gmp->buttons_act[GL_GAMEPAD_BUTTON_DPAD_DOWN] == GL_PRESS_ &&
        modsession->resolvent < 0
    ){
        modsession->focusent = -1;
        on_client_update_prompt();
    }

    //When in ContollerMode
    if(modsession->modality == ControllerMode){

        //Extract controller device events
        gamepad_events_to_buffer(gmp, modcli->client, &modsession->ievt);

        //chprintf(cli, "x: %f", gmp->axes_val[GL_GAMEPAD_AXIS_LEFT_X]);

        //If there are any events waiting, set session delta
        if(modcli->ievt.size > 0){ modsession->delta = true; }
    }
}

void gamepad_update(Gamepad* gmp, float deadzone)
{
    unsigned char* buttons = &gmp->state.buttons[0];
    float* axes = &gmp->state.axes[0];

    //Buttons
    for(int i = 0; i < GL_GAMEPAD_BUTTON_COUNT; ++i){

        //Avoid overwriting lingering deltas
        if(gmp->buttons_delta[i] == 0){
            gmp->buttons_delta[i] = 1;
            gmp->buttons_act[i] = buttons[i];
        }
    }

    //Dual axes
    for(int i = GL_GAMEPAD_AXIS_LEFT_X; i <= GL_GAMEPAD_AXIS_RIGHT_X; i += 2){
        if(gmp->axes_delta[i] == 0 && gmp->axes_delta[i+1] == 0){

            //Always set delta on both axes
            gmp->axes_delta[i] = gmp->axes_delta[i+1] = 1;

            //Circular deadzone check
            if (powf(axes[i] * axes[i] + axes[i+1] * axes[i+1], 0.5f) >
                deadzone
            ){
                float angle = atan2f(axes[i+1], axes[i]);
                gmp->axes_val[i] = (axes[i] - (deadzone * cosf(angle))) *
                    (1.0f / (1.0f - deadzone));
                gmp->axes_val[i+1] = (axes[i+1] - (deadzone * sinf(angle))) *
                    (1.0f / (1.0f - deadzone));
            }
            //Reset to zero inside the deadzone
            else {
                gmp->axes_val[i] = gmp->axes_val[i+1] = 0.0f;
            }
        }
    }

    //Triggers
    for(int i = GL_GAMEPAD_AXIS_LEFT_TRIGGER; i <= GL_GAMEPAD_AXIS_RIGHT_TRIGGER; ++i){
        
        if(gmp->axes_delta[i] == 0){

            //Always set delta
            gmp->axes_delta[i] = 1;

            //Deadzone check
            if((axes[i] + 1.0f) * 0.5f > deadzone){
                gmp->axes_val[i] = ((axes[i] + 1.0) * 0.5 - deadzone) *
                    (1.0f / (1.0f - deadzone));
            }
            //Reset to zero inside the deadzone
            else {
                gmp->axes_val[i] = 0.0f;
            }
        }
    }
}

uint8_t repeat_to_press(uint8_t button)
{
    return button > GL_RELEASE_ ? GL_PRESS_ : GL_RELEASE_;
}

uint8_t button_delta_mask(Gamepad* gmp, uint16_t mask)
{
    for(int i = 0; i < GL_GAMEPAD_BUTTON_COUNT; ++i){
        if(
            ((mask >> i) & 1) == 1 &&
            gmp->buttons_delta[i] > 0 &&
                repeat_to_press(gmp->buttons_pre[i]) !=
                repeat_to_press(gmp->buttons_act[i])
        ){ return 1; }
    }
    return 0;
}

uint8_t axis_delta_mask(Gamepad* gmp, uint16_t mask)
{
    for(int i = 0; i < GL_GAMEPAD_AXIS_COUNT; ++i){
        if(
            ((mask >> i) & 1) == 1 &&
            gmp->axes_delta[i] > 0 &&
            gmp->axes_pre[i] != gmp->axes_cur[i]
        ){ return 1; }
    }
    return 0;
}

void gamepad_events_to_buffer(Gamepad* gmp, void* cli, Buffer* buf)
{
    static uint16_t MASK_GUIDE =
        (1 << GL_GAMEPAD_BUTTON_GUIDE);
    static uint16_t MASK_THUMBS =
        (1 << GL_GAMEPAD_BUTTON_LEFT_THUMB) +
        (1 << GL_GAMEPAD_BUTTON_RIGHT_THUMB);
    static uint16_t MASK_SELSTART =
        (1 << GL_GAMEPAD_BUTTON_BACK) +
        (1 << GL_GAMEPAD_BUTTON_START);
    static uint16_t MASK_BUTTONS =
        (1 << GL_GAMEPAD_BUTTON_A) +
        (1 << GL_GAMEPAD_BUTTON_B) +
        (1 << GL_GAMEPAD_BUTTON_X) +
        (1 << GL_GAMEPAD_BUTTON_Y) +
        (1 << GL_GAMEPAD_BUTTON_DPAD_UP) +
        (1 << GL_GAMEPAD_BUTTON_DPAD_RIGHT) +
        (1 << GL_GAMEPAD_BUTTON_DPAD_DOWN) +
        (1 << GL_GAMEPAD_BUTTON_DPAD_LEFT);
    static uint16_t MASK_BUMPERS =
        (1 << GL_GAMEPAD_BUTTON_LEFT_BUMPER) +
        (1 << GL_GAMEPAD_BUTTON_RIGHT_BUMPER);

    static uint16_t MASK_LEFT_X = 1 << GL_GAMEPAD_AXIS_LEFT_X;
    static uint16_t MASK_LEFT_Y = 1 << GL_GAMEPAD_AXIS_LEFT_Y;
    static uint16_t MASK_RIGHT_X = 1 << GL_GAMEPAD_AXIS_RIGHT_X;
    static uint16_t MASK_RIGHT_Y = 1 << GL_GAMEPAD_AXIS_RIGHT_Y;
    static uint16_t MASK_LEFT_TRIGGER = 1 << GL_GAMEPAD_AXIS_LEFT_TRIGGER;
    static uint16_t MASK_RIGHT_TRIGGER = 1 << GL_GAMEPAD_AXIS_RIGHT_TRIGGER;

    //Device address
    uint8_t devaddr = UXN_DEV_CONT;

    if(button_delta_mask(gmp, MASK_GUIDE) == 1){
        buffer_push(buf, (Event){
            .devport = devaddr + UXN_PORT_CONT_GUIDE,
            .data =
            (repeat_to_press(gmp->buttons_act[GL_GAMEPAD_BUTTON_GUIDE]) << 0)
        });
    }

    if(button_delta_mask(gmp, MASK_THUMBS) == 1){
        buffer_push(buf, (Event){
            .devport = devaddr + UXN_PORT_CONT_THUMBS,
            .data =
            (repeat_to_press(gmp->buttons_act[GL_GAMEPAD_BUTTON_LEFT_THUMB]) << 1) +
            (repeat_to_press(gmp->buttons_act[GL_GAMEPAD_BUTTON_RIGHT_THUMB]) << 0)
        });
    }

    if(button_delta_mask(gmp, MASK_SELSTART) == 1){
        buffer_push(buf, (Event){
            .devport = devaddr + UXN_PORT_CONT_SELSTART,
            .data =
            (repeat_to_press(gmp->buttons_act[GL_GAMEPAD_BUTTON_BACK]) << 1) +
            (repeat_to_press(gmp->buttons_act[GL_GAMEPAD_BUTTON_START]) << 0)
        });
    }

    if(button_delta_mask(gmp, MASK_BUTTONS) == 1){
        buffer_push(buf, (Event){
            .devport = devaddr + UXN_PORT_CONT_BUTTONS,
            .data =
            (repeat_to_press(gmp->buttons_act[GL_GAMEPAD_BUTTON_A]) << 7) +
            (repeat_to_press(gmp->buttons_act[GL_GAMEPAD_BUTTON_B]) << 6) +
            (repeat_to_press(gmp->buttons_act[GL_GAMEPAD_BUTTON_X]) << 5) +
            (repeat_to_press(gmp->buttons_act[GL_GAMEPAD_BUTTON_Y]) << 4) +
            (repeat_to_press(gmp->buttons_act[GL_GAMEPAD_BUTTON_DPAD_UP]) << 3) +
            (repeat_to_press(gmp->buttons_act[GL_GAMEPAD_BUTTON_DPAD_RIGHT]) << 2) +
            (repeat_to_press(gmp->buttons_act[GL_GAMEPAD_BUTTON_DPAD_DOWN]) << 1) +
            (repeat_to_press(gmp->buttons_act[GL_GAMEPAD_BUTTON_DPAD_LEFT]) << 0)
        });
    }

    if(button_delta_mask(gmp, MASK_BUMPERS) == 1){
        buffer_push(buf, (Event){
            .devport = devaddr + UXN_PORT_CONT_BUMPERS,
            .data =
            (repeat_to_press(gmp->buttons_act[GL_GAMEPAD_BUTTON_LEFT_BUMPER]) << 1) +
            (repeat_to_press(gmp->buttons_act[GL_GAMEPAD_BUTTON_RIGHT_BUMPER]) << 0)
        });
    }

    if(axis_delta_mask(gmp, MASK_LEFT_X) == 1){
        buffer_push(buf, (Event){
            .devport = devaddr + UXN_PORT_CONT_LEFT_X,
            .data = 255 * 0.5f *
                (1.0f + gmp->axes_cur[GL_GAMEPAD_AXIS_LEFT_X])
        });
    }


    if(axis_delta_mask(gmp, MASK_LEFT_Y) == 1){
        buffer_push(buf, (Event){
            .devport = devaddr + UXN_PORT_CONT_LEFT_Y,
            .data = 255 * 0.5f *
                (1.0f + gmp->axes_cur[GL_GAMEPAD_AXIS_LEFT_Y])
        });
    }

    if(axis_delta_mask(gmp, MASK_RIGHT_X) == 1){
        buffer_push(buf, (Event){
            .devport = devaddr + UXN_PORT_CONT_RIGHT_X,
            .data = 255 * 0.5f *
                (1.0f + gmp->axes_cur[GL_GAMEPAD_AXIS_RIGHT_X])
        });
    }

    if(axis_delta_mask(gmp, MASK_RIGHT_Y) == 1){
        buffer_push(buf, (Event){
            .devport = devaddr + UXN_PORT_CONT_RIGHT_Y,
            .data = 255 * 0.5f *
                (1.0f + gmp->axes_cur[GL_GAMEPAD_AXIS_RIGHT_Y])
        });
    }

    if(axis_delta_mask(gmp, MASK_LEFT_TRIGGER) == 1){
        buffer_push(buf, (Event){
            .devport = devaddr + UXN_PORT_CONT_LEFT_TRIGGER,
            .data = 255 *
                (gmp->axes_cur[GL_GAMEPAD_AXIS_LEFT_TRIGGER])
        });
    }

    if(axis_delta_mask(gmp, MASK_RIGHT_TRIGGER) == 1){
        buffer_push(buf, (Event){
            .devport = devaddr + UXN_PORT_CONT_RIGHT_TRIGGER,
            .data = 255 *
                (gmp->axes_cur[GL_GAMEPAD_AXIS_RIGHT_TRIGGER])
        });
    }
}

void map_key_to_button(KBMouse* kbm, Gamepad* gmp, int key, int button)
{
    if(kbm->keys_delta[key] > 0 && gmp->buttons_delta[button] == 0){
        gmp->buttons_delta[button] = 1;
        gmp->buttons_act[button] = kbm->keys_act[key];
    }
}

void map_key_to_axis(KBMouse* kbm, Gamepad* gmp, int key, int axis)
{
    if(kbm->keys_delta[key] > 0 && gmp->axes_delta[axis] == 0){
        gmp->axes_delta[axis] = 1;
        gmp->axes_val[axis] = kbm->keys_act[key] > GL_RELEASE_ ? 1.0f : 0.0f;
    }
}

void map_key_to_biaxis(KBMouse* kbm, Gamepad* gmp, int key_neg, int key_pos, int axis)
{
    if(
        (kbm->keys_delta[key_pos] > 0 || kbm->keys_delta[key_neg] > 0) &&
        gmp->axes_delta[axis] == 0
    ){
        gmp->axes_delta[axis] = 1;
        gmp->axes_val[axis] =
            ((kbm->keys_delta[key_neg] > 0 && kbm->keys_act[key_neg] > 0) ?
                -1.0f : 0.0f) +
            ((kbm->keys_delta[key_pos] > 0 && kbm->keys_act[key_pos] > 0) ?
                1.0f : 0.0f);
    }
}

void map_key_to_dual_biaxis(KBMouse* kbm, Gamepad* gmp, int a1_neg, int a1_pos, int a1, int a2_neg, int a2_pos, int a2)
{
    if(
        (
	    kbm->keys_delta[a1_pos] > 0 || kbm->keys_delta[a1_neg] > 0 ||
	    kbm->keys_delta[a2_pos] > 0 || kbm->keys_delta[a2_neg] > 0
	) &&
        gmp->axes_delta[a1] == 0 && gmp->axes_delta[a2] == 0
    ){
        gmp->axes_delta[a1] = 1;
        gmp->axes_val[a1] =
            ((kbm->keys_delta[a1_neg] > 0 && kbm->keys_act[a1_neg] > 0) ?
                -1.0f : 0.0f) +
            ((kbm->keys_delta[a1_pos] > 0 && kbm->keys_act[a1_pos] > 0) ?
                1.0f : 0.0f);
        gmp->axes_delta[a2] = 1;
        gmp->axes_val[a2] =
            ((kbm->keys_delta[a2_neg] > 0 && kbm->keys_act[a2_neg] > 0) ?
                -1.0f : 0.0f) +
            ((kbm->keys_delta[a2_pos] > 0 && kbm->keys_act[a2_pos] > 0) ?
                1.0f : 0.0f);
    }
}

void map_kbmouse_to_gamepad(KBMouse* kbm, Gamepad* gmp)
{
    map_key_to_button(kbm, gmp,
        GL_KEY_B, GL_GAMEPAD_BUTTON_GUIDE);

    map_key_to_button(kbm, gmp,
        GL_KEY_X, GL_GAMEPAD_BUTTON_LEFT_THUMB);
    map_key_to_button(kbm, gmp,
        GL_KEY_COMMA, GL_GAMEPAD_BUTTON_RIGHT_THUMB);

    map_key_to_button(kbm, gmp,
        GL_KEY_R, GL_GAMEPAD_BUTTON_BACK);
    map_key_to_button(kbm, gmp,
        GL_KEY_Z, GL_GAMEPAD_BUTTON_START);

    map_key_to_button(kbm, gmp,
        GL_KEY_T, GL_GAMEPAD_BUTTON_DPAD_UP);
    map_key_to_button(kbm, gmp,
        GL_KEY_H, GL_GAMEPAD_BUTTON_DPAD_RIGHT);
    map_key_to_button(kbm, gmp,
        GL_KEY_G, GL_GAMEPAD_BUTTON_DPAD_DOWN);
    map_key_to_button(kbm, gmp,
        GL_KEY_F, GL_GAMEPAD_BUTTON_DPAD_LEFT);

    map_key_to_button(kbm, gmp,
        GL_KEY_DOWN, GL_GAMEPAD_BUTTON_A);
    map_key_to_button(kbm, gmp,
        GL_KEY_RIGHT, GL_GAMEPAD_BUTTON_B);
    map_key_to_button(kbm, gmp,
        GL_KEY_LEFT, GL_GAMEPAD_BUTTON_X);
    map_key_to_button(kbm, gmp,
        GL_KEY_UP, GL_GAMEPAD_BUTTON_Y);

    map_key_to_button(kbm, gmp,
        GL_KEY_E, GL_GAMEPAD_BUTTON_LEFT_BUMPER);
    map_key_to_button(kbm, gmp,
        GL_KEY_U, GL_GAMEPAD_BUTTON_RIGHT_BUMPER);

    map_key_to_dual_biaxis(kbm, gmp,
        GL_KEY_W, GL_KEY_S, GL_GAMEPAD_AXIS_LEFT_Y,
	GL_KEY_A, GL_KEY_D, GL_GAMEPAD_AXIS_LEFT_X);

    map_key_to_biaxis(kbm, gmp,
        GL_KEY_I, GL_KEY_K, GL_GAMEPAD_AXIS_RIGHT_Y);
    map_key_to_biaxis(kbm, gmp,
        GL_KEY_J, GL_KEY_L, GL_GAMEPAD_AXIS_RIGHT_X);

    map_key_to_axis(kbm, gmp,
        GL_KEY_Q, GL_GAMEPAD_AXIS_LEFT_TRIGGER);
    map_key_to_axis(kbm, gmp,
        GL_KEY_O, GL_GAMEPAD_AXIS_RIGHT_TRIGGER);
    
}

void map_button_to_key(Gamepad* gmp, KBMouse* kbm, int button, int key)
{
    if(
        gmp->buttons_delta[button] > 0 &&
        gmp->buttons_act[button] != gmp->buttons_pre[button]
    ){
        kbm->keys_delta[key] = 1;
        kbm->keys_act[key] = gmp->buttons_act[button];
    }
}

void map_biaxis_to_cursor_rel(Gamepad* gmp, KBMouse* kbm, int axis, int dim, float sensitivity)
{
    kbm->cursor_rel[dim] += gmp->axes_cur[axis] * sensitivity;
}

void map_biaxis_to_cursor_abs(Gamepad* gmp, KBMouse* kbm, int axis, int dim, float range)
{
    if(
        gmp->axes_delta[axis] > 0 &&
        gmp->axes_cur[axis] != gmp->axes_pre[axis]
    ){
        kbm->cursor_abs[dim] = (gmp->axes_cur[axis] + 1.0f) * 0.5f * range;
    }
}

void map_biaxis_to_keys(Gamepad* gmp, KBMouse* kbm, int axis, int key_neg, int key_pos, float threshold)
{
    if(
        gmp->axes_delta[axis] > 0 &&
        gmp->axes_cur[axis] != gmp->axes_pre[axis]
    ){
        if(fabs(gmp->axes_cur[axis]) > threshold){
            int key = gmp->axes_cur[axis] > 0 ? key_pos : key_neg;
            kbm->keys_delta[key] = 1;
            kbm->keys_act[key] = GL_PRESS_;
        } else {
            kbm->keys_delta[key_pos] = 1;
            kbm->keys_act[key_pos] = GL_RELEASE_;
            kbm->keys_delta[key_neg] = 1;
            kbm->keys_act[key_neg] = GL_RELEASE_;
        }
    }
}

void map_gamepad_to_kbmouse(Gamepad* gmp, KBMouse* kbm, float threshold, float sensitivity)
{
    map_button_to_key(gmp, kbm,
        GL_GAMEPAD_BUTTON_DPAD_UP, GL_KEY_UP);
    map_button_to_key(gmp, kbm,
        GL_GAMEPAD_BUTTON_DPAD_RIGHT, GL_KEY_RIGHT);
    map_button_to_key(gmp, kbm,
        GL_GAMEPAD_BUTTON_DPAD_DOWN, GL_KEY_DOWN);
    map_button_to_key(gmp, kbm,
        GL_GAMEPAD_BUTTON_DPAD_LEFT, GL_KEY_LEFT);

    map_biaxis_to_keys(gmp, kbm,
        GL_GAMEPAD_AXIS_LEFT_X, GL_KEY_A, GL_KEY_D, threshold);
    map_biaxis_to_keys(gmp, kbm,
        GL_GAMEPAD_AXIS_LEFT_Y, GL_KEY_W, GL_KEY_S, threshold);

    map_biaxis_to_cursor_rel(gmp, kbm,
        GL_GAMEPAD_AXIS_RIGHT_X, 0, sensitivity);
    map_biaxis_to_cursor_rel(gmp, kbm,
        GL_GAMEPAD_AXIS_RIGHT_Y, 1, sensitivity);
}
