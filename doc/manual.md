# Terminal user interface

The client uses termbox2 to display its text interface, which consists of two parts:

1. a scrolling text history in the center (PGUP/PGDN to scroll line by line),
2. a prompt at the bottom.

The terminal user interface exists on the modded side, any part of it may be changed.

# Sessions

The server uses a single port to communicate with clients, it's the only one that server operators need to forward. There is a configurable limit on the maximum number of connections that will be accepted from a single IP address. Initially, connected sessions start out unauthenticated.

The server implements its own heartbeat mechanism for sessions. Sessions that have not had a heartbeat processed for the expiration time are considered dead and will be stopped. The server requests heartbeats periodically, and waits for a short time before re-sending one. These three time lengths are all configured in `config.json`.

Session have a fixed-size memory space of `SESSION_BYTES` where mod implementations can store arbitrary data.

# Authentication

When the server is offline, it stores authentication data in a pre-existing `.csv` file in the format `username,accesslevel,salt,verifier`. While running, it stores these in memory.

The program ships with `res/auth.csv` where only a root user is added with its name as its password. To change the password (one of the first steps of setting up a server), authenticate with the server first, then use the `changepw <new-pword>` command. The server administrator can also edit the file by hand, adding the salt & verifier key that generated by the client using the `--sv` option.

The server uses the secure remote password (SRP) protocol to authenticate clients, which enables them to perform zero-knowledge password proofs. When the client registers, it generates a short salt and a host verification key, which the server stores for later use. When a client tries to authenticate with a server, they perform the following exchange.

```
1. User generates an ephemeral key "A" from what it already knows.
2. User sends the host (<uname>, <A>), which creates a verifier for the user,
   and generates its own ephemeral key "B".
3. Host sends the user (<uname>, <salt>, <B>).
   The user processes the challenge, producing "M".
4. User sends the host (<M>).
   The host tries to verify the session, producing "HAMK".
5. Host sends the user (<HAMK>).
   The user tries to verify the session as well.
   If all points were passed, the user has authenticated successfully.
```

## Access levels

Access levels start from 0 for the highest (root) access, and decrease with every increment.

# File logging

During compilation, the `PLANCKLOG_FILE` macro determines whether the logging macros will output to a file or standard output. Make the compiler define it with `-DPLANCKLOG_FILE` for the former, and omit it for the latter.

The file logging facility is capable of gracefully moving onto new log files once the timestamp of the last opened file is considered "distant enough". This is currently not runtime-configurable and changes logfiles daily.

# Initial JSON configuration

Initial parameters for the server and client are specified in `res/config.json`. The server and client will load their configurations from this file if they can open it, or load default values otherwise. In both cases, they log the final values used.

See the supplied default file for terse descriptions on what each option configures.

`WARNING:` The configuration is not validated, i.e. providing nonsensical values, or ones that can't work together, should be considered as a source of undefined behavior.

# Command line options

See the available command line options for the programs by passing them the `-h` or `--help` arguments. Options that coincide with configured values override their initially loaded values.

# Messaging

The server and client communicate through messages. Messages are composed of segments, with each segment being separated into header and body portions. The current header format for segments consists of two `uint32_t` doublewords, the first specifying the segment size in bytes, and the second signifying the "data type" of the segment. Currently, the mapping of data sizes to their meanings is the following.

```c
typedef enum {
    PKT_CHUNK_U8 = 0,
    PKT_CHUNK_U16,
    PKT_CHUNK_U32,
} PKT_CHUNK_TYPE;
```

Different kinds of messages are distinguished by their first segment. Currently, the first segment carries a single `uint16_t` that specifies the message type. See `msg.h` for the concrete indexes assigned.

## Commands

Some messages may be though of as commands, since they are emitted from clients after parsing user input.

Commands may be identifier by either their usual name, or a shorthand, which should probably be bound to non-alphabetical characters that typical messages are not likely to start with (because shorthands do not need to be separated by whitespace from their arguments).

The commands below are specififed in the format: `name[|shorthand] [args] ...`.

### Core commands

`login <uname> <pword>` starts the authentication process.

`changepw <new-pword>` sets a new password.

`changeacc <uname> <level>` changes the access level of a user. The user who changes the access level must have a higher access level than the user whose access is changed.

`register <uname> <pword>` tries to register the username with the server, attaching the required salt & verification key for SRP.

`unregister <uname>` removes the authentication of the authenticated user execting the command. The target user has to be the same one who is executing it, or the latter must have a higher access level.

`logout` removes authentication from an authenticated session.

`kick <uname>` logs out and ends all sessions of the specified user.

`auths` lists the currently registered users.

`sessions` lists all active sessions.

`settps <count>` sets the number of ticks per second the server will run at (1 or greater).

`settpspass <count>` sets the number of ticks per second the simulation will run at (0 or greater).

`logreopen` forces another logfile to be reopened, after closing the current one. Useful to inspect logs while the server is running.

`say|; <text>` sends a chat message globally.

`tell|, <uname> <text>` sends a direct message to all sessions of the specified user.

# Modifiability

The engine is separated into a core that provides the foundational mechanisms, and dynamically loaded libraries that define customizable behaviors for server and client implementations. Currently, modded code is compiled all at once, on startup. For a description of the mod shipped with the game by default, see `mod/FORNAX.md`.

## Provided definitions

For the complete list of provided definitions, see `c/common/defs.h`.