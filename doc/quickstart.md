# Getting started with doldrusidus & fornax

This guide aims to quickly introduce the concepts and mechanisms you'll need to start playing doldrusidus in the default game world called fornax.

All commands beginning with `>` are meant for the command line interface of `cli-doldrusidus`. Arguments encased in angle brackets `<>` are placeholders that you should replace with your actual parameters. Pass the `-h` or `--help` flags to the server and client programs to see their available arguments.

## Playing locally

Leave the settings found in `res/config.json` as the default.

Start a server in one terminal with `$ ./srv-doldrusidus-*.*.*`. Start a client in another terminal with `$ ./cli-doldrusidus-*.*.* -w`. The `-w` option opens a graphical window upon startup.

The default authorization file has a user named `root` with the same password.

```
> login root root
```

You should see an empty world after you log in. Generate a world from a 32-bit seed number.

```
> generate <seed>
```

## Playing remotely

Once you know the IP address of the server to connect to, you can set it as `ipv4address` in `res/config.json`, or you can pass it on the command line after `-a`. The addresses can be in dot-decimal notation, or in proquint form, such as `lusab-babad` for `127.0.0.1`.

If you have no user on the server, register first, then try to log in.

```
> register <uname> <pword>
> login <uname> <pword>
```

## Participating

Players interact with the world of fornax through the entities they pilot. Each player can pilot a few entities at any one time, the limit depends on the server. The following commands add a new piloted entity, show all of them in a list, and they delete the added entity. The last command takes an entity identifier, which can be either a hexadecimal quartet such as `0259`, or the corresponding proquint `banin`.

```
> addship
> ships
> delship <entity>
```

Open a graphical window either with the `-w` option on startup or the `ow` (open window) command while the client is running (you can close the window with `cw`). The new entity should be visible somewhere near the origin, around the hexadecimal coordinates `8000 8000 8000`. The universe is `ffff` units wide on each axis, so that's why the origin is at `8000 8000 8000`. Hover over an entity with the mouse to see its components in more detail.

Now might be a good time to familiarize yourself with the control scheme, which you can find in the `## Input bindings` section in `mod/FORNAX.md`.

## Programming

Players are free to program their entities via machine code assembled for the uxn virtual machine. The default world provides a default operating system as an example. Formerly, this system was written in the assembly language uxntal, and was called `manatee`. Going forward, the default system will be `porpoise`, written in a subset of the C language, then transpiled to uxntal with the fork of `chibicc` geared toward this purpose. An assembled version of this system comes packaged with doldrusidus, so let's upload that to a newly added entity.

```
> uxn-ram <entity> tal/porpoise.rom
```

You can also invoke this command using the shorthand `!`, in which case you don't have to separate the next argument with whitespace.

```
> !<entity> tal/porpoise.rom
```

The `uxn-ram` command also boots machines (out of convenience), but you can re-boot them at any time using the `uxn-boot <entity>` command. The `uxn-halt <entity>` and `uxn-resume <entity>` commands can be used to halt or resume execution manually, if necessary.

Once you are piloting an entity with an uxn processor, you are free to interact with the game world. This happens through an uxn device called the `host` device, and a specific data layout and communication protocol that the server mods define for it. In `mod/FORNAX.md`, you can find these in `## Uxn devices` and `## Uxn commands`.

## Porpoise

Of course, you need some way to prompt your machines to start communicating on the `host` device. One way is through the `console` device (the read and write ports of which should conform to varvara). The default system for machines, `porpoise`, provides its own command interface. Let's try telling `porpoise` to return to an idle state by feeding characters to the machine through its console device.

```
> cons <entity> idle
```

You can also invoke this command using the shorthand `.` (a single dot).

```
> .<entity> idle
```

## Moving around

If you did everything right, nothing should have happened, because `porpoise` starts in an idle state by default. Let's see an example that is a bit more interesting.

Approach the origin star with `app <target> <distance>`

```
> .<entity> app 0 7f
```

Orbit the origin star with `orb <target> <distance>`

```
> .<entity> orb 0 40
```

Impact the origin star with `imp <target>`

```
> .<entity> imp 0
```

To avoid repeating the first argument of these commands, you can press `Tab` right after writing the command's name or shorthand, and the client will insert the most recent first argument for it.

Also, since `porpoise` supports proquints in the place of identifiers, you could also specify the target entity that way. For example, `babab` for `0`.

## What happened

Once you got your entity to move around, it might be a good time to think about what actually happened in the game world. This is just one example of the mechanisms the default game world implements.

- The machine parsed input on the console into a command and arguments.
- The machine wrote the arguments to a designated location on the host device.
- The machine wrote the command index to a designated byte of the host device.
- The host device recognizes the command index, and validates the arguments.
- The game world passes the arguments to the navigation component of the entity.
- The navigation component computes the desired acceleration vector.
- The drive component of the entity accelerates it towards that vector.
- The game world integrates the simulation, accelerating the entity.

All of this behavior is specified in C code that a server compiles upon startup, so anyone who runs their own server is free to change any and all of it, without having to recompile the core engine (which has no concept of these behaviors).

## Onwards and upwards

Now that you're familiar with the main ideas behind the game, if you're interested, you can continue reading `mod/FORNAX.md` to learn about the game world, or `doc/manual.md` if you are curious about the core engine.

If you know a public server, you could also try chatting with other players there, whom you can see by listing the connected sessions.

```
> sessions
```

Write to the global chat with `say/;`, or to a specific user with `tell/,`

```
> ;is this the real life?
> ,<uname> is this just fantasy?
```
