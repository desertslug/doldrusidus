#include "filelog.h"

void filelog_init(FileLog* flog)
{
    assert(!LOGFILE);

    //Get the time in seconds since the epoch, convert it to tm
    time_t t = time(NULL);
    struct tm* tm = localtime(&t);

    //Construct log file name based on the current time
    char path[sizeof(LOGNAME_STR) + LOGTIME_CHARS + sizeof(LOGEXT_STR)];
    sprintf(path, LOGNAME_STR LOGTIME_FMT LOGEXT_STR,
        tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday,
        tm->tm_hour, tm->tm_min, tm->tm_sec);
    
    //Attempt to open logfile
#ifdef PLANCKLOG_FILE
    LOGFILE = fopen(path, "w");
#endif
    if(!LOGFILE){
        fprintf(stderr, "could not open log file, strerror: %s\n", strerror(errno));
        exit(1);
    }

    //Note the time of the last log file
    flog->tm = *tm;
}

void filelog_check_and_possibly_reopen(FileLog* flog)
{
    //Get the time in seconds since the epoch, convert it to tm
    time_t t = time(NULL);
    struct tm* tm = localtime(&t);

    //If the start time of the last log is distant enough
    if(distant_log_time(&flog->tm, tm)){

        filelog_reopen(flog, tm);
    }
}

void filelog_reopen(FileLog* flog, struct tm* tm)
{
    //Construct log file name based on the current time
    char path[sizeof(LOGNAME_STR) + LOGTIME_CHARS + sizeof(LOGEXT_STR)];
    sprintf(path, LOGNAME_STR LOGTIME_FMT LOGEXT_STR,
        tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday,
        tm->tm_hour, tm->tm_min, tm->tm_sec);

    //Attempt to reopen logfile under the new name
    FILE* new_logfile = fopen(path, "w");

    //If the new file could be opened successfully, change to it
    if(new_logfile){

        //Close the previous logfile
        fclose(LOGFILE);

        //Swap the new logfile for the old
#ifdef PLANCKLOG_FILE
        LOGFILE = new_logfile;
#endif
    }
    //If fopen fails, emit warning & keep using the old logfile
    else {
        LOG_WARN_("could not open log file, strerror: %s", strerror(errno));
    }

    //Note the time of the last log file
    flog->tm = *tm;
}

void filelog_close(FileLog* flog)
{
    //Close the logfile
    fclose(LOGFILE);
}

bool distant_log_time(struct tm* lhs, struct tm* rhs)
{
    return
        lhs->tm_year != rhs->tm_year ||
        lhs->tm_mon != rhs->tm_mon ||
        lhs->tm_mday != rhs->tm_mday;
}