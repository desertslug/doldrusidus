#ifndef HELPER_H
#define HELPER_H

#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdbool.h>
#include <math.h>
#include <string.h>
#include <signal.h>

#define PLANCKLOG_HEADER
#include "plancklog.h"

#include "proquint.h"

#define MIN(X, Y) ((X) < (Y) ? (X) : (Y))
#define MAX(X, Y) ((X) > (Y) ? (X) : (Y))

void assert_or_exit(bool cond, const char* fmt, ...);

void hash_to_string(char string[65], const uint8_t hash[32]);

int sign(int x);

int modulo(int a, int b);

int clamp(int x, int min, int max);

uint32_t clampu(uint32_t x, uint32_t min, uint32_t max);

float fclamp(float x, float min, float max);

//uint32_t lerpu(uint32_t min, uint32_t max, float);

size_t ceil_power_of_two(size_t x);

void serialize_hex(const uint8_t* src, size_t sz, char* dest);

void deserialize_hex(const char* src, size_t sz, uint8_t* dest);

size_t trailing_zero_bytes(const uint8_t* src, size_t sz);

int ipv4_from_dotdecimal_or_proquint(const char* str, uint8_t out[4]);

void port_str_to_int_or_raise(const char* src, uint16_t* dest);

#endif /* HELPER_H */
