#include "msg.h"

bool msg_pkt_chunk_u16_single(void* pkt, uint8_t* chunk, uint16_t* out)
{
    if(!pkt_chunk_check(pkt, chunk)){ return false; }
    if(pkt_chunk_size(pkt, chunk) != sizeof(uint16_t)){ return false; }
    if(pkt_chunk_type(pkt, chunk) != PKT_CHUNK_U16){ return false; }

    uint16_t* data = (uint16_t*)pkt_chunk_data(pkt, chunk);

    *out = ntohs(*data);

    return true;
}

bool msg_pkt_chunk_u32_single(void* pkt, uint8_t* chunk, uint32_t* out)
{
    if(!pkt_chunk_check(pkt, chunk)){ return false; }
    if(pkt_chunk_size(pkt, chunk) != sizeof(uint32_t)){ return false; }
    if(pkt_chunk_type(pkt, chunk) != PKT_CHUNK_U32){ return false; }

    uint32_t* data = (uint32_t*)pkt_chunk_data(pkt, chunk);

    *out = ntohl(*data);

    return true;
}

bool msg_pkt_chunk_ztprintascii(void* pkt, uint8_t* chunk, size_t min, size_t max, char** out)
{
    if(!pkt_chunk_check(pkt, chunk)){ return false; }
    if(pkt_chunk_size(pkt, chunk) < min){ return false; }
    if(pkt_chunk_size(pkt, chunk) > max){ return false; }
    if(pkt_chunk_size(pkt, chunk) == 0){ return false; }
    if(pkt_chunk_type(pkt, chunk) != PKT_CHUNK_U8){ return false; }

    uint8_t* data = pkt_chunk_data(pkt, chunk);

    //Find the first non-printable-ascii character
    size_t size = pkt_chunk_size(pkt, chunk);
    int i = 0; while(i < size - 1 && (data[i] >= 32 && data[i] <= 127)){ ++i; }

    //If the search stopped at the last character,
    //and the last character is a zero terminator
    if(i == size - 1 && data[i] == '\0'){
        *out = (char*)pkt_chunk_data(pkt, chunk);
        return true;
    } else {
        return false;
    }
}

bool msg_pkt_chunk_u8_minmax(void* pkt, uint8_t* chunk, size_t min, size_t max, uint8_t** out)
{
    if(!pkt_chunk_check(pkt, chunk)){ return false; }
    if(pkt_chunk_size(pkt, chunk) < min){ return false; }
    if(pkt_chunk_size(pkt, chunk) > max){ return false; }
    if(pkt_chunk_type(pkt, chunk) != PKT_CHUNK_U8){ return false; }

    uint8_t* data = pkt_chunk_data(pkt, chunk);
    *out = data;

    return true;
}