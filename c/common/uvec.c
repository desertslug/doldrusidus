#include "uvec.h"

/*
unsigned int rtcp_ceil_power_of_two(unsigned int x)
{
    int n = 0;
    while(x > 1){ ++n; x >>= 1; }
    return x << (n + 1);
}
*/

void rtcp_uvec_init(rtcp_uvec_t* uvec)
{
    /* Initialize values */
    uvec->size = 0;
    uvec->callback = NULL;
    uvec->user = NULL;
}

void rtcp_uvec_cb(rtcp_uvec_t* uvec, fn_idx_update_t callback, void* user)
{
    /* Initialize values */
    uvec->callback = callback;
    uvec->user = user;
}

int rtcp_uvec_malloc(rtcp_uvec_t* uvec, int capacity, int typesize)
{
    /* Store parameters */
    uvec->capacity = capacity;
    rtcp_assert_always(uvec->capacity > 0, "rtcp_uvec_malloc: capacity > 0\n");
    uvec->typesize = typesize;
    rtcp_assert_always(uvec->typesize > 0, "rtcp_uvec_malloc: typesize > 0\n");

    /* Initialize values */
    uvec->targetcapa = uvec->capacity;

    /* Allocate memory */
    uvec->values = malloc(uvec->capacity * uvec->typesize);
    if(!uvec->values){ return errno; }

    return 0;
}

void rtcp_uvec_free(rtcp_uvec_t* uvec)
{
    /* Free memory */
    assert(uvec->values); free(uvec->values);
}

int rtcp_uvec_realloc(rtcp_uvec_t* uvec, int newcapa)
{
    void* newvalues;

    /* Attempt to change the size of the allocated memory block */
    newvalues = realloc(uvec->values, newcapa * uvec->typesize);
    if(!newvalues){ return errno; }

    /* Attach block if allocation succeeded */
    uvec->values = newvalues;

    /* Note new capacity */
    uvec->capacity = newcapa;

    return 0;
}

void rtcp_uvec_tick(rtcp_uvec_t* uvec)
{
    /* Decrease target capacity until a lower bound */
    uvec->targetcapa = uvec->targetcapa > 1 ?
        uvec->targetcapa - 1 : uvec->targetcapa;
}

void rtcp_uvec_use_size(rtcp_uvec_t* uvec)
{
    int cpot;

    assert(uvec->targetcapa > 0);

    cpot = rtcp_ceil_power_of_two(
        uvec->size > uvec->targetcapa - 1 ? uvec->size : uvec->targetcapa - 1);

    /* Set target capacity to indicate usage */
    uvec->targetcapa = cpot > 0 ? cpot : 1;
}

int rtcp_uvec_shrink(rtcp_uvec_t* uvec)
{
    int ret;
    int cpot;

    cpot = rtcp_ceil_power_of_two(uvec->size);

    assert(uvec->targetcapa > 0);
    assert(uvec->capacity > 0);

    /* If the target capacity less-or-equal than the next shrink */
    if(uvec->targetcapa <= uvec->capacity / 2){
        
        /* Reallocate with the log2-ceiling of the target */
        ret = rtcp_uvec_realloc(uvec, cpot > 0 ? cpot : 1);
        if(ret){ return ret; }
    }

    assert(uvec->capacity > 0);
    assert(uvec->capacity >= uvec->size);

    return 0;
}

int rtcp_uvec_add(rtcp_uvec_t* uvec, void* val)
{
    int ret;

    /* If the new size exceeds the current capacity, reallocate */
    if(uvec->size + 1 > uvec->capacity){
        ret = rtcp_uvec_realloc(uvec, uvec->capacity * 2);
        if(ret){ return ret; }
    }
    assert(uvec->size + 1 <= uvec->capacity);

    /* New size */
    ++uvec->size;

    /* Write value */
    if(val){
        memcpy(
            ((uint8_t*)uvec->values) + (uvec->size - 1) * uvec->typesize,
            val,
            uvec->typesize
        );
    }

    /* Value update callback */
    if(uvec->callback){
        uvec->callback(
            ((uint8_t*)uvec->values) + (uvec->size - 1) * uvec->typesize,
            (uvec->size - 1),
            uvec->user
        );
    }

    /* Use the current size, shrink if necessary */
    rtcp_uvec_use_size(uvec);
    rtcp_uvec_shrink(uvec);

    return 0;
}

int rtcp_uvec_del(rtcp_uvec_t* uvec, int idx)
{
    rtcp_assert_always(idx >= 0 && idx < uvec->size,
        "rtcp_uvec_del: invalid index\n");

    /* New size */
    --uvec->size;

    /* If the vector is non-empty after this delete */
    if(uvec->size > 0 && idx != uvec->size){

        /* Write last value into the deleted */
        memcpy(
            ((uint8_t*)uvec->values) + idx * uvec->typesize,
            ((uint8_t*)uvec->values) + uvec->size * uvec->typesize,
            uvec->typesize
        );

        /* Value update callback */
        if(uvec->callback){
            uvec->callback(
                ((uint8_t*)uvec->values) + idx * uvec->typesize,
                idx,
                uvec->user
            );
        }
    }

    /* Use the current size, shrink if necessary */
    rtcp_uvec_use_size(uvec);
    rtcp_uvec_shrink(uvec);
    
    return 0;
}

void* rtcp_uvec_get(rtcp_uvec_t* uvec, int idx)
{
    rtcp_assert_always(idx >= 0 && idx < uvec->size,
        "rtcp_uvec_get: invalid index\n");

    return ((uint8_t*)uvec->values) + idx * uvec->typesize;
}

int rtcp_uvec_pop(rtcp_uvec_t* uvec, int idx)
{
    rtcp_assert_always(idx >= 0 && idx < uvec->size,
        "rtcp_uvec_pop: invalid index\n");

    /* Move all higher values down */
    memmove(
        ((uint8_t*)uvec->values) + idx * uvec->typesize,
        ((uint8_t*)uvec->values) + (idx + 1) * uvec->typesize,
        uvec->typesize * (uvec->size - idx - 1)
    );

    /* New size */
    --uvec->size;

    /* Value update callback */
    if(uvec->callback){
        for(int i = idx; i < uvec->size; ++i){
            uvec->callback(
                ((uint8_t*)uvec->values) + i * uvec->typesize,
                i,
                uvec->user
            );
        }
    }

    /* Use the current size, shrink if necessary */
    rtcp_uvec_use_size(uvec);
    rtcp_uvec_shrink(uvec);

    return 0;
}

void rtcp_uvec_insertsort(rtcp_uvec_t* uvec, fn_cmp_t cmp, void* swap)
{
	rtcp_assert_always(cmp, "rtcp_uvec_insertsort: invalid comparison\n");

	uint8_t* vs = (uint8_t*)uvec->values;
	int ts = uvec->typesize;

	/* The single-element prefix is trivially sorted */
	int i = 1;
	while(i < uvec->size){

		/* Move the i-th element to its correct position */
		int j = i;
		while(
			j > 0 &&
			cmp(vs + (j - 1) * ts, vs + j * ts) > 0
		){
			/* Swap elements */
			memcpy(swap, vs + (j - 1) * ts, ts);
			memcpy(vs + (j - 1) * ts, vs + j * ts, ts);
			memcpy(vs + j * ts, swap, ts);

			--j;
		}

		++i;
	}
}
