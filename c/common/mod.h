#ifndef MOD_H
#define MOD_H

#include <dlfcn.h>

//#include "defs.h"
//#include "cello.h"
#include "helper.h"
#include "lstr.h"

#define PLANCKLOG_HEADER
#include "plancklog.h"
/*
typedef void mod_dlsym_fun(void* lib, void* user);
*/
void* dlsym_checked(void* lib, const char* name);

int mod_compile(const char* cc, const char* c_args, const char* path);

void* mod_load(const char* path);

int mod_unload(void* handle);

#endif /* MOD_H */