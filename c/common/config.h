#ifndef CONFIG_H
#define CONFIG_H

#include "srp.h"

#define CONFIG_FILEPATH "res/config.json"

//

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "json.h"
#define JSMN_HEADER
#include "jsmn.h"

/*#include "defs.h"
#include "constants.h"*/

//#define TRANSPORT_CONFSTR "transport"
#define IPV4ADDRESS_CONFSTR "ipv4address"
#define PORT_CONFSTR "port"
/*#define PORT2_CONFSTR "port2"
*/
#define HEARTBEAT_EXPIRE_CONFSTR "heartbeat_expire_msec"
#define HEARTBEAT_RECV_CONFSTR "heartbeat_recv_msec"
#define HEARTBEAT_SEND_CONFSTR "heartbeat_send_msec"
#define SESSION_DRAIN_MSEC_CONFSTR "session_drain_msec"
/*
#define USERINPUT_MAX_CONFSTR "userinput_max"
#define MSGSIZE_MAX_CONFSTR "msgsize_max"
#define FILESIZE_MAX_CONFSTR "filesize_max"
*/
#define CC_PATH_CONFSTR "cc_path"
#define C_ARGS_CONFSTR "c_args"

#define SRV_MOD_PATH_CONFSTR "srv_mod_path"
#define SRV_AUTH_PATH_CONFSTR "srv_auth_path"
/*
#define SRV_THREADS_CONFSTR "srv_threads"
*/
#define SRV_TPS_CONFSTR "srv_tps"
#define SRV_TPS_PASS_CONFSTR "srv_tps_pass"
#define SRV_REGISTER_LIMIT_CONFSTR "srv_register_limit"
#define SRV_SESSION_LIMIT_CONFSTR "srv_session_limit"
#define SRV_USER_SESSION_LIMIT_CONFSTR "srv_user_session_limit"
/*
#define SRV_LOGIN_LIMIT_CONFSTR "srv_login_limit"
*/
#define SRV_ENTITY_MAX_CONFSTR "srv_entity_max"
/*
#define SRV_REQREP_PER_TICK_CONFSTR "srv_reqrep_per_tick"
*/
#define CLI_CONN_WAIT_MSEC_CONFSTR "cli_conn_wait_msec"
#define CLI_MOD_PATH_CONFSTR "cli_mod_path"
#define CLI_FPS_CONFSTR "cli_fps"
/*
#define CLI_TPS_CONFSTR "cli_tps"
#define CLI_HISTORY_CONFSTR "cli_history"
*/
#define CLI_WINDOW_RESIZABLE_CONFSTR "cli_window_resizable"
#define CLI_SWAP_INTERVAL_CONFSTR "cli_swap_interval"

enum { CONFIG_MEMBERS = 21, }; //WARNING: UPKEEP

typedef struct StartConfig
{
    char transport[JSON_MAX_STRING];
    char ipv4address[JSON_MAX_STRING];
    char port[JSON_MAX_STRING];
    char port2[JSON_MAX_STRING];
    int heartbeat_expire_msec;
    int heartbeat_recv_msec;
    int heartbeat_send_msec;
    int session_drain_msec;
    int userinput_max;
    int msgsize_max;
    int filesize_max;

    char cc_path[JSON_MAX_STRING];
    char c_args[JSON_MAX_STRING];
    
    char srv_mod_path[JSON_MAX_STRING];
    char srv_auth_path[JSON_MAX_STRING];
    int srv_threads;
    int srv_tps;
    int srv_tps_pass;
    int srv_tick_nsec;
    int srv_tick_usec;
    int srv_register_limit;
    int srv_session_limit;
    int srv_user_session_limit;
    int srv_login_limit;
    int srv_entity_max;
    int srv_reqrep_per_tick;

    int cli_conn_wait_msec;
    char cli_mod_path[JSON_MAX_STRING];
    int cli_fps;
    int cli_tps;
    int cli_frame_nsec;
    int cli_history;
    int cli_window_hor;
    int cli_window_ver;
    bool cli_window_resizable;
    int cli_swap_interval;

} StartConfig;

void startconfig_load(StartConfig* config, const char* path);

void startconfig_default(StartConfig* config);

void startconfig_propagate_values(StartConfig* config);

void startconfig_print(StartConfig* config);

void startconfig_init_invocations(StartConfig* config, JSONKeyInvocation* invoc);

int startconfig_validate_from_json(void* data, const char* json, jsmntok_t* tokens, int tok_count);

void start_config_parse_validated(void* data, const char* json, jsmntok_t* tokens, int tok_count);

#endif /* CONFIG_H */