#include "nanosleep.h"

#ifdef OS_WIN

int nanoschleep(const struct timespec* requested_delay, struct timespec* remaining_delay)
{
    static bool initialized;

    //Number of performance counter increments per nanosecond
    static double ticks_per_nanosec;
    if(!initialized){
        LARGE_INTEGER ticks_per_sec;
        if(QueryPerformanceFrequency(&ticks_per_sec)){
            ticks_per_nanosec = (double) ticks_per_sec.QuadPart / 1e9;
        }
        initialized = true;
    }

    //If QueryPerformanceFrequency worked, we can use QueryPerformanceCounter
    if(ticks_per_nanosec){

        //Number of milliseconds to pass to Sleep()
        int sleep_millis = (int) requested_delay->tv_nsec / 1000000 -
                           WIN_SLEEP_CONFIDENCE_MILLISEC;
        
        //Number of ticks to delay
        LONGLONG wait_ticks = requested_delay->tv_nsec * ticks_per_nanosec;

        //Performance counter before waiting has commenced
        LARGE_INTEGER counter_before;
        QueryPerformanceCounter(&counter_before);

        //Use Sleep() for the longer part of the delay
        if(sleep_millis > 0){ Sleep(sleep_millis); }

        //Performance counter after each shorter wait happens
        LARGE_INTEGER counter_after;

        //Periodically yield for the shorter part of the delay
        LONGLONG yield_until = counter_before.QuadPart + wait_ticks - WIN_YIELD_CONFIDENCE_MILLISEC * 1e6 * ticks_per_nanosec;
        QueryPerformanceCounter(&counter_after);
        while(counter_after.QuadPart < yield_until){
            Sleep(0);
            QueryPerformanceCounter(&counter_after);
        }

        //Busy wait for the shortest part of the delay
        LONGLONG wait_until = counter_before.QuadPart + wait_ticks;
        QueryPerformanceCounter(&counter_after);
        while(counter_after.QuadPart < wait_until){
            QueryPerformanceCounter(&counter_after);
        }
    }
    //Otherwise, use Sleep() as a fallback method
    else {
        Sleep(requested_delay->tv_nsec / 1000000);
    }

    //Sleep is not unterruptible, there is no remaining delay
    if(remaining_delay != NULL){
        remaining_delay->tv_sec = remaining_delay->tv_nsec = 0;
    }

    return 0;
}

#endif /* OS_WIN */

#ifdef OS_NIX

int diff_msec(const struct timespec* start, const struct timespec* end)
{
    if(start->tv_sec - end->tv_sec){
        return 1000 * (end->tv_sec - start->tv_sec - 1) +
               1000 + (end->tv_nsec - start->tv_nsec) / 1e6;
    } else {
        return (end->tv_nsec - start->tv_nsec) / 1e6;
    }
}

int diff_nsec(const struct timespec* start, const struct timespec* end)
{
    if(start->tv_sec - end->tv_sec){
        return 1000000000 * (end->tv_sec - start->tv_sec - 1) +
               1000000000 + end->tv_nsec - start->tv_nsec;
    } else {
        return end->tv_nsec - start->tv_nsec;
    }
}

struct timespec timespec_norm(const struct timespec* spec)
{
    return (struct timespec){
        spec->tv_sec  + (spec->tv_nsec / 1000000000),
        spec->tv_nsec - (spec->tv_nsec / 1000000000) * 1000000000
    };
}

struct timespec timespec_add(const struct timespec* start, const struct timespec* end)
{
    time_t start_sec_from_nsec = start->tv_nsec / 1000000000;
    time_t end_sec_from_nsec   = end->tv_nsec / 1000000000;
    
    return (struct timespec){
        end->tv_sec + start->tv_sec +
        end_sec_from_nsec + start_sec_from_nsec,
        (end->tv_nsec - end_sec_from_nsec * 1000000000) +
        (start->tv_nsec - start_sec_from_nsec * 1000000000)
    };
}

struct timespec timespec_diff(const struct timespec* start, const struct timespec* end)
{
    struct timespec start_norm = timespec_norm(start);
    struct timespec end_norm   = timespec_norm(end);

    time_t sec_diff = end_norm.tv_sec - start_norm.tv_sec;

    return (struct timespec){
        sec_diff - (sec_diff > 0),
        (sec_diff > 0) * 1000000000 + (end_norm.tv_nsec - start_norm.tv_nsec)
    };
}

bool timespec_less(const struct timespec* start, const struct timespec* end)
{
	return 1 - (
		(end->tv_sec <= start->tv_sec) *
		(end->tv_nsec <= start->tv_nsec)
	);
}

float timespec_float(const struct timespec* spec)
{
    return spec->tv_sec + spec->tv_nsec / (float)1e9;
}

int nanoschleep(const struct timespec* requested_delay, struct timespec* remaining_delay)
{
    //Note the starting time point
    struct timespec start_time;
    clock_gettime(CLOCK_REALTIME, &start_time);

    //Call standard nanosleep within the confidence interval
    if(requested_delay->tv_nsec * 1e6 > NIX_SLEEP_CONFIDENCE_MILLISEC){
        struct timespec request = (struct timespec)
            { .tv_sec = 0,
              .tv_nsec = requested_delay->tv_nsec -
                         (int) (NIX_SLEEP_CONFIDENCE_MILLISEC * 1e6) };
        nanosleep(&request, NULL);
    }
    
    //Current point in time, and the remaining length of time
    struct timespec curr_time;
    int diff;
    clock_gettime(CLOCK_REALTIME, &curr_time);
    diff = diff_nsec(&start_time, &curr_time);

    //Let go of timeslices for the yield confidence interval
    int slices = 0;
    int yield_delay_nsec = requested_delay->tv_nsec - NIX_YIELD_CONFIDENCE_MILLISEC * 1e6;
    while (diff < yield_delay_nsec){
        sched_yield(); ++slices;
        clock_gettime(CLOCK_REALTIME, &curr_time);
        diff = diff_nsec(&start_time, &curr_time);
    };

    //Busy wait for the remaining confidence interval
    while (diff < requested_delay->tv_nsec){
        clock_gettime(CLOCK_REALTIME, &curr_time);
        diff = diff_nsec(&start_time, &curr_time);
    };

    return 0;
}

#endif /* OS_NIX */
