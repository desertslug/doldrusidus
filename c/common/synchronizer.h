#ifndef SYNCHRONIZER_H
#define SYNCHRONIZER_H

#include <stdio.h>
#include <time.h>

#define PLANCKLOG_HEADER
#include "plancklog.h"

#include "nanosleep.h"
#include "defs.h"

typedef struct Synchronizer
{
    struct timespec last;
    struct timespec delta;
    struct timespec period;
} Synchronizer;

void synchronizer_init(Synchronizer* sync, time_t period_nsec);
void synchronizer_wait(Synchronizer* sync);
void synchronizer_step(Synchronizer* sync);

#endif /* SYNCHRONIZER_H */