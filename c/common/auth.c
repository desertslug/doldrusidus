#include "auth.h"

void auth_alloc(Auth* auth)
{
    rtcp_uvec_malloc(&auth->users, 1, sizeof(User));
    rtcp_uvec_init(&auth->users);
}

void auth_dealloc(Auth* auth)
{
    rtcp_uvec_free(&auth->users);
}

void auth_read(Auth* auth, const char* path)
{
    //Attempt to open the authentication file into a csv
    Csv csv;
    char** fields;
    assert_or_exit(csv_open(&csv, path, ',', AUTH_CSV_COLS) == 0,
        "auth_read: failed to open authentication file: %s", path);

    //Throw away any previous user data
    rtcp_uvec_init(&auth->users);

    //Read lines successively
    int ret = 0;
    while((ret = csv_read_next(&csv, &fields)) == CSV_OK){

        User user = {0};
        strcpy(user.uname, fields[0]);
        sscanf(fields[1], "%u", &user.access);
        deserialize_hex(fields[2], SRP_SALT_HEX_BYTES, user.salt);
        deserialize_hex(fields[3], SRP_VERIFIER_HEX_BYTES, user.verifier);

        rtcp_uvec_add(&auth->users, &user);
    }

    //Close file
    if(csv_close(&csv)){
        LOG_WARN_("could not close file descriptor, continuing...");
    }
}

void auth_write(Auth* auth, const char* path)
{
    //Attempt to open the authentication file into a csv
    Csv csv;
    char* fields[AUTH_CSV_COLS];
    char** fields_in = fields;
    assert_or_exit(csv_open(&csv, path, ',', AUTH_CSV_COLS) == 0,
        "auth_write: failed to open authentication file: %s", path);

    //Clear CSV file
    csv_clear(&csv);

    //Write authentication info out into the file for every user
    for(int i = 0; i < auth->users.size; ++i){
        User* user = rtcp_uvec_get(&auth->users, i);
        
        //Construct string representation
        char uname[UNAME_MAX];
        char access[ACCESS_MAX];
        char salt[SRP_SALT_HEX_BYTES + 1];
        char verifier[SRP_VERIFIER_HEX_BYTES + 1];
        strcpy(uname, user->uname);
        sprintf(access, "%u", user->access);
        serialize_hex(user->salt, SRP_SALT_BYTES, salt);
        salt[SRP_SALT_HEX_BYTES] = '\0';
        serialize_hex(user->verifier, SRP_VERIFIER_BYTES, verifier);
        verifier[SRP_VERIFIER_HEX_BYTES] = '\0';
        fields[0] = &uname[0];
        fields[1] = &access[0];
        fields[2] = &salt[0];
        fields[3] = &verifier[0];
        csv_append(&csv, &fields_in);
    }

    //Close file
    if(csv_close(&csv)){
        LOG_WARN_("could not close file descriptor, continuing...");
    }
}

void auth_add(Auth* auth, const char* uname, const unsigned char* s, const unsigned char* v)
{
    //Callee should ensure username does not exist yet
    assert_or_exit(auth_find_user(auth, uname, NULL) < 0,
        "auth_add: username already exists: %s", uname);

    //Append a new user authentication
    User user = { 0 };
    strcpy(user.uname, uname);
    user.access = AccessGuest;
    memcpy(user.salt, s, SRP_SALT_BYTES);
    memcpy(user.verifier, v, SRP_VERIFIER_BYTES);
    
    rtcp_uvec_add(&auth->users, &user);
}

void auth_remove(Auth* auth, const char* uname)
{
    //Callee should ensure username exists
    assert_or_exit(auth_find_user(auth, uname, NULL) >= 0,
        "auth_remove: username not found: %s", uname);

    //Find the entry with the parameter username, will succeed
    User* user = NULL;
    int idx = auth_find_user(auth, uname, &user);
    assert_or_exit(user && idx >= 0,
        "auth_remove: expected to find user, but failed to do so");

    //Remove user
    rtcp_uvec_del(&auth->users, idx);
}

int auth_find_user(Auth* auth, const char* uname, User** out)
{
    int j = 0;
    while(j < auth->users.size){

        User* user = rtcp_uvec_get(&auth->users, j);
        if(strcmp(user->uname, uname) == 0){ break; }
        ++j;
    }

    if(j < auth->users.size){
        if(out){ *out = rtcp_uvec_get(&auth->users, j); }
        return j;
    } else {
        return -1;
    }
}
