#ifndef RTCP_UVEC_H
#define RTCP_UVEC_H

#include <stddef.h>
#include <stdint.h>
#include <assert.h>
#include <errno.h>

#include "helper.h"

#define rtcp_assert_always assert_or_exit
#define rtcp_ceil_power_of_two ceil_power_of_two

/** User-defined index update function.
  * Useful when two values are associated in two different data structures,
  * when they need to know the key/position of the other value in the
  * associated structure at all times.
  \param value stored element whose index was updated
  \param index the updated index of the element
  \param user optional user pointer
 */
typedef void (*fn_idx_update_t)(void* value, int index, void* user);

//unsigned int rtcp_ceil_power_of_two(unsigned int x);

/** Generic unordered vector data structure.
  * 
  * A user-defined callback function will be invoked whenever a value is moved
  * into a new position in the vector.
  * 
  * Expands its memory pool as required, but shrinks it lazily.
*/
typedef struct rtcp_uvec_t
{
    /** Currently allocated capacity. */
    int capacity;
    /** Dynamically shrinking/expanding capacity. */
    int targetcapa;
    /** Currently used capacity. */
    int size;
    /** Size of the stored type, in bytes. */
    int typesize;
    /** Stored values. */
    void* values;
    /** User-defined value index update callback. */
    fn_idx_update_t callback;
    /** User pointer for the index update callback. */
    void* user;
} rtcp_uvec_t;

void rtcp_uvec_init(rtcp_uvec_t* uvec);
void rtcp_uvec_cb(rtcp_uvec_t* uvec, fn_idx_update_t callback, void* user);
int rtcp_uvec_malloc(rtcp_uvec_t* uvec, int capacity, int typesize);
void rtcp_uvec_free(rtcp_uvec_t* uvec);
int rtcp_uvec_realloc(rtcp_uvec_t* uvec, int newcapa);

void rtcp_uvec_tick(rtcp_uvec_t* uvec);
void rtcp_uvec_use_size(rtcp_uvec_t* uvec);
int rtcp_uvec_shrink(rtcp_uvec_t* uvec);

int rtcp_uvec_add(rtcp_uvec_t* uvec, void* val);
int rtcp_uvec_del(rtcp_uvec_t* uvec, int idx);
void* rtcp_uvec_get(rtcp_uvec_t* uvec, int idx);

int rtcp_uvec_pop(rtcp_uvec_t* uvec, int idx);

typedef int (*fn_cmp_t)(void* a, void* b);
void rtcp_uvec_insertsort(rtcp_uvec_t* uvec, fn_cmp_t cmp, void* swap);

#endif /* RTCP_UVEC_H */
