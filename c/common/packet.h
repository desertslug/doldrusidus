#ifndef PACKET_H
#define PACKET_H

#define PLANCKLOG_HEADER
#include "plancklog.h"

#include "enet/enet.h"

#include "helper.h"

typedef enum {
    PKT_CHUNK_U8 = 0,
    PKT_CHUNK_U16,
    PKT_CHUNK_U32,
} PKT_CHUNK_TYPE;

size_t pkt_len(void* pkt);
uint8_t* pkt_data(void* pkt);
uint32_t pkt_flags(void* pkt);
int pkt_resize(void* pkt, size_t size);
void* pkt_create(void* data, size_t size, uint32_t flags);
void pkt_destroy(void* pkt);

void pkt_log(void* pkt);

size_t pkt_chunk_bytes(void* pkt, uint8_t* chunk);
uint32_t pkt_chunk_size(void* pkt, uint8_t* chunk);
uint32_t pkt_chunk_type(void* pkt, uint8_t* chunk);
uint8_t* pkt_chunk_data(void* pkt, uint8_t* chunk);

bool pkt_chunk_check(void* pkt, uint8_t* chunk);

void pkt_chunk_add(void* pkt, uint8_t* data, uint32_t size, uint32_t type);

#endif /* PACKET_H */