#include "synchronizer.h"

void synchronizer_init(Synchronizer* sync, time_t period_nsec)
{
    //Set last synchronized time as the current time
    clock_gettime(CLOCK_REALTIME, &sync->last);

    //Store period
    sync->period = (struct timespec){ 0, period_nsec };
}

void synchronizer_wait(Synchronizer* sync)
{
    //Get current time
    struct timespec now; clock_gettime(CLOCK_REALTIME, &now);
    //LOG_("[now : %li s, %f ms]", now.tv_sec, now.tv_nsec / 1e6);

    //The time that elapsed since the last wakeup
    //struct timespec between = timespec_diff(&sync->last, &now);
    //LOG_("[between: %li s, %li ns]", between.tv_sec, between.tv_nsec);
    //LOG_("[between: %li s, %li us]", between.tv_sec, between.tv_nsec / 1000);

    //Get synchronization timepoint
    struct timespec next = timespec_add(&sync->last, &sync->period);
    //LOG_("[next: %li s, %f ms]", next.tv_sec, next.tv_nsec / 1e6);

    //If the timepoint was not yet passed, wait until then
    if(timespec_less(&now, &next)){
        struct timespec want = timespec_diff(&now, &next);
        //LOG_("[want: %li s, %f ms]", want.tv_sec, want.tv_nsec / 1e6);
        nanoschleep(&want, NULL);
    }
}

void synchronizer_step(Synchronizer* sync)
{
    //Note delta time
    struct timespec after; clock_gettime(CLOCK_REALTIME, &after);
    sync->delta = timespec_diff(&sync->last, &after);

    //Set the next timepoint as the last
    //sync->last = next;

    //Set the timepoint after the wait as the last
    sync->last = after;
}