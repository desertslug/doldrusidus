#include "lstr.h"

void lstr_init(lstr_t* lstr)
{
    /* Initialize values */
    lstr->size = 0;

    /* Initialize storage */
    char* str = lstr_get_mut(lstr);
    str[lstr->size] = '\0';
}

int lstr_malloc(lstr_t* lstr, int capacity)
{
    /* Store parameters */
    lstr->capacity = capacity;
    assert_or_exit(lstr->capacity > 0, "lstr_malloc: capacity > 0\n");

    /* Allocate memory */
    lstr->values = malloc(lstr->capacity * sizeof(char));
    if(!lstr->values){ return errno; }

    return 0;
}

void lstr_free(lstr_t* lstr)
{
    /* Free memory */
    assert(lstr->values); free(lstr->values);
}

int lstr_realloc(lstr_t* lstr, int newcapa)
{
    void* newvalues;

    assert_or_exit(newcapa > 0,
        "lstr_realloc: newcapa > 0");

    /* pot size to accomodate old and new capacities */
    int oldpot = ceil_power_of_two(lstr->capacity - 1);
    int newpot = ceil_power_of_two(newcapa);

    /* if the two sizes match, no need to reallocate */
    if(oldpot == newpot){ return 0; }

    /* Attempt to change the size of the allocated memory block */
    newvalues = realloc(lstr->values, newpot * sizeof(char));
    if(!newvalues){ return errno; }

    /* Attach block if allocation succeeded */
    lstr->values = newvalues;

    /* Note new capacity */
    lstr->capacity = newpot;

    return 0;
}

int lstr_len(lstr_t* lstr)
{
    return lstr->size;
}

const char* lstr_get(lstr_t* lstr)
{
    return lstr->values;
}

char* lstr_get_mut(lstr_t* lstr)
{
    return lstr->values;
}

int lstr_cat_str(lstr_t* lstr, const char* s)
{
    return lstr_cat_strn(lstr, s, strlen(s));
}

int lstr_cat_strn(lstr_t* lstr, const char* s, int slen)
{
    //Reallocate
    int ret = lstr_realloc(lstr, lstr->size + slen + 1);
    if(ret){ return ret; }

    memcpy(lstr_get_mut(lstr) + lstr->size, s, slen);
    lstr->size += slen;
    lstr_get_mut(lstr)[lstr->size] = '\0';

    return 0;
}

int lstr_insert_str(lstr_t* lstr, int pos, const char* s)
{
    assert_or_exit(pos >= 0 && pos <= lstr_len(lstr),
        "lstr_insert_str: pos invalid");
    int slen = strlen(s);

    //Reallocate
    int ret = lstr_realloc(lstr, lstr->size + slen + 1);
    if(ret){ return ret; }

    memmove(
        lstr_get_mut(lstr) + pos + slen,
        lstr_get_mut(lstr) + pos,
        lstr->size - pos);
    memcpy(lstr_get_mut(lstr) + pos, s, slen);
    lstr->size += slen;
    lstr_get_mut(lstr)[lstr->size] = '\0';

    return 0;
}

int lstr_del_pos(lstr_t* lstr, int pos)
{
    assert_or_exit(pos >= 0 && pos < lstr_len(lstr),
        "lstr_del_pos: pos invalid");

    memmove(
        lstr_get_mut(lstr) + pos,
        lstr_get_mut(lstr) + pos + 1,
        lstr_len(lstr) - pos - 1);

    //Reallocate
    int ret = lstr_realloc(lstr, lstr->size);
    if(ret){ return ret; }

    --lstr->size;
    lstr_get_mut(lstr)[lstr->size] = '\0';

    return 0;
}

int lstr_clear(lstr_t* lstr)
{
    //Reallocate
    int ret = lstr_realloc(lstr, 1);
    if(ret){ return ret; }

    lstr_init(lstr);

    return 0;
}