#ifndef MSG_H
#define MSG_H

#include "packet.h"
#include "uvec.h"

#define WHITESPACE " \t"

enum { CMD_LEN_MAX = 32, };

typedef enum {
    MSG_LOGIN = 0,
    MSG_VERIFY,
    MSG_LOGOUT,
    MSG_CHANGEPW,
    MSG_KICK,
    MSG_REGISTER,
    MSG_UNREGISTER,
    MSG_CHANGEACC,
    MSG_GETAUTHS,
    MSG_GETSESSIONS,
    MSG_SETTPS,
    MSG_SETTPSPASS,
    MSG_LOG_REOPEN,
    MSG_CHATGLOBAL,
    MSG_CHATWHISPER,
    MSG_COUNT,
} MSG_TYPE;

typedef struct Command
{
    char name[CMD_LEN_MAX];
    char shortname[CMD_LEN_MAX];
} Command;

bool msg_pkt_chunk_u16_single(void* pkt, uint8_t* chunk, uint16_t* out);

bool msg_pkt_chunk_u32_single(void* pkt, uint8_t* chunk, uint32_t* out);

bool msg_pkt_chunk_ztprintascii(void* pkt, uint8_t* chunk, size_t min, size_t max, char** out);

bool msg_pkt_chunk_u8_minmax(void* pkt, uint8_t* chunk, size_t min, size_t max, uint8_t** out);

#endif /* MSG_H */