#include "mod.h"

void* dlsym_checked(void* lib, const char* name)
{
    //Attempt to load the function
    void* ret = dlsym(lib, name);

    //Check if there was an error
    char* err = NULL;
    if((err = dlerror())){
        LOG_ERROR_("dlsym_checked: dlerror() was not NULL");
        LOG_ERROR_("dlerror(): %s", err);
        exit(1);
    }

    //Return handle to loaded function
    return ret;
}

int mod_compile(const char* cc, const char* c_args, const char* path)
{
    //Compose command to be executed
    lstr_t cmd; lstr_malloc(&cmd, 1); lstr_init(&cmd);
    lstr_cat_str(&cmd, cc); lstr_cat_str(&cmd, " ");
    lstr_cat_str(&cmd, c_args); lstr_cat_str(&cmd, " ");
    lstr_cat_str(&cmd, path);
    lstr_cat_str(&cmd, ".c ");
    lstr_cat_str(&cmd, "-o ");
    lstr_cat_str(&cmd, path);
    lstr_cat_str(&cmd, ".so ");
    lstr_cat_str(&cmd, "-lm");

    //Invoke compiler
    LOG_("%s", lstr_get(&cmd));
    int ret = system(lstr_get(&cmd));

    //Handle return
    switch(ret){
    case -1: {
        LOG_ERROR_("child process could not be created"
            ", or its status could not be retrieved");
        LOG_ERROR_("strerror(errno): %s", strerror(errno));
    } break;
    case 127: {
        LOG_ERROR_("shell could not be executed in child process");
    } break;
    default: {
        if(ret){ LOG_ERROR_("received nonzero status: %d", ret); }
    } break;
    }

    lstr_free(&cmd);
    return ret;
}

void* mod_load(const char* path)
{
    //Compose path to load the mod from
    lstr_t lib; lstr_malloc(&lib, 1); lstr_init(&lib);
    lstr_cat_str(&lib, path); lstr_cat_str(&lib, ".so");

    //Open dynamic library
    LOG_("%s", lstr_get(&lib));
    void* ret = dlopen(lstr_get(&lib), RTLD_NOW);

    //Handle return
    if(ret == NULL){
        LOG_ERROR_("mod_load: dlopen() returned NULL");
        LOG_ERROR_("dlerror(): %s", dlerror());
    }

    lstr_free(&lib);
    return ret;
}

int mod_unload(void* handle)
{
    LOG_("mod_unload, handle: %p", handle);

    //Decrement reference count on the dynamic library handle
    int ret;
    if((ret = dlclose(handle))){
        LOG_WARN_("dlclose() returned nonzero, dlerror(): %s", dlerror());
    };
    return ret;
}
