#include "hmap.h"

void rtcp_hmap_init(rtcp_hmap_t* hmap)
{
    int i;

    /* Initialize values */
    hmap->size = 0;

    /* Initialize records to empty */
    for(i = 0; i < hmap->capacity; ++i){
        hmap->records[i] = RTCP_HMAP_NIL;
    }
}

int rtcp_hmap_malloc(rtcp_hmap_t* hmap, int capacity, int keysize, int typesize)
{
    /* Store parameters */
    hmap->capacity = capacity;
    rtcp_assert_always(hmap->capacity > 0, "rtcp_hmap_malloc: capacity > 0\n");
    hmap->typesize = typesize;
    rtcp_assert_always(hmap->typesize > 0, "rtcp_hmap_malloc: typesize > 0\n");
    hmap->keysize = keysize;
    rtcp_assert_always(hmap->keysize > 0, "rtcp_hmap_malloc: keysize > 0\n");

    /* Initialize values */
    hmap->targetcapa = hmap->capacity;

    /* Allocate memory */
    hmap->records = malloc(hmap->capacity * sizeof(char));
    if(!hmap->records){ return errno; }
    hmap->keys = malloc(hmap->capacity * hmap->keysize);
    if(!hmap->keys){ return errno; }
    hmap->buckets = malloc(hmap->capacity * hmap->typesize);
    if(!hmap->buckets){ return errno; }

    return 0;
}

void rtcp_hmap_free(rtcp_hmap_t* hmap)
{
    /* Free memory */
    assert(hmap->records); free(hmap->records);
    assert(hmap->keys); free(hmap->keys);
    assert(hmap->buckets); free(hmap->buckets);
}

void rtcp_hmap_move(rtcp_hmap_t* dest, rtcp_hmap_t* src)
{
    /* Free the destination */
    rtcp_hmap_free(dest);

    /* Initialize values */
    dest->capacity = src->capacity;
    dest->size = src->size;
    dest->typesize = src->typesize;
    dest->keysize = src->keysize;

    /* Move memory */
    dest->records = src->records;
    dest->keys = src->keys;
    dest->buckets = src->buckets;

    /* Reset memory in source */
    src->records = NULL;
    src->keys = NULL;
    src->buckets = NULL;
}

int rtcp_hmap_realloc(rtcp_hmap_t* hmap, int newcapa)
{
    int ret;
    rtcp_hmap_t newhmap;
    int i;

    /* Allocate a new hmap with the new capacity */
    ret = rtcp_hmap_malloc(&newhmap, newcapa, hmap->keysize, hmap->typesize);
    if(ret){ return ret; }

    /* Initialize hmap */
    rtcp_hmap_init(&newhmap);

    /* Handle old items */
    for(i = 0; i < hmap->capacity; ++i){
        
        /* Skip non-occupied buckets */
        if(hmap->records[i] != RTCP_HMAP_OCC){ continue; }

        /* Add element to the new hmap */
        rtcp_hmap_add(&newhmap, &hmap->keys[i * hmap->keysize],
            hmap->buckets + i * hmap->typesize);
    }

    /* Move the new hmap into the old one */
    rtcp_hmap_move(hmap, &newhmap);

    return 0;
}

rtcp_hash_t rtcp_hmap_hashkey(void* key, int size)
{
    rtcp_hash_t hash = fasthash32(key, size, RTCP_HASH_SEED);
    //LOG_WARN_("rtcp_hmap_hashkey: %x", hash);
    return hash;
}

void rtcp_hmap_tick(rtcp_hmap_t* hmap)
{
    /* Decrease target capacity until a lower bound */
    hmap->targetcapa = hmap->targetcapa > 1 ?
        hmap->targetcapa - 1 : hmap->targetcapa;
}

void rtcp_hmap_use_size(rtcp_hmap_t* hmap)
{
    int cpot = rtcp_ceil_power_of_two(
        hmap->size > hmap->targetcapa - 1 ? hmap->size : hmap->targetcapa - 1);

    /* Set target capacity to indicate usage */
    hmap->targetcapa = cpot > 0 ? cpot : 1;
}

int rtcp_hmap_shrink(rtcp_hmap_t* hmap)
{
    int ret;
    int cpot;

    assert(hmap->targetcapa > 0);
    assert(hmap->capacity > 0);

    /* If the target capacity is less than the next shrink */
    if(hmap->targetcapa < hmap->capacity / 2){
            
        /* Reallocate with the log2-ceiling of the target */
        cpot = rtcp_ceil_power_of_two(hmap->size);
        ret = rtcp_hmap_realloc(hmap, cpot > 0 ? cpot : 1);
        if(ret){ return ret; }
    }

    assert(hmap->capacity > 0);
    assert(hmap->capacity >= hmap->size);

    return 0;
}

int rtcp_hmap_add(rtcp_hmap_t* hmap, void* key, void* val)
{
    int ret;
    int i, j, found;
    rtcp_hash_t hash;

    /* If the expansion load factor is exceeded, expand and rehash */
    if(hmap->size + 1 > hmap->capacity / 2){

        ret = rtcp_hmap_realloc(hmap, hmap->capacity * 2);
        if(ret){ return ret; }
    }

    /* New size */
    ++hmap->size;

    assert(hmap->size <= hmap->capacity);

    /* Initialize */
    i = 0;
    hash = rtcp_hmap_hashkey(key, hmap->keysize);
    j = hash % hmap->capacity;

    /* Step through occupied buckets */
    while(i < hmap->capacity && hmap->records[j] == RTCP_HMAP_OCC){
        ++i; j = (j + 1) % hmap->capacity;
    }

    rtcp_assert_always(i < hmap->capacity,
        "rtcp_hmap_add: failed to find non-occupied bucket\n");

    /* Mark found bucket */
    found = j;

    /* Step through non-empty buckets */
    while(i < hmap->capacity && hmap->records[j] != RTCP_HMAP_NIL){
        rtcp_assert_always(
            !(
                hmap->records[j] == RTCP_HMAP_OCC &&
                memcmp(&hmap->keys[j * hmap->keysize], key, hmap->keysize) == 0
            ),
            "rtcp_hmap_add: key already present\n");
        ++i; j = (j + 1) % hmap->capacity;
    }

    /* Write record */
    hmap->records[found] = RTCP_HMAP_OCC;
    memcpy(&hmap->keys[found * hmap->keysize], key, hmap->keysize);
    memcpy(
        ((uint8_t*)hmap->buckets) + found * hmap->typesize,
        (uint8_t*)val,
        hmap->typesize);

    /* Use the current size, shrink if necessary */
    rtcp_hmap_use_size(hmap);
    rtcp_hmap_shrink(hmap);

    return 0;
}

void* rtcp_hmap_get(rtcp_hmap_t* hmap, void* key)
{
    int idx = rtcp_hmap_get_idx(hmap, key);
    return idx >= 0 ? (hmap->buckets + idx * hmap->typesize) : NULL;
}

int rtcp_hmap_get_idx(rtcp_hmap_t* hmap, void* key)
{
    int i, j;
    rtcp_hash_t hash;

    /* Initialize */
    i = 0;
    hash = rtcp_hmap_hashkey(key, hmap->keysize);
    j = hash % hmap->capacity;

    /* Step through nonempty records */
    while(i < hmap->capacity && hmap->records[j] != RTCP_HMAP_NIL){

        /* If an occupied record with a matching key was found, return it */
        if(
            hmap->records[j] == RTCP_HMAP_OCC &&
            memcmp(&hmap->keys[j * hmap->keysize], key, hmap->keysize) == 0
        ){
            return j;
        }

        ++i; j = (j + 1) % hmap->capacity;
    }

    /* Return invalid bucket index */
    return -1;
}

void rtcp_hmap_del(rtcp_hmap_t* hmap, void* key)
{
    int idx = rtcp_hmap_get_idx(hmap, key);

    /* If a valid bucket index was found, mark it as deleted */
    if(idx >= 0){
        hmap->records[idx] = RTCP_HMAP_DEL;
        --hmap->size;
    }

    /* Use the current size, shrink if necessary */
    rtcp_hmap_use_size(hmap);
    rtcp_hmap_shrink(hmap);
}

void rtcp_hmap_iter(rtcp_hmap_t* hmap, fn_hmap_iter_t callback)
{
    int i;

    /* Check condition for each bucket */
    for(i = 0; i < hmap->capacity; ++i){

        /* Proceed only for occupied buckets */
        if(hmap->records[i] != RTCP_HMAP_OCC){ continue; }
        
        callback(hmap->buckets + i * hmap->typesize);
    }
}

void rtcp_hmap_iter_user(rtcp_hmap_t* hmap, fn_hmap_iter_user_t callback, void* user)
{
    int i;

    /* Check condition for each bucket */
    for(i = 0; i < hmap->capacity; ++i){

        /* Proceed only for occupied buckets */
        if(hmap->records[i] != RTCP_HMAP_OCC){ continue; }
        
        callback(hmap->buckets + i * hmap->typesize, user);
    }
}