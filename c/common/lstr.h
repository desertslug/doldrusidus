#ifndef LSTR_H
#define LSTR_H

#include <stdlib.h>
#include <stddef.h>
#include <stdint.h>
#include <assert.h>
#include <errno.h>
#include <string.h>

#include "helper.h"

typedef struct lstr_t
{
    /** Currently allocated capacity. */
    int capacity;
    /** Currently used capacity. */
    int size;
    /** Stored values. */
    void* values;
} lstr_t;

void lstr_init(lstr_t* lstr);
int lstr_malloc(lstr_t* lstr, int capacity);
void lstr_free(lstr_t* lstr);
int lstr_realloc(lstr_t* lstr, int newcapa);

int lstr_len(lstr_t* lstr);
const char* lstr_get(lstr_t* lstr);
char* lstr_get_mut(lstr_t* lstr);
int lstr_cat_str(lstr_t* lstr, const char* s);
int lstr_cat_strn(lstr_t* lstr, const char* s, int slen);

int lstr_insert_str(lstr_t* lstr, int pos, const char* s);

int lstr_del_pos(lstr_t* lstr, int pos);
int lstr_clear(lstr_t* lstr);

#endif /* LSTR_H */