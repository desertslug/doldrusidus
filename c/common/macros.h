#ifndef MACROS_H
#define MACROS_H

#include "nanosleep.h"

#define CLOCK_MS_START(LABEL) \
    struct timespec start_time_##LABEL; \
    clock_gettime(CLOCK_REALTIME, &start_time_##LABEL); \

#define CLOCK_MS_END(LABEL) \
    struct timespec curr_time_##LABEL; \
    clock_gettime(CLOCK_REALTIME, &curr_time_##LABEL); \
    printf("[" #LABEL "]: %f ms\n", diff_nsec(&start_time_##LABEL, &curr_time_##LABEL) / 1000000.0f); \

#define CLOCK_US_START(LABEL) \
    struct timespec start_time_##LABEL; \
    clock_gettime(CLOCK_REALTIME, &start_time_##LABEL); \

#define CLOCK_US_END(LABEL) \
    struct timespec curr_time_##LABEL; \
    clock_gettime(CLOCK_REALTIME, &curr_time_##LABEL); \
    printf("[" #LABEL "]: %f us\n", diff_nsec(&start_time_##LABEL, &curr_time_##LABEL) / 1000.0f); \

#define CLI_CLOCK_MS_START(LABEL) \
    struct timespec start_time_##LABEL; \
    clock_gettime(CLOCK_REALTIME, &start_time_##LABEL); \

#define CLI_CLOCK_MS_END(cli, LABEL) \
    struct timespec curr_time_##LABEL; \
    clock_gettime(CLOCK_REALTIME, &curr_time_##LABEL); \
    chprintf((cli),"[" #LABEL "]: %f ms", diff_nsec(&start_time_##LABEL, &curr_time_##LABEL) / 1000000.0f); \

#define CLI_CLOCK_US_START(LABEL) \
    struct timespec start_time_##LABEL; \
    clock_gettime(CLOCK_REALTIME, &start_time_##LABEL); \

#define CLI_CLOCK_US_END(cli, LABEL) \
    struct timespec curr_time_##LABEL; \
    clock_gettime(CLOCK_REALTIME, &curr_time_##LABEL); \
    chprintf((cli),"[" #LABEL "]: %f us", diff_nsec(&start_time_##LABEL, &curr_time_##LABEL) / 1000.0f); \

#define HASH_BUFFER_TYPE(name) name##_HASH_BUFFER

#define HASH_BUFFER_DECL(name, size) \
typedef struct {                     \
    uint32_t buf[size];              \
    uint32_t last_hash;              \
                                     \
} name##_HASH_BUFFER;

#define HASH_BUFFER_STEP(hash_buf, size)  \
hash_buf->last_hash =                     \
    fasthash32(                           \
        hash_buf->buf,                    \
        sizeof(uint32_t) * size,          \
        hash_buf->last_hash);

#define CALL_NONNULL_OR_SILENTFAIL(func, ...) if(func){ func(__VA_ARGS__); }

#endif /* MACROS_H */