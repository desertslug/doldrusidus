#include "packet.h"

size_t pkt_len(void* pkt)
{
    ENetPacket* packet = (ENetPacket*)pkt;

    return packet->dataLength;
}

uint8_t* pkt_data(void* pkt)
{
    ENetPacket* packet = (ENetPacket*)pkt;

    return packet->data;
}

uint32_t pkt_flags(void* pkt)
{
    ENetPacket* packet = (ENetPacket*)pkt;

    return packet->flags;
}

int pkt_resize(void* pkt, size_t size)
{
    ENetPacket* packet = (ENetPacket*)pkt;

    return enet_packet_resize(packet, size);
}

void* pkt_create(void* data, size_t size, uint32_t flags)
{
    return enet_packet_create(data, size, flags);
}

void pkt_destroy(void* pkt)
{
    ENetPacket* packet = (ENetPacket*)pkt;

    return enet_packet_destroy(packet);
}

void pkt_log(void* pkt)
{
    //Process a series of chunks
    uint8_t* chunk = pkt_data(pkt);
    fprintf(LOGFILE, "[ %x ]", pkt_len(pkt));
    while(pkt_chunk_check(pkt, chunk)){

        fprintf(LOGFILE, "( ");

        //Chunk size & type
        uint32_t size = pkt_chunk_size(pkt, chunk);
        uint32_t type = pkt_chunk_type(pkt, chunk);
        fprintf(LOGFILE, "0x%x ", size);
        fprintf(LOGFILE, "%d ", type);
        fprintf(LOGFILE, "| ");
        
        
        switch(type){
        /*case PKT_CHUNK_U8: {
        
            uint8_t* data = pkt_chunk_data(pkt, chunk);
            for(int i = 0; i < size / sizeof(uint8_t); ++i){
                fprintf(LOGFILE, "%02hhx ", data[i]);
            }
    
        } break;*/
        case PKT_CHUNK_U16: {
            
            uint16_t* data = (uint16_t*)pkt_chunk_data(pkt, chunk);
            for(int i = 0; i < size / sizeof(uint16_t); ++i){
                fprintf(LOGFILE, "%04hx ", ntohs(data[i]));
            }
        
        } break;
        default: {} break;
        }
        
        
        fprintf(LOGFILE, ") ");

        chunk += pkt_chunk_bytes(pkt, chunk);
    }

    fprintf(LOGFILE, "\n");
}

size_t pkt_chunk_bytes(void* pkt, uint8_t* chunk)
{
    size_t header = sizeof(uint32_t) + sizeof(uint32_t);
    return header + pkt_chunk_size(pkt, chunk);
}

uint32_t pkt_chunk_size(void* pkt, uint8_t* chunk)
{
    return ntohl(*(uint32_t*)chunk);
}

uint32_t pkt_chunk_type(void* pkt, uint8_t* chunk)
{
    return ntohl(*(uint32_t*)(chunk + sizeof(uint32_t)));
}

uint8_t* pkt_chunk_data(void* pkt, uint8_t* chunk)
{
    return chunk + sizeof(uint32_t) + sizeof(uint32_t);
}

bool pkt_chunk_check(void* pkt, uint8_t* chunk)
{
    size_t end = chunk - pkt_data(pkt);
    
    end += sizeof(uint32_t) + sizeof(uint32_t);
    if(end > pkt_len(pkt)){ return false; }
    
    uint32_t size = pkt_chunk_size(pkt, chunk);
    if(size > pkt_len(pkt)){ return false; }
    
    if(end + size > pkt_len(pkt)){ return false; }
    
    uint32_t type = pkt_chunk_type(pkt, chunk);
    if(type < PKT_CHUNK_U8 || type > PKT_CHUNK_U32){ return false; }
    
    return true;
}

void pkt_chunk_add(void* pkt, uint8_t* data, uint32_t size, uint32_t type)
{
    assert_or_exit(size >= 0, "pkt_chunk_add: invalid size\n");
    assert_or_exit(type >= PKT_CHUNK_U8 && type <= PKT_CHUNK_U32,
        "pkt_chunk_add: invalid type");

    //Total chunk size
    size_t total = sizeof(uint32_t) + sizeof(uint32_t) + size;

    size_t curr = pkt_len(pkt);

    //Append chunk size and data type
    pkt_resize(pkt, pkt_len(pkt) + total);
    *(uint32_t*)(pkt_data(pkt) + curr) = htonl(size);
    curr += sizeof(uint32_t);
    *(uint32_t*)(pkt_data(pkt) + curr) = htonl(type);
    curr += sizeof(uint32_t);

    //Append contents, converting byte order if necessary
    switch(type){
    case PKT_CHUNK_U16: {

        uint16_t* src = (uint16_t*)data;
        uint16_t* dst = (uint16_t*)(pkt_data(pkt) + curr);
        for(int i = 0; i < size / sizeof(uint16_t); ++i){
            dst[i] = htons(src[i]);
        }

    } break;
    case PKT_CHUNK_U32: {

        uint32_t* src = (uint32_t*)data;
        uint32_t* dst = (uint32_t*)(pkt_data(pkt) + curr);
        for(int i = 0; i < size / sizeof(uint32_t); ++i){
            dst[i] = htonl(src[i]);
        }

    } break;
    default: { memcpy(pkt_data(pkt) + curr, data, size); } break;
    }
}