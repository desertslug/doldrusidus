#ifndef RTCP_HASH_H
#define RTCP_HASH_H

#include <stdint.h>
#include <assert.h>
#include <errno.h>

#include "helper.h"
#include "fasthash.h"

#define rtcp_assert_always assert_or_exit
#define rtcp_ceil_power_of_two ceil_power_of_two

enum { RTCP_HASH_SEED = 0x8badf00d, };

typedef uint32_t rtcp_hash_t;

/** User-defined hashmap iteration function.
*/
typedef void (*fn_hmap_iter_t)(void*);
typedef void (*fn_hmap_iter_user_t)(void*, void*);

/** Hashmap bucket states. A bucket can be empty, deleted, or occupied. */
enum {
    RTCP_HMAP_NIL = 0,
    RTCP_HMAP_DEL,
    RTCP_HMAP_OCC
};

/** Generic hashmap data structure.
  *
  * Uses open addressing, linear probing, and a load factor of 1/2.
  * Hashes keys with 3 iterations of the romu_mono32 pseudorandom generator.
  *
  * Expands its memory pool as required, but shrinks it lazily.
*/
typedef struct rtcp_hmap_t
{
    /** Currently allocated capacity. */
    int capacity;
    /** Dynamically shrinking/expanding capacity. */
    int targetcapa;
    /** Currently used capacity. */
    int size;
    /** Size of the key, in bytes. */
    int keysize;
    /** Size of the stored type, in bytes. */
    int typesize;
    /** Bucket states. */
    char* records;
    /** Bucket keys. */
    uint8_t* keys;
    /** Bucket values. */
    uint8_t* buckets;
} rtcp_hmap_t;

void rtcp_hmap_init(rtcp_hmap_t* hmap);
int rtcp_hmap_malloc(rtcp_hmap_t* hmap, int capacity, int keysize, int typesize);
void rtcp_hmap_free(rtcp_hmap_t* hmap);
void rtcp_hmap_move(rtcp_hmap_t* dest, rtcp_hmap_t* src);
int rtcp_hmap_realloc(rtcp_hmap_t* hmap, int newcapa);

rtcp_hash_t rtcp_hmap_hashkey(void* key, int size);

void rtcp_hmap_tick(rtcp_hmap_t* hmap);
void rtcp_hmap_use_size(rtcp_hmap_t* hmap);
int rtcp_hmap_shrink(rtcp_hmap_t* hmap);

int rtcp_hmap_add(rtcp_hmap_t* hmap, void* key, void* val);
void* rtcp_hmap_get(rtcp_hmap_t* hmap, void* key);
int rtcp_hmap_get_idx(rtcp_hmap_t* hmap, void* key);
void rtcp_hmap_del(rtcp_hmap_t* hmap, void* key);
void rtcp_hmap_iter(rtcp_hmap_t* hmap, fn_hmap_iter_t callback);

void rtcp_hmap_iter_user(rtcp_hmap_t* hmap, fn_hmap_iter_user_t callback, void* user);

#endif /* RTCP_HASH_H */
