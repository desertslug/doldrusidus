#ifndef CSV_H
#define CSV_H

#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#define CSV_BUF_MAX 4096

#define CSV_END 1
#define CSV_ERR -1
#define CSV_OK 0

typedef struct Csv
{
    FILE* fd;
    int cols;
    size_t cur;
    size_t end;
    size_t readbytes;
    char sep;
    char buf[CSV_BUF_MAX];
    char **fields;
} Csv;

int csv_open(Csv* csv, const char* path, char sep, int cols);
int csv_close(Csv* csv);
void csv_reset(Csv* csv);
int csv_read_next(Csv* csv, char*** fields);
int csv_append(Csv* csv, char*** fields);
int csv_clear(Csv* csv);

#endif /* CSV_H */