#ifndef PLANCKLOG_H
#define PLANCKLOG_H

/* plancklog.h
 * ===========
 * 
 * The simplest logger implementation I could come up with.
 * Can be used by itself or as a base for a more complex logger library.
 */

#include <stddef.h>
#include <stdio.h>
#include <time.h>

//Uncomment to avoid compiling detailed plancklog functionality
#define PLANCKLOG_OMIT

//#define PLANCKLOG_FILE

#ifdef PLANCKLOG_FILE
    #ifdef PLANCKLOG_HEADER
        extern FILE* logfile;
    #else
        FILE* logfile;
    #endif
    #define LOGFILE (logfile)
#else
    #define LOGFILE (stdout)
#endif

#define ESC_RST "\033[0m"
#define ESC_BLD "\033[1m"

#define ESC_WHT "\033[0m"
#define ESC_RED "\033[31m"
#define ESC_GRN "\033[32m"
#define ESC_YEL "\033[33m"
#define ESC_BLU "\033[34m"
#define ESC_MAG "\033[35m"

#ifndef PLANCKLOG_OMIT
    #define PLNCK_OMIT(str, ...) str
#else
    #define PLNCK_OMIT(str, ...)
#endif

#ifndef PLANCKLOG_OMIT

#define LOG_GENERIC(FORMAT_COLORS, FORMAT_NOCOLORS, FILENAME, LINENO, FUNCNAME, LOGFILE, FORMAT, ...) do { \
    time_t t = time(NULL);                                                     \
    char timestr[20];                                                          \
    strftime(timestr, 20, "%F %T", localtime(&t));                             \
    fprintf(                                                                   \
        (LOGFILE),                                                             \
        ((LOGFILE) == stdout) ?                                                \
            (FORMAT_COLORS FORMAT "\n") : (FORMAT_NOCOLORS FORMAT "\n"),       \
        timestr,                                                               \
        (FILENAME),                                                            \
        (LINENO),                                                              \
        (FUNCNAME), ## __VA_ARGS__                                             \
    );                                                                         \
} while (0)

#else

#define LOG_GENERIC(FORMAT_COLORS, FORMAT_NOCOLORS, FILENAME, LINENO, FUNCNAME, LOGFILE, FORMAT, ...) do { \
    fprintf(                                                                   \
        (LOGFILE),                                                             \
        ((LOGFILE) == stdout) ?                                                \
            (FORMAT_COLORS FORMAT "\n") : (FORMAT_NOCOLORS FORMAT "\n"),       \
        ## __VA_ARGS__                                                         \
    );                                                                         \
} while (0)

#endif

#define PLANCKLOG_STARTUP_COLORS \
ESC_RST "[" ESC_MAG " START " \
PLNCK_OMIT(ESC_RST ESC_GRN "%s " ESC_RST "%s:" \
               ESC_BLU "%i" ESC_GRN " in " ESC_RST "%s ") \
ESC_RST "] " ESC_RST
#define PLANCKLOG_STARTUP_NOCOLORS \
"[ START " PLNCK_OMIT("%s %s:%i in %s") "] "

#define PLANCKLOG_SHUTDOWN_COLORS \
ESC_RST "[" ESC_MAG " STOP " \
PLNCK_OMIT(ESC_RST ESC_GRN " %s " ESC_RST "%s:" \
               ESC_BLU "%i" ESC_GRN " in " ESC_RST "%s ") \
ESC_RST "] " ESC_RST
#define PLANCKLOG_SHUTDOWN_NOCOLORS \
"[ STOP " PLNCK_OMIT(" %s %s:%i in %s") "] "

#define PLANCKLOG_INFO_COLORS \
ESC_RST "[" ESC_BLU " INFO " \
PLNCK_OMIT(ESC_RST ESC_GRN " %s " ESC_RST "%s:" \
               ESC_BLU "%i" ESC_GRN " in " ESC_RST "%s ") \
ESC_RST "] " ESC_RST
#define PLANCKLOG_INFO_NOCOLORS \
"[ INFO " PLNCK_OMIT(" %s %s:%i in %s") "] "

#define PLANCKLOG_DEBUG_COLORS \
ESC_RST "[" ESC_RST " DEBUG " \
PLNCK_OMIT(ESC_RST ESC_GRN "%s " ESC_RST "%s:" \
               ESC_BLU "%i" ESC_GRN " in " ESC_RST "%s ") \
ESC_RST "] " ESC_RST
#define PLANCKLOG_DEBUG_NOCOLORS \
"[ DEBUG " PLNCK_OMIT("%s %s:%i in %s") "] "

#define PLANCKLOG_WARN_COLORS \
ESC_RST "[" ESC_YEL " WARN " \
PLNCK_OMIT(ESC_RST ESC_GRN " %s " ESC_RST "%s:" \
               ESC_BLU "%i" ESC_GRN " in " ESC_RST "%s ") \
ESC_RST "] " ESC_RST
#define PLANCKLOG_WARN_NOCOLORS \
"[ WARN " PLNCK_OMIT(" %s %s:%i in %s") "] "

#define PLANCKLOG_ERROR_COLORS \
ESC_RST "[" ESC_RED " ERROR " \
PLNCK_OMIT(ESC_RST ESC_GRN "%s " ESC_RST "%s:" \
               ESC_BLU "%i" ESC_GRN " in " ESC_RST "%s ") \
ESC_RST "] " ESC_RST
#define PLANCKLOG_ERROR_NOCOLORS \
"[ ERROR " PLNCK_OMIT("%s %s:%i in %s") "] "

/* Log startup and shutdown */
#define LOG_STARTUP_CONTROLLED(FILENAME, LINENO, FUNCNAME, LOGFILE) \
	LOG_GENERIC(PLANCKLOG_STARTUP_COLORS, PLANCKLOG_STARTUP_NOCOLORS, FILENAME, LINENO, FUNCNAME, LOGFILE, "")

#define LOG_STARTUP(LOGFILE) \
	LOG_STARTUP_CONTROLLED(__FILE__, __LINE__, __func__, LOGFILE)
#define LOG_STARTUP_() LOG_STARTUP(LOGFILE)

#define LOG_SHUTDOWN_CONTROLLED(FILENAME, LINENO, FUNCNAME, LOGFILE) \
	LOG_GENERIC(PLANCKLOG_SHUTDOWN_COLORS, PLANCKLOG_SHUTDOWN_NOCOLORS, FILENAME, LINENO, FUNCNAME, LOGFILE, "")

#define LOG_SHUTDOWN(LOGFILE) \
	LOG_SHUTDOWN_CONTROLLED(__FILE__, __LINE__, __func__, LOGFILE)
#define LOG_SHUTDOWN_() LOG_SHUTDOWN(LOGFILE)

/* Simple info logs */
#define LOG_CONTROLLED(FILENAME, LINENO, FUNCNAME, LOGFILE, FORMAT, ...) \
	LOG_GENERIC(PLANCKLOG_INFO_COLORS, PLANCKLOG_INFO_NOCOLORS, FILENAME, LINENO, FUNCNAME, LOGFILE, FORMAT, ## __VA_ARGS__)

#define LOG(LOGFILE, FORMAT, ...) \
	LOG_CONTROLLED(__FILE__, __LINE__, __func__, LOGFILE, FORMAT, ## __VA_ARGS__)
#define LOG_(FORMAT, ...) LOG(LOGFILE, FORMAT, ## __VA_ARGS__)

/* Debug logs */
#define LOG_DEBUG_CONTROLLED(FILENAME, LINENO, FUNCNAME, LOGFILE, FORMAT, ...) \
	LOG_GENERIC(PLANCKLOG_DEBUG_COLORS, PLANCKLOG_DEBUG_NOCOLORS, FILENAME, LINENO, FUNCNAME, LOGFILE, FORMAT, ## __VA_ARGS__)

#define LOG_DEBUG(LOGFILE, FORMAT, ...) \
	LOG_DEBUG_CONTROLLED(__FILE__, __LINE__, __func__, LOGFILE, FORMAT, ## __VA_ARGS__)
#define LOG_DEBUG_(FORMAT, ...) LOG_DEBUG(LOGFILE, FORMAT, ## __VA_ARGS__)

/* Warning logs */
#define LOG_WARN_CONTROLLED(FILENAME, LINENO, FUNCNAME, LOGFILE, FORMAT, ...) \
	LOG_GENERIC(PLANCKLOG_WARN_COLORS, PLANCKLOG_WARN_NOCOLORS, FILENAME, LINENO, FUNCNAME, LOGFILE, FORMAT, ## __VA_ARGS__)

#define LOG_WARN(LOGFILE, FORMAT, ...) \
	LOG_WARN_CONTROLLED(__FILE__, __LINE__, __func__, LOGFILE, FORMAT, ## __VA_ARGS__)
#define LOG_WARN_(FORMAT, ...) LOG_WARN(LOGFILE, FORMAT, ## __VA_ARGS__)

/* Error logs */
#define LOG_ERROR_CONTROLLED(FILENAME, LINENO, FUNCNAME, LOGFILE, FORMAT, ...) \
	LOG_GENERIC(PLANCKLOG_ERROR_COLORS, PLANCKLOG_ERROR_NOCOLORS, FILENAME, LINENO, FUNCNAME, LOGFILE, FORMAT, ## __VA_ARGS__)

#define LOG_ERROR(LOGFILE, FORMAT, ...) \
	LOG_ERROR_CONTROLLED(__FILE__, __LINE__, __func__, LOGFILE, FORMAT, ## __VA_ARGS__)
#define LOG_ERROR_(FORMAT, ...) LOG_ERROR(LOGFILE, FORMAT, ## __VA_ARGS__)

/*
 * Logging in release mode -----------------------------------------------------
 */
#ifdef NDEBUG

#ifndef PLANCKLOG_OMIT

#undef LOG_GENERIC
#define LOG_GENERIC(FORMAT_COLORS, FORMAT_NOCOLORS, FILENAME, LINENO, FUNCNAME, LOGFILE, FORMAT, ...) do { \
    time_t t = time(NULL);                                                     \
    char timestr[20];                                                          \
    strftime(timestr, 20, "%F %T", localtime(&t));                             \
    fprintf(                                                                   \
        (LOGFILE),                                                             \
        ((LOGFILE) == stdout) ?                                                \
            (FORMAT_COLORS FORMAT "\n") : (FORMAT_NOCOLORS FORMAT "\n"),       \
        timestr, ## __VA_ARGS__                                                \
    );                                                                         \
} while (0)

#else

#undef LOG_GENERIC
#define LOG_GENERIC(FORMAT_COLORS, FORMAT_NOCOLORS, FILENAME, LINENO, FUNCNAME, LOGFILE, FORMAT, ...) do { \
    fprintf(                                                                   \
        (LOGFILE),                                                             \
        ((LOGFILE) == stdout) ?                                                \
            (FORMAT_COLORS FORMAT "\n") : (FORMAT_NOCOLORS FORMAT "\n"),       \
        ## __VA_ARGS__                                                         \
    );                                                                         \
} while (0)

#endif /* PLANCKLOG_OMIT */

#undef PLANCKLOG_STARTUP_COLORS
#undef PLANCKLOG_STARTUP_NOCOLORS
#define PLANCKLOG_STARTUP_COLORS \
ESC_RST "[" ESC_MAG " START " PLNCK_OMIT(ESC_GRN "%s ") ESC_RST "] " ESC_RST
#define PLANCKLOG_STARTUP_NOCOLORS " [ START " PLNCK_OMIT("%s ") "] "

#undef PLANCKLOG_SHUTDOWN_COLORS
#undef PLANCKLOG_SHUTDOWN_NOCOLORS
#define PLANCKLOG_SHUTDOWN_COLORS \
ESC_RST "[" ESC_MAG " STOP " PLNCK_OMIT(ESC_GRN " %s ") ESC_RST "] " ESC_RST
#define PLANCKLOG_SHUTDOWN_NOCOLORS " [ STOP " PLNCK_OMIT(" %s ") "] "

#undef PLANCKLOG_INFO_COLORS
#undef PLANCKLOG_INFO_NOCOLORS
#define PLANCKLOG_INFO_COLORS \
ESC_RST "[" ESC_BLU  " INFO " PLNCK_OMIT(ESC_GRN " %s ") ESC_RST "] " ESC_RST
#define PLANCKLOG_INFO_NOCOLORS " [ INFO " PLNCK_OMIT(" %s ") "] "

/* Don't print debug logs in release mode */
#undef LOG_DEBUG_CONTROLLED
#define LOG_DEBUG_CONTROLLED(...) ((void*)0)

#undef PLANCKLOG_WARN_COLORS
#undef PLANCKLOG_WARN_NOCOLORS
#define PLANCKLOG_WARN_COLORS \
ESC_RST "[" ESC_YEL  " WARN " PLNCK_OMIT(ESC_GRN " %s ") ESC_RST "] " ESC_RST
#define PLANCKLOG_WARN_NOCOLORS " [ WARN " PLNCK_OMIT(" %s ") "] "

#undef PLANCKLOG_ERROR_COLORS
#undef PLANCKLOG_ERROR_NOCOLORS
#define PLANCKLOG_ERROR_COLORS \
ESC_RST "[" ESC_RED  " ERROR " PLNCK_OMIT(ESC_GRN "%s ") ESC_RST "] " ESC_RST
#define PLANCKLOG_ERROR_NOCOLORS " [ ERROR " PLNCK_OMIT("%s ") "] "

#endif /* NDEBUG */
#endif /* Include guard */