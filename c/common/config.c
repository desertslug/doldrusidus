#include "config.h"

void startconfig_load(StartConfig* config, const char* path)
{
    //Load the hardwired default configuration at first
    startconfig_default(config);

    //Attempt loading the initial configuration from a file
    if(!JSONLoadFromFile(config, path,
        startconfig_validate_from_json,
        start_config_parse_validated)
    ){
        LOG_WARN_("could not load starting configuration");
        LOG_WARN_("the default configuration will be used");
    };

    //Propagate configuration values
    startconfig_propagate_values(config);
}

void startconfig_default(StartConfig* config){

    //Load default configuration values
    //strcpy(config->transport, "tcp");
    strcpy(config->ipv4address, "127.0.0.1");
    strcpy(config->port, "8888");
    /*strcpy(config->port2, "8889");*/
    config->heartbeat_expire_msec = 8000;
    config->heartbeat_recv_msec = 4000;
    config->heartbeat_send_msec = 1000;
    config->session_drain_msec = 2000;
    /*
    config->userinput_max = 64;
    config->msgsize_max = 128;
    config->filesize_max = 32;
    */
    strcpy(config->cc_path, "");
    strcpy(config->c_args, "");
    
    strcpy(config->srv_mod_path, "");
    strcpy(config->srv_auth_path, "");
    /*
    config->srv_threads = 1;
    */
    config->srv_tps = 10;
    config->srv_tps_pass = 10;
    config->srv_register_limit = 8;
    config->srv_session_limit = 1;
    config->srv_user_session_limit = 1;
    /*
    config->srv_login_limit = 2;
    */
    config->srv_entity_max = 0;
    /*
    config->srv_reqrep_per_tick = 1;
    */

    config->cli_conn_wait_msec = 5000;
    strcpy(config->cli_mod_path, "");
    config->cli_fps = 30;
    /*
    config->cli_tps = 5;
    config->cli_history = 256;
    */
    config->cli_window_resizable = true;
    config->cli_swap_interval = 0;
}

void startconfig_propagate_values(StartConfig* config){

    //Calculate all configuration values that are derived from other values
    config->srv_tick_nsec = NSEC_SEC / config->srv_tps;
    config->srv_tick_usec = USEC_SEC / config->srv_tps;
    config->cli_frame_nsec = NSEC_SEC / config->cli_fps;
}

void startconfig_print(StartConfig* config)
{
    //LOG_("config->transport: %s", config->transport);
    LOG_("config->ipv4address: %s", config->ipv4address);
    LOG_("config->port: %s", config->port);
    /*LOG_("config->port2: %s", config->port2);*/
    LOG_("config->heartbeat_expire_msec: %d", config->heartbeat_expire_msec);
    LOG_("config->heartbeat_recv_msec: %d", config->heartbeat_recv_msec);
    LOG_("config->heartbeat_send_msec: %d", config->heartbeat_send_msec);
    LOG_("config->session_drain_msec: %d", config->session_drain_msec);
    /*
    LOG_("config->userinput_max: %d", config->userinput_max);
    LOG_("config->msgsize_max: %d", config->msgsize_max);
    LOG_("config->filesize_max: %d", config->filesize_max);
    */
    LOG_("config->cc_path: %s", config->cc_path);
    LOG_("config->c_args: %s", config->c_args);
    
    LOG_("config->srv_mod_path: %s", config->srv_mod_path);
    LOG_("config->srv_auth_path: %s", config->srv_auth_path);
    /*
    LOG_("config->srv_threads: %d", config->srv_tps);
    */
    LOG_("config->srv_tps: %d", config->srv_tps);
    LOG_("config->srv_tps_pass: %d", config->srv_tps_pass);
    LOG_("config->srv_tick_nsec: %d", config->srv_tick_nsec);
    LOG_("config->srv_register_limit: %d", config->srv_register_limit);
    LOG_("config->srv_session_limit: %d", config->srv_session_limit);
    LOG_("config->srv_user_session_limit: %d", config->srv_user_session_limit);
    /*
    LOG_("config->srv_login_limit: %d", config->srv_login_limit);
    */
    LOG_("config->srv_entity_max: %d", config->srv_entity_max);
    /*
    LOG_("config->srv_reqrep_per_tick: %d", config->srv_reqrep_per_tick);
    */
    LOG_("config->cli_conn_wait_msec: %d", config->cli_conn_wait_msec);
    LOG_("config->cli_mod_path: %s", config->cli_mod_path);
    LOG_("config->cli_fps: %d", config->cli_fps);
    /*
    LOG_("config->cli_tps: %d", config->cli_tps);
    */
    LOG_("config->cli_frame_nsec: %d", config->cli_frame_nsec);
    /*
    LOG_("config->cli_history: %d", config->cli_history);
    */
    LOG_("config->cli_window_resizable: %d", config->cli_window_resizable);
    LOG_("config->cli_swap_interval: %d", config->cli_swap_interval);
}

void startconfig_init_invocations(StartConfig* config, JSONKeyInvocation* invoc)
{
    memcpy(invoc,
        (JSONKeyInvocation[CONFIG_MEMBERS]){
        /*{ .key = TRANSPORT_CONFSTR,
          .data = &config->transport,
          .invocation = json_arraypair_first_string_recursebase, },*/
        { .key = IPV4ADDRESS_CONFSTR,
          .data = &config->ipv4address,
          .invocation = json_arraypair_first_string_recursebase, },
        { .key = PORT_CONFSTR,
          .data = &config->port,
          .invocation = json_arraypair_first_string_recursebase, },
        /*{ .key = PORT2_CONFSTR,
          .data = &config->port2,
          .invocation = json_arraypair_first_string_recursebase, },*/
        { .key = HEARTBEAT_EXPIRE_CONFSTR,
          .data = &config->heartbeat_expire_msec,
          .invocation = json_arraypair_first_unsigned_recursebase, },
        { .key = HEARTBEAT_RECV_CONFSTR,
          .data = &config->heartbeat_recv_msec,
          .invocation = json_arraypair_first_unsigned_recursebase, },
        { .key = HEARTBEAT_SEND_CONFSTR,
          .data = &config->heartbeat_send_msec,
          .invocation = json_arraypair_first_unsigned_recursebase, },
        { .key = SESSION_DRAIN_MSEC_CONFSTR,
          .data = &config->session_drain_msec,
          .invocation = json_arraypair_first_unsigned_recursebase, },
        /*
        { .key = USERINPUT_MAX_CONFSTR,
          .data = &config->userinput_max,
          .invocation = json_arraypair_first_unsigned_recursebase, },
        { .key = MSGSIZE_MAX_CONFSTR,
          .data = &config->msgsize_max,
          .invocation = json_arraypair_first_unsigned_recursebase, },
        { .key = FILESIZE_MAX_CONFSTR,
          .data = &config->filesize_max,
          .invocation = json_arraypair_first_unsigned_recursebase, },
        */
        { .key = CC_PATH_CONFSTR,
          .data = &config->cc_path,
          .invocation = json_arraypair_first_string_recursebase, },
        { .key = C_ARGS_CONFSTR,
          .data = &config->c_args,
          .invocation = json_arraypair_first_string_recursebase, },
        
        { .key = SRV_MOD_PATH_CONFSTR,
          .data = &config->srv_mod_path,
          .invocation = json_arraypair_first_string_recursebase, },
        { .key = SRV_AUTH_PATH_CONFSTR,
          .data = &config->srv_auth_path,
          .invocation = json_arraypair_first_string_recursebase, },
        /*
        { .key = SRV_THREADS_CONFSTR,
          .data = &config->srv_threads,
          .invocation = json_arraypair_first_unsigned_recursebase, },
        */
        { .key = SRV_TPS_CONFSTR,
          .data = &config->srv_tps,
          .invocation = json_arraypair_first_unsigned_recursebase, },
        { .key = SRV_TPS_PASS_CONFSTR,
          .data = &config->srv_tps_pass,
          .invocation = json_arraypair_first_unsigned_recursebase, },
        { .key = SRV_REGISTER_LIMIT_CONFSTR,
          .data = &config->srv_register_limit,
          .invocation = json_arraypair_first_unsigned_recursebase, },
        { .key = SRV_SESSION_LIMIT_CONFSTR,
          .data = &config->srv_session_limit,
          .invocation = json_arraypair_first_unsigned_recursebase, },
        { .key = SRV_USER_SESSION_LIMIT_CONFSTR,
          .data = &config->srv_user_session_limit,
          .invocation = json_arraypair_first_unsigned_recursebase, },
        /*
        { .key = SRV_LOGIN_LIMIT_CONFSTR,
          .data = &config->srv_login_limit,
          .invocation = json_arraypair_first_unsigned_recursebase, },
        */
        { .key = SRV_ENTITY_MAX_CONFSTR,
          .data = &config->srv_entity_max,
          .invocation = json_arraypair_first_unsigned_recursebase, },
        /*
        { .key = SRV_REQREP_PER_TICK_CONFSTR,
          .data = &config->srv_reqrep_per_tick,
          .invocation = json_arraypair_first_unsigned_recursebase, },
        */

        { .key = CLI_CONN_WAIT_MSEC_CONFSTR,
          .data = &config->cli_conn_wait_msec,
          .invocation = json_arraypair_first_unsigned_recursebase, },
        { .key = CLI_MOD_PATH_CONFSTR,
          .data = &config->cli_mod_path,
          .invocation = json_arraypair_first_string_recursebase, },
        { .key = CLI_FPS_CONFSTR,
          .data = &config->cli_fps,
          .invocation = json_arraypair_first_unsigned_recursebase, },
        /*
        { .key = CLI_TPS_CONFSTR,
          .data = &config->cli_tps,
          .invocation = json_arraypair_first_unsigned_recursebase, },
        { .key = CLI_HISTORY_CONFSTR,
          .data = &config->cli_history,
          .invocation = json_arraypair_first_unsigned_recursebase, },
        */
        { .key = CLI_WINDOW_RESIZABLE_CONFSTR,
          .data = &config->cli_window_resizable,
          .invocation = json_arraypair_first_bool_recursebase, },
        { .key = CLI_SWAP_INTERVAL_CONFSTR,
          .data = &config->cli_swap_interval,
          .invocation = json_arraypair_first_unsigned_recursebase, },
        },
        CONFIG_MEMBERS * sizeof(JSONKeyInvocation)
    );
}

int startconfig_validate_from_json(void* data, const char* json, jsmntok_t* tokens, int tok_count)
{
    //Convert opaque data pointer, assert success
    StartConfig* config = (StartConfig*)data; assert(config);

    //Initialize the invocations
    JSONKeyInvocation invoc[CONFIG_MEMBERS];
    startconfig_init_invocations(config, invoc);

    //Start invoking the recursive validation
    return RecurseOnJSONObject(data, 0, false, json, tokens, tok_count, invoc, CONFIG_MEMBERS);
}

void start_config_parse_validated(void* data, const char* json, jsmntok_t* tokens, int tok_count)
{
    //Convert opaque data pointer, assert success
    StartConfig* config = (StartConfig*)data; assert(config);

    //Initialize the invocations
    JSONKeyInvocation invoc[CONFIG_MEMBERS];
    startconfig_init_invocations(config, invoc);
    

    //Start invoking the recursive validation
    RecurseOnJSONObject(data, 0, true, json, tokens, tok_count, invoc, CONFIG_MEMBERS);
}
