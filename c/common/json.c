#include "json.h"

int RecurseOnJSONObject(void* data, size_t offset, bool write, const char* json, jsmntok_t* tokens, int tok_count, JSONKeyInvocation* invoc, int invocations)
{
    //Assume the top-level element is an object
    if(tok_count < 1 || tokens[0].type != JSMN_OBJECT){
        LOG_ERROR_("object expected as the top-level element.");
        return -1;
    }

    //Parse values associated with the keys, each with its designated invocation
    int i = 1;
    while(i < tok_count){

        /*LOG_DEBUG_("json recurse, desc: %d, nest: %d, tok: %.*s",
            tokens[i].size, json_tokens_in_token(&tokens[i+1], tok_count - (i+1)),
            tokens[i].end - tokens[i].start, json + tokens[i].start);*/

        //Loop through possible invocations until one matches
        int j = 0;
        while(j < invocations){

            //Check whether the key of an invocation matches the current key
            if(json_eq(json, &tokens[i], invoc[j].key)){

                //Invoke the function to handle the associated value
                int ret = 0;
                if((ret = invoc[j].invocation(
                    invoc[j].data, offset, write, json, &tokens[i + 1],
                    json_tokens_in_token(&tokens[i + 1], tok_count - (i + 1)),
                    invoc[j].next, invoc[j].next_count)) > 0
                ){
                    i += ret + 1;
                } else {
                    LOG_ERROR_("failed to parse value associated with key.");
                    LOG_ERROR_("key: %.*s", tokens[i].end - tokens[i].start, json + tokens[i].start);
                    LOG_ERROR_("val: %.*s", tokens[i+1].end - tokens[i+1].start, json + tokens[i+1].start);
                    return -1;
                }

                //For non-dry runs, check for an optional value converter function
                if(write && invoc[j].converter){
                    invoc[j].converter(invoc[j].data + offset, invoc[j].input, invoc[j].output);
                }
                
                //Break the invocation loop, as the right one was found
                break;
            }
            ++j;
        }

        //If none of the possible invocations was matched
        if(j == invocations){
            LOG_ERROR_("unexpected key: %.*s", tokens[i].end - tokens[i].start, json + tokens[i].start);
            return -1;
        }
    }

    //Assert the well-formed nature of the configuration file
    if(i != tok_count){
        LOG_ERROR_("json file was structured unexpectedly.");
        return -1;
    }

    return tok_count;
}

int RecurseOnJSONArray(void* data, size_t offset, bool write, const char* json, jsmntok_t* tokens, int tok_count, JSONKeyInvocation* invoc, int invocations)
{
    //Assert that the top-level token is an array
    if(tok_count < 1 || tokens[0].type != JSMN_ARRAY){
        LOG_ERROR_("array expected as the top-level element.");
        return -1;
    }

    //Reset the number of elements parsed in the invocation
    invoc->elements = 0;

    //Parse array elements, each with the same invocation
    int i = 1;
    while(i < tok_count){

        /*LOG_DEBUG_("json recurse, desc: %d, nest: %d, tok: %.*s",
            tokens[i].size, json_tokens_in_token(&tokens[i+1], tok_count - (i+1)),
            tokens[i].end - tokens[i].start, json + tokens[i].start);*/

        //Get the invocation to be called
        JSONKeyInvocation* invocation = &invoc[invocations > 1 ? invoc->elements : 0];

        int ret = 0;
        if((ret = invocation->invocation(
            invocation->data, offset + invoc->stride * invoc->elements, write, json,
            &tokens[i], json_tokens_in_token(&tokens[i], tok_count - i),
            invocation->next, invocation->next_count)) > 0
        ){
            i += ret;
            ++invoc->elements;
        } else {
            LOG_ERROR_("failed to parse array element.");
            LOG_ERROR_("element: %.*s", tokens[i].end - tokens[i].start, json + tokens[i].start);
            return -1;
        }
    }

    //Assert the well-formed nature of the configuration file
    if(i != tok_count){
        LOG_ERROR_("json file was structured unexpectedly.");
        return -1;
    }

    return tok_count;
}

bool JSONLoadFromFile(void* data, const char* path, ValidateAndAllocateFromJSONFun validator_allocator, ParseFromJSONFun parser)
{
    //Attempt to load a JSON string from a file
    char* json = NULL;
    if(AllocateAndLoadJSONFromFile(&json, path) <= 0){
        LOG_ERROR_("could not open file \"%s\"", path);
        return false;
    }
    
    //Attempt to parse JSON data into a configuration
    jsmntok_t* tokens = NULL;
    int tok_count = AllocateAndParseJSMNTokens(json, &tokens);
    if(tok_count < 0){
        LOG_ERROR_("error: could not parse json string");

        //Deallocate
        DeallocateJSON(json);
        return false;
    }

    //Assert the well-formed nature of the JSON string,
    //and possibly allocate the data through the opaque data pointer
    if(validator_allocator(data, json, tokens, tok_count) < 0){
        LOG_ERROR_("could not assert well-structuredness of the loaded json string");

        //Deallocate
        DeallocateJSON(json);
        DeallocateJSMNTokens(tokens);
        return false;
    }

    //Parse JSON string
    parser(data, json, tokens, tok_count);

    //Deallocate
    DeallocateJSON(json);
    DeallocateJSMNTokens(tokens);
    return true;
}

// WARNING: this function allocates optionally into one of its parameters,
// the responsibility to free that resource lies with the caller.
// A negative return value means no allocation happened.
// A positive return value signifies the length of the allocated string.
int AllocateAndLoadJSONFromFile(char** json, const char* path)
{
    //Attempt to open the configuration file from the specified path
    FILE* file = fopen(path, "r");
    if(!file){
        return -1;
    }

    //Seek to the end of the file, retrieve file position indiactor to tell its size
    fseek(file, 0L, SEEK_END);
    int file_size = ftell(file);

    //Set the file position indicator to the beginning of the file
    rewind(file);

    //Allocate storage for the json string
    *json = malloc((file_size + 1) * sizeof(char));

    //Read the entire file into the json string
    fread(*json, file_size, 1, file);

    //Put null terminator at the end of the string
    (*json)[file_size] = '\0';

    //Close file
    if(fclose(file)){
        LOG_WARN_("could not close file descriptor, continuing...");
    }

    return file_size + 1;
}

void DeallocateJSON(char* json)
{
    free(json);
}

// WARNING: this function allocates optionally into one of its parameters,
// the responsibility to free that resource lies with the caller.
// A negative return value means nothing must be freed, parsing failed.
// A positive return value signifies the number of tokens in the JSON string.
int AllocateAndParseJSMNTokens(const char* json, jsmntok_t** tokens)
{
    //Allocate JSON parser
    jsmn_parser parser;
    
    //Retrieve number of tokens & allocate tokens necessary to parse 
    jsmn_init(&parser);
    int token_num = jsmn_parse(&parser, json, strlen(json), NULL, 0);
    
    //Allocate the necessary number of tokens to parse the JSON string
    *tokens = malloc(token_num * sizeof(jsmntok_t));

    //Attempt to parse the json string into the token array
    jsmn_init(&parser);
    int tok_s = jsmn_parse(&parser, json, strlen(json), *tokens, token_num);

    //Handle negative return values, which correspond to error codes
    if(tok_s < 0){
        LOG_ERROR_("failed to parse JSON, error code: %d", tok_s);
        switch(tok_s){
            case JSMN_ERROR_INVAL:
                LOG_ERROR_("bad token, JSON string is corrupted");
            break;
            case JSMN_ERROR_NOMEM:
                LOG_ERROR_("not enough tokens, JSON string is too large");
            break;
            case JSMN_ERROR_PART:
                LOG_ERROR_("JSON string is too short, expecting more JSON data");
            break;
        }

        return tok_s;
    }

    //Return the number of parsed tokens upon success
    return token_num;
}

void DeallocateJSMNTokens(jsmntok_t* tokens)
{
    free(tokens);
}

int json_tokens_in_token(jsmntok_t* tok_start, int tok_count)
{
    //Calculate the total number of nested tokens
    int i = 0; jsmntok_t* token = tok_start;
    //while(i < tok_count && token->start < tok_start->end){ ++i; ++token; }
    while(i < tok_count && token->start <= tok_start->end){ ++i; ++token; }
    return i;
}

int json_eq(const char* json, jsmntok_t* tok, const char* str)
{
    //Check that a portion of a JSON data string is a string token equal to another string
    if(tok->type == JSMN_STRING && (int)strlen(str) == tok->end - tok->start && strncmp(json + tok->start, str, tok->end - tok->start) == 0){
        return 1;
    }
    return 0;
}


int json_bool(const char* json, jsmntok_t* tok, void* data)
{
    //Attempt to convert the data pointer to the appropriate type
    bool* ret = (bool*)data;

    //Check whether the token is a primitive starting with 't' or 'f'
    if(tok->type == JSMN_PRIMITIVE && (json[tok->start] == 't' || json[tok->start] == 'f')){

        //Write data only if the opaque pointer conversion succeeded
        //(this behavior can be used for dry runs)
        if(ret){
            if(json[tok->start] == 't'){ *ret = true; }
            else { *ret = false; }
        }
        return 1;
    }
    return -1;
}

int json_unsigned(const char* json, jsmntok_t* tok, void* data)
{
    //Attempt to convert the data pointer to the appropriate type
    int* ret = (int*)data;
    int dummy;

    //Check whether the token is a primitive with alphanumeric
    //that can be scanned as an unsigned integer
    if(tok->type == JSMN_PRIMITIVE && json[tok->start] >= '0' && json[tok->start] <= '9' && sscanf(json + tok->start, "%u", &dummy) == 1){

        //Write data only if the opaque pointer conversion succeeded
        //(this behavior can be used for dry runs)
        if(ret){ sscanf(json + tok->start, "%u", ret); }
        return 1;
    }
    return -1;
}

int json_unsigned_short(const char* json, jsmntok_t* tok, void* data)
{
    //Attempt to convert the data pointer to the appropriate type
    uint16_t* ret = (uint16_t*)data;
    uint16_t dummy;

    //Check whether the token is a primitive with alphanumeric
    //that can be scanned as an unsigned integer
    if(tok->type == JSMN_PRIMITIVE && json[tok->start] >= '0' && json[tok->start] <= '9' && sscanf(json + tok->start, "%hu", &dummy) == 1){

        //Write data only if the opaque pointer conversion succeeded
        //(this behavior can be used for dry runs)
        if(ret){ sscanf(json + tok->start, "%hu", ret); }
        return 1;
    }
    return -1;
}

int json_unsigned_byte(const char* json, jsmntok_t* tok, void* data)
{
    //Attempt to convert the data pointer to the appropriate type
    uint8_t* ret = (uint8_t*)data;
    uint8_t dummy;

    //Check whether the token is a primitive with alphanumeric
    //that can be scanned as an unsigned integer
    if(tok->type == JSMN_PRIMITIVE && json[tok->start] >= '0' && json[tok->start] <= '9' && sscanf(json + tok->start, "%hhu", &dummy) == 1){

        //Write data only if the opaque pointer conversion succeeded
        //(this behavior can be used for dry runs)
        if(ret){ sscanf(json + tok->start, "%hhu", ret); }
        return 1;
    }
    return -1;
}

int json_signed(const char* json, jsmntok_t* tok, void* data)
{
    //Attempt to convert the data pointer to the appropriate type
    int* ret = (int*)data;
    int dummy;

    //Check whether the token is a primitive with alphanumeric
    //that can be scanned as a signed integer
    if(tok->type == JSMN_PRIMITIVE && ((json[tok->start] >= '0' && json[tok->start] <= '9') || json[tok->start] == '-') && sscanf(json + tok->start, "%d", &dummy) == 1){

        //Write data only if the opaque pointer conversion succeeded
        //(this behavior can be used for dry runs)
        if(ret){ sscanf(json + tok->start, "%d", ret); }
        return 1;
    }
    return -1;
}

int json_int(const char* json, jsmntok_t* tok, void* data)
{
    //Attempt to convert the data pointer to the appropriate type
    int* ret = (int*)data;
    int dummy;

    //Check whether the token is a primitive with alphanumeric
    //that can be scanned as an unsigned integer
    if(
        tok->type == JSMN_PRIMITIVE &&
        (
            json[tok->start] == '-' ||
            (json[tok->start] >= '0' && json[tok->start] <= '9')
        ) &&
        sscanf(json + tok->start, "%d", &dummy) == 1
    ){

        //Write data only if the opaque pointer conversion succeeded
        //(this behavior can be used for dry runs)
        if(ret){ sscanf(json + tok->start, "%d", ret); }
        return 1;
    }
    return -1;
}

int json_float(const char* json, jsmntok_t* tok, void* data)
{
    //Attempt to convert the data pointer to the appropriate type
    float* ret = (float*)data;
    float dummy;

    //Check whether the token is a primitive starting with a sign or alphanumeric
    //that can be scanned as a floating point value
    if(tok->type == JSMN_PRIMITIVE && (json[tok->start] == '-' || (json[tok->start] >= '0' && json[tok->start] <= '9')) && sscanf(json + tok->start, "%f",  &dummy) == 1){

        //Write data only if the opaque pointer conversion succeeded
        //(this behavior can be used for dry runs)
        if(ret){ sscanf(json + tok->start, "%f", ret); }
        return 1;
    }
    return -1;
}

int json_string(const char* json, jsmntok_t* tok, void* data)
{
    //Attempt to convert the data pointer to the appropriate type
    char* ret = (char*)data;

    //Check whether the token is a string
    if(tok->type == JSMN_STRING){

        //Write data only if the opaque pointer conversion succeeded
        //(this behavior can be used for dry runs)
        if(ret){
            memcpy(ret, &json[tok->start], tok->end - tok->start);
            ret[tok->end - tok->start] = '\0';
        }
        return 1;
    }
    return -1;
}

int json_hex(const char* json, jsmntok_t* tok, void* data)
{
    //Attempt to convert the data pointer to the appropriate type
    uint8_t* ret = (uint8_t*)data;

    //Check whether the token is a string
    if(tok->type != JSMN_STRING){ return -1; }

    //Check that the string is of even length
    if((tok->end - tok->start) % 2 != 0){ return -1; }

    //Check that every character is hexadecimal
    int i = 0; int len = tok->end - tok->start;
    while(i < len &&
        ((json[tok->start + i] >= '0' && json[tok->start + i] <= '9') ||
         (json[tok->start + i] >= 'a' && json[tok->start + i] <= 'f'))
    ){ ++i; }
    if(i != len){ return -1; }

    //Write data only if the opaque pointer conversion succeeded
    //(this behavior can be used for dry runs)
    if(ret){
        for(int i = 0; i < len; i += 2){
            ret[i / 2] = 0;
            for(int j = 0; j < 2; j++){
                char c = json[tok->start + i + j];
                //LOG_("ternary: %hhu", c >= '0' && c <= '9' ? c - '0' : c - 'a' + 10);
                ret[i / 2] += (
                    c >= '0' && c <= '9' ? c - '0' : c - 'a' + 10
                ) << (4 * (1 - j));
            }
        }
    }

    return 1;
}

/*
int json_vec3(const char* json, jsmntok_t* tok, void* data)
{
    //Attempt to convert the data pointer to the appropriate type
    vec3* ret = (vec3*)data;
    
    //Create the targets where the values should be written
    float* targets[3];
    if(ret){
        targets[0] = &(*ret)[0];
        targets[1] = &(*ret)[1];
        targets[2] = &(*ret)[2];
    } else {
        targets[0] = targets[1] = targets[2] = NULL;
    }

    //Check whether the token is an array
    if(tok->type == JSMN_ARRAY){

        //Check whether the next three tokens parse successfully as floats
        int i = 0;
        while (i < 3){
            if(json_float(json, tok + i + 1, targets[i]) > 0){
                ++i;
            } else { return -1; }
        }

        //If everything was parsed succesfully, return the number of parsed tokens
        return 4;
    }
    
    return -1;
}
*/
int json_bool_recursebase(void* data, size_t offset, bool write, const char* json, jsmntok_t* tok_start, int tok_count, JSONKeyInvocation* invoc, int invocations)
{
    return json_bool(json, tok_start, IF_THEN_ELSENULL(write, data + offset));
}

int json_unsigned_recursebase(void* data, size_t offset, bool write, const char* json, jsmntok_t* tok_start, int tok_count, JSONKeyInvocation* invoc, int invocations)
{
    return json_unsigned(json, tok_start, IF_THEN_ELSENULL(write, data + offset));
}

int json_unsigned_short_recursebase(void* data, size_t offset, bool write, const char* json, jsmntok_t* tok_start, int tok_count, JSONKeyInvocation* invoc, int invocations)
{
    return json_unsigned_short(json, tok_start, IF_THEN_ELSENULL(write, data + offset));
}

int json_unsigned_byte_recursebase(void* data, size_t offset, bool write, const char* json, jsmntok_t* tok_start, int tok_count, JSONKeyInvocation* invoc, int invocations)
{
    return json_unsigned_byte(json, tok_start, IF_THEN_ELSENULL(write, data + offset));
}

int json_signed_recursebase(void* data, size_t offset, bool write, const char* json, jsmntok_t* tok_start, int tok_count, JSONKeyInvocation* invoc, int invocations)
{
    return json_signed(json, tok_start, IF_THEN_ELSENULL(write, data + offset));
}

int json_int_recursebase(void* data, size_t offset, bool write, const char* json, jsmntok_t* tok_start, int tok_count, JSONKeyInvocation* invoc, int invocations)
{
    return json_int(json, tok_start, IF_THEN_ELSENULL(write, data + offset));
}

int json_float_recursebase(void* data, size_t offset, bool write, const char* json, jsmntok_t* tok_start, int tok_count, JSONKeyInvocation* invoc, int invocations)
{
    return json_float(json, tok_start, IF_THEN_ELSENULL(write, data + offset));
}

int json_string_recursebase(void* data, size_t offset, bool write, const char* json, jsmntok_t* tok_start, int tok_count, JSONKeyInvocation* invoc, int invocations)
{
    return json_string(json, tok_start, IF_THEN_ELSENULL(write, data + offset));
}

int json_hex_recursebase(void* data, size_t offset, bool write, const char* json, jsmntok_t* tok_start, int tok_count, JSONKeyInvocation* invoc, int invocations)
{
    return json_hex(json, tok_start, IF_THEN_ELSENULL(write, data + offset));
}

/*
int json_vec3_recursebase(void* data, size_t offset, bool write, const char* json, jsmntok_t* tok_start, int tok_count, JSONKeyInvocation* invoc, int invocations)
{
    return json_vec3(json, tok_start, IF_THEN_ELSENULL(write, data + offset));
}
*/

int json_arraypair_first_bool_recursebase(void* data, size_t offset, bool write, const char* json, jsmntok_t* tok_start, int tok_count, JSONKeyInvocation* invoc, int invocations)
{
    //Check that the starting token is an array
    if(tok_start->type == JSMN_ARRAY){

        //Check that the next token can be parsed as the required type
        if(json_bool(json, tok_start + 1, IF_THEN_ELSENULL(write, data + offset)) > 0){

            //Return success for three tokens:
            //the array, the value, and the ignored token
            return 3;
        }
    }
    return -1;
}

int json_arraypair_first_unsigned_recursebase(void* data, size_t offset, bool write, const char* json, jsmntok_t* tok_start, int tok_count, JSONKeyInvocation* invoc, int invocations)
{
    //Check that the starting token is an array
    if(tok_start->type == JSMN_ARRAY){

        //Check that the next token can be parsed as the required type
        if(json_unsigned(json, tok_start + 1, IF_THEN_ELSENULL(write, data + offset)) > 0){

            //Return success for three tokens:
            //the array, the value, and the ignored token
            return 3;
        }
    }
    return -1;
}

int json_arraypair_first_unsigned_short_recursebase(void* data, size_t offset, bool write, const char* json, jsmntok_t* tok_start, int tok_count, JSONKeyInvocation* invoc, int invocations)
{
    //Check that the starting token is an array
    if(tok_start->type == JSMN_ARRAY){

        //Check that the next token can be parsed as the required type
        if(json_unsigned_short(json, tok_start + 1, IF_THEN_ELSENULL(write, data + offset)) > 0){

            //Return success for three tokens:
            //the array, the value, and the ignored token
            return 3;
        }
    }
    return -1;
}

int json_arraypair_first_unsigned_byte_recursebase(void* data, size_t offset, bool write, const char* json, jsmntok_t* tok_start, int tok_count, JSONKeyInvocation* invoc, int invocations)
{
    //Check that the starting token is an array
    if(tok_start->type == JSMN_ARRAY){

        //Check that the next token can be parsed as the required type
        if(json_unsigned_byte(json, tok_start + 1, IF_THEN_ELSENULL(write, data + offset)) > 0){

            //Return success for three tokens:
            //the array, the value, and the ignored token
            return 3;
        }
    }
    return -1;
}

int json_arraypair_first_float_recursebase(void* data, size_t offset, bool write, const char* json, jsmntok_t* tok_start, int tok_count, JSONKeyInvocation* invoc, int invocations)
{
    //Check that the starting token is an array
    if(tok_start->type == JSMN_ARRAY){

        //Check that the next token can be parsed as the required type
        if(json_float(json, tok_start + 1, IF_THEN_ELSENULL(write, data + offset)) > 0){

            //Return success for three tokens:
            //the array, the value, and the ignored token
            return 3;
        }
    }
    return -1;
}

int json_arraypair_first_string_recursebase(void* data, size_t offset, bool write, const char* json, jsmntok_t* tok_start, int tok_count, JSONKeyInvocation* invoc, int invocations)
{
    //Check that the starting token is an array
    if(tok_start->type == JSMN_ARRAY){

        //Check that the next token can be parsed as the required type
        if(json_string(json, tok_start + 1, IF_THEN_ELSENULL(write, data + offset)) > 0){

            //Return success for three tokens:
            //the array, the value, and the ignored token
            return 3;
        }
    }
    return -1;
}

int json_find_string_in_array(const char (*str_array)[JSON_MAX_STRING], int count, const char* str, size_t n)
{
    int i = 0;
    while(i < count && 0 != strncmp(str, str_array[i], n)){ ++i; }
    return i < count ? i : -1;
}

void json_convert_float_divide(void* data, JSONConvertValue input, void* dest)
{
    //Attempt to cast the input data pointer void* to float*
    float* num = (float*)data; assert(num);

    //Attempt to cast the destination pointer,
    //NULL is interpreted as the output replacing the input
    float* out = (float*)(dest ? dest : data); assert(out);

    //Divide number by the input float, which should measure how many
    //real-world units is a simulation unit equivalent to
    *out = *num / input.f;
}

/*
void json_convert_vec3_divide(void* data, JSONConvertValue input, void* dest)
{
    //Attempt to cast the input data pointer void* to vec3*
    vec3* vec = (vec3*)data; assert(vec);

    //Attempt to cast the destination pointer,
    //NULL is interpreted as the output replacing the input
    vec3* out = (vec3*)(dest ? dest : data); assert(out);

    //Divide vector components by the input float, which should measure
    //how many real-world units is a simulation unit equivalent to
    glm_vec3_divs(*vec, input.f, *out);
}
*/
