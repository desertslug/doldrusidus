#ifndef DEFS_H
#define DEFS_H

//#include <math.h>
#include <stdint.h>
#include <stdarg.h>
#include <stdbool.h>
#include <time.h>
#include <semaphore.h>

#include "enet/enet.h"

// constants

enum { TWOPOW8 = 0x100, };
enum { TWOPOW16 = 0x10000, };
enum { PROQUINT_CHARS = 5, };
enum { CHAT_TEXT_MAX = 256, };

// components

//enum { SESSION_BYTES = 0x1000, }; 
enum { SETGET_ENTITY_HANDLER_NAME_MAX = 64, };
enum { COMPO_NAME_MAX = 64, };
enum { MEMBER_NAME_MAX = 64, };

// functions

bool session_authenticated(void* sess);
const char* session_get_uname(void* sess);

void* server_get_sessions(void* server);
void* server_get_auth(void* server);
void* server_get_config(void* server);

bool client_connect_wait(void* cli, int msec);

void* client_get_peer(void* cli);

bool client_auth_started(void* client);
const char* client_get_uname(void* client);
int client_get_access(void* client);

void client_login_start_auth(void* client, const char* uname, const char* pword, const unsigned char** bytes_A, int* len_A);
void client_logout_end_auth(void* client);

void* client_get_config(void* client);

#endif /* DEFS_H */
