#ifndef JSON_H
#define JSON_H

#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define PLANCKLOG_HEADER
#include "plancklog.h"

#define JSMN_HEADER
#include "jsmn.h"

#include "constants.h"
#include "helper.h"
#include "defs.h"

enum { JSON_MAX_STRING = 256, };

#define IF_THEN_ELSENULL(cond, arg) ((cond) ? arg : NULL)

typedef union {
    bool b;
    int i;
    float f;

} JSONConvertValue;

typedef void JSONValueConverterFun(void* data, JSONConvertValue input, void* dest);

typedef struct JSONKeyInvocation JSONKeyInvocation;

typedef int ValidateRecursiveJSONObjectFun(void* data, size_t offset, bool write, const char* json, jsmntok_t* tok_start, int tok_count, JSONKeyInvocation* invoc, int invocations);

struct JSONKeyInvocation {
    char key[JSON_MAX_STRING];
    void* data;
    void* user;
    size_t user_size;

    ValidateRecursiveJSONObjectFun* invocation;
    JSONKeyInvocation* next;
    int next_count;

    JSONValueConverterFun* converter;
    JSONConvertValue input;
    void* output;

    size_t stride;

    int elements;
};

#define RECURSE_ON_JSON_FUNDECL(name) \
int RecurseOn##name(void* data, size_t offset, bool write, const char* json, jsmntok_t* tok_start, int tok_count, JSONKeyInvocation* invoc, int invocations)

int RecurseOnJSONObject(void* data, size_t offset, bool write, const char* json, jsmntok_t* tokens, int tok_count, JSONKeyInvocation* invoc, int invocations);

int RecurseOnJSONArray(void* data, size_t offset, bool write, const char* json, jsmntok_t* tokens, int tok_count, JSONKeyInvocation* invoc, int invocations);

typedef int ValidateAndAllocateFromJSONFun(void* data, const char* json, jsmntok_t* tok_start, int tok_count);

typedef void ParseFromJSONFun(void* data, const char* json, jsmntok_t* tok_start, int tok_count);

bool JSONLoadFromFile(void* data, const char* path, ValidateAndAllocateFromJSONFun validator_allocator, ParseFromJSONFun parser);

int AllocateAndLoadJSONFromFile(char** json, const char* path);

void DeallocateJSON(char* json);

int AllocateAndParseJSMNTokens(const char* json, jsmntok_t** tokens);

void DeallocateJSMNTokens(jsmntok_t* tokens);



int json_tokens_in_token(jsmntok_t* tok_start, int tok_count);

int json_eq(const char* json, jsmntok_t* tok, const char* str);

int json_bool(const char* json, jsmntok_t* tok, void* data);

int json_unsigned(const char* json, jsmntok_t* tok, void* data);

int json_unsigned_short(const char* json, jsmntok_t* tok, void* data);

int json_unsigned_byte(const char* json, jsmntok_t* tok, void* data);

int json_signed(const char* json, jsmntok_t* tok, void* data);

int json_int(const char* json, jsmntok_t* tok, void* data);

int json_float(const char* json, jsmntok_t* tok, void* data);

int json_string(const char* json, jsmntok_t* tok, void* data);

int json_hex(const char* json, jsmntok_t* tok, void* data);

//int json_vec3(const char* json, jsmntok_t* tok, void* data);

int json_bool_recursebase(void* data, size_t offset, bool write, const char* json, jsmntok_t* tok_start, int tok_count, JSONKeyInvocation* invoc, int invocations);

int json_unsigned_recursebase(void* data, size_t offset, bool write, const char* json, jsmntok_t* tok_start, int tok_count, JSONKeyInvocation* invoc, int invocations);

int json_unsigned_short_recursebase(void* data, size_t offset, bool write, const char* json, jsmntok_t* tok_start, int tok_count, JSONKeyInvocation* invoc, int invocations);

int json_unsigned_byte_recursebase(void* data, size_t offset, bool write, const char* json, jsmntok_t* tok_start, int tok_count, JSONKeyInvocation* invoc, int invocations);

int json_signed_recursebase(void* data, size_t offset, bool write, const char* json, jsmntok_t* tok_start, int tok_count, JSONKeyInvocation* invoc, int invocations);

int json_int_recursebase(void* data, size_t offset, bool write, const char* json, jsmntok_t* tok_start, int tok_count, JSONKeyInvocation* invoc, int invocations);

int json_float_recursebase(void* data, size_t offset, bool write, const char* json, jsmntok_t* tok_start, int tok_count, JSONKeyInvocation* invoc, int invocations);

int json_string_recursebase(void* data, size_t offset, bool write, const char* json, jsmntok_t* tok_start, int tok_count, JSONKeyInvocation* invoc, int invocations);

int json_hex_recursebase(void* data, size_t offset, bool write, const char* json, jsmntok_t* tok_start, int tok_count, JSONKeyInvocation* invoc, int invocations);

//int json_vec3_recursebase(void* data, size_t offset, bool write, const char* json, jsmntok_t* tok_start, int tok_count, JSONKeyInvocation* invoc, int invocations);

int json_arraypair_first_bool_recursebase(void* data, size_t offset, bool write, const char* json, jsmntok_t* tok_start, int tok_count, JSONKeyInvocation* invoc, int invocations);

int json_arraypair_first_unsigned_recursebase(void* data, size_t offset, bool write, const char* json, jsmntok_t* tok_start, int tok_count, JSONKeyInvocation* invoc, int invocations);

int json_arraypair_first_unsigned_short_recursebase(void* data, size_t offset, bool write, const char* json, jsmntok_t* tok_start, int tok_count, JSONKeyInvocation* invoc, int invocations);

int json_arraypair_first_unsigned_byte_recursebase(void* data, size_t offset, bool write, const char* json, jsmntok_t* tok_start, int tok_count, JSONKeyInvocation* invoc, int invocations);

int json_arraypair_first_float_recursebase(void* data, size_t offset, bool write, const char* json, jsmntok_t* tok_start, int tok_count, JSONKeyInvocation* invoc, int invocations);

int json_arraypair_first_string_recursebase(void* data, size_t offset, bool write, const char* json, jsmntok_t* tok_start, int tok_count, JSONKeyInvocation* invoc, int invocations);

int json_find_string_in_array(const char (*str_array)[JSON_MAX_STRING], int count, const char* str, size_t n);

void json_convert_float_divide(void* data, JSONConvertValue input, void* dest);

//void json_convert_vec3_divide(void* data, JSONConvertValue input, void* dest);

#endif /* JSON_H */