#ifndef CONSTANTS_H
#define CONSTANTS_H

#define NSEC_SEC 1000000000
#define USEC_SEC 1000000
#define WHITESPACE " \t"
#define STR_EMPTY ""

#define uxn_word_t uint16_t
//#define UXN_WORD_NIBBLES 4
#define UXN_WORD_FMT "%hu"
#define UXN_PAGESIZE 256

#define ENTITY_ID_FMT "0x%04x"
#define ENTITY_ID_FMT_LEN (/*UXN_WORD_NIBBLES*/ 6 + 2)

//#define TWOPOW16 65536
#define TWOPOW16F 65536.0

#endif /* CONSTANTS_H */