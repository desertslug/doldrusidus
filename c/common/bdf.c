#include "bdf.h"

/** Convert a series of hex strings into a single glyph. One byte per pixel.
    \param bitmap destination to store glyph to
    \param glyph hex lines separated by line feed characters
    \param bytecount number of hex characters including line feeds
    \param bbox font bounding box
    \param glyphbbox glyph bounding box
    \param dwidthx TODO proportional fonts
    \param dwidthy TODO proportional fonts
 */
int bdf_assign_bitmap(uint8_t* bitmap, uint8_t* glyph, int bytecount, BDFBBox bbox, BDFBBox glyphbbox, int dwidthx, int dwidthy)
{
    static char* hextobin[] = {
        "0000","0001","0010","0011","0100","0101","0110","0111",
        "1000","1001","1010","1011","1100","1101","1110","1111"
    };

    int result = 0;
    
    LOG_DEBUG_("bitmap: %p, glyph: %p, bytecount: %d, bbox: %d %d %d %d, dwidthx: %d, dwidthy: %d",
        bitmap, glyph, bytecount,
        bbox.w, bbox.h, bbox.xoff, bbox.yoff,
        dwidthx, dwidthy);

    //Convert hexadecimal strings to bitmaps

    //Find the length of the hex string
    uint8_t* temp = (uint8_t*)strstr((char*)glyph, "\n");

    //Handle the case of "BITMAP\nENDCHAR", converting it to "00\n"
    if(!temp){
        glyph[0] = '0'; glyph[1] = '0'; glyph[2] = '\n';
        temp = glyph + 2;
    }
    int hexlen = temp - glyph;

    //Find byte dimensions (width, height), allocate source
    int bytedim[2] = { hexlen * 4, bytecount / (hexlen + 1) };
    uint8_t* src = malloc(bytedim[0] * bytedim[1]);
    //LOG_WARN_("bytedim: %d, %d", bytedim[0], bytedim[1]);

    //Convert
    int i = 0; int x = 0; int y = 0;
    while(i < bytecount){
        if(glyph[i] == '\n'){ x = 0; ++y; }
        else {
            //Convert one hexadecimal character to a decimal digit
            uint8_t c = glyph[i];
            uint8_t d = c >= '0' && c <= '9' ? c - '0' :
                       (c >= 'a' && c <= 'f' ? c - 'a' + 10 : c - 'A' + 10);
            
            //Copy the binary representation of the hex character to source
            for(int j = 0; j < 4; ++j, ++x){
                
                //Assert that the area pointed to is valid
                if(bytedim[0] * y + x > bytedim[0] * bytedim[1]){
                    LOG_ERROR_("bdf_assign_bitmap: out of bounds");
                    result = 1; goto bdf_assign_bitmap_cleanup;
                }
                src[bytedim[0] * y + x] = (hextobin[d][j] - '0') * 255;
            }
        }

        ++i;
    }

    //Initialize destination with zeros
    uint8_t dest[BDF_GLYPH_BYTES_MAX];
    memset(dest, 0, BDF_GLYPH_BYTES_MAX);
    //TODO: support proportional fonts

    //Copy source (inside BBX) to destination, shifting by the offset
    int loff, boff, toff; //bottom, left, top offsets
    loff = glyphbbox.xoff - bbox.xoff;
    boff = glyphbbox.yoff - bbox.yoff;
    toff = bbox.h - glyphbbox.h - boff;
    for(i = 0; i < bbox.h; ++i){
    for(int j = 0; j < bbox.w; ++j){
        if(
            !(i < toff || i >= toff + glyphbbox.h) &&
            !(j < loff || j >= loff + glyphbbox.w)
        ){
            dest[bbox.w * i + j] = src[bytedim[0] * (i - toff) + (j - loff)];
        }
    }}

    //Copy result to bitmap
    memcpy(bitmap, dest, bbox.w * bbox.h);

bdf_assign_bitmap_cleanup:
    free(src);
    return result;
}

/** Read a series of glyphs from a BDF file. One byte per pixel.
    \param file the opened source file
    \param maxglyphs the largest glyph code plus one
    \param bitmap_out[out] address of pointer the result will be returned to
    \param chars_out[out] number of glyphs
    \param bbox_out[out] font bounding box
 */
int bdf_read(FILE* file, int maxglyphs, uint8_t** bitmap_out, int* chars_out, BDFBBox* bbox_out)
{
    int length = 0;
    char line[BDF_LINE_CHARS_MAX];
    bool validated = false;
    char* tok = NULL;
    BDFBBox bbox;
    uint8_t glyph[BDF_GLYPH_BYTES_MAX];
    BDFBBox glyphbbox;
    int glyphcount = 0;
    uint8_t* bitmap = NULL;
    uint8_t flagbitmap = 0;
    uint8_t* nextglyph;
    int chars = 0;
    int glyphcode = 0;

    //Position of the next glyph's origin relative to the current
    int dwidthx = 0; int dwidthy = 0;

    //Read file line by line
    for(;;){

        char* ret = fgets(line, BDF_LINE_CHARS_MAX, file);
        if(!ret){ break; }
        else { length = strlen(line); }

        //Substitute carriage return for line feed
        for(int i = 0; i < length; ++i){
            if(line[i] == '\r'){ line[i] = '\n'; --length; }
        }

        //Branch based on the first character
        switch(line[0]){
        case 'S': {

            //If the file is not yet validated, check for "STARTFONT"
            if(!validated){
                if(strncmp(line, "STARTFONT ", 10) == 0){
                    LOG_DEBUG_("STARTFONT found");
                    validated = true;
                } else {
                    LOG_ERROR_("STARTFONT expected but not found");
                    goto bdf_read_cleanup;
                }
            }

        } break;
        case 'F': {
            if(strncmp(line, "FONTBOUNDINGBOX ", 16) == 0){

                tok = strtok(line, " "); //tokenize until keyword
                tok += strlen(tok) + 1; //skip to space after keyword

                //Extract bounding box for the font

            tok = strtok(tok," "); bbox.w = atoi(tok);    tok += strlen(tok)+1;
            tok = strtok(tok," "); bbox.h = atoi(tok);    tok += strlen(tok)+1;
            tok = strtok(tok," "); bbox.xoff = atoi(tok); tok += strlen(tok)+1;
            tok = strtok(tok," "); bbox.yoff = atoi(tok); tok += strlen(tok)+1;

                LOG_DEBUG_("bounding box: w: %d, h: %d, xoff: %d, yoff: %d",
                    bbox.w, bbox.h, bbox.xoff, bbox.yoff);

            } else { BDF_STORE_BITMAP(); break; }
        } break;
        case 'C': {
            if(strncmp(line, "CHARS ", 6) == 0){

                tok = strtok(line, " "); //tokenize until keyword
                tok += strlen(tok) + 1; //skip to space after keyword

                //Extract number of glyphs in font
                tok = strtok(tok, "\n");
                glyphcount = atoi(tok);

                //Allocate bitmap
                if(!(bitmap = malloc(maxglyphs * bbox.w * bbox.h))){
                    LOG_ERROR_("malloc error");
                    goto bdf_read_cleanup;
                }

                LOG_DEBUG_("glyphcount: %d", glyphcount);

            } else { BDF_STORE_BITMAP(); break; }
        } break;
        case 'D': {
            if(strncmp(line, "DWIDTH ", 7) == 0){

                tok = strtok(line, " "); //tokenize until keyword
                tok += strlen(tok) + 1; //skip to space after keyword

                //Extract dwidth dimensions
                
            tok = strtok(tok, " ");  dwidthx = atoi(tok); tok += strlen(tok)+1;
            tok = strtok(tok, "\n"); dwidthy = atoi(tok); tok += strlen(tok)+1;

                LOG_DEBUG_("dwidthx: %d, dwidthy: %d", dwidthx, dwidthy);

            } else { BDF_STORE_BITMAP(); break; }
        } break;
        case 'B': {
            if(strncmp(line, "BITMAP", 6) == 0){

                //Clear glyph
                memset(glyph, 0, BDF_GLYPH_BYTES_MAX);

                //Set pointer to next character of glyph
                nextglyph = glyph;

                //Set flag indicating that we're storing a glyph
                flagbitmap = 1;

            } else
            if(strncmp(line, "BBX ", 4) == 0){

                tok = strtok(line, " "); //tokenize until keyword
                tok += strlen(tok) + 1; //skip to space after keyword

                //Extract bounding box for individual glyph

    tok = strtok(tok," "); glyphbbox.w = atoi(tok);    tok += strlen(tok)+1;
    tok = strtok(tok," "); glyphbbox.h = atoi(tok);    tok += strlen(tok)+1;
    tok = strtok(tok," "); glyphbbox.xoff = atoi(tok); tok += strlen(tok)+1;
    tok = strtok(tok," "); glyphbbox.yoff = atoi(tok); tok += strlen(tok)+1;

                LOG_DEBUG_("glyphbbox: w: %d, h: %d, xoff: %d, yoff: %d",
                    glyphbbox.w, glyphbbox.h, glyphbbox.xoff, glyphbbox.yoff);

            } else { BDF_STORE_BITMAP(); break; }
        } break;
        case 'E': {
            if(strncmp(line, "ENDCHAR", 7) == 0){

                //
                if(bdf_assign_bitmap(
                    bitmap + glyphcode * bbox.w * bbox.h,
                    glyph, nextglyph - glyph, bbox, glyphbbox, dwidthx, dwidthy)
                ){
                    goto bdf_read_cleanup;
                };
                ++chars;
                
                //Unset flag indicating we're storing a bitmap
                flagbitmap = 0;

            } else
            if(strncmp(line, "ENCODING ", 9) == 0){

                tok = strtok(line, " "); //tokenize until keyword
                tok += strlen(tok) + 1; //skip to space after keyword

                //Extract glyph code
                tok = strtok(tok, " ");
                if(sscanf(tok, "%d", &glyphcode) != 1){
                    LOG_ERROR_("STARTCHAR glyph code failed to parse as decimal");
                    goto bdf_read_cleanup;
                }
                tok += strlen(tok)+1;

                LOG_WARN_("glyphcode: %d", glyphcode);
            } else { BDF_STORE_BITMAP(); break; }
        } break;
        default: { BDF_STORE_BITMAP(); break; }
        }
    }

    *bitmap_out = bitmap;
    *chars_out = chars;
    *bbox_out = bbox;
bdf_read_cleanup:
    return validated == false;
}

/** Transfer a series of glyphs to an image.
    \param src glyph bytes, one byte per pixel
    \param glyphs number of glyphs
    \param fontw width of the font bounding box
    \param fonth height of the font bounding box
    \param columns number of columns in the resulting image
    \param dest[out] address of pointer the result will be returned to
    \return the number of glyph rows in the resulting image
 */
int bdf_glyphs_to_image(uint8_t* src, int glyphs, int fontw, int fonth, int columns, uint8_t** dest)
{
    //Calculate image dimensions
    int rows = ((glyphs - 1) / (columns)) + 1;
    int dim[2] = { columns * fontw, rows * fonth };
    int glyphbytes = fontw * fonth;
    int pixels = dim[0] * dim[1];

    //Allocate returned memory
    *dest = malloc(pixels);

    //Map pixels to their proper places in the image
    for(int i = 0; i < pixels; ++i){
        int destidx[2] = { i / dim[0], i % dim[0] };
        int srcidx[2] = { destidx[0] / fonth, destidx[1] / fontw };
        int glyphpix[2] = {
            destidx[0] - srcidx[0] * fonth,
            destidx[1] - srcidx[1] * fontw
        };

        int charidx = srcidx[0] * columns + srcidx[1];
        if(charidx >= glyphs){ continue; }
        
        (*dest)[i] = src[
            charidx * glyphbytes + glyphpix[0] * fontw + glyphpix[1]
        ];
    }

    return rows;
}