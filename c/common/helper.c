#include "helper.h"

void assert_or_exit(bool cond, const char* fmt, ...)
{
    va_list argp;
    va_start(argp, fmt);
    if(!cond){
        LOG_ERROR_("exiting because of assert_or_exit condition");
        vfprintf(LOGFILE, fmt, argp);
        exit(1);
    }
    va_end(argp);
    
}

void hash_to_string(char string[65], const uint8_t hash[32])
{
	size_t i;
	for (i = 0; i < 32; i++) {
		string += sprintf(string, "%02x", hash[i]);
	}
}

int sign(int x)
{
    return (x > 0) - (x < 0);
}

int modulo(int a, int b)
{
    int x = a % b;
    return x < 0 ? x + b : x;
}

int clamp(int x, int min, int max)
{
    int a = x < min ? min : x;
    return a > max ? max : a;
}

uint32_t clampu(uint32_t x, uint32_t min, uint32_t max)
{
    uint32_t a = x < min ? min : x;
    return a > max ? max : a;
}

float fclamp(float x, float min, float max)
{
    float a = x < min ? min : x;
    return a > max ? max : a;
}

size_t ceil_power_of_two(size_t x)
{
    int n = 0;
    while(x > 1){ ++n; x >>= 1; }
    return x << (n + 1);
}

void serialize_hex(const uint8_t* src, size_t sz, char* dest)
{
    const char* hex = "0123456789abcdef";
    for(int i = 0; i < sz; ++i){
        dest[i*2]   = hex[(src[i] & 0xf0) >> 4];
        dest[i*2+1] = hex[(src[i] & 0x0f)];
    }
}

void deserialize_hex(const char* src, size_t sz, uint8_t* dest)
{
    for(int i = 0; i < sz; i += 2){
        dest[i / 2] = 0;
        for(int j = 0; j < 2; j++){
            char c = src[i + j];
            dest[i / 2] += (
                c >= '0' && c <= '9' ? c - '0' : c - 'a' + 10
            ) << (4 * (1 - j));
        }
    }
}

size_t trailing_zero_bytes(const uint8_t* src, size_t sz)
{
    int i = sz - 1;
    while(i >= 0 && src[i] == 0){ --i; }
    return sz - 1 - i;
}

int ipv4_from_dotdecimal_or_proquint(const char* str, uint8_t out[4])
{
    int length = strlen(str);

    //Dot-decimal
    if(str[0] >= '0' && str[0] <= '9'){

	//Check using sscanf
	uint8_t addr[4];
	if(sscanf(str, "%hhu.%hhu.%hhu.%hhu",
	    &addr[0], &addr[1], &addr[2], &addr[3]) != 4){ return 1; }
	
	for(int i = 0; i < 4; ++i){ out[i] = addr[i]; }
    }
    //Proquint
    else {

	//Check for a valid double-proquint
	if(!(length == 11 && quint32valid(str))){ return 1; }

	uint32_t tetra = quint322tetra(str);
	out[0] = tetra >> 24;
	out[1] = tetra >> 16;
	out[2] = tetra >> 8;
	out[3] = tetra & 0xff;
    }

    return 0;
}

void port_str_to_int_or_raise(const char* src, uint16_t* dest)
{
    if(sscanf(src, "%hu", dest) != 1){
        LOG_ERROR_("port_str_to_int_or_raise: failed to parse port");
        raise(SIGINT);
    }
}