/* This file is a slightly rewritten version of proquint:
 * http://github.com/dsw/proquint
 * Thank you to Daniel S. Wilkerson for the concept and implementation.
 */

#ifndef PROQUINT_H
#define PROQUINT_H

#include <stdint.h>
#include <assert.h>

/*
Convert between proquint, hex, and decimal strings.
Please see the article on proquints: http://arXiv.org/html/0901.4016
Daniel S. Wilkerson

Four-bits as a consonant:
    0 1 2 3 4 5 6 7 8 9 A B C D E F
    b d f g h j k l m n p r s t v z

Two-bits as a vowel:
    0 1 2 3
    a i o u

Whole 16-bit word, where "con" = consonant, "vo" = vowel.
     0 1 2 3 4 5 6 7 8 9 A B C D E F
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |con0   |vo1|con2   |vo3|con4   |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |-Hex0--|-Hex1--|-Hex2--|-Hex3--|
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*/

int quartvalid(const char* quart);

void short2quart(uint16_t i, char* quart);

uint16_t quart2short(const char* quart);

int quintvalid(const char* quint);

void short2quint(uint16_t i, char* quint);

uint16_t quint2short(const char* quint);

int quint32valid(const char* quint);

void tetra2quint32(uint32_t i, char* quint, char sep);

uint32_t quint322tetra(const char* quint);

int quartorquintvalid(const char* str);

int quartorquint2short(const char* str, uint16_t* out);

#endif /* PROQUINT_H */
