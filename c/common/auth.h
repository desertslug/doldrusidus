#ifndef AUTH_H
#define AUTH_H

#include "assert.h"

//#include "sha256.h"
#include "defs.h"
#include "helper.h"
#include "csv.h"
#include "uvec.h"

#define PLANCKLOG_HEADER
#include "plancklog.h"

#define SRP_HASH SRP_SHA1
#define SRP_NG SRP_NG_2048
#define SRP_SALT_BYTES 4
#define SRP_SALT_HEX_BYTES (2 * SRP_SALT_BYTES)
#define SRP_VERIFIER_BYTES 256
#define SRP_VERIFIER_HEX_BYTES (2 * SRP_VERIFIER_BYTES)
#define SRP_M1_BYTES 20
#define SHA512_DIGEST_LENGTH 64

#define AUTH_CSV_COLS 4
#define ACCESS_MAX 16

enum { UNAME_MIN = 1, };
enum { UNAME_MAX = 32, };

typedef enum Access
{
    AccessRoot = 0,
    AccessGuest,
    AccessDenied,
    AccessCount,
} Access;

typedef struct User
{
    char uname[UNAME_MAX];
    Access access;
    uint8_t salt[SRP_SALT_BYTES];
    uint8_t verifier[SRP_VERIFIER_BYTES];
} User;

bool user_has_password(User* user, const char* pword);

typedef struct Auth
{
    rtcp_uvec_t users;
} Auth;

void auth_alloc(Auth* auth);
void auth_dealloc(Auth* auth);
void auth_read(Auth* auth, const char* path);
void auth_write(Auth* auth, const char* path);
void auth_add(Auth* auth, const char* uname, const unsigned char* s, const unsigned char* v);
void auth_remove(Auth* auth, const char* uname);

int auth_find_user(Auth* auth, const char* uname, User** out);

#endif /* AUTH_H */
