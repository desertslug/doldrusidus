#ifndef BDF_H
#define BDF_H

#define PLANCKLOG_HEADER
#include "plancklog.h"

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

#define BDF_STORE_BITMAP()\
{ \
    if(flagbitmap){ memcpy(nextglyph, line, length); nextglyph += length; } \
}

enum { BDF_LINE_CHARS_MAX = 1024, }; // no rhyme or reason
enum { BDF_GLYPH_BYTES_MAX = 4096, }; // no rhyme or reason

typedef struct BDFBBox
{
    int w, h, xoff, yoff;
} BDFBBox;

int bdf_assign_bitmap(uint8_t* bitmap, uint8_t* glyph, int bytecount, BDFBBox bbox, BDFBBox glyphbbox, int dwidthx, int dwidthy);

int bdf_read(FILE* file, int maxglyphs, uint8_t** bitmap_out, int* chars_out, BDFBBox* bbox_out);

int bdf_glyphs_to_image(uint8_t* src, int glyphs, int fontw, int fonth, int columns, uint8_t** dest);

#endif /* BDF_H */