#ifndef NANOSLEEP_H
#define NANOSLEEP_H

#include <stdbool.h>
#include <time.h>

//#include "macros.h" //temporary

int diff_msec(const struct timespec* start, const struct timespec* end);
int diff_nsec(const struct timespec* start, const struct timespec* end);
struct timespec timespec_norm(const struct timespec* spec);
struct timespec timespec_add(const struct timespec* start, const struct timespec* end);
struct timespec timespec_diff(const struct timespec* start, const struct timespec* end);
bool timespec_less(const struct timespec* start, const struct timespec* end);
float timespec_float(const struct timespec* spec);

#ifdef OS_WIN

#include <windows.h>

#define WIN_SLEEP_CONFIDENCE_MILLISEC 2

#define WIN_YIELD_CONFIDENCE_MILLISEC 0.25f

int nanoschleep(const struct timespec* requested_delay, struct timespec* remaining_delay);

#endif /* OS_WIN */

#ifdef OS_NIX

#include <sched.h>

#define NIX_SLEEP_CONFIDENCE_MILLISEC 0.5f

#define NIX_YIELD_CONFIDENCE_MILLISEC 0.25f

int nanoschleep(const struct timespec* requested_delay, struct timespec* remaining_delay);

#endif /* OS_NIX */

#endif /* NANOSLEEP_H */