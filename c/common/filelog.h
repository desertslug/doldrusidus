#ifndef FILELOG_H
#define FILELOG_H

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <errno.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>
#include <signal.h>

#define PLANCKLOG_HEADER
#include "plancklog.h"

#define LOGTIME_FMT "%04d%02d%02d-%02d%02d%02d"
#define LOGTIME_CHARS 15
#define LOGEXT_STR ".txt"

typedef struct FileLog
{
    struct tm tm;
} FileLog;

void filelog_init(FileLog* flog);

void filelog_check_and_possibly_reopen(FileLog* flog);

void filelog_reopen(FileLog* flog, struct tm* tm);

void filelog_close(FileLog* flog);

bool distant_log_time(struct tm* lhs, struct tm* rhs);

#endif /* FILELOG_H */