/* This file is a slightly rewritten version of proquint:
 * http://github.com/dsw/proquint
 * Thank you to Daniel S. Wilkerson for the concept and implementation.
 */

#include "proquint.h"

static uint8_t const consonantorvowel2shift[256] = {
    ['b'] = 4, ['d'] = 4, ['f'] = 4, ['g'] = 4,
    ['h'] = 4, ['j'] = 4, ['k'] = 4, ['l'] = 4,
    ['m'] = 4, ['n'] = 4, ['p'] = 4, ['r'] = 4,
    ['s'] = 4, ['t'] = 4, ['v'] = 4, ['z'] = 4,
    ['a'] = 2, ['i'] = 2, ['o'] = 2, ['u'] = 2
};

static uint8_t const consonantorvowel2uint[256] = {
  ['b'] = 0,  ['d'] = 1,  ['f'] = 2,  ['g'] = 3,
  ['h'] = 4,  ['j'] = 5,  ['k'] = 6,  ['l'] = 7,
  ['m'] = 8,  ['n'] = 9,  ['p'] = 10, ['r'] = 11,
  ['s'] = 12, ['t'] = 13, ['v'] = 14, ['z'] = 15,
  ['a'] = 0,  ['i'] = 1,  ['o'] = 2,  ['u'] = 3
};

static uint8_t const hexadecimal2uint[256] = {
    ['0'] = 0,  ['1'] = 1,  ['2'] = 2,  ['3'] = 3,
    ['4'] = 4,  ['5'] = 5,  ['6'] = 6,  ['7'] = 7,
    ['8'] = 8,  ['9'] = 9,
    ['a'] = 10, ['b'] = 11, ['c'] = 12, ['d'] = 13, ['e'] = 14, ['f'] = 15,
    ['A'] = 10, ['B'] = 11, ['C'] = 12, ['D'] = 13, ['E'] = 14, ['F'] = 15,
};

static uint8_t const uint2hexadecimal[256] = {
    '0', '1', '2', '3', '4', '5', '6', '7',
    '8', '9', 'a', 'b', 'c', 'd', 'e', 'f',
};

static char const uint2consonant[] = {
  'b', 'd', 'f', 'g',
  'h', 'j', 'k', 'l',
  'm', 'n', 'p', 'r',
  's', 't', 'v', 'z'
};

static char const uint2vowel[] = {
  'a', 'i', 'o', 'u'
};

int quartvalid(const char* q)
{
#define VALID_HEX(c) ( \
    ((c) >= 'a' && (c) <= 'f') || ((c) >= 'A' && (c) <= 'F') || \
    ((c) >= '0' && (c) <= '9') \
)

    return
        (q[0] && q[1] && q[2] && q[3]) &&
        (
            VALID_HEX(q[0]) &&
            VALID_HEX(q[1]) &&
            VALID_HEX(q[2]) &&
            VALID_HEX(q[3])
        );
}

void short2quart(uint16_t i, char* quart)
{
    for(int j = 0; j < 4; ++j){
        quart[3 - j] = uint2hexadecimal[(i >> (j << 2)) & 0xf];
    }
}

uint16_t quart2short(const char* quart)
{
    uint16_t res = 0;
    char c;

    for(; (c=*quart); ++quart) {
        res <<= 4; res += hexadecimal2uint[(int)c];
    }

    return res;
}

int quintvalid(const char* q)
{
#define VALID_CONSONANT(c) (                                \
    (c) == 'b' || (c) == 'd' || (c) == 'f' || (c) == 'g' || \
    (c) == 'h' || (c) == 'j' || (c) == 'k' || (c) == 'l' || \
    (c) == 'm' || (c) == 'n' || (c) == 'p' || (c) == 'r' || \
    (c) == 's' || (c) == 't' || (c) == 'v' || (c) == 'z'    \
)
#define VALID_VOWEL(c) (                                 \
    (c) == 'a' || (c) == 'i' || (c) == 'o' || (c) == 'u' \
)

    return
        (q[0] && q[1] && q[2] && q[3] && q[4]) &&
        (
            VALID_CONSONANT(q[0]) &&
            VALID_VOWEL(q[1]) &&
            VALID_CONSONANT(q[2]) &&
            VALID_VOWEL(q[3]) &&
            VALID_CONSONANT(q[4])
        );
}

void short2quint(uint16_t i, char* quint)
{
    //K&R section 2.9:
    //"Right shifting an unsigned quantity always fills vacated it with zero."
    uint32_t j;

#define APPEND(X) *quint = (X); ++quint

#define MASK_FIRST4 0xF000
#define MASK_FIRST2 0xC000

#define HANDLE_CONSONANT     \
    j = i & MASK_FIRST4;     \
    i <<= 4; j >>= 12;       \
    APPEND(uint2consonant[j])

#define HANDLE_VOWEL         \
    j = i & MASK_FIRST2;     \
    i <<= 2; j >>= 14;       \
    APPEND(uint2vowel[j])

    HANDLE_CONSONANT;
    HANDLE_VOWEL;
    HANDLE_CONSONANT;
    HANDLE_VOWEL;
    HANDLE_CONSONANT;

#undef HANDLE_VOWEL
#undef HANDLE_CONSONANT
#undef APPEND
#undef MASK_FIRST2
#undef MASK_FIRST4
}

uint16_t quint2short(const char* quint)
{
    uint16_t res = 0;
    char c;

    for(; (c=*quint); ++quint) {
        res <<= consonantorvowel2shift[(int)c];
        res += consonantorvowel2uint[(int)c];
    }

    return res;
}

int quint32valid(const char* quint)
{
    return quintvalid(&quint[0]) && quint[5] != '\0' && quintvalid(&quint[6]);
}

void tetra2quint32(uint32_t i, char* quint, char sep)
{
    short2quint(i >> 16, &quint[0]);
    quint[5] = sep;
    short2quint(i & 0xffff, &quint[6]);
}

uint32_t quint322tetra(const char* quint)
{
    char pq[6]; for(int i = 0; i < 5; ++i){ pq[i] = quint[i]; }; pq[5] = '\0';
    return (quint2short(pq) << 16) + quint2short(&quint[6]);
}

int quartorquintvalid(const char* str)
{
    return quartvalid(str) || quintvalid(str);
}

int quartorquint2short(const char* str, uint16_t* out)
{
    if(quintvalid(str)){ *out = quint2short(str); return 1; }
    else if(quartvalid(str)){ *out = quart2short(str); return 1; }
    else { return 0; }
}