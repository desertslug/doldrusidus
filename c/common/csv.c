#include "csv.h"

int csv_open(Csv* csv, const char *path, char sep, int cols)
{
    //Open file under path for reading and writing
    if((csv->fd = fopen(path, "r+")) == NULL){ return CSV_ERR; }

    //Store arguments
    csv->sep = sep;
    csv->cols = cols;
    
    //Initialize read position and bytes read
    csv->cur = csv->end = csv->readbytes = 0;

    //Initialize buffer to all null-terminators
    memset(csv->buf, '\0', CSV_BUF_MAX);

    //Allocate the field pointers
    csv->fields = malloc(sizeof(char*) * csv->cols);

    //Set the first field to point to the beginning of the buffer
    csv->fields[0] = &csv->buf[0];

    return 0;
}

int csv_close(Csv* csv)
{
    //Free field pointers
    free(csv->fields);

    //Close file
    return fclose(csv->fd);
}

void csv_reset(Csv* csv)
{
    //Initialize read position and bytes read
    csv->cur = csv->end = csv->readbytes = 0;

    //Seek to the beginning of the file
    return rewind(csv->fd);
}

int csv_read_next(Csv* csv, char*** fields)
{
    //If the buffer is empty, and end of file was reached, return early
    if(feof(csv->fd) && csv->cur >= csv->end){ return CSV_END; }

    //Push leftover data to the front of the buffer
    //printf("shuffling %d bytes to the front\n", csv->end - csv->cur);
    memcpy(csv->buf, &csv->buf[csv->cur], csv->end - csv->cur);
    csv->end -= csv->cur; csv->cur = 0;

    //Read all columns of a line
    int col = 0;
    csv->fields[0] = &csv->buf[0];
    while(
        col < csv->cols &&
        (!feof(csv->fd) || csv->cur < csv->end) &&
        csv->cur < CSV_BUF_MAX &&
        csv->buf[csv->cur] != '\n'
    ){
        //Fill the end of the buffer with data
        csv->readbytes =
            fread(&csv->buf[csv->end], 1, CSV_BUF_MAX - csv->end, csv->fd);

        //If reading was unsuccessful, propagate error
        if(csv->readbytes < 0){ return CSV_ERR; }
        //If reading was successful, update positions
        else { csv->end += csv->readbytes; }
        /*
        printf("csv->readbytes: %d\n", csv->readbytes);
        printf("csv: cur-end: %d-%d\n", csv->cur, csv->end);
        printf("buf[%d]: %c\n", csv->cur, csv->buf[csv->cur]);
        */
        //If a separator is found, terminate field & set the next field
        if(csv->buf[csv->cur] == csv->sep){
            csv->buf[csv->cur] = '\0';
            csv->fields[++col] = &csv->buf[csv->cur + 1];
        }

        //Step current byte
        ++csv->cur;
    }

    //Attempt to terminate the last field
    if(csv->cur < CSV_BUF_MAX){
        csv->buf[csv->cur++] = '\0';
        *fields = csv->fields;
    } else {
        return CSV_ERR;
    }

    return col ? CSV_OK : CSV_END;
}

int csv_append(Csv* csv, char*** fields)
{
    //Attempt to retrieve the last character of the file
    //if(!fseek(csv->fd, -1, SEEK_END)){ return CSV_ERR; }
    /*int seek = */fseek(csv->fd, -1, SEEK_END);
    char last;
    int bytes = fread(&last, 1, 1, csv->fd);

    //Add newline to nonempty file that is not terminated by it
    if(bytes > 0 && last != '\n'){
        if(!fprintf(csv->fd, "\n")){ return CSV_ERR; }
    }

    //Format the fields of the line to be appended into a buffer
    char buf[CSV_BUF_MAX]; buf[0] = '\0';
    for(int i = 0; i < csv->cols; ++i){
        //printf("fields[%d]: %s\n", i, (*fields)[i]);

        if(i > 0){ strncat(buf, &csv->sep, 1); }
        strcat(buf, (*fields)[i]);
    }

    //Write the new line to the end of the file
    if(!fwrite(buf, 1, strlen(buf), csv->fd)){ return CSV_ERR; }

    return CSV_OK;
}

int csv_clear(Csv* csv)
{
    csv_reset(csv);

    //Truncate the file to zero length
    return ftruncate(fileno(csv->fd), 0);
}