#ifndef CLIENT_H
#define CLIENT_H

#include <signal.h>

#include "enet/enet.h"

#define PLANCKLOG_HEADER
#include "plancklog.h"

#include "synchronizer.h"
#include "helper.h"
#include "config.h"
#include "mod.h"
#include "srp.h"
#include "packet.h"
#include "auth.h"
#include "msg.h"
#include "bdf.h"
#include "stb_image_write.h"

typedef struct ClientFuns
{
    void (*on_client_start)(void* cli);
    void (*on_client_stop)();
    void (*on_client_step)(struct timespec delta);
    void (*on_client_recv)(ENetPeer* peer, void* pkt);
    void (*on_client_args)(int argc, char** argv);
    void (*chprintf)(const char* fmt, ...);
} ClientFuns;

typedef struct Client
{
    StartConfig config;
    Synchronizer sync;

    uint8_t ipv4[4];
    ENetAddress address;
    ENetHost* host;
    ENetPeer* peer;

    void* coremod;
    ClientFuns funs;

    struct SRPUser* srpuser;
	Access access;

} Client;

void client_dlsym(ClientFuns* funs, void* lib);
void client_alloc(Client* cli);
void client_dealloc(Client* cli);
void client_start(Client* cli, int argc, char** argv);
void client_stop(Client* cli);

void client_args_default(Client* cli);
void client_args_process(Client* cli, int argc, char** argv);

void client_connect(Client* cli);
bool client_connect_wait(void* cli, int msec);
//void client_disconnect(Client* cli);

void client_step(Client* cli);
void client_recv(Client* cli, ENetPeer* peer, void* pkt);
void client_pkt_process(Client* cli, void* ipkt, void* opkt);

void* client_get_peer(void* cli);
const char* client_get_uname(void* client);
int client_get_access(void* client);

#endif /* CLIENT_H */
