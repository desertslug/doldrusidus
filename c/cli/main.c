#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <string.h>

#include "plancklog.h"

#ifdef PLANCKLOG_FILE
    #include "filelog.h"
#endif

#include "client.h"
Client cli;

//#include "constants.h"
//#include "macros.h"

#ifdef PLANCKLOG_FILE
    FileLog filelog;
#endif

void terminate()
{
    client_stop(&cli);
    client_dealloc(&cli);

#ifdef PLANCKLOG_FILE
    filelog_close(&filelog);
#endif
    
    exit(0);
}

void sigint_handler(int signum)
{
    LOG_WARN_("SIGINT received");
    terminate();
}

void sigterm_handler(int signum)
{
    LOG_WARN_("SIGTERM received");
    terminate();
}

int main(int argc, char** argv)
{
#ifdef PLANCKLOG_FILE
    filelog_init(&filelog);
#endif

    //Print help menu if specified
    if(
        argc > 1 &&
        (strcmp(argv[1], "-h") == 0 || strcmp(argv[1], "--help") == 0)
    ){
        fprintf(stdout,
"OVERVIEW: cli-doldrusidus\n\
  An open-ended, obscure simulation realized as a multiplayer universe.\n\
\n\
  doldrum - noun:\n\
  1. doldrums (plural) - a spell of listlessness or despondency\n\
  2. archaic - a sluggish or slow-witted person\n\
  3. doldrums (plural) - a region over the ocean near the equator abounding\n\
                         in calms, squalls, and light baffling winds\n\
\n\
  sidus (latin) - neutral noun:\n\
  1. a group of stars, constellation, heavenly body\n\
  2. tempest\n\
\n\
USAGE: cli-doldrusidus [options]\n\
\n\
OPTIONS:\n\
  -a <address>  dial initially into the address specified.\n\
                address can be in either dot notation or proquint form.\n\
  --bdf2tga <path> <columns>\n\
                loads the BDF font file from <path> and exports it to a\n\
                .tga image of the same name, with 1 color channel of 1 byte.\n\
                (each new row of glyphs starts after <columns> glyphs\n\
                have elapsed)\n\
  -c <string>   feeds the specififed command string as input to the client\n\
                after initialization. To make the client interpret multiple\n\
                lines, insert a \\n between lines, and use the $\'\' construct\n\
                to intepret escape sequences (then single quoting the result).\n\
  --cmds <path> feeds the specified command file as input to the client.\n\
  --conf <path> load the starting configuration from the specified path.\n\
  -h|--help     prints this help menu with the available options, then exits.\n\
  --ipv4-proquint\n\
                emits an IPv4 address in proquint form, then exits.\n\
  -p <port>     dial initially into the port specified.\n\
  --sv <uname> <pword>\n\
                generates & emits a salt & verification key that corresponds\n\
                to the username and password entered, then exits.\n\
  -w            opens a graphical window on startup.\n\
"
        );
        exit(0);
    }
    
    //Set signal handlers
    signal(SIGINT, sigint_handler);
    signal(SIGTERM, sigterm_handler);
    
    //Block signals initially, so all child processes created during
    //initialization will also have them blocked
    sigset_t ss, os;
    sigemptyset(&ss);
    sigaddset(&ss, SIGINT);
    sigaddset(&ss, SIGTERM);
    sigprocmask(SIG_BLOCK, &ss, &os);
    
    client_alloc(&cli);
    client_start(&cli, argc, argv);

    for(;;){
        sigprocmask(SIG_BLOCK, &ss, &os);
        //CLI_CLOCK_US_START(RENDER);
        //client_swap(&cli);
        //CLI_CLOCK_US_END(&cli, RENDER);
        //client_poll(&cli);
        client_step(&cli);
        //client_step_with_server(&cli);
        //client_draw(&cli);
	    sigprocmask(SIG_UNBLOCK, &ss, &os);
    }

    terminate();

    return 0;
}
