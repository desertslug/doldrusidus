#ifndef GLFW_H
#define GLFW_H

#define GLFW_INCLUDE_NONE
#include "GLFW/glfw3.h"
#define GLEW_NO_GLU
#include "GL/glew.h"

#define PLANCKLOG_HEADER
#include "plancklog.h"

#include "config.h"

GLFWwindow* glfwwindow_initialize(StartConfig* config, const char* title, void* cli, void* error_callback, void* key_callback, void* mouse_button_callback, void* cursorpos_callback, void* joystick_callback, void* window_size_callback);

void glfwwindow_terminate(GLFWwindow* window);

#endif /* GLFW_H */
