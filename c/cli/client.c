#include "client.h"

void client_dlsym(ClientFuns* funs, void* lib)
{
    //Load functions
    funs->on_client_start = dlsym_checked(lib, "on_client_start");
    funs->on_client_stop = dlsym_checked(lib, "on_client_stop");
    funs->on_client_step = dlsym_checked(lib, "on_client_step");
    funs->on_client_recv = dlsym_checked(lib, "on_client_recv");
    funs->on_client_args = dlsym_checked(lib, "on_client_args");
    funs->chprintf = dlsym_checked(lib, "chprintf");
}

void client_alloc(Client* cli)
{
    
}

void client_dealloc(Client* cli)
{
    
}

void client_start(Client* cli, int argc, char** argv)
{
    //Initialize values
    cli->peer = NULL;
    cli->srpuser = NULL;

    //Load starting configuration
    startconfig_load(&cli->config, CONFIG_FILEPATH);
    startconfig_print(&cli->config);

    //Set defaults for command line arguments, process arguments
    client_args_default(cli);
    client_args_process(cli, argc, argv);

    //Compile & load core mod, load functions
    int ret = mod_compile(
        cli->config.cc_path, cli->config.c_args, cli->config.cli_mod_path);
    assert_or_exit(ret == 0, "client_start: failed to compile mod\n");
    cli->coremod = mod_load(cli->config.cli_mod_path);
    assert_or_exit(cli->coremod, "client_start: failed to load mod\n");
    client_dlsym(&cli->funs, cli->coremod);

    //Initialize synchronizer
    synchronizer_init(&cli->sync, cli->config.cli_frame_nsec);

    //Initialize enet globally
    enet_initialize();

    //Create enet host
    cli->host = enet_host_create(NULL, 1, 1, 0, 0);
    assert_or_exit(cli->host, "client_start: could not create enet host\n");

    //Attempt to resolve host
    char ipv4address[16] = { '\0' };
    sprintf(ipv4address, "%hhu.%hhu.%hhu.%hhu",
        (cli->ipv4[0]) & 0xff, (cli->ipv4[1]) & 0xff,
        (cli->ipv4[2]) & 0xff, (cli->ipv4[3]) & 0xff);
    assert_or_exit(
        enet_address_set_host(&cli->address, ipv4address) == 0,
        "client_start: could not resolve enet host\n");

    //Connect to host
    client_connect(cli);

    //Propagate call
    cli->funs.on_client_start(cli);
    cli->funs.on_client_args(argc, argv);

    LOG_("client started");
}

void client_stop(Client* cli)
{
    //Propagate call
    cli->funs.on_client_stop();

    //Unload core mod
    if(mod_unload(cli->coremod)){
        LOG_WARN_("client_stop: mod_unload failed");
    }

    //Destroy enet host
    enet_host_destroy(cli->host);

    //Deinitialize enet globally
    enet_deinitialize();

    //Delete SRPUser if it was initialized
    if(cli->srpuser){ srp_user_delete(cli->srpuser); }

    LOG_("client stopped");
}

void client_args_default(Client* cli)
{
    //Convert host and set host
    if(ipv4_from_dotdecimal_or_proquint(cli->config.ipv4address, cli->ipv4) != 0){
        LOG_ERROR_("client_args_default: invalid address (dot-decimal or proquint)");
        raise(SIGINT);
    }

    //Convert port and set port number
    port_str_to_int_or_raise(cli->config.port, &cli->address.port);
}

void client_args_process(Client* cli, int argc, char** argv)
{
    //Process all arguments according to hardcoded interpretations
    int idx = 0;
    while(idx < argc){
        char* arg = argv[idx];

        //Address to dial into
        if(strcmp(arg, "-a") == 0 && argc - idx > 1){
            char* nextarg = argv[idx + 1];

            if(ipv4_from_dotdecimal_or_proquint(nextarg, cli->ipv4) != 0){
                LOG_ERROR_("-a: invalid address (dot-decimal or proquint)");
                raise(SIGINT);
            }
        }

        //Convert BDF font
        if(strcmp(arg, "--bdf2tga") == 0 && argc - idx > 2){
            char* path = argv[idx + 1];

            FILE* bdf = NULL;
            uint8_t* bitmap = NULL;
            uint8_t* img = NULL;

            //Find position of first '.' from the back
            int length = strlen(path);
            int pos_dot = length - 1;
            while(pos_dot >= 0 && path[pos_dot] != '.'){ --pos_dot; }

            //Check that a dot was indeed found
            if(pos_dot < 0){
                LOG_ERROR_("bdf2bmp: path contains no '.' before extension");
                goto bdf2bmp_cleanup;
            }

            //Check that the file has the bdf extension
            if(strcmp(&path[pos_dot], ".bdf") != 0){
                LOG_ERROR_("bdf2bmp: file does not have .bdf as its extension");
                goto bdf2bmp_cleanup;
            }

            //Check that the second argument is an integer
            int columns;
            if(sscanf(argv[idx + 2], "%d", &columns) != 1){
                LOG_ERROR_("bdf2bmp: could not parse argument as int32_t");
                goto bdf2bmp_cleanup;
            }
            //Check that the number of columns is between 1 and 256
            if(columns < 1 || columns > 256){
                LOG_ERROR_("bdf2bmp: columns must be between 1 and 256");
                goto bdf2bmp_cleanup;
            }

            //Attempt to open file for reading
            bdf = fopen(path, "r");
            if(!bdf){
                LOG_ERROR_("bdf2bmp: could not open file for reading: \"%s\"", path);
                LOG_ERROR_("strerror(errno): \"%s\"", strerror(errno));
                goto bdf2bmp_cleanup;
            }

            //Read bdf file
            int chars; BDFBBox bbox;
            bdf_read(bdf, 256, &bitmap, &chars, &bbox);

            //Write glyphs into an image
            int rows = bdf_glyphs_to_image(bitmap, chars,
                bbox.w, bbox.h, columns, &img);

            //Write result file
            strcpy(&path[length - 3], "tga");
            stbi_write_tga(path, columns * bbox.w, rows * bbox.h, 1, img);

bdf2bmp_cleanup:
            if(bitmap){ free(bitmap); }
            if(img){ free(img); }
            if(bdf){ fclose(bdf); }
            raise(SIGINT);
        }

        //Alternative path to load configuration from
        if(strcmp(arg, "--conf") == 0 && argc - idx > 1){
            char* nextarg = argv[idx + 1];

            //Attempt to load configuration from the parameter path
            startconfig_load(&cli->config, nextarg);
            startconfig_print(&cli->config);
        }

        //Port to listen on
        if(strcmp(arg, "-p") == 0 && argc - idx > 1){
            char* nextarg = argv[idx + 1];

            //Convert port and set port number
            port_str_to_int_or_raise(nextarg, &cli->address.port);
        }

        //Emit address in proquint form
        if(strcmp(arg, "--ipv4-proquint") == 0 && argc - idx > 1){
            char* nextarg = argv[idx + 1];

            //Attempt to parse IPv4 address
            uint8_t addr[4];
            if(
                sscanf(nextarg, "%hhu.%hhu.%hhu.%hhu",
                    &addr[0], &addr[1], &addr[2], &addr[3]) == 4
            ){
                char quint32[12] = {0};
                tetra2quint32(
                    (addr[0] << 24) + (addr[1] << 16) +
                    (addr[2] << 8)  + (addr[3] << 0),
                    quint32, '-');

                LOG_("--ipv4-proquint: %s %s", nextarg, quint32);
                raise(SIGINT);
            } else {
                LOG_WARN_("--ipv4-proquint: invalid address");
            }            
        }

        //Salt & verification key
        if(strcmp(arg, "--sv") == 0 && argc - idx > 2){
            char* uname = argv[idx + 1];
            char* pword = argv[idx + 2];

            //Small salt 's' & host password verifier 'v'
            const unsigned char* bytes_s = 0; int len_s = 0;
            const unsigned char* bytes_v = 0; int len_v = 0;

            //Create salt & verification key for the user's password
            srp_create_salted_verification_key(
                SRP_HASH, SRP_NG,
                uname, (const unsigned char*)pword,
                strlen(pword),
                &bytes_s, &len_s, &bytes_v, &len_v, NULL, NULL);

            //Convert & log both as hex
            char* hex_s = malloc(sizeof(char) * len_s * 2);
            char* hex_v = malloc(sizeof(char) * len_v * 2);
            serialize_hex(bytes_s, len_s, hex_s);
            serialize_hex(bytes_v, len_v, hex_v);
            LOG_("<%.*s>", len_s * 2, hex_s);
            LOG_("<%.*s>", len_v * 2, hex_v);
            
            //Free allocated salt & verification key representations
            free(hex_s); free(hex_v);
            free((char*)bytes_s); free((char*)bytes_v);

            raise(SIGINT);
        }

        ++idx;
    }
}

void client_connect(Client* cli)
{
    //Initiate connection to foreign host
    cli->peer = enet_host_connect(cli->host, &cli->address, 1, 0);
    assert_or_exit(cli->peer, "client_connect: enet_host_connect\n");
}

bool client_connect_wait(void* cli, int msec)
{
    ENetEvent event;

    //Wait for the connection attempt to succeed
    if(
        enet_host_service(((Client*)cli)->host, &event, msec) > 0 &&
        event.type == ENET_EVENT_TYPE_CONNECT
    ){ return true; }
    //The wait time expired or a disconnect event was received
    else { return false; }
}

void client_step(Client* cli)
{
    synchronizer_wait(&cli->sync);
    synchronizer_step(&cli->sync);

    ENetEvent event;
    while(enet_host_service(cli->host, &event, 0) > 0){

        switch(event.type){
        case ENET_EVENT_TYPE_NONE: {} break;
        case ENET_EVENT_TYPE_CONNECT: {
            
            uint32_t host = event.peer->address.host;
            LOG_("client connected to %hhu.%hhu.%hhu.%hhu:%hu",
                (host >> 0) & 0xff, (host >> 8) & 0xff,
                (host >> 16) & 0xff, (host >> 24) & 0xff,
                event.peer->address.port);
            cli->funs.chprintf("client connected to %hhu.%hhu.%hhu.%hhu:%hu\n",
                (host >> 0) & 0xff, (host >> 8) & 0xff,
                (host >> 16) & 0xff, (host >> 24) & 0xff,
                event.peer->address.port);

        } break;
        case ENET_EVENT_TYPE_RECEIVE: {

            //If the packet is empty, it is considered a heartbeat
            if(event.packet->dataLength == 0){

                //Send empty packet
                ENetPacket* pkt = pkt_create(NULL, 0, 0);
                enet_peer_send(event.peer, 0, pkt);
            } else {

                //Receive packet
                client_recv(cli, cli->peer, event.packet);
            }

            pkt_destroy(event.packet);

        } break;
        case ENET_EVENT_TYPE_DISCONNECT: {

            uint32_t host = event.peer->address.host;
            LOG_("client disconnected from %hhu.%hhu.%hhu.%hhu:%hu",
                (host >> 0) & 0xff, (host >> 8) & 0xff,
                (host >> 16) & 0xff, (host >> 24) & 0xff,
                event.peer->address.port);
            cli->funs.chprintf("client disconnected from %hhu.%hhu.%hhu.%hhu:%hu\n",
                (host >> 0) & 0xff, (host >> 8) & 0xff,
                (host >> 16) & 0xff, (host >> 24) & 0xff,
                event.peer->address.port);

            //Forcefully disconnect peer
            enet_peer_reset(cli->peer);

            //raise(SIGINT);

        } break;
        }
    }

    //Propagate call
    cli->funs.on_client_step(cli->sync.period);
}

void client_recv(Client* cli, ENetPeer* peer, void* pkt)
{
    assert_or_exit(peer, "client_recv: peer was NULL\n");

    //Attempt to process packet
    ENetPacket* opkt = pkt_create(NULL, 0, ENET_PACKET_FLAG_RELIABLE);
    client_pkt_process(cli, pkt, opkt);

    //Send nonempty output packet
    if(pkt_len(opkt)){

        enet_peer_send(peer, 0, opkt);

    } else { pkt_destroy(opkt); }

    //Propagate call
    cli->funs.on_client_recv(peer, pkt);
}

void client_pkt_process(Client* cli, void* ipkt, void* opkt)
{
    uint8_t* chunk = pkt_data(ipkt);
    if(!pkt_chunk_check(ipkt, chunk)){ return; }

    //Attempt to get command index
    uint16_t cmdidx;
    if(!msg_pkt_chunk_u16_single(ipkt, chunk, &cmdidx)){ return; }
    chunk += pkt_chunk_bytes(ipkt, chunk);

    switch(cmdidx){
    case MSG_LOGIN: {

        //Attempt to get username
        char* uname;
        if(!msg_pkt_chunk_ztprintascii(ipkt, chunk,
            UNAME_MIN + 1, UNAME_MAX, &uname)){ break; }
        chunk += pkt_chunk_bytes(ipkt, chunk);

        //Attempt to get bytes_s
        uint8_t* bytes_s; size_t len_s = SRP_SALT_BYTES;
        if(!msg_pkt_chunk_u8_minmax(ipkt, chunk, len_s, len_s, &bytes_s))
        { break; }
        chunk += pkt_chunk_bytes(ipkt, chunk);

        //Attempt to get bytes_B
        uint8_t* bytes_B; size_t len_B = SRP_VERIFIER_BYTES;
        if(!msg_pkt_chunk_u8_minmax(ipkt, chunk, len_B, len_B, &bytes_B))
        { break; }
        chunk += pkt_chunk_bytes(ipkt, chunk);

        //Process challenge
        unsigned char* bytes_M; int len_M;
        srp_user_process_challenge(cli->srpuser,
            bytes_s, len_s, bytes_B, len_B,
            (const unsigned char**)&bytes_M, &len_M);

        //SRP-6a safety check
        if(!bytes_M){ break; }

        //Attach msg type
        uint16_t idx = MSG_VERIFY;
        pkt_chunk_add(opkt, (uint8_t*)&idx, sizeof(uint16_t), PKT_CHUNK_U16);

        //Attach bytes_M
        pkt_chunk_add(opkt, (uint8_t*)bytes_M, len_M, PKT_CHUNK_U8);

    }; break;
    case MSG_VERIFY: {

        //Attempt to get bytes_HAMK
        uint8_t* bytes_HAMK; size_t len_HAMK = SHA512_DIGEST_LENGTH;
        if(!msg_pkt_chunk_u8_minmax(ipkt, chunk, len_HAMK, len_HAMK, &bytes_HAMK))
        { break; }
        chunk += pkt_chunk_bytes(ipkt, chunk);

        //Attempt to verify the session
        srp_user_verify_session(cli->srpuser, bytes_HAMK);
        if(!srp_user_is_authenticated(cli->srpuser)){ break; }

		//Attempt to get access level
		uint16_t access;
		if(!msg_pkt_chunk_u16_single(ipkt, chunk, &access)){ break; }
		chunk += pkt_chunk_bytes(ipkt, chunk);

		//Store access level
		cli->access = access;

    } break;
    default: {} break;
    }
}

void* client_get_peer(void* cli)
{
    return ((Client*)cli)->peer;
}

bool client_auth_started(void* client)
{
    Client* cli = (Client*)client;
    return cli->srpuser != NULL;
}

const char* client_get_uname(void* client)
{
    Client* cli = (Client*)client;
    return cli->srpuser && srp_user_is_authenticated(cli->srpuser) ?
        srp_user_get_username(cli->srpuser) : NULL;
}

int client_get_access(void* client)
{
    Client* cli = (Client*)client;
    return cli->srpuser && srp_user_is_authenticated(cli->srpuser) ?
        cli->access : AccessCount;
}

void client_login_start_auth(void* client, const char* uname, const char* pword, const unsigned char** bytes_A, int* len_A)
{
    Client* cli = (Client*)client;

    //Delete user if it was already initialized
    if(cli->srpuser){ srp_user_delete(cli->srpuser); }

    //Create new user
    struct SRPUser* srpuser = srp_user_new(
        SRP_HASH, SRP_NG,
        uname, (const unsigned char*)pword, strlen(pword), 0, 0
    );

    //Start authentication
    const char* username;
    srp_user_start_authentication(srpuser, &username, bytes_A, len_A);

    //Set user
    cli->srpuser = srpuser;
}

void client_logout_end_auth(void* client)
{
    Client* cli = (Client*)client;

    //Delete user if it was already initialized
    if(cli->srpuser){ srp_user_delete(cli->srpuser); }
    cli->srpuser = NULL;
}

void* client_get_config(void* client)
{
    Client* cli = (Client*)client;

    return &cli->config;
}
