#include "glfw.h"

GLFWwindow* glfwwindow_initialize(StartConfig* config, const char* title, void* cli, void* error_callback, void* key_callback, void* mouse_button_callback, void* cursorpos_callback, void* joystick_callback, void* window_size_callback)
{
    glEnable(GL_DEBUG_OUTPUT);

    glfwSetErrorCallback(error_callback);
    
    //Initialize GLFW
    if(!glfwInit()){
        LOG_ERROR_("failed to initialize GFLW");
        return NULL;
    }

    //Set whether the windowed window is resizable by the user
    glfwWindowHint(GLFW_RESIZABLE, config->cli_window_resizable);

    //Open a window and create its OpenGL context
    GLFWwindow* window = glfwCreateWindow(128, 128, title, NULL, NULL);
    if(!window){
        LOG_ERROR_("failed to open GLFW window");
        glfwTerminate();
        return NULL;
    }

    //Make the context the current one in the window
    glfwMakeContextCurrent(window);

    glfwSetJoystickCallback(joystick_callback);
    glfwSetKeyCallback(window, key_callback);
    glfwSetMouseButtonCallback(window, mouse_button_callback);
    glfwSetCursorPosCallback(window, cursorpos_callback);
    glfwSetWindowSizeCallback(window, window_size_callback);
    glfwSetWindowUserPointer(window, cli);

    //Make window the context the current one
    glfwMakeContextCurrent(window);

    //Log detected OpenGL version
    LOG_("glGetString(GL_VERSION): %s", glGetString(GL_VERSION));

    //Initialize GLEW
    int ret;
    if((ret = glewInit()) != GLEW_OK){
        LOG_ERROR_("failed to initialize GLEW");
        LOG_ERROR_("glewInit() returned: %s", glewGetErrorString(ret));
        return NULL;
    }

    //Set swap interval
    glfwSwapInterval(config->cli_swap_interval);
    
    //Return the window
    return window;
}

void glfwwindow_terminate(GLFWwindow* window)
{
    //Destroy the window
    glfwDestroyWindow(window);

    //Terminate GLFW
    glfwTerminate();
}
