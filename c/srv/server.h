#ifndef SERVER_H
#define SERVER_H

#include <signal.h>

#include "enet/enet.h"

#define PLANCKLOG_HEADER
#include "plancklog.h"

#include "synchronizer.h"
#include "helper.h"
#include "config.h"
#include "uvec.h"
#include "session.h"
#include "mod.h"
#include "packet.h"
#include "msg.h"
#include "auth.h"
#include "macros.h"
#include "filelog.h"

typedef struct ServerFuns
{
    void (*on_server_start)(void* srv);
    void (*on_server_stop)();
    void (*on_server_step)(struct timespec delta);
    void (*on_server_recv)(Session* session, void* pkt);
} ServerFuns;

typedef struct Server
{
    FileLog* flog;
    StartConfig config;
    Synchronizer sync;

    ENetAddress address;
    ENetHost* host;

    rtcp_uvec_t sessions;
    rtcp_uvec_t oldsessions;

    Auth auth;

    void* coremod;
    ServerFuns funs;

} Server;

void server_dlsym(ServerFuns* funs, void* lib);
void server_alloc(Server* srv);
void server_dealloc(Server* srv);
void server_start(Server* srv, int argc, char** argv, FileLog* flog);
void server_stop(Server* srv);

void server_args_default(Server* srv);
void server_args_process(Server* srv, int argc, char** argv);

void server_step(Server* srv);
void server_recv(Server* srv, void* pkt, Session* session);
void server_pkt_process(Server* srv, Session* session, void* ipkt, void* opkt);
void server_kick_sessions(Server* srv, char* uname);

#endif /* SERVER_H */