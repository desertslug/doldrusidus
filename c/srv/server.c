#include "server.h"

void server_dlsym(ServerFuns* funs, void* lib)
{
    //Load functions
    funs->on_server_start = dlsym_checked(lib, "on_server_start");
    funs->on_server_stop = dlsym_checked(lib, "on_server_stop");
    funs->on_server_step = dlsym_checked(lib, "on_server_step");
    funs->on_server_recv = dlsym_checked(lib, "on_server_recv");
}

void server_alloc(Server* srv)
{
    rtcp_uvec_malloc(&srv->sessions, 1, sizeof(Session));
    rtcp_uvec_malloc(&srv->oldsessions, 1, sizeof(int));
    auth_alloc(&srv->auth);
}

void server_dealloc(Server* srv)
{
    for(int i = 0; i < srv->sessions.size; ++i){
        session_dealloc(rtcp_uvec_get(&srv->sessions, i));
    }
    rtcp_uvec_free(&srv->sessions);
    rtcp_uvec_free(&srv->oldsessions);
    auth_dealloc(&srv->auth);
}

void server_start(Server* srv, int argc, char** argv, FileLog* flog)
{
    //Store values
    srv->flog = flog;

    //Load starting configuration
    startconfig_load(&srv->config, CONFIG_FILEPATH);
    startconfig_print(&srv->config);

    //Set defaults for command line arguments, process arguments
    server_args_default(srv);
    server_args_process(srv, argc, argv);

    //Compile & load core mod, load functions
    int ret = mod_compile(
        srv->config.cc_path, srv->config.c_args, srv->config.srv_mod_path);
    assert_or_exit(ret == 0, "server_start: failed to compile mod\n");
    srv->coremod = mod_load(srv->config.srv_mod_path);
    assert_or_exit(srv->coremod, "server_start: failed to load mod\n");
    server_dlsym(&srv->funs, srv->coremod);

    //Initialize synchronizer
    synchronizer_init(&srv->sync, srv->config.srv_tick_nsec);

    //Initialize enet globally
    enet_initialize();

    //Create enet host
    srv->address.host = ENET_HOST_ANY;
    srv->host = enet_host_create(&srv->address,
        srv->config.srv_session_limit, 1, 0, 0);
    assert_or_exit(srv->host, "server_start: could not create enet host\n");

    //Clear sessions
    rtcp_uvec_init(&srv->sessions);
    rtcp_uvec_init(&srv->oldsessions);

    //Read authentication
    auth_read(&srv->auth, srv->config.srv_auth_path);

    //Propagate call
    srv->funs.on_server_start(srv);

    LOG_("server started");
}

void server_stop(Server* srv)
{
    //Propagate call
    srv->funs.on_server_stop();

    //Write authentication
    auth_write(&srv->auth, srv->config.srv_auth_path);

    //Unload core mod
    if(mod_unload(srv->coremod)){
        LOG_WARN_("server_stop: mod_unload failed");
    }

    //Destroy enet host
    enet_host_destroy(srv->host);

    //Deinitialize enet globally
    enet_deinitialize();

    LOG_("server stopped");
}

void server_args_default(Server* srv)
{
    //Convert port and set port number
    port_str_to_int_or_raise(srv->config.port, &srv->address.port);
}

void server_args_process(Server* srv, int argc, char** argv)
{
    //Process all arguments according to hardcoded interpretations
    int idx = 0;
    while(idx < argc){
        char* arg = argv[idx];

        //Alternative path to load configuration from
        if(strcmp(arg, "--conf") == 0 && argc - idx > 1){
            char* nextarg = argv[idx + 1];

            //Attempt to load configuration from the parameter path
            startconfig_load(&srv->config, nextarg);
            startconfig_print(&srv->config);
        }

        //Port to listen on
        if(strcmp(arg, "-p") == 0 && argc - idx > 1){
            char* nextarg = argv[idx + 1];

            //Convert port and set port number
            port_str_to_int_or_raise(nextarg, &srv->address.port);
        }

        ++idx;
    }
}

void server_step(Server* srv)
{
    synchronizer_wait(&srv->sync);
    synchronizer_step(&srv->sync);

    ENetEvent event;
    while(enet_host_service(srv->host, &event, 0) > 0){

        switch(event.type){
        case ENET_EVENT_TYPE_NONE: {} break;
        case ENET_EVENT_TYPE_CONNECT: {

            //Check number of matching session addresses
            int same = 0;
            for(int i = 0; i < srv->sessions.size; ++i){
                Session* session = rtcp_uvec_get(&srv->sessions, i);
                if(enet_address_eq(&event.peer->address, &session->peer->address)){
                    ++same;
                }
            }

            //Only accept connection if it fits into the same-address limit
            //this helps protect the server against same-address loiterers,
            //but this means two users with the same address will have to share
            if(same < srv->config.srv_user_session_limit){

                //Start new session
                Session session;
                session_init(&session, session_id_get(event.peer), event.peer);

                //Add session
                rtcp_uvec_add(&srv->sessions, &session);

                uint32_t host = event.peer->address.host;
                LOG_("client connected from %hhu.%hhu.%hhu.%hhu:%hu",
                    (host >> 0) & 0xff, (host >> 8) & 0xff,
                    (host >> 16) & 0xff, (host >> 24) & 0xff,
                    event.peer->address.port);
            }
            //Otherwise, force the connection down
            else {
                enet_peer_reset(event.peer);

                uint32_t host = event.peer->address.host;
                LOG_("client refused from %hhu.%hhu.%hhu.%hhu:%hu",
                    (host >> 0) & 0xff, (host >> 8) & 0xff,
                    (host >> 16) & 0xff, (host >> 24) & 0xff,
                    event.peer->address.port);
            }

        } break;
        case ENET_EVENT_TYPE_RECEIVE: {

            //If the packet is empty, it is considered a heartbeat
            if(event.packet->dataLength == 0){

                //If the session can be found among the existing ones
                int idx = sessions_find_id(
                    &srv->sessions, session_id_get(event.peer));
                if(idx >= 0){

                    //Register heartbeat
                    session_recvheart(rtcp_uvec_get(&srv->sessions, idx));
                }
            } else {

                //If the session can be found among the existing ones
                int idx = sessions_find_id(
                    &srv->sessions, session_id_get(event.peer));
                if(idx >= 0){

                    //Receive packet
                    server_recv(srv,
                        event.packet, rtcp_uvec_get(&srv->sessions, idx));
                    
                    //Consider the packet as a heartbeat
                    session_recvheart(rtcp_uvec_get(&srv->sessions, idx));
                }
            }

            pkt_destroy(event.packet);

        } break;
        case ENET_EVENT_TYPE_DISCONNECT: {
            
            uint32_t host = event.peer->address.host;
            LOG_("client disconnected from %hhu.%hhu.%hhu.%hhu:%hu",
                (host >> 0) & 0xff, (host >> 8) & 0xff,
                (host >> 16) & 0xff, (host >> 24) & 0xff,
                event.peer->address.port);

            //If the session can be found among the existing ones
            int idx = sessions_find_id(
                &srv->sessions, session_id_get(event.peer));
            if(idx >= 0){

                //Stop session if it wasn't already
                Session* session = rtcp_uvec_get(&srv->sessions, idx);
                if(!session_stopped(session)){
                    session_stop(session);
                    rtcp_uvec_add(&srv->oldsessions, &idx);
                }
            }

        } break;
        }
    }

    //Check session heartbeats
    for(int i = 0; i < srv->sessions.size; ++i){
        Session* session = rtcp_uvec_get(&srv->sessions, i);

        //List session as expired if it's heartbeat stopped,
        //but only if it doesn't already have a stop timepoint
        struct timespec now; clock_gettime(CLOCK_REALTIME, &now);
        if(
            diff_msec(&session->recvheart, &now) >=
                srv->config.heartbeat_expire_msec &&
            !session_stopped(session)
        ){
            session_stop(session);
            rtcp_uvec_add(&srv->oldsessions, &i);

            //Disconnect session peer gently
            enet_peer_disconnect(session->peer, 0);
        }

        //Handle heartbeat for active sessions
        if(!session_stopped(session)){

            session_heartbeat(session,
                srv->config.heartbeat_recv_msec,
                srv->config.heartbeat_send_msec);
        }
    }

    //Check stopped sessions
    for(int i = srv->oldsessions.size - 1; i >= 0; --i){
        int idx = *(int*)rtcp_uvec_get(&srv->oldsessions, i);
        Session* session = rtcp_uvec_get(&srv->sessions, idx);

        //Wait for a short time to let the session drain
        struct timespec now; clock_gettime(CLOCK_REALTIME, &now);
        if(
            diff_msec(&session->stopped, &now) >=
                srv->config.session_drain_msec
        ){
            //Reset connection
            enet_peer_reset(session->peer);

            //Delete session
            session_dealloc(session);
            rtcp_uvec_del(&srv->oldsessions, i);
            rtcp_uvec_del(&srv->sessions, idx);
        }
    }

    //Compute variable-length delta time in microseconds
    uint32_t usec = (
        (srv->config.srv_tps_pass * srv->config.srv_tick_usec) /
        srv->config.srv_tps
    );
    struct timespec delta = (struct timespec){
        .tv_sec = usec / USEC_SEC,
        .tv_nsec = (usec - (usec / USEC_SEC) * USEC_SEC) * 1000
    };

    //Propagate call
    srv->funs.on_server_step(delta);
}

void server_recv(Server* srv, void* pkt, Session* session)
{
    //Attempt to process packet
    ENetPacket* opkt = pkt_create(NULL, 0, ENET_PACKET_FLAG_RELIABLE);
    server_pkt_process(srv, session, pkt, opkt);

    //Send nonempty output packet
    if(pkt_len(opkt) && session->peer){

        enet_peer_send(session->peer, 0, opkt);

    } else { pkt_destroy(opkt); }

    //Propagate call
    assert_or_exit(session, "server_recv: session was NULL\n");
    srv->funs.on_server_recv(session, pkt);
}

void server_pkt_process(Server* srv, Session* session, void* ipkt, void* opkt)
{
    uint8_t* chunk = pkt_data(ipkt);
    if(!pkt_chunk_check(ipkt, chunk)){ return; }

    //Attempt to get message index
    uint16_t cmdidx;
    if(!msg_pkt_chunk_u16_single(ipkt, chunk, &cmdidx)){ return; }
    chunk += pkt_chunk_bytes(ipkt, chunk);

    switch(cmdidx){
    case MSG_LOGIN: {

        //Attempt to get username
        char* uname;
        if(!msg_pkt_chunk_ztprintascii(ipkt, chunk,
            UNAME_MIN + 1, UNAME_MAX, &uname)){ break; }
        chunk += pkt_chunk_bytes(ipkt, chunk);

        //Attempt to get bytes_A
        uint8_t* bytes_A; size_t len_A = SRP_VERIFIER_BYTES;
        if(!msg_pkt_chunk_u8_minmax(ipkt, chunk, len_A, len_A, &bytes_A))
        { break; }
        chunk += pkt_chunk_bytes(ipkt, chunk);

        //Attempt to find username
        User* user; if(auth_find_user(&srv->auth, uname, &user) < 0){ break; }

        //Attempt to create verifier
        const unsigned char* bytes_B = NULL; int len_B;
        struct SRPVerifier* verifier = srp_verifier_new(
            SRP_HASH, SRP_NG, uname,
            user->salt, SRP_SALT_BYTES,
            user->verifier, SRP_VERIFIER_BYTES,
            bytes_A, len_A, &bytes_B, &len_B, 0, 0
        );

        //SRP-6a safety check
        if(!bytes_B){ srp_verifier_delete(verifier); break; }

        //Attach verifier to the session
        if(session->verifier){ srp_verifier_delete(session->verifier); }
        session->verifier = verifier;

        //Attach msg type
        uint16_t idx = MSG_LOGIN;
        pkt_chunk_add(opkt, (uint8_t*)&idx, sizeof(uint16_t), PKT_CHUNK_U16);

        //Attach username
        pkt_chunk_add(opkt, (uint8_t*)uname, strlen(uname) + 1, PKT_CHUNK_U8);

        //Attach bytes_s & bytes_B
        pkt_chunk_add(opkt, (uint8_t*)user->salt, SRP_SALT_BYTES, PKT_CHUNK_U8);
        pkt_chunk_add(opkt, (uint8_t*)bytes_B, len_B, PKT_CHUNK_U8);

    } break;
    case MSG_VERIFY: {

        //Attempt to get bytes_M
        uint8_t* bytes_M; size_t len_M = SRP_M1_BYTES;
        if(!msg_pkt_chunk_u8_minmax(ipkt, chunk, len_M, len_M, &bytes_M))
        { break; }
        chunk += pkt_chunk_bytes(ipkt, chunk);

        //Check the length of bytes_M
        if(srp_verifier_get_session_key_length(session->verifier) != len_M)
        { break; }

        //To get around the mysteriously uninitialized memory in the verifier,
        //zero-initialize a similar portion manually
        uint8_t HAMK[SHA512_DIGEST_LENGTH] = { 0 };

        //Attempt to verify the user, producing bytes_HAMK
        const unsigned char* bytes_HAMK;
        srp_verifier_verify_session(session->verifier, bytes_M, &bytes_HAMK);
        //if(!bytes_HAMK){ break; }

        //If verification failed, send logout message
        if(!bytes_HAMK){
            uint16_t idx = MSG_LOGOUT;
            pkt_chunk_add(opkt, (uint8_t*)&idx, sizeof(uint16_t), PKT_CHUNK_U16);
            break;
        }

		//Attempt to find user
		const char* uname = srp_verifier_get_username(session->verifier);
		User* user; if(auth_find_user(&srv->auth, uname, &user) < 0){ break; }

        //Extract bytes from bytes_HAMK that were probably initialized
        //WARNING: I have no idea how many bytes this should truly be
        memcpy(HAMK, bytes_HAMK, len_M);

        //Attach msg type
        pkt_chunk_add(opkt, (uint8_t*)&cmdidx, sizeof(uint16_t), PKT_CHUNK_U16);

        //Attach bytes_HAMK
        pkt_chunk_add(opkt, (uint8_t*)HAMK, SHA512_DIGEST_LENGTH, PKT_CHUNK_U8);
        //Note: for some reason, the contents of bytes_HAMK seem shorter
        //than SHA512_DIGEST_LENGTH, 20 bytes instead of 64

		//Attach access level
		uint16_t acclev = user->access;
		pkt_chunk_add(opkt, (uint8_t*)&acclev, sizeof(uint16_t), PKT_CHUNK_U16);


    } break;
    case MSG_LOGOUT: {

        //Process only for authenticated session
        if(!session_authenticated(session)){ break; }

        //If the session is authenticated, it must have a verifier
        assert_or_exit(session->verifier,
            "MSG_LOGOUT: authenticated session has no verifier");

        //Delete verifier
        srp_verifier_delete(session->verifier);
        session->verifier = NULL;

    } break;
    case MSG_CHANGEPW: {

        //Process only for authenticated session
        if(!session_authenticated(session)){ break; }

        //Attempt to find user
        const char* uname = srp_verifier_get_username(session->verifier);
        User* user; if(auth_find_user(&srv->auth, uname, &user) < 0){ break; }

        //Attempt to get bytes_s
        uint8_t* bytes_s; size_t len_s = SRP_SALT_BYTES;
        if(!msg_pkt_chunk_u8_minmax(ipkt, chunk, len_s, len_s, &bytes_s))
        { break; }
        chunk += pkt_chunk_bytes(ipkt, chunk);

        //Attempt to get bytes_v
        uint8_t* bytes_v; size_t len_v = SRP_VERIFIER_BYTES;
        if(!msg_pkt_chunk_u8_minmax(ipkt, chunk, len_v, len_v, &bytes_v))
        { break; }
        chunk += pkt_chunk_bytes(ipkt, chunk);

        //Set the new salt & password verifier key
        memcpy(user->salt, bytes_s, SRP_SALT_BYTES);
        memcpy(user->verifier, bytes_v, SRP_VERIFIER_BYTES);

    } break;
    case MSG_KICK: {

        //Process only for authenticated session
        if(!session_authenticated(session)){ break; }

        //Attempt to find user
        const char* uname0 = srp_verifier_get_username(session->verifier);
        User* user0; if(auth_find_user(&srv->auth, uname0, &user0) < 0){ break; }

        //Check access privileges
        if(!(user0->access <= AccessRoot)){ break; }

        //Attempt to get username
        char* uname;
        if(!msg_pkt_chunk_ztprintascii(ipkt, chunk,
            UNAME_MIN + 1, UNAME_MAX, &uname)){ break; }
        chunk += pkt_chunk_bytes(ipkt, chunk);

        //Attempt to find user
        User* user; if(auth_find_user(&srv->auth, uname, &user) < 0){ break; }

        //Kicked user must be at a lower access level
        if(user0->access >= user->access){ break; }

        //Kick all sessions of the user
        server_kick_sessions(srv, uname);

    } break;
    case MSG_REGISTER: {

        //Process only for unauthenticated session
        if(session_authenticated(session)){ break; }

        //Attempt to get username
        char* uname;
        if(!msg_pkt_chunk_ztprintascii(ipkt, chunk,
            UNAME_MIN + 1, UNAME_MAX, &uname)){ break; }
        chunk += pkt_chunk_bytes(ipkt, chunk);

        //Attempt to get bytes_s
        uint8_t* bytes_s; size_t len_s = SRP_SALT_BYTES;
        if(!msg_pkt_chunk_u8_minmax(ipkt, chunk, len_s, len_s, &bytes_s))
        { break; }
        chunk += pkt_chunk_bytes(ipkt, chunk);

        //Attempt to get bytes_v
        uint8_t* bytes_v; size_t len_v = SRP_VERIFIER_BYTES;
        if(!msg_pkt_chunk_u8_minmax(ipkt, chunk, len_v, len_v, &bytes_v))
        { break; }
        chunk += pkt_chunk_bytes(ipkt, chunk);

        //Attempt to find user, check that it fails
        User* user; if(auth_find_user(&srv->auth, uname, &user) >= 0){ break; }

        //Check the current user count against the registration limit
        if(srv->auth.users.size >= srv->config.srv_register_limit){ break; }

        //Add user to the authentications
        auth_add(&srv->auth, uname, bytes_s, bytes_v);

    }
    case MSG_UNREGISTER: {

        //Process only for authenticated session
        if(!session_authenticated(session)){ break; }

        //Attempt to find user
        const char* uname0 = srp_verifier_get_username(session->verifier);
        User* user0; if(auth_find_user(&srv->auth, uname0, &user0) < 0){ break; }

        //Attempt to get username
        char* uname;
        if(!msg_pkt_chunk_ztprintascii(ipkt, chunk,
            UNAME_MIN + 1, UNAME_MAX, &uname)){ break; }
        chunk += pkt_chunk_bytes(ipkt, chunk);

        //Attempt to find user
        User* user; if(auth_find_user(&srv->auth, uname, &user) < 0){ break; }

        //If the unregistered user differs from the authenticated one
        if(strcmp(uname0, uname) != 0){

            //Unregistered user must be at a lower access level
            if(user0->access >= user->access){ break; }
        }

        //Kick all sessions of the user
        server_kick_sessions(srv, uname);

        //Remove user from the authentications
        auth_remove(&srv->auth, uname);

    } break;
    case MSG_CHANGEACC: {

        //Process only for authenticated session
        if(!session_authenticated(session)){ break; }

        //Attempt to find user
        const char* uname0 = srp_verifier_get_username(session->verifier);
        User* user0; if(auth_find_user(&srv->auth, uname0, &user0) < 0){ break; }

        //Check access privileges
        if(!(user0->access <= AccessRoot)){ break; }

        //Attempt to get username
        char* uname;
        if(!msg_pkt_chunk_ztprintascii(ipkt, chunk,
            UNAME_MIN + 1, UNAME_MAX, &uname)){ break; }
        chunk += pkt_chunk_bytes(ipkt, chunk);

        //Attempt to find user
        User* user; if(auth_find_user(&srv->auth, uname, &user) < 0){ break; }

        //Changed user must be at a lower access level
        if(user0->access >= user->access){ break; }

        //Attempt to get access level
        uint16_t access;
        if(!msg_pkt_chunk_u16_single(ipkt, chunk, &access)){ break; }
        chunk += pkt_chunk_bytes(ipkt, chunk);

        //Check that the access level is a known one
        if(access < AccessRoot || access >= AccessCount){ break; }

        //Set the access level of the specified user
        user->access = access;

    } break;
    case MSG_GETAUTHS: {

        //Process only for authenticated session
        if(!session_authenticated(session)){ break; }

        //Attempt to find user
        const char* uname0 = srp_verifier_get_username(session->verifier);
        User* user0; if(auth_find_user(&srv->auth, uname0, &user0) < 0){ break; }

        //Check access privileges
        if(!(user0->access <= AccessRoot)){ break; }

        //Attach msg type
        pkt_chunk_add(opkt, (uint8_t*)&cmdidx, sizeof(uint16_t), PKT_CHUNK_U16);

        //Attach all registered users
        for(int i = 0; i < srv->auth.users.size; ++i){
            User* user = rtcp_uvec_get(&srv->auth.users, i);
            pkt_chunk_add(opkt, (uint8_t*)user->uname,
                strlen(user->uname) + 1, PKT_CHUNK_U8);
        }

    } break;
    case MSG_GETSESSIONS: {

        //Process only for authenticated session
        if(!session_authenticated(session)){ break; }

        //Attach msg type
        pkt_chunk_add(opkt, (uint8_t*)&cmdidx, sizeof(uint16_t), PKT_CHUNK_U16);

        //Attach all authenticated sessions
        for(int i = 0; i < srv->sessions.size; ++i){
            Session* session = rtcp_uvec_get(&srv->sessions, i);
            const char* uname = session_get_uname(session);
            if(uname){
                pkt_chunk_add(opkt, (uint8_t*)uname,
                    strlen(uname) + 1, PKT_CHUNK_U8);
            }
        }

    } break;
    case MSG_SETTPS: {

        //Process only for authenticated session
        if(!session_authenticated(session)){ break; }

        //Attempt to find user
        const char* uname0 = srp_verifier_get_username(session->verifier);
        User* user0; if(auth_find_user(&srv->auth, uname0, &user0) < 0){ break; }

        //Check access privileges
        if(!(user0->access <= AccessRoot)){ break; }

        //Attempt to get ticks per second
        uint16_t tps;
        if(!msg_pkt_chunk_u16_single(ipkt, chunk, &tps)){ break; }
        chunk += pkt_chunk_bytes(ipkt, chunk);

        //Check that the tps value is in range
        if(tps < 1){ break; }

        //Set value, propagate configuration change
        srv->config.srv_tps = tps;
        startconfig_propagate_values(&srv->config);

        //Initialize synchronizer
        synchronizer_init(&srv->sync, srv->config.srv_tick_nsec);

    } break;
    case MSG_SETTPSPASS: {

        //Process only for authenticated session
        if(!session_authenticated(session)){ break; }

        //Attempt to find user
        const char* uname0 = srp_verifier_get_username(session->verifier);
        User* user0; if(auth_find_user(&srv->auth, uname0, &user0) < 0){ break; }

        //Check access privileges
        if(!(user0->access <= AccessRoot)){ break; }

        //Attempt to get ticks per second
        uint16_t tps;
        if(!msg_pkt_chunk_u16_single(ipkt, chunk, &tps)){ break; }
        chunk += pkt_chunk_bytes(ipkt, chunk);

        //Set value, propagate configuration change
        srv->config.srv_tps_pass = tps;

    } break;
    case MSG_LOG_REOPEN: {

        //Process only for authenticated session
        if(!session_authenticated(session)){ break; }

        //Attempt to find user
        const char* uname0 = srp_verifier_get_username(session->verifier);
        User* user0; if(auth_find_user(&srv->auth, uname0, &user0) < 0){ break; }

        //Check access privileges
        if(!(user0->access <= AccessRoot)){ break; }

#ifdef PLANCKLOG_FILE
        //Get the time in seconds since the epoch, convert it to tm
        time_t t = time(NULL);
        struct tm* tm = localtime(&t);

        filelog_reopen(srv->flog, tm);
#endif

    } break;
    default: {} break;
    }
}

void server_kick_sessions(Server* srv, char* uname)
{
    for(int i = 0; i < srv->sessions.size; ++i){
        Session* session = rtcp_uvec_get(&srv->sessions, i);

        if(session_stopped(session)){ continue; }

        const char* sess_uname = session_get_uname(session);
        if(!sess_uname){ continue; }

        if(strcmp(uname, sess_uname) != 0){ continue; }

        session_stop(session);
        rtcp_uvec_add(&srv->oldsessions, &i);

        //Disconnect session peer gently
        enet_peer_disconnect(session->peer, 0);
    }
            
}

void* server_get_sessions(void* server)
{
    Server* srv = (Server*)server;

    return &srv->sessions;
}

void* server_get_auth(void* server)
{
    Server* srv = (Server*)server;

    return &srv->auth;
}

void* server_get_config(void* server)
{
    Server* srv = (Server*)server;

    return &srv->config;
}
