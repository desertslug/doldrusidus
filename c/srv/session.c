#include "session.h"

void session_dealloc(Session* session)
{
    //Delete verifier if it was allocated
    if(session->verifier){ srp_verifier_delete(session->verifier); }
}

bool enet_address_eq(ENetAddress* a, ENetAddress* b)
{
    return a->host == b->host;
}

SessionID session_id_get(ENetPeer* peer)
{
    assert_or_exit(peer, "session_id_get: peer was NULL\n");
    return (SessionID){ .peer = peer };
}

bool session_id_eq(SessionID a, SessionID b)
{
    return a.peer == b.peer;
}

void session_init(Session* session, SessionID id, ENetPeer* peer)
{
    //Initialize values
    session->id = id;
    session->peer = peer;
    session->verifier = NULL;

    //Zero out stopping timepoint
    session->stopped = (struct timespec){ 0 };

    //First heartbeat
    session_recvheart(session);

    //Zero out arbitrary session data
    memset(session->data, 0, SESSION_BYTES);
}

void session_stop(Session* session)
{
    //Note timepoint of stopping
    clock_gettime(CLOCK_REALTIME, &session->stopped);
}

bool session_stopped(Session* session)
{
    return !(session->stopped.tv_sec == 0 && session->stopped.tv_nsec == 0);
}

void session_recvheart(Session* session)
{
    clock_gettime(CLOCK_REALTIME, &session->recvheart);
    clock_gettime(CLOCK_REALTIME, &session->sendheart);
}

void session_heartbeat(Session* session, int recv_msec, int send_msec)
{
    //Send heartbeat if the last receive was long ago enough,
    //and if the last send was long ago enough
    struct timespec now; clock_gettime(CLOCK_REALTIME, &now);
    if(
        diff_msec(&session->recvheart, &now) >= recv_msec &&
        diff_msec(&session->sendheart, &now) >= send_msec
    ){
        //Send empty packet
        ENetPacket* pkt = pkt_create(NULL, 0, 0);
        enet_peer_send(session->peer, 0, pkt);

        //Note timepoint of last send
        clock_gettime(CLOCK_REALTIME, &session->sendheart);
    }
}

bool session_authenticated(void* sess)
{
    Session* session = (Session*)sess;

    return session->verifier &&
        srp_verifier_is_authenticated(session->verifier);
}

const char* session_get_uname(void* sess)
{
    Session* session = (Session*)sess;

    return
        session->verifier &&
        srp_verifier_is_authenticated(session->verifier) ?
            srp_verifier_get_username(session->verifier) : NULL;
}

int sessions_find_id(rtcp_uvec_t* sessions, SessionID id)
{
    int j = 0;
    while(j < sessions->size){

        Session* session = rtcp_uvec_get(sessions, j);
        if(session_id_eq(session->id, id)){ break; }
        ++j;
    }

    int ret = j >= sessions->size ? -1 : j;
    assert_or_exit(ret >= -1 && ret < sessions->size,
        "sessions_find_id: ret out of range");

    return ret;
}