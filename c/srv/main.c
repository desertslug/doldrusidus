#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <string.h>

#include "plancklog.h"

#ifdef PLANCKLOG_FILE
    #include "filelog.h"
#endif

#include "server.h"
Server srv;

//#include "constants.h"

#ifdef PLANCKLOG_FILE
    FileLog filelog;
#endif

void terminate()
{
    server_stop(&srv);
    server_dealloc(&srv);

#ifdef PLANCKLOG_FILE
    filelog_close(&filelog);
#endif

    exit(0);
}

void sigint_handler(int signum)
{
    LOG_WARN_("SIGINT received");
    terminate();
}

void sigterm_handler(int signum)
{
    LOG_WARN_("SIGTERM received");
    terminate();
}

int main(int argc, char** argv)
{
#ifdef PLANCKLOG_FILE
    filelog_init(&filelog);
#endif

    //Print help menu if specified
    if(
        argc > 1 &&
        (strcmp(argv[1], "-h") == 0 || strcmp(argv[1], "--help") == 0)
    ){
        fprintf(stdout,
"OVERVIEW: srv-doldrusidus\n\
  An open-ended, obscure simulation realized as a multiplayer universe.\n\
\n\
  doldrum - noun:\n\
  1. doldrums (plural) - a spell of listlessness or despondency\n\
  2. archaic - a sluggish or slow-witted person\n\
  3. doldrums (plural) - a region over the ocean near the equator abounding\n\
                         in calms, squalls, and light baffling winds\n\
\n\
  sidus (latin) - neutral noun:\n\
  1. a group of stars, constellation, heavenly body\n\
  2. tempest\n\
\n\
USAGE: srv-doldrusidus [options]\n\
\n\
OPTIONS:\n\
  --conf <path> load the starting configuration from the specified path.\n\
  -h|--help     prints this help menu with the available options, then exits.\n\
  -p <port>     listen initially on the port specified.\n\
"
        );
        exit(0);
    }
    
    //Set signal handlers
    signal(SIGINT, sigint_handler);
    signal(SIGTERM, sigterm_handler);
    
    //Block signals initially, so all child processes created during
    //initialization will also have them blocked
    sigset_t ss, os;
    sigemptyset(&ss);
    sigaddset(&ss, SIGINT);
    sigaddset(&ss, SIGTERM);
    sigprocmask(SIG_BLOCK, &ss, &os);
    
    server_alloc(&srv);
    server_start(&srv, argc, argv, &filelog);

    for(;;){
        sigprocmask(SIG_BLOCK, &ss, &os);
        server_step(&srv);
	    sigprocmask(SIG_UNBLOCK, &ss, &os);
    }

    terminate();

    return 0;
}
