#ifndef SESSION_H
#define SESSION_H

#include <stdbool.h>

#include "enet/enet.h"

#include "defs.h"
#include "helper.h"
#include "uvec.h"
#include "nanosleep.h"
#include "srp.h"
#include "packet.h"

bool enet_address_eq(ENetAddress* a, ENetAddress* b);

typedef struct SessionID
{
    ENetPeer* peer;
} SessionID;

SessionID session_id_get(ENetPeer* peer);
bool session_id_eq(SessionID a, SessionID b);

enum { SESSION_BYTES = 0x1000 };

typedef struct Session
{
    SessionID id;
    ENetPeer* peer;
    struct SRPVerifier* verifier;
    
    struct timespec stopped;
    struct timespec sendheart;
    struct timespec recvheart;

    uint8_t data[SESSION_BYTES];
} Session;

void session_dealloc(Session* session);
void session_init(Session* session, SessionID id, ENetPeer* peer);
void session_stop(Session* session);
bool session_stopped(Session* session);
void session_recvheart(Session* session);
void session_heartbeat(Session* session, int recv_msec, int send_msec);

int sessions_find_id(rtcp_uvec_t* sessions, SessionID id);

#endif /* SESSION_H */