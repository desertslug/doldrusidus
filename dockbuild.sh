#!/bin/sh

# Get the absolute path to the directory the script exists in
DIR_ABS="$(cd "$(dirname $0)" && pwd)"

# Extract username from the path
UNAME="$(echo ${DIR_ABS} | cut -f3 -d /)"

# Docker image to use for the build
IMAGE=""

# Parse arguments
SHIFT="0"
for arg in "$@" ; do
    case $arg in
        (--image=* | -i=*) IMAGE=${arg##*=}; SHIFT=$((SHIFT+1)) ;;
	    (--help | --he | -h)
	    cat <<-EOF
Usage: ./dockbuild.sh --image=<> ini-file
options:

    --image=<>, -i=<>
        Use the specified docker image for the build.

Run this script to build the cross build specififed by a .ini file in docker.
EOF
	    exit 0 ;;
    esac
done

# Shift out keyword arguments
shift $SHIFT

# The .ini file corresponding to the cross build
# should appear as a positional argument
INI_FILE="$1"

# Assert that the .ini file exists
if ! [ -f "${INI_FILE}" ] ; then
    echo "error: meson cross file: \"${INI_FILE}\" not found."
    exit 1
fi

run_build() {

    # The first argument should be the build type
    BUILD_TYPE=$1
    if [ ${BUILD_TYPE} != "debug" ] && [ ${BUILD_TYPE} != "release" ] ; then
	echo "error: build type was not debug or release"
	exit 1
    fi

    # Clean package directory on the host forcefully if it exists, create it otherwise
    (
	if cd "${DIR_ABS}/${PACK_DIR}" ; then
	    echo "[cleaning \"${DIR_ABS}/${PACK_DIR}\"...]"
	    pwd
	    find * -exec rm -r {} \;
	else
	    echo "failed to change into the package directory."
	    echo "[creating \"${DIR_ABS}/${PACK_DIR}\"...]"
	    mkdir "${DIR_ABS}/${PACK_DIR}"
	fi
    )

    # Run docker container with the package directory mounted to the host's package directory
    docker run --rm -v ${DIR_ABS}/${PACK_DIR}:/doldrusidus/${PACK_DIR} ${IMAGE} \
        sh xconfbuild.sh ${BUILD_TYPE} ${INI_FILE}

    # Change the owner of the build directory to $USER
    chown -R ${USER}:${USER} ${DIR_ABS}/${PACK_DIR}
    
}

# Run the cross-build corresponding to each <architecture>.ini file
for INI in ${INI_FILE} ; do

    # Get the machine architecture by stripping the .ini extension
    ARCH="$(basename ${INI} | cut -d . -f1)"

    # Run release build
    PACK_DIR="doldrusidus-${ARCH}-release"
    #run_build release

    # Run debug build
    PACK_DIR="doldrusidus-${ARCH}-debug"
    run_build debug

done

