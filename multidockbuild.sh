#!/bin/sh

# Get the absolute path to the directory the script exists in
DIR_ABS="$(cd "$(dirname $0)" && pwd)"

# Docker image to use for the build
IMAGE=""

# Parse arguments
SHIFT="0"
for arg in "$@" ; do
    case $arg in
        (--image=* | -i=*) IMAGE=${arg##*=}; SHIFT=$((SHIFT+1)) ;;
	    (--help | --he | -h)
	    cat <<-EOF
Usage: ./multidockbuild.sh --image=<> ini-file-expr
options:

    --image=<>, -i=<>
        Use the specified docker image for the build.

    ini-file-expr
        Substring that built .ini cross files should all have.
        Specify an empty string to match all cross files.

Run this script to build multiple cross files at once.
EOF
	    exit 0 ;;
    esac
done

# Shift out keyword arguments
shift $SHIFT

# Assert that *something* was specified as an image argument
if [ -z "${IMAGE}" ] ; then
    echo "error: argument missing, specify docker image to be used with -i=<image>"
    exit 1
fi

# Optionally, a positional argument may be specified,
# an expression which grep will match .ini filenames against
EXPR="$1"

# The .ini files are collected from the source directory
if [ -n "${EXPR}" ] ; then
    INI_FILES="$(ls ${DIR_ABS}/*.ini | grep ${EXPR})"
else
    INI_FILES="$(ls ${DIR_ABS}/*.ini)"
fi

# Run the cross-build in docker corresponding to each <architecture>.ini file
for INI in ${INI_FILES} ; do

    ./dockbuild.sh --image=${IMAGE} "$(basename ${INI})"

done
