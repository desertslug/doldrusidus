#!/bin/sh

# Get the absolute path to the directory the script exists in
DIR_ABS="$(cd "$(dirname $0)" && pwd)"

# The build type should be the first argument on the command line
BUILD_TYPE="$1"; shift

# The .ini files should appear listed on the command line
INI_FILES="$@"

echo "INI_FILES: ${INI_FILES}"

# Run the cross-build corresponding to each <architecture>.ini file
for INI in ${INI_FILES} ; do

    # Get the machine architecture by stripping the .ini extension
    ARCH="$(basename ${INI} | cut -d . -f1)"

    # Configure the build
    echo "[running sh configure.sh --host=${ARCH} --bt=${BUILD_TYPE}]"
    sh ${DIR_ABS}/configure.sh --host=${ARCH} --bt=${BUILD_TYPE}

    # Run the build
    echo "[running sh ${DIR_ABS}/build.sh --dir=${ARCH}-${BUILD_TYPE}]"
    sh ${DIR_ABS}/build.sh --dir=${ARCH}-${BUILD_TYPE}

    # Get the name of the built target executable, package manually
    TARGET="$(ls ${ARCH}-${BUILD_TYPE} | grep "desert-.\..\..\(.exe\)\?$")"
    make BUILD_DIR=${ARCH}-${BUILD_TYPE} EXEC=${TARGET}

done
