#version 330 core

layout (location = 0) in vec3 pos;
layout (location = 1) in vec4 color;

uniform mat4 mat_mv;
uniform mat4 mat_pr;

out VS_OUT
{
    vec4 color;
} vs_out;

void main(void)
{
    gl_Position = mat_pr * mat_mv * vec4(pos, 1.0);
    vs_out.color = color;
}
