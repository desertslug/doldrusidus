#version 330 core

layout (location = 0) out vec4 colour;

uniform sampler3D textures;

in VS_OUT
{
    vec2 tex;
    float type;
    vec4 col;
} vs_in;

void main(void)
{
    colour = texture(textures, vec3(vs_in.tex.x, vs_in.tex.y, vs_in.type));
    if(colour.a < 0.5) discard;
    colour *= vs_in.col;
}
