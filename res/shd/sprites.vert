#version 330 core

layout (location = 0) in vec2 geom;
layout (location = 1) in vec2 tex;
layout (location = 2) in vec3 pos;
layout (location = 3) in float type;
layout (location = 4) in vec4 col;
layout (location = 5) in ivec2 off;
layout (location = 6) in ivec2 scale;

uniform mat4 mat_mv;
uniform mat4 mat_pr;

uniform ivec2 maxdim;
uniform ivec2 window;

out VS_OUT
{
    vec2 tex;
    float type;
    vec4 col;
} vs_out;

void main(void)
{
    gl_Position = mat_pr * (mat_mv * vec4(pos, 1.0));
    vec2 screencoord = ivec2(gl_Position.xy * window) - vec2(0.5);
    gl_Position = vec4(
        (
            screencoord +
            ivec2((geom * maxdim * scale + off * 2 + 0.5) * gl_Position.w)
        ) / window,
        gl_Position.z, gl_Position.w);
    vs_out.tex = tex;
    vs_out.type = type;
    vs_out.col = col;
}
