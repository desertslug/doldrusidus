#version 330 core

layout (location = 0) out vec4 colour;

in VS_OUT
{
    vec4 color;
} vs_in;

void main(void)
{
    colour = vs_in.color;
}