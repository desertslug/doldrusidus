#version 330 core

layout (location = 0) out vec4 colour;

//uniform vec4 color_in;

uniform sampler2D texture2;

in VS_OUT
{
    vec2 tex;
    float type;
    vec3 col;
} vs_in;

void main(void)
{
    vec2 uv = vs_in.tex / textureSize(texture2, 0);
    float sample = texture(texture2, uv).x;
    colour = vec4(vs_in.col * sample, sample);
    if(colour.a < 0.5) discard;
    /*colour = texture(textures, vec3(vs_in.tex.x, vs_in.tex.y, vs_in.type));
    if(colour.a < 0.5) discard;
    colour.xyz *= vs_in.col;*/
    //colour = vec4(vec3(vs_in.color.x), 1.0);
}