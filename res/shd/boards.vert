#version 330 core

layout (location = 0) in vec2 geom;
layout (location = 1) in vec2 tex;
layout (location = 2) in vec3 pos;
layout (location = 3) in int code;
layout (location = 4) in vec3 col;
layout (location = 5) in ivec2 off;

uniform mat4 mat_mv;
uniform mat4 mat_pr;
uniform ivec2 bbox;
uniform ivec2 chardim;
uniform ivec2 window;

out VS_OUT
{
    vec2 tex;
    float type;
    vec3 col;
} vs_out;

void main(void)
{
    ivec2 charcoord = ivec2(
        code % chardim.x,
        chardim.y - 1 - (code - code % chardim.x) / chardim.x
    );

    ivec2 imgcoord = (charcoord + ivec2(tex)) * bbox;
    gl_Position = mat_pr * (mat_mv * vec4(pos, 1.0));
    vec2 screencoord = ivec2(gl_Position.xy * window) - vec2(0.5);
    gl_Position = vec4(
        (
            screencoord +
            ivec2((geom * bbox + off * 2 + 0.5) * gl_Position.w)
        ) / window,
        gl_Position.z, gl_Position.w);
    vs_out.tex = imgcoord;
    vs_out.col = col;
}
