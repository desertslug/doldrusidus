#!/bin/sh
HOST_MACH=""
BUILD_TYPE=""

# Parse arguments
for arg in "$@" ; do
    case $arg in
	    (--host=*  | --ho=*) HOST_MACH=${arg##*=} ;;
	    (--buildtype=* | --bt=*) BUILD_TYPE=${arg##*=} ;;
	    (--help | --he | -h)
	    cat <<-EOF
Usage: ./configure.sh --host=<> --buildtype={debug|release}
options:

    --host=<>, --ho=<>
        Target triplet for a cross build. Omission specifies a native build.
        Specifies the host system the project will be built for.
	    See the target triplets in the names of .ini files at the top level
	    of the source tree for already existing cross configurations.
	    The full meson cross-file's name may also be specified, for convenience.

    --buildtype, --bt
        Configures the build type and the project to build into
        a directory suffixed with the same name as the build type.

Run this script to configure the project initially.
EOF
	    exit 0
	;;
    esac
done

# If a host machine was specified, the project is to be cross-compiled
HOST_MACH="$(echo ${HOST_MACH} | sed 's/\.ini$//g')"
if [ -n "${HOST_MACH}" ] ; then

    # Assert that a cross file exists for the chosen host triplet
    if ! [ -f "./${HOST_MACH}.ini" ] ; then
	echo "error: no suitable meson cross file found"
	echo "the cross file for \"${HOST_MACH}\" will be looked for as \"${HOST_MACH}\".ini"
	exit 1
    fi
fi

# Validate build type and name build directory
if [ "${BUILD_TYPE}" = "debug" ] || [ "${BUILD_TYPE}" = "release" ] ; then
    if [ -z "${HOST_MACH}" ] ; then
	BUILD_DIR="./${BUILD_TYPE}"
    else
	BUILD_DIR="./${HOST_MACH}-${BUILD_TYPE}"
    fi
else
    echo "error: build type unspecified"
    echo "specify one from the meson build option \"buildtype\""
    exit 1
fi

# Forcefully clean build directory if it exists, create it otherwise
(
if cd "${BUILD_DIR}" ; then
    echo "[cleaning \"${BUILD_DIR}\"...]"
    pwd
    find * -exec rm -r {} \;
else
    echo "failed to change into the build directory."
    echo "[creating \"${BUILD_DIR}\"...]"
    mkdir "${BUILD_DIR}"
fi
)

# Compile tinycc
#(
#cd "${BUILD_DIR}"
#export MESON_BUILD_ROOT="$(pwd)"
#export HOST_MACH="${HOST_MACH}"
#echo "[compiling tinycc]"
#if ! ../tinycc.sh; then
#    echo "error: failed to compile tinycc"
#fi
#)

# Compile pcc
#(
#cd "${BUILD_DIR}"
#export MESON_BUILD_ROOT="$(pwd)"
#export HOST_MACH="${HOST_MACH}"
#echo "[compiling pcc]"
#if ! ../pcc.sh; then
#    echo "error: failed to compile pcc"
#fi
#)

#echo $PATH

# Setup and configure meson for native and cross builds
if [ -z "${HOST_MACH}" ] ; then

    # Setup meson to build into the directory
    echo "[running meson setup]"
    meson setup "${BUILD_DIR}" --buildtype="${BUILD_TYPE}"
else

    # Setup meson to build into the directory
    echo "[running meson setup]"
    meson setup "${BUILD_DIR}" --buildtype="${BUILD_TYPE}" --cross-file="${HOST_MACH}".ini
fi